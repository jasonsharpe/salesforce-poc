({
    getInstanceURL : function(c,e,h) {
        try{
            var action = c.get("c.genericdatafetcher");
            action.setParams({ n : 'CFA_Update_Profile' });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log("From server: " + response.getReturnValue());
                    var resp = response.getReturnValue();
                    if(resp){
                        window.open(resp,'_blank');
                    }
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
        catch(e){
            console.log('Error ---->>'+e);
        }
    }
})