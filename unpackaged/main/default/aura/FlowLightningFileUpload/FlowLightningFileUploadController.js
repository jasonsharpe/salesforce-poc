({
    handleFilesChange: function(c, e, h) {
        try{            
            if (c.find("fileId").get("v.files").length > 0) {
                var fileName = c.find("fileId").get("v.files")[0]['name'];
                c.set("v.fileName",fileName);
                h.uploadHelper(c, e);
            }
        }
        catch(e){
            console.log('Error---->>'+e);
        }
    },
})