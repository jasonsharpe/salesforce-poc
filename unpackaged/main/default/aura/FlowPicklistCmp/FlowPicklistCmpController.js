({
    doInit : function(component, event, helper) {
        try{
            var objectName = component.get("v.sObjectName");
            var fieldAPIName = component.get("v.fieldAPIName");
            var value = component.get("v.value");
            if(objectName && fieldAPIName){
                var action = component.get("c.getPicklistvalues");
                action.setParams({objectName : objectName,
                                  field_apiname : fieldAPIName,
                                  nullRequired : false,
                                 });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var returnValue = response.getReturnValue();
                        if(returnValue && returnValue.length >0){
                            component.set("v.options",returnValue);
                            if(value){
                                if(Array.isArray(value)){
                                    value = value.toString();
                                }
                                var values = [];
                                if(value.includes(';')){
                                    values = value.split(';');
                                }
                                else{
                                    values = value;
                                }
                                component.set("v.values",values);
                            }
                        }	
                    }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
                });
                $A.enqueueAction(action);   
            }                        
        }
        catch(e){
            console.log('Error ---->>'+e);
        }
    },
    
    handleChange : function(component, event, helper){
        try{
            var selectedOptionValue = event.getParam("value");
            if(selectedOptionValue){
                var value = '';
                if(selectedOptionValue.length == 1){
                    value = selectedOptionValue;
                }
                else{
                    selectedOptionValue.forEach(function(e){
                        value = value + e + ';';
                    });
                }
                component.set("v.value",value);
            }
        }
        catch(e){
            console.log('Error ---->>'+e);
        }
    }
})