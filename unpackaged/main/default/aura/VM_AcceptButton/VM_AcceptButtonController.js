({
	
        volAccepted:function(component, event, helper){
        
        component.set("v.appstatus1",true);
        var action=component.get("c.getStatus");
         action.setParams({ flag : true });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                component.set("v.src",response.getReturnValue());
                alert(component.get("v.src"));
                alert("From server: " + response.getReturnValue());
                if(response.getReturnValue()=="Volunteer Accepted"){
             
                    component.set("v.appstatus1",true);
                    component.set("v.appstatus2",false);
                   // component.set("v.JobApp",true);
                    
                }
                else if(response.getReturnValue()=="Volunteer Rejected"){
                    component.set("v.appstatus2",true);
                    component.set("v.appstatus1",false);
                    //component.set("v.JobApp",true);
                }

            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
		
	}
})