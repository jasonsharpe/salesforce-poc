({
    doInit:function(component, event, helper){
        var action=component.get("c.getStatus");
        var jobId = component.get("v.recordId");
        action.setParams({ "jobId" : jobId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.src",response.getReturnValue());
               
                if(response.getReturnValue()=="True"){
                    component.set("v.buttonHide",true);
                }
                else if(response.getReturnValue()=="False"){
                    component.set("v.buttonHide",false);
                }
                
            }
            
        });
        
        $A.enqueueAction(action);
    },
    
    
    /* Modal Box Starts***************************************************************/
    
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
       
        var a=null;
        
        var ButtonClick = event.getSource().get("v.value");
        var action=component.get("c.updateStatus");
        var jobId = component.get("v.recordId");
       
        action.setParams({ "ButtClick" : ButtonClick,"jobId" : jobId });// jobId : jobId
        action.setCallback(this, function(response){
            var state = response.getState();
           
            if (state === "SUCCESS") {
                
                component.set("v.src",response.getReturnValue());
                
               
                a=response.getReturnValue();
                if(a=='Volunteer Accepted'){
                    component.set("v.isOpen1", true);
                    
                }
                else if(a=='Error121'){
                    component.set("v.isOpen3", true);
                    var action1=component.get("c.getacknowledgeform"); 
                    action1.setCallback(this, function(response){
                       
                        component.set("v.AcknowledgementFormID", response.getReturnValue());
                       
                        
                        /* action1.setCallback(this, function(response) {
            component.set("v.Competencies",response.getReturnValue());*/
                        
                       
                        
                        
                    });
                     $A.enqueueAction(action1);
                    
                }
                
            }                 
           
           
        }); 
                           $A.enqueueAction(action);
                
                           },
                            
    openAckPdf: function(component, event, helper){
        window.open(component.get("v.AcknowledgementFormID"),'_blank');
    },
                           
           /* TDIXIT 29/01/2018 SonarQube Comments : This function appears twice in controller with same naem and not being used. Hence commented
                           closeModel: function(component, event, helper) {                              
            
            // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
            component.set("v.isOpen1", false);
        }, */
            
    
    /* TDIXIT 29/01/2018 SonarQube Comments : This function appears twice in controller with same naem and not being used. Hence commented     
            likenClose: function(component, event, helper) {
                // Display alert message on the click on the "Like and Close" button from Model Footer 
                // and set set the "isOpen" attribute to "False for close the model Box.
                
                component.set("v.isOpen1", false);
            },
            
           */ 
                //modal box2 starts
                opmodel: function(component, event, helper) {
                    // for Display Model,set the "isOpen" attribute to "true"
                    component.set("v.isOpen2", true);        
                    
                    var ButtonClick = event.getSource().get("v.value");
                    var action=component.get("c.updateStatus");
                    var jobId = component.get("v.recordId");
                    action.setParams({ "ButtClick" : ButtonClick,"jobId" : jobId });// jobId : jobId
                    action.setCallback(this, function(response){
                        var state = response.getState();
                        
                        if (state === "SUCCESS") {
                            
                            component.set("v.src",response.getReturnValue());
                            
                        }
                    });
                    
                    $A.enqueueAction(action);
                },
                    
    /* TDIXIT 29/01/2018 SonarQube Comments : This function appears twice in controller with same naem and not being used. Hence commented     
                closeModel: function(component, event, helper) {
                        
                        
                        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
                        component.set("v.isOpen2", false);
                    },
                 */
    
    /* TDIXIT 29/01/2018 SonarQube Comments : This function appears twice in controller with same naem and not being used. Hence commented     
                        likenClose: function(component, event, helper) {
                            // Display alert message on the click on the "Like and Close" button from Model Footer 
                            // and set set the "isOpen" attribute to "False for close the model Box.
                           
                            component.set("v.isOpen2", false);
                        },
                */            //modal box2 ends
                            
                            /* Modal Box Starts***************************************************************/    
                            
                            
                            
                            volAcceptReject:function(component, event, helper){

                                component.set("v.attachmentUrl",URL.getSalesforceBaseUrl().toExternalForm() 
                                              + '/servlet/servlet.FileDownload?file=0152F0000009vvI');
                                var ButtonClick = event.getSource().get("v.value");
                                var action=component.get("c.updateStatus");
                                var jobId = component.get("v.recordId");
                                action.setParams({ "ButtClick" : ButtonClick,"jobId" : jobId });// jobId : jobId
                                action.setCallback(this, function(response) {
                                    var state = response.getState();
                                   
                                    if (state === "SUCCESS") {
                                        
                                        component.set("v.src",response.getReturnValue());
                                        
                                        if(response.getReturnValue()=="Volunteer Accepted"){
                                            
                                            component.set("v.appstatus1",true);
                                            component.set("v.appstatus2",false);
                                            component.set("v.buttonHide",false);
                                           
                                        }
                                        else if(response.getReturnValue()=="Volunteer Rejected"){
                                            component.set("v.appstatus2",true);
                                            component.set("v.appstatus1",false);
                                            component.set("v.buttonHide",false);
                                           
                                        }
                                        
                                    }
                                    else if (state === "INCOMPLETE") {
                                        // do something
                                    }
                                        else if (state === "ERROR") {
                                            var errors = response.getError();
                                            if (errors) {
                                                if (errors[0] && errors[0].message) {
                                                    console.log("Error message: " + 
                                                                errors[0].message);
                                                }
                                            } else {
                                                console.log("Unknown error");
                                            }
                                        }
                                });
                                
                                $A.enqueueAction(action);
                            },
                                /*volRejected:function(component, event, helper){
        var action=component.get("c.updateStatus");
        var jobId = component.get("v.recordId");
        action.setParams({ flag : false });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                component.set("v.src",response.getReturnValue());
                alert(component.get("v.src"));
                alert("From server: " + response.getReturnValue());
                if(response.getReturnValue()=="Volunteer Accepted"){
             
                   // component.set("v.appstatus",true);
                    component.set("v.appstatus1",true);
                     component.set("v.appstatus2",false);
                    
                }
                 if(response.getReturnValue()=="Volunteer Rejected"){
                    //component.set("v.appstatus",false);
                    component.set("v.appstatus2",true);
                     component.set("v.appstatus1",false);
                }

            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },*/
                    vol_home_page : function(component, event, helper) {
                        
                        
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url":"/CFAVolunteer/s/"
                        });
                        urlEvent.fire();
                    },
                        closeModal1:function(component, event, helper){ 
                            
                            component.set("v.isOpen3",false);
                        },

    
    Agree:function(component, event, helper){ 
                                
                                 var field1 = component.get("v.checkbox.I_agree_that_this_information_is_correct__c");
        
        var field2 = component.get("v.checkbox.I_ve_read_and_understand_the_role_des__c");
       
        var field3 = component.get("v.checkbox.I_ve_Read_and_accept_Volunteer_Agreement__c");
        
        if((field1==false)||(field2==false)||(field3==false))
        {

        	 component.set("v.ErrorMessage","Please Agree to All the three Conditions");     
        }
                                else{
                                    
                                    
                                                     
                            
                                
                                var check = component.get("v.checkbox");
                             
                                var jobId = component.get("v.recordId");
                                
                                var action = component.get("c.getCheckbox");
                                action.setParams({
                                    "jobId": jobId,
                                    "jobRec" : check
                                });
                                
                                action.setCallback(this, function(a) {
                                    var state = a.getState();
                                   
                                    if (state === "SUCCESS") {
                                                    
                                    }
                                });
                                
                                $A.enqueueAction(action) 
                                component.set("v.isOpen3",false);
                                    component.set("v.isOpen1",true);}
                             /* var field1 = component.get("v.checkbox.I_agree_that_this_information_is_correct__c");
        //alert("field1 "+field1);
        var field2 = component.get("v.checkbox.I_ve_read_and_understand_the_role_des__c");
       // alert("field2 "+field2);
        var field3 = component.get("v.checkbox.I_ve_Read_and_accept_Volunteer_Agreement__c");
        //alert("field3 "+field3);
        //var field4 = component.get("v.TaskNew.Task_type__c");
        if((field1==false)||(field2==false)||(field3==false))
        {
        	 component.set("v.ErrorMessage","Please Agree to All the three Conditions");     
        }
                                else{
                                    component.set("v.isOpen1",true);
                                    
                                }      
                                
                                
                                
                                
                                
                                
                                
                                //component.set("v.isOpen1",true);*/
                            },
                                navigateToRecord:function(component,event,helper){
                                    var VolunteerApp;
                                    var action=component.get("c.getROle");
                                    var jobId = component.get("v.recordId");
                                    action.setParams({
                                        "jobId": jobId,
                                        
                                    });
                                   
                                    
                                    action.setCallback(this, function(response) {
                                        var state = response.getState();
                                        
                                        if(state == 'SUCCESS'){
                                           
                                            VolunteerApp = response.getReturnValue();
                                        }
                                        
                                       
                                        window.open('/CFAVolunteer/s/detail/'+VolunteerApp, '_blank');
                                        var urlEvent = $A.get("e.force:navigateToURL");
                                        urlEvent.setParams({
                                            
                                            
                                            // "url":"https://devcfacogn-cfainstitute.cs91.force.com/CFAVolunteer/s/detail/"+VolunteerApp
                                            
                                        });
                                        urlEvent.fire();
                                        
                                    });
                                    $A.enqueueAction(action);   
                                }    
        
        
    })