({
	updateprofile : function(component, event, helper){
		let updateProfile = $A.get("$Label.c.VM_Update_CFA_Profile");
		window.open(updateProfile);
	},
    
    closeModal : function(component, event, helper) {
        document.getElementById("backGroundSectionId").style.display = "none";
        document.getElementById("newAccountSectionId").style.display = "none";
        //console.log('RecordType-->>',response.getElementById())
        window.location.reload();
    },
    resizeCanvas:function resize() { 
        Sfdc.canvas(function() {
            sr = JSON.parse('<%=signedRequestJson%>');
            Sfdc.canvas.client.autogrow(sr.client);
        });
     }
})