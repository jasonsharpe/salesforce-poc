({
	gotofacebook : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"http://www.facebook.com/CFAInstitute"
        });
        urlEvent.fire();
	},gotolinkedin : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"http://www.linkedin.com/company/cfainstitute"
        });
        urlEvent.fire();
	},gototwitter : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
  			"url":"https://twitter.com/cfainstitute"
        });
        urlEvent.fire();
	}
})