({
	leave_feedback : function(component, event, helper){
        /*alert("Name"+component.get("v.simpleRecord.Name"));
        alert("Engagement_name__c"+component.get("v.simpleRecord.Engagement_name__c"));
        alert("Engagement_name__r.Business_Objective__c"+component.get("v.simpleRecord.Engagement_name__r.Business_Objective__c"));
        */
        var label=$A.get("$Label.c.VM_FeedbackURL");

        var urlEvent = $A.get("e.force:navigateToURL");
        var url_Engg_vol=component.get("v.recordId");
        var url_Role=component.get("v.simpleRecord.Role_del__c");
        var url_Contact=component.get("v.simpleRecord.Contact__c");
        var url_Engg=component.get("v.simpleRecord.Engagement__c");
        
        urlEvent.setParams({
           // "url":"https://www.getfeedback.com/r/COEDj8FA?gf_unique="+url_role
            //"url":"https://www.getfeedback.com/r/px4hDp84?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg+"&gf_unique=unique" 
            //"url":"https://www.getfeedback.com/r/sILMX6Yl?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&VM_Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg
			//"url":"https://www.getfeedback.com/r/TUpOM3lx?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg
            "url":label+"?VM_Engagement_Volunteer__c="+url_Engg_vol+"&VM_Role__c="+url_Role+"&Contact__c="+url_Contact+"&VM_Engagement__c="+url_Engg
        });
        urlEvent.fire();
	}
})