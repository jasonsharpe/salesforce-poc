({
    doInit : function(component, event, helper) {
        
        var action = component.get('c.getTaskLst');
        var EvDetail=component.get("v.recordId");
        action.setParams({"wId" :EvDetail});
        // Set up the callback
      
        action.setCallback(this, function(actionResult) {
            component.set('v.TaskList', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
        
        
    },
    
    NewTask:function(component, event, helper) {
        //component.set("v.isOpen1",true);
        var action3=component.get("c.getNewTask");
       action3.setCallback(this, function(response) {
        component.set("v.TaskNew",response.getReturnValue());            
        });
        $A.enqueueAction(action3); 
        
        
        
        
        var action1 = component.get("c.getTaskType");
        action1.setCallback(this, function(response) {
        component.set("v.TskType",response.getReturnValue());            
        });
        $A.enqueueAction(action1); 
        
        var action2 = component.get("c.getTaskStatus");
        action2.setCallback(this, function(response) {
            component.set("v.TskState",response.getReturnValue());            
        });
        $A.enqueueAction(action2);  
        
        
        
        
        
        
        component.set("v.isOpen1",true);
         /*var action1 = component.get("c.getTaskDetail");
        action1.setCallback(this, function(response) {
            component.set("v.TaskNew",response.getReturnValue());            
        });
        $A.enqueueAction(action1);*/
        
        //component.set("v.isOpen1",true); getTaskDetail
        
        
    },
    
    
    
    
    Createtask:function(component, event, helper) {
        
        var field1 = component.get("v.TaskNew.Subject");
        //alert("field1 "+field1);
        var field2 = component.get("v.TaskNew.Travel_Hours__c");
       // alert("field2 "+field2);
        var field3 = component.get("v.TaskNew.Working_Hours__c");
        //alert("field3 "+field3);
        //var field4 = component.get("v.TaskNew.Task_type__c");
        if((field1==null)||(field2==null)||(field3==null))
        {
        	 component.set("v.ErrorMessage","Please Enter Value for All Mandatory Fields");     
        }
        else
        {
        
        //component.set("v.TaskNew.Id","v.recordId");
        //alert("inside createtask");
        var action=component.get("c.newtask");
        var taskDetail=component.get("v.TaskNew");
        var engVolDetail=component.get("v.recordId");
        //alert('task detail VVV'+component.get("v.TaskNew"));
        //alert('task detail  CCC '+component.get("c.newtask"));
        action.setParams({"tsk" :taskDetail,"whtId":engVolDetail});
        
        action.setCallback(this, function(response) {
            
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var msg=response.getReturnValue();
               // alert('value of msg '+msg);
                if(msg==null)
                {
                	//alert("Value of return task"+response.getReturnValue());               
                	//component.set("v.ErrorMessage",response.getReturnValue());
                	component.set("v.isOpen1",false); 
                	var action = component.get('c.getTaskLst');
                    var EvDetail=component.get("v.recordId");
                    action.setParams({"wId" :EvDetail});
                    // Set up the callback
                    
                    action.setCallback(this, function(actionResult) {
                        component.set('v.TaskList', actionResult.getReturnValue());
                    });
                    $A.enqueueAction(action);
                }
                else
                {
                	component.set("v.ErrorMessage",response.getReturnValue());    
                }
            }
        })
        $A.enqueueAction(action);
     }
    },
    
    CancelTaskCreate:function(component, event, helper) {        
        component.set("v.isOpen1",false);             
    },
    
    navToTask:function(component, event, helper) {
        var taskId=event.target.id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/task/"+taskId
        });
        urlEvent.fire();
        
        /*
        var taskId=event.target.id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
                    "url":"/taskpage?key1="+taskId           
                });
                urlEvent.fire();  
        */
    }
})