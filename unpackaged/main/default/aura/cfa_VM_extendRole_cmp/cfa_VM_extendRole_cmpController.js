({
    doInit : function(component, event, helper) {
        console.log(component.get('v.recordId'));
        var action = component.get("c.getIconName");
        
        action.setParams({ sObjectName : 'Role__c' });
        
        // Create a callback that is executed after 
        // the server-side action returns 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Response From Server 
                component.set('v.ProjectIcon',response.getReturnValue());
                
                var action2 = component.get("c.checkEligibility");
                action2.setParams({ engagementId : component.get('v.recordId') });
                action2.setCallback(this, function(res) {
                    var state = res.getState();
                    if (state === "SUCCESS") {
                        if(res.getReturnValue()==null) {
                            component.get('v.Validation',null);
                            var action1 = component.get("c.getRoles");
                            
                            action1.setParams({ engagementId : component.get('v.recordId') });
                            
                            action1.setCallback(this, function(resp) {
                                var state = resp.getState();
                                if (state === "SUCCESS") {
                                    component.set('v.RoleList',resp.getReturnValue());
                                    component.set('v.StepCount',1);
                                }
                            });
                            $A.enqueueAction(action1);
                        } else {
                            component.set('v.Validation',res.getReturnValue());
                        }
                    }
                });
                $A.enqueueAction(action2);
            }
        });
        $A.enqueueAction(action);
    },
    handleSelectAll : function(component, event, helper) {
        var selectAll = component.find("selectAll").get("v.value");   
        var checkRole = component.find("checkRole"); 
        if(checkRole.length != null) {
            if(selectAll == true){
                for(var i=0; i<checkRole.length; i++){
                    checkRole[i].set("v.value",true);
                }
            }
            else{ 
                for(var i=0; i<checkRole.length; i++){
                    checkRole[i].set("v.value",false);
                }
            }
        }else {
            if(selectAll == true)
                checkRole.set("v.value",true);
            else 
                checkRole.set("v.value",false);
        }
        
    },
    handleNext : function(component, event, helper) {
        if(component.get('v.StepCount') == 1) {
            component.set('v.StepCount',2);
            component.set('v.nextOrPrevious','Previous');
            var rolesSelected = component.get('v.RoleList');
            for(var i=0;i<rolesSelected.length;i++) {
                if(rolesSelected[i].isChecked) {
                    component.set('v.isSave',true);
                    break;
                } else {
                    component.set('v.isSave',false);
                }
            }
        } else {
            component.set('v.StepCount',1);
            component.set('v.nextOrPrevious','Next');
            component.set('v.isSave',false);
        }
        
    },
    
    handleCancel : function(component, event, helper) {
        var cntId = component.get('v.recordId');
        //var action = component.get("c.redirecting");
        /*var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": 'https://cfainstitute--devint.cs24.my.salesforce.com/' + cntId
    });
    urlEvent.fire();*/
        //location.href('/'+component.get("v.recordId"));
        window.location.replace('/'+component.get("v.recordId"),"_self");
    },
    handleSave : function(component, event, helper) {
        helper.handleSaveHelper(component,event,helper);
    }
})