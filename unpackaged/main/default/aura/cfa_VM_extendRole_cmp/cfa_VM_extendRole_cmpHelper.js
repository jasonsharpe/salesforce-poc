({
    handleSaveHelper : function(component,event,helper) {
        window.setTimeout(
            $A.getCallback(function() {
                
                var action = component.get("c.saveRoles");
                component.get('v.RoleList');
                
                action.setParams({ strlstOfRole : JSON.stringify(component.get('v.RoleList')),
                                  engId : component.get('v.recordId')});
                
                // Create a callback that is executed after 
                // the server-side action returns 
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        // Response From Server
                        if(response.getReturnValue().substring(0,3) == 'a2s') {
                            if(!component.get('v.isVF')) {
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                                "recordId": response.getReturnValue(),
                                "slideDevName": "related"
                            });
                            navEvt.fire();
                            } else {
                                window.location.replace('/'+response.getReturnValue(),"_self");
                            }                    
                        } else {
                            if(!component.get('v.isVF')) {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    "title": "Error!!!",
                                    "message": response.getReturnValue(),
                                    "type": "error"
                                });
                                toastEvent.fire();
                            } else {
                                alert(response.getReturnValue());
                            }
                        }
                        
                        //component.set('v.RoleList',resp.getReturnValue());
                    }
                });
                $A.enqueueAction(action);
            })
        );
        
    }
})