({
    doInit : function(component, event, helper) {
        console.log(component.get('v.recordId'));
        var action = component.get("c.getIconName");
        
        action.setParams({ sObjectName : 'Engagement_Volunteer__c' });
        
        // Create a callback that is executed after 
        // the server-side action returns 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Response From Server 
                component.set('v.ProjectIcon',response.getReturnValue());
                var action2 = component.get("c.checkEligibility");
                action2.setParams({ engagementId : component.get('v.recordId') });
                action2.setCallback(this, function(resp) {
                    var state = resp.getState();
                    if (state === "SUCCESS") {
                        if(resp.getReturnValue()==null) {
                        var action1 = component.get("c.getVolunteers");
                        
                        action1.setParams({ engagementId : component.get('v.recordId') });
                        
                        action1.setCallback(this, function(resp) {
                            var state = response.getState();
                            if (state === "SUCCESS") {
                                component.set('v.StepCount',1);
                                component.set('v.VolunteerList',resp.getReturnValue());
                            }
                        });
                        $A.enqueueAction(action1); 
                        } else {
                            component.set('v.Validation',resp.getReturnValue());
                        }
                    }
                });
                $A.enqueueAction(action2);      
            }
        });
        $A.enqueueAction(action);
    },
    handleSelectAll : function(component, event, helper) {
        var selectAll = component.find("selectAll").get("v.value");   
        var checkVolunteer = component.find("checkVolunteer"); 
        if(checkVolunteer.length != null) {
            if(selectAll == true){
                for(var i=0; i<checkVolunteer.length; i++){
                    checkVolunteer[i].set("v.value",true);
                }
            }
            else{ 
                for(var i=0; i<checkVolunteer.length; i++){
                    checkVolunteer[i].set("v.value",false);
                }
            }
        }else {
            if(selectAll == true)
                checkVolunteer.set("v.value",true);
            else 
                checkVolunteer.set("v.value",false);
        }
        
    },
    handleCancel : function(component, event, helper) {
        var cntId = component.get('v.recordId');
        window.location.replace('/'+component.get("v.recordId"),"_self");
    },
    handleNext : function(component, event, helper) {
        if(component.get('v.StepCount') == 1) {
            component.set('v.StepCount',2);
            component.set('v.nextOrPrevious','Previous');
            var volSelected = component.get('v.VolunteerList');
            for(var i=0;i<volSelected.length;i++) {
                if(volSelected[i].isChecked) {
                    component.set('v.isSave',true);
                    break;
                } else {
                    component.set('v.isSave',false);
                }
            }
        } else {
            component.set('v.StepCount',1);
            component.set('v.nextOrPrevious','Next');
            component.set('v.isSave',false);
        }
        
    },
    handleSave : function(component, event, helper) {
        component.set('v.isSaveInitiated',true);
        helper.handleSaveHelper(component,event,helper);
    }
})