({
    doInit : function(component, event, helper) {
        console.log(component.get('v.recId'));
        var action = component.get("c.getIconName");
        
        action.setParams({ sObjectName : 'Case' });
        
        // A callback that is executed after 
        // the server-side action returns 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Response From Server 
                component.set('v.ProjectIcon',response.getReturnValue());
                
                var action1 = component.get("c.getInitDetails");
                
                action1.setParams({ engagementId : component.get('v.recId')});
                action1.setCallback(this, function(resp) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        component.set('v.initWrap',resp.getReturnValue());
                    }
                });  
                $A.enqueueAction(action1);
            }
        });
        $A.enqueueAction(action);
        
        
    },
    handleOnLoad: function (component, event, helper) {
        console.log(component.get('v.recId'));
        var objectInfo = event.getParams().objectInfos;
        
        var eventFields = event.getParam("fields");
        console.log(JSON.stringify(objectInfo));
        
    },
    saveRec: function(component,event,helper){
        var eventParams = event.getParams();
        var eventFields = event.getParam("fields");
        eventFields["Id"] = "";
        
        var validationCheck = true;
        
        
        event.preventDefault();
        
        var action = component.get("c.saveCase");
        
        action.setParams({ json : JSON.stringify(eventParams),
                          caseId : component.get('v.recId')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Response From Server 
                if(response.getReturnValue() !=null) {
                    if(response.getReturnValue().substring(0,3) == '500') {
                        
                        var navEvt = $A.get("e.force:navigateToSObject");
                        navEvt.setParams({
                            "recordId": response.getReturnValue(),
                            "slideDevName": "details"
                        });
                        navEvt.fire();
                        
                        
                    } else {
                        
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!!!",
                            "message": response.getReturnValue(),
                            "type": "error"
                        });
                        toastEvent.fire();
                        
                    }
                    
                }
            }
        });
        $A.enqueueAction(action);
        
        
    },
    success: function(component,event,helper){
        
    }
})