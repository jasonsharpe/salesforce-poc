/**
   * @author: Team Gravity - Przemyslaw Giszczak
   * @date 2021
   * @description : Account trigger handler
    **/

public with sharing class AccountTriggerHandler implements ITriggerHandler {

    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean triggerDisabled = false;

    //Checks to see if the trigger has been disabled either by custom metadata or by running code
    public Boolean isDisabled() {
        if (Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('AccountTrigger') || triggerDisabled;
        }
    }

    public void beforeInsert(List<SObject> newItems) {}

    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        AccountTriggerHelper.validateARXOnUncheck((Map<Id, Account>) newItems, (Map<Id, Account>) oldItems);
    }

    public void beforeDelete(Map<Id, SObject> oldItems) {}
    
    public void afterInsert(Map<Id, SObject> newItems) {}
    
    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void afterDelete(Map<Id, SObject> oldItems) {}
    
    public void afterUndelete(Map<Id, SObject> oldItems) {}
}