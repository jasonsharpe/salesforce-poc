@isTest
public class AccountTriggerHelperTest {

    @isTest
    static void testpositiveArxValidation() {
        //Get User to use
        //Get User to use
		User userobj = [SELECT Id,Name,Email,Profile.Name FROM User WHERE Profile.Name='System Administrator' AND Isactive=true LIMIT 1];
        PermissionSet ps = new PermissionSet();

        ps.Name = 'Test_Membership_Application';
        ps.Label = 'Test_Membership_Application';
        insert ps;
        List<SetupEntityAccess> seaList = new List<SetupEntityAccess>();
        for(CustomPermission cp: [select Id from CustomPermission where DeveloperName = 'ARX']) {
            seaList.add(new SetupEntityAccess(ParentId=ps.Id, SetupEntityId=cp.Id));
        }
        insert seaList;
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        insert new PermissionSetAssignment(AssigneeId=userobj.Id, PermissionSetId=ps.Id);
        
        //Assign Permission set

        test.startTest();

        System.runAs(userObj) {
            Map<id, Account> oldmap = new Map<id, Account>();
            Map<id, Account> newmap = new Map<id, Account>();
            //get Account that ia ARX Approved (true)
            Account arxaccount = new Account();
            arxaccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId();
            arxaccount.ARX_Approved__c = true;
            arxaccount.Name = 'sfdcarxtest';
            arxaccount.CurrencyIsoCode = 'AED';
            arxaccount.Site = 'checktest';
            arxaccount.Relationship_Manager__c = UserInfo.getUserId();           
            Insert arxaccount;
            system.assert(arxaccount != null);
            oldmap.put(arxaccount.Id, arxaccount);

            //Affiliation record associated with arx approved account and
            //record type access provisioning and active affiliation and product=arx and role=content contributor.


			Contact newContact = new Contact();
			newContact.AccountId = arxaccount.Id;
			newContact.LastName = cfa_ApexUtilities.generateRandomString(10);
			newContact.FirstName = 'Geetha';
			newContact.Email =  cfa_ApexUtilities.generateRandomString(10) + '@cfa.org';
			newContact.Partner_Id__c = cfa_ApexUtilities.generateRandomString(5);
			newContact.Person_Id__c = cfa_ApexUtilities.generateRandomString(5);
			insert newContact;

			Account act = [SELECT Id,ARX_Approved__c FROM Account WHERE ARX_Approved__c = true];
			Affiliation__c affiliation = new Affiliation__c();
			affiliation.RecordTypeId = Schema.SObjectType.Affiliation__C.getRecordTypeInfosByName().get('Access Provisioning').getRecordTypeId();
			affiliation.Contact__c = newContact.Id;
			affiliation.Account__c = act.Id;
			affiliation.Start_Date__c = Date.today();
			affiliation.Active_Affiliation__c = true;
			affiliation.MDM_Primary__c = true;
			affiliation.Role__c = 'Content Contributor';
			affiliation.Products__c = 'ARX';
			insert affiliation;

            system.assert(affiliation != null);
            system.assertequals(True, arxaccount.ARX_Approved__c);

            try {
                arxaccount.ARX_Approved__c = false;
                newmap.put(arxaccount.Id, arxaccount);
                update arxaccount;
                AccountTriggerHelper.validateARXOnUncheck(oldmap, newmap);
            } catch (Exception error) {
                //Assert Error message
                System.assert(error.getMessage().contains('Update failed.'));
                System.assert(error.getMessage().contains('You must remove the active content contributor affiliations first.'));
            }


            test.stopTest();
        }
    }
    //test1
    @isTest
    static void testpositive2ArxValidation() {

        //Get User to use
		User userobj = [SELECT Id,Name,Email,Profile.Name FROM User WHERE Profile.Name='System Administrator' AND Isactive=true LIMIT 1];
        PermissionSet ps = new PermissionSet();

        ps.Name = 'Test_Membership_Application';
        ps.Label = 'Test_Membership_Application';
        insert ps;
        List<SetupEntityAccess> seaList = new List<SetupEntityAccess>();
        for(CustomPermission cp: [select Id from CustomPermission where DeveloperName = 'Membership_Application']) {
            seaList.add(new SetupEntityAccess(ParentId=ps.Id, SetupEntityId=cp.Id));
        }
        insert seaList;
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        insert new PermissionSetAssignment(AssigneeId=userobj.Id, PermissionSetId=ps.Id);

        test.startTest();

        System.runAs(userObj) {
            //Test Account2
            Account arxaccount = new Account();
            arxaccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId();
            arxaccount.ARX_Approved__c = true;
            arxaccount.Name = 'sfdcarxtest';
            arxaccount.CurrencyIsoCode = 'AED';
            arxaccount.Site = 'checktest';
            arxaccount.Relationship_Manager__c = UserInfo.getUserId();           
            Insert arxaccount;
            system.assert(arxaccount != null);
            //test contact
            Contact testContact2 = CFA_TestDataFactory.createContact('CFA Contact', arxaccount.Id, 'CFA Contact', true);

            // Create Affiliation record
            //Getting Record Type 'Access Provisioning' to filter on

            Id RecID3 = Schema.SObjectType.Affiliation__C.getRecordTypeInfosByName().get('Access Provisioning').getRecordTypeId();
            Affiliation__C aff1 = new Affiliation__C();
            aff1.RecordTypeId = RecID3;
            aff1.Account__c = arxaccount.Id;
            aff1.Contact__c = testContact2.Id;
            aff1.Start_Date__c = Date.today();
            aff1.Active_Affiliation__c = true;
            aff1.Type__c = 'society';
            aff1.Role__c = 'Reviewer';
            aff1.Products__c = 'Membership Application';
            Insert aff1;
            system.assert(aff1 != null);

            arxaccount.ARX_Approved__c = false;
            update arxaccount;
            test.stopTest();
            system.assertequals(false, arxaccount.ARX_Approved__c);
        }

    }

    //test2 user has permission set assigned with no affliations on account and arx approved field is false.
    @isTest
    static void testpositive3ArxValidation() {

        //Get User to use
		User userobj = [SELECT Id,Name,Email,Profile.Name FROM User WHERE Profile.Name='System Administrator' AND Isactive=true LIMIT 1];
        PermissionSet ps = new PermissionSet();

        ps.Name = 'Test_Membership_Application';
        ps.Label = 'Test_Membership_Application';
        insert ps;
        List<SetupEntityAccess> seaList = new List<SetupEntityAccess>();
        for(CustomPermission cp: [select Id from CustomPermission where DeveloperName = 'Manage_ARX']) {
            seaList.add(new SetupEntityAccess(ParentId=ps.Id, SetupEntityId=cp.Id));
        }
        insert seaList;
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        insert new PermissionSetAssignment(AssigneeId=userobj.Id, PermissionSetId=ps.Id);

        //Assign Permission set
        PermissionSet PermID1 =[SELECT id, name from permissionset where name = 'Manage_ARX' LIMIT 1];
        PermissionSetAssignment psa1 = new PermissionSetAssignment(PermissionSetId = PermID1.Id, AssigneeId = userObj.Id);

        test.startTest();
        try {
            insert psa1;

            System.runAs(userObj) {
                List<Account> accountList = new List<Account>();
                //Test Account2
                Id RecID2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId();
                Account abc = new Account();
                abc.RecordTypeId = RecID2;
                abc.ARX_Approved__c = false;
                abc.Name = 'sfdcarxtest1';
                abc.CurrencyIsoCode = 'AED';
                abc.Site = 'site2';
                abc.Relationship_Manager__c = userObj.Id;
                Insert abc;
                system.assert(abc != null);

                abc.ARX_Approved__c = true;
                update abc;
                system.assertequals(true, abc.ARX_Approved__c);
                test.stopTest();
            }
        } catch (exception e) {
        }

    }
    //test3 - permission set is not assigned to the user to access ARX.

    private static testmethod void testnegativeArxValidation() {
        //Get User to use
        User user3 = [SELECT Id,Name,Email,Profile.Name FROM User WHERE Profile.Name='System Administrator' AND Isactive=true LIMIT 1];
        test.startTest();
        System.RunAs(user3) {
            //Test Account
            Id RecID1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId();
            Account testacc = new Account();
            testacc.RecordTypeId = RecID1;
            testacc.Name = 'testnegative';
            testacc.CurrencyIsoCode = 'AED';
            testacc.Site = 'nosite';
            testacc.Relationship_Manager__c = user3.Id;
            insert testacc;
            try {
                testacc.ARX_Approved__c = true;
                update testacc;
            } catch (Exception error) {
                //Assert Error message
                System.assert(error.getMessage().contains('Update failed.'));
                System.assert(error.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
                System.assert(error.getMessage().contains('You do not have permission to Manage ARX. '));
            }
        }
    }
}