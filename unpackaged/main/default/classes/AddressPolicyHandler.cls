/*****************************************************************
Name: ContactTriggerHandler
Copyright © 2021 ITC
============================================================
Purpose: Logic for Assigning Contacts To Societies By Address based on Policies
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   29.01.2021   Created   Logic for Assigning Contacts To Societies By Address
*****************************************************************/
public without sharing class AddressPolicyHandler implements INSAPolicyHandler {
    private final static String ACCOUNT_ID_FIELD = 'AccountId';
    private final static String DELIMITER = ':::';
    private final static String VALID_ZIP_CODE = 'Not Empty Zip Code';
    private final static String NOT_VALID_ZIP_CODE = 'Not Correct Zip Code';
    private final static String EMPTY_ZIP_CODE = 'Empty Zip Code';
    private final static String VALID_STATE = 'Not Empty State';
    private final static String NOT_VALID_STATE = 'Not Valid State';
    private final static String EMPTY_STATE = 'Empty State';
    private final static Id CFA_CONTACT_RECORD_TYPE_ID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CFA_Contact').getRecordTypeId();
    private final static Map<String, Integer> lengthZipCodeByCountryISOCode = new Map<String, Integer>{
            'CAN' => 3,
            'USA' => 5,
            'AUS' => 4
    };
    private final static Map<String, Integer> priorityDictionary = new Map<String, Integer>{
            VALID_STATE + DELIMITER + VALID_ZIP_CODE => 4, //state zip
            VALID_STATE + DELIMITER + NOT_VALID_ZIP_CODE => null, //state notValidZipCode
            VALID_STATE + DELIMITER + EMPTY_ZIP_CODE => 2, //state null

            NOT_VALID_STATE + DELIMITER + VALID_ZIP_CODE => null, //notValidState zip
            NOT_VALID_STATE + DELIMITER + NOT_VALID_ZIP_CODE => null, //notValidState notValidZipCode
            NOT_VALID_STATE + DELIMITER + EMPTY_ZIP_CODE => null, //notValidState null

            EMPTY_STATE + DELIMITER + VALID_ZIP_CODE => 3, //null zip
            EMPTY_STATE + DELIMITER + NOT_VALID_ZIP_CODE => null, //null notValidZipCode
            EMPTY_STATE + DELIMITER + EMPTY_ZIP_CODE => 1 //null null

    };
    private final static Set<String> PARTNER_PROFILES = new Set<String>{
            'Society Partner User Admin', 'Society Partner User Read-Only'
    };
    private Set<Id> partnerUserContactIds;
    private Boolean updateAccount;
    private Boolean isSinglePolicyRun;
    private List<NSA_Policy__c> policies;
    private String CFA_INSTITUTE_ACCOUNT_ID = ContactTriggerHelper.getCFAInstituteAccountId();

    public AddressPolicyHandler() {
        this.updateAccount = true;
        this.isSinglePolicyRun = false;
    }

    public AddressPolicyHandler(Boolean updateAccount) {
        this.updateAccount = updateAccount;
        this.isSinglePolicyRun = false;
    }
    
    public AddressPolicyHandler(Boolean updateAccount, Boolean isSinglePolicyRun) {
        this.updateAccount = updateAccount;
        this.isSinglePolicyRun = isSinglePolicyRun;
    }

    /**
     * @description run policies for contacts
     *
     * @param policies List<NSA_Policy__c>
     * @param contactsWithNewAddress List<Contact>
     *
     */
    public void run(List<NSA_Policy__c> policies, List<Contact> contactsWithNewAddress) {
        this.policies = policies;
        List<Contact> contactsWithoutPolicy;
        List<Contact> contactsToUpdate = new List<Contact>();

        List<Contact> cfaContacts = findCFAContacts(contactsWithNewAddress);
        if(!cfaContacts.isEmpty()) {
            Map<Id, Contact> newContactMap = new Map<Id, Contact>(cfaContacts);
            findPartnerUserContactIds(newContactMap.keySet());

            if (policies != null) {
                Map<Id, NSAPolicyWrapper> policyWrapperMap = createPolicyWrapperMap(policies);
                Map<Id, Map<Id, Integer>> policiesByContactId = searchPoliciesForContacts(newContactMap.values(), policyWrapperMap.values());
                AddressPolicyInfo addrPolicyInfo = sortContactsByPolicies(newContactMap, policiesByContactId);
                contactsWithoutPolicy = addrPolicyInfo.contactsWithoutPolicy;

                //add Contact visibility
                for (Id policyId : addrPolicyInfo.policyIdToContacts.keySet()) {
                    contactsToUpdate.addAll(addContactVisibilityByAddress(policyWrapperMap.get(policyId).nsaPolicy, addrPolicyInfo.policyIdToContacts.get(policyId)));
                }
            } else {
                contactsWithoutPolicy = cfaContacts;
            }
            contactsToUpdate.addAll(addContactVisibilityByAddress(null, contactsWithoutPolicy));
        }
    }

    // *********************** HELPERS **************************//

    /**
     * @description Apply Address Policy just for CFA Contacts
     *
     * @param contacts List<Contact>
     *
     * @return List<Contact>
     */
    private static List<Contact> findCFAContacts(List<Contact> contacts){
        List<Contact> cfaContacts = new List<Contact>();
        for(Contact con : contacts){
            if(con.RecordTypeId == CFA_CONTACT_RECORD_TYPE_ID){
                cfaContacts.add(con);
            }
        }
        return cfaContacts;
    }

    /**
     * @description Search Policies For Contacts
     *
     * @param contactList List<Contact>
     * @param policyWrapperList  List<NSAPolicyWrapper>
     *
     * @return Map<Id, Map<Id, Integer>>
     */
    private Map<Id, Map<Id, Integer>> searchPoliciesForContacts(List<Contact> contactList, List<NSAPolicyWrapper> policyWrapperList) {
        Map<Id, Map<Id, Integer>> policiesByContactId = new Map<Id, Map<Id, Integer>>();

        for (Contact con : contactList) {
            for (NSAPolicyWrapper policyWrapper : policyWrapperList) {
                String key = con.Country_ISO_Code__c + DELIMITER + con.State_Province_ISO_Code__c;
                String keyWithEmptyState = con.Country_ISO_Code__c + DELIMITER + null;

                // if Geographic rule has record with empty state it returns true
                Boolean hasEmptyState = policyWrapper.externalKeys.contains(keyWithEmptyState);

                // contact has correct state
                // if contact has empty state and Geographic rule has record with empty state it also returns true
                Boolean hasState = policyWrapper.externalKeys.contains(key);

                //State
                String state;
                if(hasState){
                    state = VALID_STATE;
                } else if(hasEmptyState){
                    state = EMPTY_STATE;
                } else {
                    state = NOT_VALID_STATE;
                }
                if (hasEmptyState || hasState) {
                    String zipCodeAccessLevel = searchByZipCode(con, policyWrapper.zipCodeMap.values());
                    Integer newAccessLevel = priorityDictionary.get(state + DELIMITER + zipCodeAccessLevel);
                    if (newAccessLevel != null) {
                        if (!policiesByContactId.containsKey(con.Id)) {
                            policiesByContactId.put(con.Id, new Map<Id, Integer>());
                            policiesByContactId.get(con.Id).put(policyWrapper.nsaPolicy.Id, null);
                        }
                        Integer existedAccessLevel = policiesByContactId.get(con.Id).get(policyWrapper.nsaPolicy.Id);
                        if (existedAccessLevel == null || existedAccessLevel < newAccessLevel) {
                            policiesByContactId.get(con.Id).put(policyWrapper.nsaPolicy.Id, newAccessLevel);
                        }
                    }
                }
            }
        }
        return policiesByContactId;
    }

    /**
     * @description Search policy By ZipCode
     *
     * @param con Contact
     * @param zipCodeWrapperList  List<ZipCodeWrapper>
     *
     * @return String
     */
    private String searchByZipCode(Contact con, List<ZipCodeWrapper> zipCodeWrapperList) {
        Set<String> accessLevel = new Set<String>();
        for (ZipCodeWrapper zipCode : zipCodeWrapperList) {
            if (zipCode.belongs(con.MailingPostalCode, con.Country_ISO_Code__c, con.State_Province_ISO_Code__c)) {
                accessLevel.add(VALID_ZIP_CODE);
            } else if (zipCode.belongs(null, con.Country_ISO_Code__c, con.State_Province_ISO_Code__c)) {
                accessLevel.add(EMPTY_ZIP_CODE);
            }
        }
        if (accessLevel.size() == 1) {
            return new List<String>(accessLevel)[0];
        } else if (accessLevel.size() > 1) {
            return VALID_ZIP_CODE;
        }
        return NOT_VALID_ZIP_CODE;
    }

    /**
     * @description Sort Contacts By Policies
     *
     * @param contacts Map<Id, Contact>
     * @param policiesByContactId Map<Id, Map<Id, Integer>>
     *
     * @return AddressPolicyInfo
     */
    private AddressPolicyInfo sortContactsByPolicies(Map<Id, Contact> contacts, Map<Id, Map<Id, Integer>> policiesByContactId) {
        AddressPolicyInfo addrPolicyInfo = new AddressPolicyInfo();
        List<Contact> contactsWithoutPolicy = new List<Contact>();
        Map<Id, List<Contact>> policyIdToContacts = new Map<Id, List<Contact>>();
        for (Contact con : contacts.values()) {
            if (policiesByContactId.containsKey(con.Id)) {
                Map<Id, Integer> policies = policiesByContactId.get(con.Id);
                Id appropriatePolicyId = getPolicyId(policies);
                if (!policyIdToContacts.containsKey(appropriatePolicyId)) {
                    policyIdToContacts.put(appropriatePolicyId, new List<Contact>());
                }
                policyIdToContacts.get(appropriatePolicyId).add(con);
            } else {
                contactsWithoutPolicy.add(con);
            }
        }
        addrPolicyInfo.contactsWithoutPolicy = contactsWithoutPolicy;
        addrPolicyInfo.policyIdToContacts = policyIdToContacts;
        return addrPolicyInfo;
    }

    /**
     * @description Get Policy Id based on access level from the Dictionary
     *
     * @param policies Map<Id, Integer>
     *
     * @return Id
     */
    private Id getPolicyId(Map<Id, Integer> policies) {
        Id appropriatePolicyId = (new List<Id>(policies.keySet()))[0];
        if (policies.size() > 1) {
            Integer maxAccessLevel = 0;
            appropriatePolicyId = null;
            for (Id policyId : policies.keySet()) {
                Integer accessLevel = policies.get(policyId);
                if (maxAccessLevel < accessLevel) {
                    maxAccessLevel = accessLevel;
                    appropriatePolicyId = policyId;
                }
            }
        }
        return appropriatePolicyId;
    }

    /**
     * @description Create PolicyWrapper Map
     *
     * @param policies List<NSA_Policy__c>
     *
     * @return ap<Id, NSAPolicyWrapper>
     */
    private Map<Id, NSAPolicyWrapper> createPolicyWrapperMap(List<NSA_Policy__c> policies) {
        Map<Id, NSAPolicyWrapper> policyWrappers = new Map<Id, NSAPolicyWrapper>();
        for (NSA_Policy__c policy : policies) {
            for (NSA_Geographic_Rule__c rule : policy.NSA_Geograhpic_Rules__r) {
                if (!policyWrappers.containsKey(rule.NSA_Policy__c)) {
                    NSAPolicyWrapper policyWrapper = new NSAPolicyWrapper();
                    policyWrapper.nsaPolicy = policy;
                    policyWrappers.put(rule.NSA_Policy__c, policyWrapper);
                }
                NSAPolicyWrapper policyWrapper = policyWrappers.get(rule.NSA_Policy__c);
                if(policyWrapper == null){
                    policyWrapper = new NSAPolicyWrapper();
                }
                String countryStateKey = rule.Country_Name__c + DELIMITER + rule.State__c;
                if (!policyWrapper.externalKeys.contains(countryStateKey)) {
                    policyWrapper.externalKeys.add(countryStateKey);
                }
                String zipCodeKey = rule.Country_Name__c + DELIMITER + rule.State__c + DELIMITER + rule.Zip_Code_Low__c + DELIMITER + rule.Zip_Code_High__c;
                if (!policyWrapper.zipCodeMap.containsKey(zipCodeKey)) {
                    policyWrapper.zipCodeMap.put(zipCodeKey, new ZipCodeWrapper(rule.Country_Name__c,rule.State__c, rule.Zip_Code_Low__c, rule.Zip_Code_High__c));
                }
            }
        }
        return policyWrappers;
    }

    /**
     * @description Add Contact Visibility By Address
     *
     * @param policy NSA_Policy__c
     * @param contacts List<Contact>
     *
     * @return List<Contact>
     */
    private List<Contact> addContactVisibilityByAddress(NSA_Policy__c policy, List<Contact> contacts) {
        Map<Id,Contact> updatedContacts = new Map<Id,Contact>();
        Boolean isUpdatedAccountField = false;
        Boolean isUpdatedSocietyField = false;

        for (Contact con : contacts) {
            isUpdatedAccountField = updateAccountField(policy,con);
            isUpdatedSocietyField = updateSocietyField(policy,con);

            if(isUpdatedAccountField || isUpdatedSocietyField){
                updatedContacts.put(con.Id,con);
            }
        }
        return updatedContacts.values();
    }

    /**
     * @description Update Society Field
     *
     * @param policy NSA_Policy__c
     * @param con Contact
     *
     * @return Boolean
     */
    private Boolean updateSocietyField(NSA_Policy__c policy, Contact con){
        if (policy != null) {
            //update society field
            updateContactField(con, policy.Contact_Society_Field_API_Name__c, true);
            return true;
        }
        return false;
    }

    /**
     * @description Update Account Field
     *
     * @param policy NSA_Policy__c
     * @param con Contact
     *
     * @return Boolean
     */
    private Boolean updateAccountField(NSA_Policy__c policy, Contact con){
        if(updateAccount && !partnerUserContactIds.contains(con.Id)) {
            if (policy != null) {
                //update account field
                Id accId = policy.Society_Account__c;
                if (accId == null) {
                    accId = CFA_INSTITUTE_ACCOUNT_ID;
                }
                updateContactField(con, ACCOUNT_ID_FIELD, accId);
            } else if(!isSinglePolicyRun || policies == null || con.AccountId == policies[0].Society_Account__c) {
                //update account field
                updateContactField(con, ACCOUNT_ID_FIELD, CFA_INSTITUTE_ACCOUNT_ID);
            }
            return true;
        }
        return false;
    }

    /**
     * @description Update Contact Field
     *
     * @param con Contact
     * @param fieldName String
     * @param value Object
     */
    private void updateContactField(Contact con, String fieldName, Object value) {
        con.put(fieldName, value);
    }

    /**
     * @description Find Partner User Contact Ids
     *
     * @param contactIds Set<Id>
     */
    private void findPartnerUserContactIds(Set<Id> contactIds){
        Set<Id> partnerUserContactIds = new Set<Id>();

        List<User> users = [SELECT ContactId FROM User WHERE ContactId IN :contactIds AND Profile.Name IN :PARTNER_PROFILES];
        for(User u : users){
            partnerUserContactIds.add(u.ContactId);
        }
        this.partnerUserContactIds = partnerUserContactIds;
    }
    /* *********************** WRAPPERS ************************** */

    private class NSAPolicyWrapper {
        NSA_Policy__c nsaPolicy;
        Set<String> externalKeys;
        Map<String, ZipCodeWrapper> zipCodeMap;

        public NSAPolicyWrapper() {
            this.externalKeys = new Set<String>();
            this.zipCodeMap = new Map<String, ZipCodeWrapper>();
        }
    }

    public class ZipCodeWrapper {
        Integer zipCodeLength;
        String lowZipCode;
        String highZipCode;
        String state;
        String countryISOCode;

        public ZipCodeWrapper(String countryISOCode, String state, String lowZipCode, String highZipCode) {
            this.countryISOCode = countryISOCode;
            this.state = state;
            if(lengthZipCodeByCountryISOCode.containsKey(countryISOCode)){
                this.zipCodeLength = lengthZipCodeByCountryISOCode.get(countryISOCode);
                this.lowZipCode = lowZipCode;
                if(lowZipCode != null && lowZipCode.length() > zipCodeLength) {
                    this.lowZipCode = lowZipCode.substring(0, zipCodeLength);
                }
                this.highZipCode = highZipCode;
                if(highZipCode != null && highZipCode.length() > zipCodeLength) {
                    this.highZipCode = highZipCode.substring(0,zipCodeLength);
                }
            } else {
                this.lowZipCode = lowZipCode;
                this.highZipCode = highZipCode;
            }

        }

        public Boolean belongs(String zipCode, String country, String state) {
            if(this.countryISOCode == country && (this.state == state || this.state == null)) {
                if (zipCode == null && (lowZipCode == null || highZipCode == null)) {
                    return true;
                }
                if (zipCode == null || lowZipCode == null || highZipCode == null) {
                    return false;
                }
                if (zipCodeLength != null && zipCode.length() > zipCodeLength) {
                    zipCode = zipCode.substring(0, zipCodeLength);
                }
                return ((lowZipCode.replaceAll(' ', '') <= zipCode.replaceAll(' ', '')) && (highZipCode.replaceAll(' ', '') >= zipCode.replaceAll(' ', '')));
            } else {
                return false;
            }
        }
    }

    public class AddressPolicyInfo {
        public List<Contact> contactsWithoutPolicy;
        public Map<Id, List<Contact>> policyIdToContacts;
    }

}