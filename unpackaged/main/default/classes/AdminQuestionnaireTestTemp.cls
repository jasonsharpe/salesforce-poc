@isTest
private class AdminQuestionnaireTestTemp{
    
    @testsetup
    static void createLead(){
        
        EventApi__Event_Category__c EventCategory = new EventApi__Event_Category__c(
            Name = 'Test category'
            );
        insert new List<EventApi__Event_Category__c>{
            EventCategory
        };
        
        // creating event
        EventApi__Event__c Event = new EventApi__Event__c(
            EventApi__Event_Category__c = EventCategory.id,
            EventApi__Event_Key__c = '123',
            Name = 'test event',
            EventApi__Status__c = 'Active',
            CFAMN__Enable_Event_Reminder__c = true,
            EventApi__Ticket_Sales_Start_Date__c = system.today(),
            EventApi__Registration_Style__c = 'Simple',
            EventApi__Start_Date__c = system.today().addDays(1),
            CFAMN__Event_Reminder_Days_Variable__c = 1,
            EventApi__Capacity__c = 100,
            EventApi__Quantity_Sold__c = 0,
            EventApi__Start_Date_Time__c = system.today(),
            EventApi__End_Date_Time__c = system.today().addDays(1)
            );
        insert new List<EventApi__Event__c>{
            Event
        };
     // creating Attendee
        List<EventApi__Attendee__c> lst_attendee =new List<EventApi__Attendee__c>();
        EventApi__Attendee__c attendee = new EventApi__Attendee__c(
            EventApi__Event__c = Event.id,
            EventApi__Send_Notification__c= true,
            EventApi__Email__c = 'abc@gmail.com',
            EventApi__Status__c = 'Registered'
            );
         lst_attendee.add(attendee);
           
         EventApi__Attendee__c attendee2 = new EventApi__Attendee__c(
            EventApi__Event__c = Event.id,
            EventApi__Send_Notification__c= true,
            EventApi__Email__c = 'abcd@gmail.com',
            EventApi__Status__c = 'Registered'
            );
        
        lst_attendee.add(attendee2);

        insert lst_attendee;
        
        PagesApi__Form__c form = new PagesApi__Form__c();
        form.Name = 'Test Form';
        insert form;
        
        // insert PagesApi__Form_Response__c
        List<PagesApi__Form_Response__c> lst_fRes = new List<PagesApi__Form_Response__c>();
        
        PagesApi__Form_Response__c fRes = new PagesApi__Form_Response__c();
        fRes.EventApi__Attendee__c = lst_attendee[0].Id;
        fRes.PagesApi__Form__c = form.Id;
        lst_fRes.add(fRes);
        
        PagesApi__Form_Response__c fRes2 = new PagesApi__Form_Response__c();
        fRes2.EventApi__Attendee__c = lst_attendee[1].Id;
        fRes2.PagesApi__Form__c = form.Id;
        lst_fRes.add(fRes2);

        insert lst_fRes;
        
        PagesApi__Field_Group__c fGroup = new PagesApi__Field_Group__c();
        fGroup.PagesApi__Form__c = form.Id;
        fGroup.Name = 'Test Field Group';
        insert fGroup;
        
                
        List<PagesApi__Field_Response__c> lst_fieldResponse = new List<PagesApi__Field_Response__c>();
        
        PagesApi__Field_Response__c fieldResponse = new PagesApi__Field_Response__c();
        fieldResponse.EventApi__Attendee__c = lst_attendee[0].Id;
        fieldResponse.PagesApi__Form_Response__c = lst_fRes[0].Id;
        fieldResponse.PagesApi__Field_Group__c = fGroup.Id;
        //fieldResponse.Download_Attachment__c = 'https://test.com';
        fieldResponse.PagesApi__Response__c = 'testing';
        lst_fieldResponse.add(fieldResponse);
        
        PagesApi__Field_Response__c fieldResponse2 = new PagesApi__Field_Response__c();
        fieldResponse2.EventApi__Attendee__c = lst_attendee[1].Id;
        fieldResponse2.PagesApi__Form_Response__c = lst_fRes[1].Id;
        fieldResponse2.PagesApi__Field_Group__c = fGroup.Id;
       // fieldResponse.Download_Attachment__c = 'https://test.com';
        fieldResponse.PagesApi__Response__c = 'No';
        lst_fieldResponse.add(fieldResponse2);
        
        insert lst_fieldResponse;
        
    }
    
    private static testMethod void Method1() {
        
        // getting Event
        EventApi__Event__c eve = [SELECT ID
                                    FROM EventApi__Event__c 
                                    WHERE EventApi__Event_Key__c = '123'];
        
        System.currentPageReference().getParameters().put('id', eve.Id);
        adminQuestionnaireTemp obj = new adminQuestionnaireTemp();
        FormResponseWrapperTemp formWrap = new FormResponseWrapperTemp();
        
        // calling methods
        obj.getAttendeeResponses();
        obj.getAttendeeQuestions();
        string res = obj.xlsHeader;
    }

}