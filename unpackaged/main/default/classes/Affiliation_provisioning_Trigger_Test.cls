/**************************************************************************************************
* Apex Class Name   : Affiliation_provisioning_Trigger_Test
* Purpose           : This is the test class which is used for Affiliation_Provisioning_TriggerHandler class.
* Version           : 1.0 Initial Version
* Organization      : Chan -- Lightning Team
* Created Date      : 13-Nov-2018
***************************************************************************************************/
@isTest
public class Affiliation_provisioning_Trigger_Test {
    
    private static testMethod void testAccessProvisioning() {

		User userObj = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND isActive=true LIMIT 1];
        PermissionSet ps = new PermissionSet();
        ps.Name = 'Test_Membership_Application';
        ps.Label = 'Test_Membership_Application';
        RetryForTest.upsertOnUnableToLockRow(ps);

        List<SetupEntityAccess> seaList = new List<SetupEntityAccess>();
        for(CustomPermission cp: [select Id from CustomPermission where DeveloperName = 'Membership_Application']) {
            seaList.add(new SetupEntityAccess(ParentId=ps.Id, SetupEntityId=cp.Id));
        }
        RetryForTest.upsertOnUnableToLockRow(seaList);
        RetryForTest.upsertOnUnableToLockRow(
            new PermissionSetAssignment(AssigneeId=userObj.Id, PermissionSetId=ps.Id)
        );

        System.runAs(userObj){
            //Get test contact    
            Contact testContact = CFA_TestDataFactory.createContact('Test Contact',null,'CFA Contact', false);
            testContact.Person_Id__c = '45364654';
            testContact.Partner_Id__c = cfa_ApexUtilities.generateRandomString(5);
            RetryForTest.upsertOnUnableToLockRow(testContact);
            //Get Test Account
			Account Testacc = CFA_TestDataFactory.createAccount('Test Account', null, true);
            // Create Affiliation record for provisioning
            //Getting Record Type 'Access Provisioning' to filter on         
            Id RecID = Schema.SObjectType.Affiliation__C.getRecordTypeInfosByName().get('Access Provisioning').getRecordTypeId();
            
            Affiliation__C Aff = new Affiliation__C();
            Aff.RecordTypeId = RecID;
            Aff.Account__c = Testacc.Id;
            Aff.Contact__c = testContact.Id;
            Aff.Start_Date__c = Date.today();
            Aff.Type__c = 'Society';
            Aff.Role__c = 'Reviewer';
            Aff.Products__c = 'Membership Application';
            
            RetryForTest.upsertOnUnableToLockRow(Aff);
        }
    }
}