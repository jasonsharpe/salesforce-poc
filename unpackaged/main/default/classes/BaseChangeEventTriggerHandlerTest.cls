/*****************************************************************
Name: BaseChangeEventTriggerHandlerTest
Copyright © 2020 ITC
============================================================
Purpose: Unit tests for BaseChangeEventTriggerHandler
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Lukasz Kwiatkowski   30.12.2020   Created   CSR:
*****************************************************************/

@IsTest
private class BaseChangeEventTriggerHandlerTest {
    @IsTest
    private static void isDisabledTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();

        Test.stopTest();

        System.assertEquals(false, handler.isDisabled());
    }
    
    @IsTest
    private static void handleCreateTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleCreate(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleGAPCreateTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleGAPCreate(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleAllCreateTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleAllCreate(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleUpdateTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleUpdate(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleGAPUpdateTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleGAPUpdate(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleAllUpdateTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleAllUpdate(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleDeleteTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleDelete(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleGAPDeleteTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleGAPDelete(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleAllDeleteTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleAllDelete(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleUndeleteTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleUndelete(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleGAPUndeleteTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleGAPUndelete(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleAllUndeleteTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleAllUndelete(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void handleOverflowTest() {
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
			handler.handleOverflow(null);
        Test.stopTest();
    }
    
    @IsTest
    private static void getRecordIdsTest() {
        Set<String> result;
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
        	result = handler.getRecordIds(new List<sObject> {});
        Test.stopTest();
        
        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());
    }

    @IsTest
    private static void getRecordIdsWithParametersTest() {
        Set<String> result;
        Test.startTest();
            TestEventTriggerHandler handler = new TestEventTriggerHandler();
        	result = handler.getRecordIds(new List<sObject> {},new Set<String>{});
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());
    }
    
    private class TestEventTriggerHandler extends BaseChangeEventTriggerHandler {
        public override String getTriggerName() {
            return 'TestEventTriggerHandler';
        }
    }
}