@isTest
public class CFA_CommunityContactTriggerTest {
    private static Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Internal CFA Contact').getRecordTypeId();

    @TestSetup
    static void makeData(){
        Account portalAccount = new Account(
            Name = 'TestAccountCommunityUser'
        );
        insert portalAccount;

        //Create contact for the account, which will enable partner portal for account
        Contact contact = new Contact(
            Lastname = 'testConCommunityUser',
            AccountId = portalAccount.Id,
            RecordTypeId = devRecordTypeId,
            Email = 'testCommunityUser@test.com',
            CFAMN__CFACandidateCode__c = '1PX',
            CFAMN__CFAMemberCode__c = 'NMP'
        );
        insert contact;

    }
    @isTest
    static void afterUpdatetestDA(){
        Id communityProfId = [SELECT Id FROM Profile WHERE Name = 'CFA_Customer_Community'].Id;
        List < Contact > contacts = [SELECT Id, Name , CFA_Program_Candidate__c,CFA_Institute_Member__c
                                    FROM Contact
                                    WHERE LastName = 'testConCommunityUser'];
        List < User > lstUser = new List < User > ();
        User user = new User(
        Username = 'CommunityUsertest@test.com.test',
        ContactId = contacts[0].Id,
        ProfileId = communityProfId,
        Alias = 'cut',
        Email = 'CommunityUsertest@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'testUsertest',
        CommunityNickname = 'testUsertest',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US',
        Provision_Self_Service__c = false,
        IsActive = true
        );
        lstUser.add(user);

        insert lstUser;

        if(!contacts.isEmpty()){
            contacts[0].CFAMN__CFACandidateCode__c = '';
            contacts[0].CFAMN__CFAMemberCode__c = '';
            List<Database.SaveResult> sr = Database.update(contacts, false);
            System.debug('sr: ' + sr);


            contacts[0].CFAMN__CFACandidateCode__c = '';
            contacts[0].CFAMN__CFAMemberCode__c = '';
            sr = Database.update(contacts, false);
            System.debug('sr: ' + sr);
        }

    }
    @isTest
    static void afterUpdatetestSS(){
        Id communityProfId = [SELECT Id FROM Profile WHERE Name = 'CFA_Customer_Community'].Id;
        List < Contact > contacts = [SELECT Id, Name , CFA_Program_Candidate__c,CFA_Institute_Member__c
        FROM Contact
        WHERE LastName = 'testConCommunityUser'];
        List < User > lstUser = new List < User > ();
        User user = new User(
                Username = 'CommunityUsertest@test.com.test',
                ContactId = contacts[0].Id,
                ProfileId = communityProfId,
                Alias = 'cut',
                Email = 'CommunityUsertest@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'testUsertest',
                CommunityNickname = 'testUsertest',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                Provision_Self_Service__c = true,
                IsActive = true
        );
        lstUser.add(user);

        insert lstUser;

        if(!contacts.isEmpty()){
            contacts[0].CFAMN__CFACandidateCode__c = '1C';
            contacts[0].CFAMN__CFAMemberCode__c = '';
            List<Database.SaveResult> sr = Database.update(contacts, false);
            System.debug('sr: ' + sr);
            contacts[0].CFAMN__CFACandidateCode__c = '';
            contacts[0].CFAMN__CFAMemberCode__c = 'AOA';
            sr = Database.update(contacts, false);
            System.debug('sr: ' + sr);


        }

    }
}