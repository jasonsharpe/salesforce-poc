/**
 * Created by bsebl on 4/22/2020.
 */

public with sharing class CFA_CommunityUserTriggerHandler implements ITriggerHandler{
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;

    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        if (Test.isRunningTest()) {
            return TriggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('CommunityUserTrigger') || triggerDisabled;
        }
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        Id communityProfId = [SELECT Id, Name FROM Profile WHERE Name= 'CFA_Customer_Community' LIMIT 1].Id;

        List<User> communityUsers = new List<User>();
        List<User> allUsers = newItems.values();
        
        for(User currentUser: allUsers){
            if(currentUser.ProfileId == communityProfId || currentUser.Provision_VM_Community__c){
                communityUsers.add(currentUser);
            }
        }
        
        if(communityUsers != null && !communityUsers.isEmpty()){
            CFA_CommunityuserTriggerLogic.handleSelfServicePermissionSets(communityUsers);
        }
        
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}