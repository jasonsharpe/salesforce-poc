@isTest
public class CFA_CommunityUserTriggerTest {
    private static Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Internal CFA Contact').getRecordTypeId();
    
    @TestSetup
    static void makeData() {
        User userAdmin = CFA_TestDataFactory.getUserWithPermission('System Administrator', 'Edit_Account_Name');

        System.runAs(userAdmin){
           UserRole ur = new UserRole(DeveloperName = 'CEO', Name = 'CEO');
           RetryForTest.upsertOnUnableToLockRow(ur);
           
           userAdmin.UserRoleId = ur.Id;
           RetryForTest.upsertOnUnableToLockRow(userAdmin);
        }

        List<Account> lstAccount = new List<Account>();

        Account portalAccount = new Account(
            Name = 'TestAccountCommunityUser'
        );
        lstAccount.add(portalAccount);

        Account portalAccount1 = new Account(
            Name = 'TestAccountCommunityUser1'
        );
        lstAccount.add(portalAccount1);

        Account newAccount = new Account(
            Name = 'newUpdateAccount'
        );
        lstAccount.add(newAccount);

        RetryForTest.upsertOnUnableToLockRow(lstAccount);

        List<Contact> lstContacts = new List<Contact>();
        //Create contact for the account, which will enable partner portal for account
        Contact contact = new Contact(
            Lastname = 'testConCommunityUser',
            AccountId = portalAccount.Id,
            RecordTypeId = devRecordTypeId,
            Email = 'testCommunityUser@test.com',
            CFA_Member_Code__c = 'AOA',
            CFA_Candidate_Code__c = '1C'
        );
        lstContacts.add(contact);
        
        //Create contact for the account, which will enable partner portal for account
        Contact contact1 = new Contact(
            Lastname = 'testConCommunityUser1',
            AccountId = portalAccount1.Id,
            RecordTypeId = devRecordTypeId,
            Email = 'testCommunityUser1@test.com'
        );
        lstContacts.add(contact1);

        Contact newContact = new Contact(
            Lastname = 'newContact',
            AccountId = newAccount.Id,
            RecordTypeId = devRecordTypeId,
            Email = 'newContact@test.com',
            CFAMN__CFAMemberCode__c = 'AOA',
            CFAMN__CFACandidateCode__c = '1C'
        );
        lstContacts.add(newContact);
        RetryForTest.upsertOnUnableToLockRow(lstContacts);
    }

    @isTest
    static void afterInsertTest() {
        Id communityProfId = [SELECT Id FROM Profile WHERE Name = 'CFA_Customer_Community'].Id;
        List < Contact > contacts = [SELECT Id, Name FROM Contact WHERE LastName = 'testConCommunityUser'];

        List < User > lstUser = new List < User > ();
        
        User user = new User(
            Username = 'CommunityUsertest@test.com.test',
            ContactId = contacts[0].Id,
            ProfileId = communityProfId,
            Alias = 'cut',
            Email = 'CommunityUsertest@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'testUsertest',
            CommunityNickname = 'testUsertest',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            IsActive = true
        );
        lstUser.add(user);

        insert lstUser;

        User userAdmin = CFA_TestDataFactory.getUserWithPermission('System Administrator', 'Edit_Account_Name');
        
        List<User> updateUser = [Select Id,ContactId FROM USER WHERE Username =:'CommunityUsertest@test.com.test'];
        System.runAs(userAdmin) {
            Test.startTest();
            
            for(User usr : updateUser){
                usr.Provision_Self_Service__c = true;
                usr.Provision_VM_Community__c  = true;
            }
            RetryForTest.upsertOnUnableToLockRow(updateUser);

            CFA_CommunityUserTriggerHandler ccth = new CFA_CommunityUserTriggerHandler();
            ccth.BeforeInsert(null);
            ccth.BeforeUpdate(null,null);
            ccth.BeforeDelete(null);
            contacts[0].CFAMN__CFAMemberCode__c = 'CNX';
            contacts[0].CFAMN__CFACandidateCode__c = '1P';
            RetryForTest.upsertOnUnableToLockRow(contacts);

            Test.stopTest(); 
        }
    }

    @isTest
    static void testCleanPermissionSet() {
        Id communityProfId = [SELECT Id FROM Profile WHERE Name = 'CFA_Customer_Community'].Id;
        
        List < Contact > contacts = [SELECT Id, Name FROM Contact WHERE LastName = 'testConCommunityUser1'];
        
        User u = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true AND UserRoleId != null LIMIT 1];
        
        list < User > lstUser = new list < User > ();
        User user = new User(
            Username = 'CommunityUsertest@test.com.test',
            ContactId = contacts[0].Id,
            ProfileId = communityProfId,
            Alias = 'cut',
            Email = 'CommunityUsertest@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'testUsertest',
            CommunityNickname = 'testUsertest',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            IsActive = true
        );
        
        lstUser.add(user);

        User userAdmin = CFA_TestDataFactory.getUserWithPermission('System Administrator', 'Edit_Account_Name');

        System.runAs(userAdmin){
            RetryForTest.upsertOnUnableToLockRow(lstUser);
        }

        Id candidatePermissionSetId = [SELECT Id, Name FROM PermissionSet WHERE Name = 'CFA_Candidate' LIMIT 1].Id;
        System.runAs(u) {

            PermissionSetAssignment currentAssignment = new PermissionSetAssignment();
            currentAssignment.PermissionSetId = candidatePermissionSetId;
            currentAssignment.AssigneeId = user.Id;
            RetryForTest.upsertOnUnableToLockRow(currentAssignment);

            Test.startTest();

            System.assertEquals( 1, [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :user.Id AND PermissionSetId = :candidatePermissionSetId].size() );
                CFA_CommunityuserTriggerLogic.cleanPermissionSets( lstUser );

            Test.stopTest(); 
        }
        System.assertEquals( 0, [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :user.Id AND PermissionSetId = :candidatePermissionSetId].size() );
    }
    
}