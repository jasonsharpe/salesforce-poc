/**
 * Created by bsebl on 4/22/2020.
 * Modified by Geetha based upon new enhancements
 */

public with sharing class CFA_CommunityuserTriggerLogic { 

    private class InsertFuturePermissionSets implements Queueable {

        public Map<Id, List<Id>> assignmentMap;

        public InsertFuturePermissionSets(Map<Id, List<Id>> assignmentMap){
            this.assignmentMap = assignmentMap;
        }

        public void execute(QueueableContext context) {
            for(Id permissionSetId : assignmentMap.keySet()) {
                List<Id> userList = assignmentMap.get(permissionSetId);
                List<PermissionSetAssignment> assignments = new List<PermissionSetAssignment>();
                List<User> usersToUpdate = [SELECT Id, ProfileId, Provision_Self_Service__c FROM User WHERE Id IN :userList];

                for(User currentUser: usersToUpdate) {
                    PermissionSetAssignment currentAssignment = new PermissionSetAssignment();
                    currentAssignment.PermissionSetId = permissionSetId;
                    currentAssignment.AssigneeId = currentUser.Id;
                    assignments.add(currentAssignment);
                }

                for(Database.UpsertResult result: Database.upsert(assignments, false)) {
                    if(result.isSuccess()){
                        System.debug('Successful permission set assignment: ' + result);
                    } else {
                        for(Database.Error currentError: result.getErrors()){
                            System.debug('Unsuccessful permission set assignment: ' + currentError);
                        }
                    }
                }
            }
        }
    }

    private class CleanPermissionSets implements Queueable {

        public Map<Id, List<Id>> assignmentMap;

        public CleanPermissionSets(Map<Id, List<Id>> assignmentMap){
            this.assignmentMap = assignmentMap;
        }

        public void execute(QueueableContext context) {
            List<PermissionSetAssignment> toDelete = new List<PermissionSetAssignment>();
            for(Id permissionSetId : assignmentMap.keySet()) {
                List<Id> userList = assignmentMap.get(permissionSetId);
                toDelete.addAll( [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId IN :userList AND PermissionSetId = :permissionSetId] );
            }

            for(Database.DeleteResult result: Database.delete(toDelete, false)){
                if(result.isSuccess()){
                    System.debug('Successful permission set unassignment: ' + result);
                }else{
                    for(Database.Error currentError: result.getErrors()){
                        System.debug('Unsuccessful permission set unassignment: ' + currentError);
                    }
                }
            }
        }
    }

    public static void handleSelfServicePermissionSets(List<User> userList){
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        List<Id> affectedContactIds = new List<Id>();
        List<PermissionSetAssignment> psAssignments = new List<PermissionSetAssignment>();
        Map<Id, List<Id>> assignmentMap = new Map<Id, List<Id>>();

        Id candidatePermissionSetId = [SELECT Id, Name FROM PermissionSet WHERE Name = 'CFA_Candidate' LIMIT 1].Id;
        Id prospectPermissionSetId = [SELECT Id, Name FROM PermissionSet WHERE Name = 'CFA_Prospect' LIMIT 1].Id;
        Id memberPermissionSetId = [SELECT Id, Name FROM PermissionSet WHERE Name = 'CFA_Member' LIMIT 1].Id;


        for(User currentUser: userList){
            affectedContactIds.add(currentUser.ContactId);
        }

        for(Contact currentContact: [SELECT Id, CFA_Institute_Member__c, CFA_Program_Candidate__c FROM Contact WHERE Id IN :affectedContactIds]){
            contactMap.put(currentContact.Id, currentContact);
        }

        for(User currentUser: userList){
            Contact currentContact = contactMap.get(currentUser.ContactId);
            Id currentPSId = null;

            if(currentContact.CFA_Institute_Member__c){
                if(assignmentMap.containsKey(memberPermissionSetId)){
                    assignmentMap.get(memberPermissionSetId).add(currentUser.Id);
                }
                else{
                    assignmentMap.put(memberPermissionSetId, new List<Id>{currentUser.Id});
                }
            }
            if(currentContact.CFA_Program_Candidate__c){
                if(assignmentMap.containsKey(candidatePermissionSetId)){
                    assignmentMap.get(candidatePermissionSetId).add(currentUser.Id);
                }
                else{
                    assignmentMap.put(candidatePermissionSetId, new List<Id>{currentUser.Id});
                }
            }
            if(currentUser.Provision_Self_Service__c && !currentContact.CFA_Institute_Member__c && !currentContact.CFA_Program_Candidate__c){
                if(assignmentMap.containsKey(prospectPermissionSetId)){
                    assignmentMap.get(prospectPermissionSetId).add(currentUser.Id);
                }
                else{
                    assignmentMap.put(prospectPermissionSetId, new List<Id>{currentUser.Id});
                }
            }

        }

        System.enqueueJob(new InsertFuturePermissionSets(assignmentMap));
    }

    public static void cleanPermissionSets(List<User> userList){
        Map<Id, List<Contact>> userToContactMap = new Map<Id, List<Contact>>();
        List<Id> contactIds = new List<Id>();
        Map<Id, User> contactUserMap = new Map<Id, User>();
        Map<Id, List<Id>> assignmentMap = new Map<Id, List<Id>>();


        Id candidatePermissionSetId = [SELECT Id, Name FROM PermissionSet WHERE Name = 'CFA_Candidate' LIMIT 1].Id;
        Id memberPermissionSetId = [SELECT Id, Name FROM PermissionSet WHERE Name = 'CFA_Member' LIMIT 1].Id;


        for(User currentUser: userList){
            contactIds.add(currentUser.ContactId);
            contactUserMap.put(currentUser.ContactId, currentUser);
        }

        for(Contact currentContact: [SELECT Id, CFA_Institute_Member__c, CFA_Program_Candidate__c FROM Contact WHERE Id IN :contactIds]){

            Id currentPSId = null;
            User currentUser = contactUserMap.get(currentContact.Id);

            if(currentContact.CFA_Institute_Member__c != true){
                currentPSId = memberPermissionSetId;
                List<Id> currentUsersAssigned = new List<Id>();
                if(assignmentMap.containsKey(currentPSId)){
                    currentUsersAssigned = assignmentMap.get(currentPSId);
                }
                currentUsersAssigned.add(currentUser.Id);
                assignmentMap.put(currentPSId, currentUsersAssigned);
            }
            if(currentContact.CFA_Program_Candidate__c != true){
                currentPSId = candidatePermissionSetId;
                List<Id> currentUsersAssigned = new List<Id>();
                if(assignmentMap.containsKey(currentPSId)){
                    currentUsersAssigned = assignmentMap.get(currentPSId);
                }
                currentUsersAssigned.add(currentUser.Id);
                assignmentMap.put(currentPSId, currentUsersAssigned);
            }
        }

        System.enqueueJob( new CleanPermissionSets(assignmentMap) );
    }

}