/**
 * Encapsulates all behaviour logic relating to the
 *
 * For more guidelines and details see
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
 * @group Common Layer
 **/
public with sharing class CFA_Configuration {

    public class CfaException extends Exception {}

    ////////////////////////////////////////////////////////////////////////
    // Data Members
    /////////////////////////////////////////////////////////////////////////

    public static final string SPACE_COMMA_s = ' |\\,';
    // our custom setting
    @TestVisible
    private static CFA_Configuration__c m_settings;
    // used for unit testing
    @TestVisible
    private static Boolean m_forUnitTesting = false;


    ////////////////////////////////////////////////////////////////////////
    // Public Methods
    /////////////////////////////////////////////////////////////////////////

    /**
    * @description is debug enabled
    *
    * @return true if enabled
    */
    public static Boolean isDebug() {
        return CFA_Configuration.findDebug();
    } // end of isDebug

    /**
    * @description is debug enabled
    *
    * @return true if enabled
    */
    public static string getNotifyUserEmailAddress() {
        return CFA_Configuration.getNotifyUserEmail();
    } // end of isDebug

    /**
    * @description is the trigger object name is off
    *
    * @param objectName String , name of object/class name
    * @return true if enabled or 'All' is enabled
    */
    public static Boolean excludeTrigger(String objectName) {
        Boolean isTriggerOff = false;

        if ( !string.isBlank(objectName)  ) {
            isTriggerOff = CFA_Configuration.findExcludeTrigger(objectName);
        }

        return  isTriggerOff;
    } // end of excludeTrigger

    /**
    * @description  is the validation object name is off
    *
    * @param objectName String , name of object/class name
    * @return true if enabled or 'All' is enabled
    */
    public static Boolean excludeValidation(String objectName) {
        Boolean isTriggerOff = false;

        if ( !string.isBlank(objectName)  ) {
            isTriggerOff = CFA_Configuration.findExcludeValidation(objectName);
        }

        return  isTriggerOff;
    } // end of excludeValidation

    /**
    * @description  is the power-builder object name is off
    *
    * @param objectName String , name of object/class name
    * @return true if enabled or 'All' is enabled
    */
    public static Boolean excludePB(String objectName) {
        Boolean isTriggerOff = false;

        if ( !string.isBlank(objectName)  ) {
            isTriggerOff = CFA_Configuration.findExcludePB(objectName);
        }

        return  isTriggerOff;
    } // end of excludePB
    /**
    * @description  handler included; it overrides the exclude.
    *
    * @param handlerName String , name of methods to include
    * @return Boolean false if not included, if true then execute it
    */
    public static Boolean includeHandlerName(String handlerName) {
        Boolean isHandlerOn = false;

        if ( !string.isBlank(handlerName)  ) {
            isHandlerOn = CFA_Configuration.findHandlerName(handlerName);
        }
        return isHandlerOn;
    }// end of includeHandlerName

    /**
    * @description is allow rollover accounts enabled
    *
    * @return true if enabled
    */
    public static Boolean allowRolloverDummyAccounts() {
        return CFA_Configuration.findRolloverDummyAccounts();
    } // end of allowRolloverDummyAccounts

    /**
    * @description is allow contacts to be private  (i.e. no account)
    *
    * @return true if enabled
    */
    public static Boolean allowPrivateContacts() {
        return CFA_Configuration.findAllowPrivateContacts();
    } // end of allowPrivateContacts

    ////////////////////////////////////////////////////////////////////////
    // Private Methods
    /////////////////////////////////////////////////////////////////////////


    /**
     * @description determine if te debug option is on
     *
     * @return true, if enabled
     */
    @TestVisible
    private static Boolean findDebug() {
        Boolean result = false;
        try {
            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();

            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if ( config != null ) {
                result = config.Debug__c;
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findDebug

    /**
     * @description get the notification email address
     *
     * @return notification email address
     */
    @TestVisible
    private static string getNotifyUserEmail() {
        String result = null;
        try {
            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();

            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if ( config != null ) {
                result = config.Notification__c;
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of getNotifyUserEmail

    /**
     * @description determine if the object name is off for trigger(excluded)
     *
     * @param objectName sobject name, i.e. 'Account', 'Contact', etc.
     * @return true, if turn off
     */
    @TestVisible
    private static Boolean findExcludeTrigger(String objectName) {
        Boolean result = false;
        try {
            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();
            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if ( config != null ) {
                result = findItemToExclude(config.Exclude_Triggers_By_Objects__c, objectName);
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findExcludeTrigger
    /**
     * @description determine if the object name is off for validation (excluded)
     *
     * @param objectName sobject name, i.e. 'Account', 'Contact', etc.
     * @return true, if turn off
     */
    @TestVisible
    private static Boolean findExcludeValidation(String objectName) {
        Boolean result = false;
        try {
            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();
            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if ( config != null ) {
                result = findItemToExclude(config.Exclude_Validation_by_Objects__c, objectName);
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findExcludeValidation
    /**
     * @description determine if the object name is off for process builder (excluded)
     *
     * @param objectName sobject name, i.e. 'Account', 'Contact', etc.
     * @return true, if turn off
     */
    @TestVisible
    private static Boolean findExcludePB(String objectName) {
        Boolean result = false;
        try {
            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();
            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if ( config != null ) {
                result = findItemToExclude(config.Exclude_PB_by_Objects__c, objectName);
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findExcludePB
    /**
     * @description determine if the object name is off  (excluded)
     *
     * @param objectName sobject name, i.e. 'Account', 'Contact', etc.
     * @return true, if turn off
     */
    @TestVisible
    private static Boolean findItemToExclude(String source, String objectName) {
        Boolean result = false;
        try {

            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if (  !string.isBlank(source) ) {
                for  (string item : source.split(SPACE_COMMA_s) ) {
                    if ( objectName.containsIgnoreCase(item) ) {
                        return true;
                    }
                }
            }

        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findItemToExclude

    /**
     * @description determine if the object name is off  (excluded)
     *
     * @param handlerName method name, i.e. 'AccountsDomain'
     * @return true, if turn on
     */
    @TestVisible
    private static Boolean findHandlerName( String handlerName) {
        Boolean result = false;
        try {

            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();

            // is there are result;
            if ( config != null && !string.isBlank(config.Include_Handler_Names__c) ) {
                String source = config.Include_Handler_Names__c;
                integer hnameLen = handlerName.length();
                for  (string item : source.split(SPACE_COMMA_s) ) {
                    if ( item.length() == hnameLen
                            && handlerName.containsIgnoreCase(item) ) {
                        return true;
                    }
                }
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findHandlerName

    /**
     * @description determine if user allows private contacts to be created
     *
     * @return true, if turn on
     */
    @TestVisible
    private static Boolean findAllowPrivateContacts( ) {
        Boolean result = false;
        try {
            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();

            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if ( config != null ) {
                result = config.Allow_Private_Contacts__c == null ? false : config.Allow_Private_Contacts__c;
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findAllowPrivateContacts
    //
    /**
     * @description determine if we should allow rollover dummy accounts
     *
     * @return true, if turn on
     */
    @TestVisible
    private static Boolean findRolloverDummyAccounts( ) {
        Boolean result = false;
        try {
            CFA_Configuration__c config = CFA_Configuration.findConfigurationSetting();

            // is there are result; if so, then check to see if this is an object we avoid the trigger method
            if ( config != null ) {
                result = config.Rollover_Dummy_Accounts__c == null ? false : config.Rollover_Dummy_Accounts__c;
            }
        } catch (Exception excp ) {
            cfa_ApexUtilities.log(excp);
        }
        return result;
    } // end of findRolloverDummyAccounts
    /**
     * @brief get the cfa global settings
     * @description get the cfa global settings
     * @return CFA_Configuration__c
     */
    // Cache the settings
    @TestVisible
    private static CFA_Configuration__c findConfigurationSetting() {

        if (CFA_Configuration.m_settings ==  null) {
            CFA_Configuration.m_settings = CFA_Configuration__c.getInstance(UserInfo.getUserId());
        }
        if ( Test.isRunningTest() ) {
            if ( CFA_Configuration.m_forUnitTesting) {
                throw new CfaException('Unit Testing Exception Handler');
            }
        }
        return CFA_Configuration.m_settings;
    } // end of findMigrationSetting

} // end of CFA_Configuration