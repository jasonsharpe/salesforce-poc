/* 
    # Created By: Dhana Prasad
    # Created Date: 06/08/2020
    # Description: Handler for ContentVersionTrigger, used for create content document link for case contact related case of DA requet and CBT DA request cases,

    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                        Associated Task (If)
    # 
    ---------------------------------------------------------------------------------------------------------------
*/
public with sharing class CFA_ContentVersionTriggerHandler 
{
     /* 
    # Created By: Dhana Prasad
    # Created Date:06/08/2020
    # Description: Method to create content document link for case contact related case of DA requet and CBT DA request cases,

    ---------------------------------------------------------------------------------------------------------------
    # Modification information
    - Modified By                 Modified Date               Description                        Associated Task (If)
    # 
    ---------------------------------------------------------------------------------------------------------------
*/
    public static void createContentDocumentLink(List<ContentVersion> newList){
        Set<Id> contentDocumentIdSet = new Set<Id>();
        List<Case> caseList = new List<Case>();
        List<Case> relatedCaseList = new List<Case>();
    
        for(ContentVersion cv:newList)
        {
            System.debug('cv'+cv);
            if(cv.ContentDocumentId != null && cv.VersionNumber == '1')
            {
                contentDocumentIdSet.add(cv.ContentDocumentId);
                System.debug('step 1');
            }
        }
        Map<Id,List<Id>> contentDocumentLinkedEntityMap = new Map<Id,List<Id>>();
        List<ContentDocumentLink> cdl = new List<ContentDocumentLink>();
        if(contentDocumentIdSet.size()!=0)
         cdl = new List<ContentDocumentLink>([SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN:contentDocumentIdSet]);
         if(!cdl.isEmpty())
        for(ContentDocumentLink eachCdl : cdl){
            if(!contentDocumentLinkedEntityMap.containsKey(eachCdl.LinkedEntityId)){
                contentDocumentLinkedEntityMap.put(eachCdl.LinkedEntityId, new List<Id>());
            }
            contentDocumentLinkedEntityMap.get(eachCdl.LinkedEntityId).add(eachCdl.ContentDocumentId);
        }
        System.debug('cdl'+ cdl);
        Id DAResourceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('DA Request').getRecordTypeId(); 
        Id CBTDAResourceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CBT DA Request').getRecordTypeId(); 
        if(contentDocumentLinkedEntityMap.size()!=0)
          caseList = new List<Case>([SELECT Id, ContactId, RecordTypeID FROM Case where Id =:contentDocumentLinkedEntityMap.keySet() AND (RecordTypeID =: DAResourceRecordTypeId OR  RecordTypeID =: CBTDAResourceRecordTypeId) ]);  
        
        System.debug('caseList'+caseList);
        if(!caseList.isEmpty()){
            System.debug('step 2');
            relatedCaseList =  new List<Case>([SELECT Id, ContactId FROM Case where ContactId =:caseList[0].ContactId ]);
        }

        List<ContentDocumentLink> newContentDocumentLinks = new  List<ContentDocumentLink>();
        if(!relatedCaseList.isEmpty()){
            System.debug('step 3');
            for(Case  relatedCase : relatedCaseList) {
                System.debug('step 4');
                if(relatedCase.Id != caseList[0].Id){
                    List<List<Id>> contentDocumentList= contentDocumentLinkedEntityMap.values();
                    for(Id contentDocumentId : contentDocumentList[0]){
                        ContentDocumentLink newCdl = new ContentDocumentLink();
                        newCdl.LinkedEntityId = relatedCase.Id;
                        newCdl.ContentDocumentId = contentDocumentId;
                        newCdl.shareType = 'V';
                        newContentDocumentLinks.add(newCdl);
                    }   
                }
            }
        }

        if(!newContentDocumentLinks.isEmpty()){
            System.debug('step 5');
            insert newContentDocumentLinks;
        }

    }
}