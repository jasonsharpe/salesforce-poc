/* 
        # Created By: Dhana Prasad
        # Created Date: 06/08/2020
        # Description: Test Class for CFA_ContentVersionTrigger class
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        #
        ---------------------------------------------------------------------------------------------------------------
    */
@isTest
public class CFA_ContentVersionTrigger_Test {
	  /* 
        # Created By: Dhana Prasad
        # Created Date: 06/08/2020
        # Description: Method to test createContentDocumentLink method  CFA_ContentVersionTrigger class
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        #
        ---------------------------------------------------------------------------------------------------------------
    */
    @isTest
    static void createContentDocumentLinkTest()
    {
        CaseTriggerHandler.TriggerDisabled = true;
        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.Name= 'trigger-details';
        societyOps.No_reply_email__c = 'noreply@cfainstitute.org';
        societyOps.CaseComment_TemplateName__c = 'SocietyOps-New Comment Notification';
        societyOps.CaseReassignment_TemplateName__c = 'SocietyOps-Case Reassignment';
        insert societyOps;
        List<Account> accList =  CFA_TestDataFactory.createAccountRecords('TestAccount', null , 1 ,true);
        List<Contact> conList = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestContact', 1, false);
        conList[0].AccountId = accList[0].Id;
        RetryForTest.upsertOnUnableToLockRow(conList);

        List<Case> caseList = CFA_TestDataFactory.createCaseRecords(Label.CFA_ADALabel,2,false);
        caseList[0].ContactId = conList[0].Id;
        caseList[1].ContactId = conList[0].Id;
        RetryForTest.upsertOnUnableToLockRow(caseList);

        Id testRecordType = caseList[0].RecordTypeId;
         String recordName = CFA_RecordTypeUtility.getRecordTypeName(Case.getSObjectType(), testRecordType, CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        RetryForTest.upsertOnUnableToLockRow(contentVersion);
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = caseList[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        List<ContentVersion> contVerList = [SELECT Id, ContentDocumentId, VersionNumber FROM ContentVersion WHERE Id =: contentVersion.Id];
        
        Test.startTest();
         CFA_ContentVersionTriggerHandler.createContentDocumentLink(contVerList);
         CFA_ContentVersionTriggerController newCtrl = new CFA_ContentVersionTriggerController();
        newCtrl.BeforeDelete(null);
        newCtrl.AfterDelete(null);
        newCtrl.AfterUndelete(null);
        Test.stopTest(); 
        List<ContentDocumentLink> cdList = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId =:  caseList[1].Id ];
        System.assertEquals(cdList.size()>0, true);
        
    }
    
}