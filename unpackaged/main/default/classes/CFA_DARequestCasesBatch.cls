/* 
    # Created By: Dhana Prasad
    # Created Date: 04/20/2020
    # Description: To Delete Cases which are DA Request record type and mark for legal retention is false and 7 years old cases

    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                        Associated Task (If)

    ---------------------------------------------------------------------------------------------------------------
*/

global class CFA_DARequestCasesBatch implements Database.Batchable<sObject>, Schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        String caseDeleteDays = System.Label.CFA_DACaseDelete; 
     Id DAResourceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('DA Request').getRecordTypeId();
        Id CBTResourceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CBT DA Request').getRecordTypeId();
        String query = 'SELECT Id, Mark_for_Legal_Retention__c FROM CASE WHERE (RecordTypeID =\''+ DAResourceRecordTypeId+'\' OR RecordTypeID =\''+CBTResourceRecordTypeId+'\')AND Mark_for_Legal_Retention__c = false AND CreatedDate < LAST_N_DAYS:'+caseDeleteDays+' ORDER BY CreatedDate DESC ';
       // system.debug('query'+query);
        return Database.getQueryLocator(query);
    }   
    
    global void execute(Database.BatchableContext context, List<sObject> sObjectList) {
      //  system.debug('sObjectList'+sObjectList);
        delete sObjectList;
    }

    global void finish(Database.BatchableContext context) {
       
    }
    global void execute(SchedulableContext SC) {
        Database.executebatch(new CFA_DARequestCasesBatch());
    }


}