/* 
    # Created By: Dhana Prasad
    # Created Date: 04/20/2020
    # Description: Test class for CFA_DARequestCasesBatch 
    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                        Associated Task (If)

    ---------------------------------------------------------------------------------------------------------------
*/

@isTest
public class CFA_DARequestCasesBatch_Test {
    /* 
    # Created By: Dhana Prasad
    # Created Date: 04/20/2020
    # Description: method to create test data
    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                        Associated Task (If)

    ---------------------------------------------------------------------------------------------------------------
*/
    @testSetup
    public static void createData(){
        CaseTriggerHandler.TriggerDisabled = true;
        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.Name= 'trigger-details';
        societyOps.No_reply_email__c = 'noreply@cfainstitute.org';
        societyOps.CaseComment_TemplateName__c = 'SocietyOps-New Comment Notification';
        societyOps.CaseReassignment_TemplateName__c = 'SocietyOps-Case Reassignment';
        insert societyOps;
        List<Account> accList =  CFA_TestDataFactory.createAccountRecords('TestAccount', null , 1 ,true);
        List<Contact> conList = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestContact', 1, false);
        conList[0].AccountId = accList[0].Id;
        insert conList;
        List<Case>  caseList = CFA_TestDataFactory.createCaseRecords(Label.CFA_ADALabel,1,false);
        caseList[0].ContactId = conList[0].Id;
        caseList[0].Mark_for_Legal_Retention__c = false;
        
        insert caseList;
        Id testRecordType = caseList[0].RecordTypeId;
        String recordName = CFA_RecordTypeUtility.getRecordTypeName(Case.getSObjectType(), testRecordType, CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
        Integer days =  Integer.valueOf( System.Label.CFA_DACaseDelete);
        days = (days+30)*-1;
        Datetime sevenYearsAgo = Datetime.now().addDays(days);
		Test.setCreatedDate(caseList[0].Id, sevenYearsAgo);
    }
     /* 
    # Created By: Dhana Prasad
    # Created Date: 04/20/2020
    # Description: method to test batch class for deleting case records
    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                        Associated Task (If)

    ---------------------------------------------------------------------------------------------------------------
*/
    @isTest
    static void testBatch()
    {
        CaseTriggerHandler.TriggerDisabled = true;
        List<Case> caseList = [SELECT Id, Mark_for_Legal_Retention__c ,createdDate FROM Case];
        System.debug('caseList'+caseList);
        Test.startTest();
        CFA_DARequestCasesBatch batchObj = new CFA_DARequestCasesBatch();
        DataBase.executeBatch(batchObj,1);
        Test.stopTest();
        List<case> caseList1 = new List<Case>();
        caseList1 =  [SELECT Id, Mark_for_Legal_Retention__c ,createdDate FROM Case];
        System.assertEquals(caseList1.size()==0, true);
    }
    
    @isTest static void testScheduler () {
        // CRON expression: midnight on March 15. Because this is a test, 
        // job is supposed to execute immediately after Test.stopTest()
        String cronExpr = '0 0 0 15 3 ? 2022';
        String jobRunningTime = '2022-03-15 00:00:00';
        Test.startTest();
        CFA_DARequestCasesBatch abs= new CFA_DARequestCasesBatch();
        String jobId = System.schedule('myJobTestJobName', cronExpr, abs);
        abs.execute(null);
        Test.stopTest();
    }


}