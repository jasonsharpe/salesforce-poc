/* 
    # Created By: Dhana Prasad
    # Created Date: 12/31/2019
    # Description: 
    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                     Associated Task (If)
    #
    ---------------------------------------------------------------------------------------------------------------
*/
public with sharing class CFA_DescribeSchemaUtility {
    
    public static Map<String, String> mapFieldLabelandAPIName = new Map<String, String>();
    public static Map<String, String> mapFieldAPINameandLabel = new Map<String, String>();

    public static String getSObjectAPIName(String pObjectType){
        List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{pObjectType});
        String objectAPIName = describeSobjectsResult[0].getName();
        return objectAPIName;
    }
}