public class CFA_IntegrationLogException extends Exception{

    //Method for logging the error.
     public static List<CFA_Integration_Log__c> logError(Exception ex,String busProcess,String dataSrc,DateTime loggedAt,
                                                          Boolean validJSON,String response, String req1,String req2,
                                                         Boolean FaliuresIncluded, String TransactionID){
                                                              
         List<CFA_Integration_Log__c> listlog = new List<CFA_Integration_Log__c> ();
         //String ProcessingStatus = 'Failed';
         CFA_Integration_Log__c intLog = new CFA_Integration_Log__c ();
             intLog.Business_Process__c = busProcess;
             intLog.Data_Source__c = dataSrc;
             if(ex!=null) {
             intLog.Exception_Type__c = ex.getTypeName();
             intLog.Exception__c = ex.getMessage()+ '; ' + ex.getStackTraceString();
             }
             intLog.Logged_at__c = loggedAt;
             intLog.Valid_JSON_Payload__c= validJSON;
             intLog.Response__c = response;
             intLog.Request_Body_1__c = req1;	
             intLog.Request_Body_2__c = req2;
             intLog.Failures_Included__c = FaliuresIncluded;
             intLog.Transaction_ID__c = TransactionID;
         listlog.add(intLog);             
         return listlog;
     }
}