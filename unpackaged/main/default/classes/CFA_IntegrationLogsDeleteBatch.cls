/*******************************************************************************
 * Description - Delete the records that are created 30 dyas ago in CFA_Integration_Log__c object
 * Author - Yaswanth   
 *******************************************************************************/

global class CFA_IntegrationLogsDeleteBatch implements Database.Batchable<sObject> {

// Start Method

    global Database.QueryLocator start(Database.BatchableContext BC) {
    //get all the log records that were created 30 days ago
        Date daysBeforeDate;
        if(DynamicEnvVariableService.genericdatafetcher('Num_DaysToDeleteLogs') != null) {
            Integer daysBefore = Integer.valueOf(DynamicEnvVariableService.genericdatafetcher('Num_DaysToDeleteLogs'));
            daysBeforeDate = System.today() - daysBefore;
        } else {
            daysBeforeDate = System.today() - 30;
        }
            return Database.getQueryLocator([SELECT Id,CreatedDate,Logged_at__c FROM CFA_Integration_Log__c WHERE Logged_at__c < :daysBeforeDate]);
    }

   global void execute(Database.BatchableContext BC, List<CFA_Integration_Log__c > logRecords) {
   
   //if we have logs that are created 30 days ago-perform delete
       if(logRecords.size()>0) {
           Database.delete(logRecords,false);
       }
    }   

    // Finish Method

    global void finish(Database.BatchableContext BC) {

    }

}