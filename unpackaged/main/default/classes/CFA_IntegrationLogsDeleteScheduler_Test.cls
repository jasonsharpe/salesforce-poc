/*********************************************************************
 * Description - Test class for CFA_IntegrationLogsDeleteScheduler
 * Author - Yaswanth
 *********************************************************************/ 
 
@istest
public class CFA_IntegrationLogsDeleteScheduler_Test{
    static testMethod void schedulerLogsDelete() {
        Test.StartTest();
            CFA_IntegrationLogsDeleteScheduler sh1 = new CFA_IntegrationLogsDeleteScheduler();      
            String sch = '0 0 0 * * ?';
            system.schedule('Test check', sch, sh1);
        Test.stopTest();
    }
}