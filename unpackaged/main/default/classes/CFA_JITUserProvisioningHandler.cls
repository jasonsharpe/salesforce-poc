//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class CFA_JITUserProvisioningHandler implements Auth.SamlJitHandler {
    @TestVisible 
    private static String partnerId = Label.CFA_Institute;

    
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        User u = new User();
        try {
            handleJit(true, u, samlSsoProviderId, communityId, portalId,
                    federationIdentifier, attributes, assertion);
        } catch(Exception e) {
            insert CFA_IntegrationLogException.logError(e, 'CFA_JITUserProvisioningHandler.CreateUser', 'Create User', DateTime.now(), true, '', '', '', true, '');
        }
        return u;
    }
    
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        User u = [SELECT Id, FirstName, ContactId FROM User WHERE Id=:userId AND IsActive = true];
        try {
            if(u != null) {
                handleJit(false, u, samlSsoProviderId, communityId, portalId,
                        federationIdentifier, attributes, assertion);
            }
        } catch(Exception e) {
            insert CFA_IntegrationLogException.logError(e, 'CFA_JITUserProvisioningHandler.updateUser', 'Update User', DateTime.now(), true, '', '', '', true, '');
        }
    }

    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        if(communityId != null || portalId != null) {
            String account = handleAccount();
            handleContact(account, u, attributes);
            handleUser(create, u, attributes, federationIdentifier, false, communityId, portalId);
        }
    }
    
    private void handleContact(String accountId, User u, Map<String, String> attributes) {
        Contact c;

        if(u.ContactId == NULL) {
            //need to locate a contact
            String partnerId;
            if(attributes.containsKey('User.Username')) {
                partnerId = attributes.get('User.Username').substringBefore('@');
            } else {
                insert CFA_IntegrationLogException.logError(null, 'CFA_JITUserProvisioningHandler.handleContact', 'get username', DateTime.now(), true, 'Partner ID not included in SAML Assertion', '', '', true, '');
            }
            
            List<Contact> lstContacts = new List<Contact>([SELECT Id,RecordType.Name, Email, FirstName, LastName FROM Contact
                                                           WHERE Partner_Id__c=:partnerId AND RecordType.Name = 'CFA Contact']);
            if(lstContacts.isEmpty()) {
                c = new Contact();
                c.AccountId = accountId;
            } else {
                c = lstContacts[0];
            }
        } else {
            c = new Contact(Id=u.ContactId); // existing user contact
        }
        
        // always update these fields
        c.LastUpdatedBy_JIT__c = system.now();
        
        if(attributes.containsKey('Contact.Email')) {
            c.Email = attributes.get('Contact.Email');
        }
        if(attributes.containsKey('Contact.Firstname')) {
            c.FirstName = attributes.get('Contact.Firstname');
        }
        if(attributes.containsKey('Contact.Lastname')) {
            c.LastName = attributes.get('Contact.Lastname');
        }
        if(attributes.containsKey('Contact.PartnerId')) {
            c.Partner_Id__c = attributes.get('Contact.PartnerId');
        }else if(attributes.containsKey('User.Username')) {
            c.Partner_Id__c = attributes.get('User.Username').substringBefore('@');
        }
        if(attributes.containsKey('Contact.PersonId')) {
            c.Person_Id__c = attributes.get('Contact.PersonId'); //Persion Id field
        }
        
        upsert c;

        if(u.ContactId == null) { // check user contact id
            u.ContactId = c.Id; // assign newly inserted contact id to new user
        }
    }
    
    private User handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard, Id communityId, Id portalid) {
        //get community we are trying to access
        String communityName = [SELECT Id, Name FROM Network WHERE Id = :communityId LIMIT 1].Name;
        System.debug('Community Name: ' + communityName);
        
        //always update these fields
        if(attributes.containsKey('User.Email')) {
            u.Email = attributes.get('User.Email');
        }
        if(attributes.containsKey('User.Firstname')) {
            u.FirstName = attributes.get('User.Firstname');
        }
        if(attributes.containsKey('User.Lastname')) {
            u.LastName = attributes.get('User.Lastname');
        }
        if(attributes.containsKey('User.Phone')) {
            u.Phone = attributes.get('User.Phone');
        }
        
        if (communityName == 'Self Service' || communityName == 'CFA Volunteer') {
            u.Provision_Self_Service__c = true;
            u.Provision_VM_Community__c = true;
        }           
        
        if(create) {
            if (attributes.containsKey('User.Username')) {
                u.Username = attributes.get('User.Username');
            }
            if (attributes.containsKey('User.FederationIdentifier')) {
                u.FederationIdentifier = attributes.get('User.FederationIdentifier');
            } else {
                u.FederationIdentifier = federationIdentifier;
            }
            
            if (attributes.containsKey('User.UserRoleId')) { //RoleName
                String userRole = attributes.get('User.UserRoleId');
                
                UserRole r = [SELECT Id FROM UserRole WHERE Id = :userRole];
                if(r != null) {
                    u.UserRoleId = r.Id;
                }
            }
            
            if (communityName == 'Self Service' || communityName == 'CFA Volunteer') {
                Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'CFA_Customer_Community'];
                if(p != null) {
                    u.ProfileId = p.Id;
                }
            } else {
                insert CFA_IntegrationLogException.logError(null, 'CFA_JITUserProvisioningHandler.handleUser', 'Profile not found', DateTime.now(), true, 'Partner ID not included in SAML Assertion', '', '', true, '');
            }
            
            if (attributes.containsKey('User.FederationIdentifier')) {
                u.FederationIdentifier = attributes.get('User.FederationIdentifier');
            } else {
                u.FederationIdentifier = federationIdentifier;
            }
            String alias = '';
            if (u.FirstName == null) {
                alias = u.LastName;
            } else {
                alias = u.FirstName.charAt(0) + u.LastName;
            }
            if (alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
            
            //set fields if creating
            u.Username = attributes.get('User.Username');
            u.ReceivesAdminInfoEmails = false;
            u.ReceivesInfoEmails = false;
            u.IsActive = true;
            
            User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id = :UserInfo.getUserId() AND IsActive = true];
            if(currentUser != null) {
                u.LocaleSidKey = currentUser.LocaleSidKey;
                u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
                u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
                u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
                u.EmailEncodingKey = currentUser.EmailEncodingKey;
            }
        }
        
        if(!create) {
            update(u);
        }
        
        return u;
    }
    
    
    private String handleAccount() {
        return [SELECT Id FROM Account WHERE Partner_Id__c =: partnerId].Id;
    }
}