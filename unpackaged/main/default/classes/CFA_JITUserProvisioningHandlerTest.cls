@IsTest
private class CFA_JITUserProvisioningHandlerTest {
    
    @testSetup 
    private static void makeData() {    
    }
    
    private static void buildAttribs(Map<String, String> attributes, Boolean isNewUser, Boolean isNewContact, User tu, Contact tc) {
        //locate a account
        Account a = [SELECT Id, Name FROM Account WHERE Name = 'CFA Institute' LIMIT 1];
        attributes.put('Account.Name', 'CFA Institute');
        attributes.put('User.AccountId', a.Id);
        attributes.put('Account.AccountNumber', 'testAccNumber');
        
        //locate a profile
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'CFA_Customer_Community' LIMIT 1];
        attributes.put('User.ProfileId', p.Id);   
        UserRole ur = [SELECT Id, Name FROM UserRole WHERE PortalType = 'None' LIMIT 1];
        attributes.put('User.UserRoleId', ur.Id);
        
        if (isNewUser) {
            if (isNewContact) {
                //new user new contact
                
                attributes.put('User.Email', 'totallyfakeemail@cfainstitute.org');
                attributes.put('User.Lastname', 'emailname');
                attributes.put('User.Firstname', 'totallyfake');
                attributes.put('User.Username', '0987654321@cfainstitute.org');
                attributes.put('User.Phone', '0987654321');
                attributes.put('Contact.Email', 'totallyfakeemail@cfainstitute.org');
                attributes.put('Contact.Firstname', 'totallyfake');
                attributes.put('Contact.Lastname', 'emailname');
                attributes.put('Contact.PartnerId', 'abc12');                
                attributes.put('Contact.PersonId', 'abc13');
                attributes.put('Account.Name', a.Name);                
                attributes.put('User.ProfileId', tu.ProfileId);
                
                tu.FederationIdentifier = '0987654321@cfainstitute.org';
            } else {
                // new user existing contact
                //need to create a fake contact to consume
                Contact c = new Contact();
                c.Email = 'afdsdfads@cfainstitute.org';
                c.Firstname = 'asdfsdfa';
                c.Lastname = 'adsfasdfafdsafdaf';
                c.AccountId = a.Id;
                insert(c);
                
                attributes.put('User.Email', c.Email);
                attributes.put('User.Lastname', 'email');
                attributes.put('User.Firstname', 'totallyfake');
                attributes.put('User.Username', '0987654321@cfainstitute.org');
                attributes.put('User.FederationIdentifier', '0987654321@cfainstitute.org');
                attributes.put('Contact.Email', c.Email);
                attributes.put('Contact.Firstname', 'totallyfake');
                attributes.put('Contact.Lastname', 'email');
                attributes.put('Account.Name', a.Name);
                attributes.put('User.Account', a.Id);
                attributes.put('User.Contact', c.Id);
                attributes.put('User.ProfileId', tu.ProfileId);
                attributes.put('User.Phone', '0987654321');
                
                tc.Id = c.Id;
                tu.FederationIdentifier = '0987654321@cfainstitute.org';
            }
        } else {
            if (isNewContact) {
                // existing user new contact
                User u = [SELECT Id, FederationIdentifier, ProfileId,Username, Firstname, Lastname, Email, PagesApi__Is_Federated_User__c FROM User WHERE ContactId = null LIMIT 1];
                
                attributes.put('User.Email', 'totallyfakeemail@cfainstitute.org');
                attributes.put('User.Lastname', 'email');
                attributes.put('User.Firstname', 'totallyfake');
                attributes.put('User.Username', u.Username);
                attributes.put('User.FederationIdentifier', u.FederationIdentifier);
                attributes.put('Contact.Email', 'totallyfakeemail@cfainstitute.org');
                attributes.put('Contact.Firstname', 'totallyfake');
                attributes.put('Contact.Lastname', 'email');
                attributes.put('Account.Name', a.Name);
                attributes.put('User.ProfileId', p.Id);
                attributes.put('User.Phone', '0987654321');
                
                tu.Id = u.Id;
                tu.FederationIdentifier = u.FederationIdentifier;
            } else {
                // existing user existing contact
                User u = [SELECT Id, Username, FederationIdentifier, ContactId, ProfileId , Firstname, Lastname, Email FROM User WHERE PagesApi__Is_Federated_User__c = true AND FederationIdentifier LIKE '%@cfainstitute.org%'
                          AND contactid != null LIMIT 1];
                
                Contact c = [SELECT Id, FirstName, LastName, Email,Partner_Id__c, AccountId FROM Contact WHERE Id =: tc.Id];
                
                attributes.put('User.Email', u.Email);
                attributes.put('User.Lastname', 'email');
                attributes.put('User.Firstname', 'totallyfake');
                attributes.put('User.Username', u.Username);
                attributes.put('User.FederationIdentifier', u.FederationIdentifier);
                attributes.put('Contact.Email', c.Email);
                attributes.put('Contact.Firstname', c.FirstName);
                attributes.put('Contact.Lastname', c.LastName);
                attributes.put('Contact.PartnerId', c.Partner_Id__c);
                attributes.put('Contact.PersonId', 'abc12');
                attributes.put('Account.Name', a.Name);
                attributes.put('Account.AccountNumber', 'testAccNumber');
                attributes.put('User.ProfileId', u.ProfileId);
                attributes.put('User.Account', a.Id);
                attributes.put('User.Contact', c.Id);
                attributes.put('User.Phone', '0987654321');
              
                tu.Id = u.Id;
                tu.FederationIdentifier = u.FederationIdentifier;
                tc.Id = c.Id;
                tc.Partner_Id__c = c.Partner_Id__c;
            }
        }
    }
    
    @IsTest
    private static void testUpdateUserUpdateContact() {
        //just get any id
        Id rid = UserInfo.getUserId();
        User u = new User();
        Account acc = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', false);
        RetryForTest.upsertOnUnableToLockRow(acc);
        acc = [ SELECT Id, Partner_Id__c FROM Account WHERE Id = :acc.Id LIMIT 1 ];
        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
        
        Contact c = new Contact(
            LastName = 'testCon',
            FirstName = 'testFirst',
            AccountId = acc.Id, 
            RecordTypeId = devRecordTypeId,
            Partner_Id__c = acc.Partner_Id__c
        );
        RetryForTest.upsertOnUnableToLockRow(c);
        
        User user = CFA_TestDataFactory.createPartnerUser(c,'CFA_Customer_Community');

        Map<String, String> attributes = new Map<String, String> ();
        List<Network> testnetworkList = [SELECT Id, Name FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        // get community info from mdt
        Map<String, String> communityIDmaps = new Map<String, String> ();
        for (Network community: testnetworkList) {
            if (community.Name == 'Self Service') {
                communityIDmaps.put('SScommunityId', community.Id);
            }
        }
        //use fake samlSsoProviderId
        Id samlSsoProviderId = '0DB190000008QvcGAE';
        
        buildAttribs(attributes, false, false, user, c);
        
        CFA_JITUserProvisioningHandler sut = new CFA_JITUserProvisioningHandler();
        CFA_JITUserProvisioningHandler.partnerId = acc.Partner_Id__c;
        attributes.remove('Contact.PartnerId');
        attributes.remove('Contact.PersonId');
        attributes.remove('Contact.Username');

        Test.startTest();
            sut.updateUser(user.Id, samlSsoProviderId, communityIDmaps.get('SScommunityId'), networkId, user.FederationIdentifier, attributes, '');
        Test.stopTest();
        
        user = [SELECT Id,Name,Email,ContactId FROM User WHERE Id =: user.Id LIMIT 1];
        System.assertEquals(user.Email, attributes.get('User.Email'));
    }
    
    @IsTest
    private static Void testNewUserNewContactNullId() {
        //just get any id
        Id rid = UserInfo.getUserId();
        Id communityProfId = [SELECT Id FROM Profile WHERE Name = 'CFA_Customer_Community'].Id;
        
        Account acc = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        acc = [ SELECT Partner_Id__c FROM Account WHERE Id = :acc.Id LIMIT 1 ];
        
        Contact c = new Contact();        
        User u = new User();
        u.ProfileId = communityProfId;
        
        Map<String, String> attributes = new Map<String, String> ();     
        buildAttribs(attributes, true, true, u, c);
        
        Test.startTest();
            CFA_JITUserProvisioningHandler sut = new CFA_JITUserProvisioningHandler();            
            sut.createUser(rid, null, null, u.FederationIdentifier, attributes, '');
        Test.stopTest();

        System.assertEquals([SELECT Id,Name FROM User WHERE FederationIdentifier =: u.FederationIdentifier].size(), 0);
    }
    
    @IsTest
    private static Void testNewUserNewContact() {
        //just get any id
        Id rid = UserInfo.getUserId();
        Id communityProfId = [SELECT Id FROM Profile WHERE Name = 'CFA_Customer_Community'].Id;
        
        Account acc = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        acc = [ SELECT Partner_Id__c FROM Account WHERE Id = :acc.Id LIMIT 1 ];
        
        Contact c = new Contact();
        c.Partner_Id__c = 'abc12';
        
        User u = new User();
        u.ProfileId = communityProfId;
        
        Map<String, String> attributes = new Map<String, String>();
        List<Network> testnetworkList = [SELECT Id, Name FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        // get community info from mdt
        Map<String, String> communityIDmaps = new Map<String, String> ();
        for (Network community: testnetworkList) {
            if (community.Name == 'Self Service') {
                communityIDmaps.put('SScommunityId', community.Id);
                
            }
        }
        //use fake samlSsoProviderId
        Id samlSsoProviderId = '0DB190000008QvcGAE';
        
        
        buildAttribs(attributes, true, true, u, c);
        
        CFA_JITUserProvisioningHandler sut = new CFA_JITUserProvisioningHandler();
        
        Test.startTest();
            CFA_JITUserProvisioningHandler.partnerId = acc.Partner_Id__c;
            sut.createUser(samlSsoProviderId, communityIDmaps.get('SScommunityId'), networkId, u.FederationIdentifier, attributes, '');
        Test.stopTest();
        
        System.assertEquals([SELECT Id,Name FROM User WHERE FederationIdentifier =: u.FederationIdentifier].size(), 0);
    }
    
    @IsTest
    private static Void testNewUserNewContact_NoFName() {
        //just get any id
        Id rid = UserInfo.getUserId();
        
        User u = new User();
        Contact c = new Contact();
        Account acc = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        acc = [ SELECT Partner_Id__c FROM Account WHERE Id = :acc.Id LIMIT 1 ];
        
        Map<String, String> attributes = new Map<String, String> ();
        List<Network> testnetworkList = [SELECT Id, Name FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        // get community info from mdt
        Map<String, String> communityIDmaps = new Map<String, String> ();
        for (Network community: testnetworkList) {
            if (community.Name == 'Self Service') {
                communityIDmaps.put('SScommunityId', community.Id);
                
            }
            if (community.Name == 'CFA Volunteer') {
                communityIDmaps.put('CFA_Volunteer_Id', community.Id);
            }
            
        }
        //use fake samlSsoProviderId
        Id samlSsoProviderId = '0DB190000008QvcGAE';
        
        buildAttribs(attributes, true, true, u, c);
        
        attributes.put('User.Firstname', null);
        attributes.remove('Contact.PartnerId');
        attributes.remove('Contact.PersonId');
        attributes.remove('Contact.Username');
        CFA_JITUserProvisioningHandler sut = new CFA_JITUserProvisioningHandler();
        CFA_JITUserProvisioningHandler.partnerId = acc.Partner_Id__c;

        Test.startTest();
            sut.createUser(samlSsoProviderId, communityIDmaps.get('SScommunityId'), networkId, u.FederationIdentifier, attributes, '');
            sut.createUser(samlSsoProviderId, communityIDmaps.get('CFA_Volunteer_Id'), networkId, u.FederationIdentifier, attributes, '');
        Test.stopTest();
        
        System.assertEquals([SELECT Id FROM Contact WHERE Email =: attributes.get('Contact.Email')].size(), 1);
    } 
    
    @IsTest
    private static Void testNewUserExistingContact() {
        //just get any id
        Id rid = UserInfo.getUserId();
        User u = new User();
        Account acc = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        acc = [ SELECT Partner_Id__c FROM Account WHERE Id = :acc.Id LIMIT 1 ];
        
        Id p = [ SELECT Id FROM Profile WHERE Name = 'CFA_Customer_Community' ].Id;
        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
        Contact c = new Contact(LastName = 'testCon', AccountId = acc.Id, Partner_Id__c='abc12', recordtypeid = devRecordTypeId);
        insert c;
        
        User user = CFA_TestDataFactory.createPartnerUsers(new List<Contact>{c},'Society Partner User Admin',1)[0];
        
        Id p1 = [select id from profile where name = 'Integration Profile'].id;
        User user1 = new User(alias = 'tsdf23', email = 'test3123@noemail.com',
                              emailencodingkey = 'UTF-8', lastname = 'jhsafdasjfhu4', languagelocalekey = 'en_US',
                              localesidkey = 'en_US', profileid = p1, country = 'United States', IsActive = true, FederationIdentifier = 'total4welyfakeemail@cfainstitute.org',
                              timezonesidkey = 'America/Los_Angeles', username = 'te83532hd83ster@noemail.com');
        insert user1;
        Map<String, String> attributes = new Map<String, String> ();
        List<Network> testnetworkList = [SELECT Id, Name FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        // get community info from mdt
        Map<String, String> communityIDmaps = new Map<String, String> ();
        for (Network community: testnetworkList) {
            if (community.Name == 'Self Service') {
                communityIDmaps.put('SScommunityId', community.Id);
            }
        }
        //use fake samlSsoProviderId
        Id samlSsoProviderId = '0DB190000008QvcGAE'; 
        buildAttribs(attributes, true, false, user, c);     
        CFA_JITUserProvisioningHandler sut = new CFA_JITUserProvisioningHandler();
        CFA_JITUserProvisioningHandler.partnerId = acc.Partner_Id__c;

        Test.startTest();
            sut.createUser(samlSsoProviderId, communityIDmaps.get('SScommunityId'), networkId, user.FederationIdentifier, attributes, '');   
        Test.stopTest();

        System.assertEquals([SELECT Id,Email FROM Contact WHERE Id =: c.Id].Email, attributes.get('Contact.Email'));
    }
    
    @IsTest
    private static void handleUserTest() {
        Id communityProfId = [SELECT Id, Name FROM Profile WHERE Name = 'CFA_Customer_Community' LIMIT 1].Id;
        Id devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
        Account acc = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        acc = [ SELECT Partner_Id__c FROM Account WHERE Id = :acc.Id LIMIT 1 ];
        Contact c = new Contact(LastName = 'testConTest', AccountId = acc.Id, Partner_Id__c = acc.Partner_Id__c, recordtypeid = devRecordTypeId);
        insert c;
        
        User u2 = new User();
        u2.ProfileId = communityProfId;
        u2.ContactId = c.Id;
        
        Map<String, String> attributes = new Map<String, String> ();
        List<Network> testnetworkList = [SELECT Id, Name FROM Network];
        Id networkId = testnetworkList[0].Id;
        
        // get community info from mdt
        Map<String, String> communityIDmaps = new Map<String, String> ();
        for (Network community: testnetworkList) {
            if (community.Name == 'Self Service') {
                communityIDmaps.put('SScommunityId', community.Id);
            }
        }
        Id samlSsoProviderId = '0DB190000008QvcGAE';
        
        buildAttribs(attributes, true, true, u2, c);
        
        CFA_JITUserProvisioningHandler ctr = new CFA_JITUserProvisioningHandler();
        CFA_JITUserProvisioningHandler.partnerId = acc.Partner_Id__c;
        
        Test.startTest();
            ctr.createUser(samlSsoProviderId, communityIDmaps.get('SScommunityId'), networkId, u2.FederationIdentifier, attributes, '');
        Test.stopTest();
        
        System.assertEquals([SELECT Id FROM User WHERE FederationIdentifier =: u2.FederationIdentifier].size(), 0);
    }
}