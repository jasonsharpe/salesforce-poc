/* 
    # Created By: Dhana Prasad
    # Created Date: 12/31/2019
    # Description: 

    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                     Associated Task (If)
    #
    ---------------------------------------------------------------------------------------------------------------
*/
public class CFA_RecordTypeUtility {
    private static Map<String, Map<String, Schema.RecordTypeInfo>> mapRecordTypeByName = new Map<String, Map<String, Schema.RecordTypeInfo>>();
    private static Map<String, Map<Id, Schema.RecordTypeInfo>> mapRecordTypeById = new Map<String, Map<Id, Schema.RecordTypeInfo>>();

    // Create Map Data with Record Type Id or Record Type Name for specific object.
    private static void initialize(Schema.SObjectType sObjType, String sObjectAsString) {
        if(!mapRecordTypeById.containsKey(sObjectAsString) || !mapRecordTypeByName.containsKey(sObjectAsString)) {
            Schema.DescribeSObjectResult sObjResult = sObjType.getDescribe();
            mapRecordTypeById.put(sObjectAsString, sObjResult.getRecordTypeInfosById());
            mapRecordTypeByName.put(sObjectAsString, sObjResult.getRecordTypeInfosByName());
        }
    }

    // Get Record Type Id
    public static ID getRecordTypeId(Schema.SObjectType sObjType, String sRecordTypeName, String sObjectAsString) {
        initialize(sObjType, sObjectAsString);
        if(mapRecordTypeByName.get(sObjectAsString).containsKey(sRecordTypeName)) {
            Schema.RecordTypeInfo recordTypeInfo = mapRecordTypeByName.get(sObjectAsString).get(sRecordTypeName);
            return recordTypeInfo.getRecordTypeId();
        }
        return null;
    }

    // Get Record Type Name
    public static String getRecordTypeName(Schema.SObjectType sObjType, Id sRecordTypeId, String sObjectAsString) {
        initialize(sObjType, sObjectAsString);
        if(mapRecordTypeById.get(sObjectAsString).containsKey(sRecordTypeId)) {
            Schema.RecordTypeInfo recordTypeInfo = mapRecordTypeById.get(sObjectAsString).get(sRecordTypeId);
            return recordTypeInfo.getName();
        }
        return null;
    }

}