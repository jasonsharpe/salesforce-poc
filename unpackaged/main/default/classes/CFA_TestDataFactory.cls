/* 
    # Created By: Dhana Prasad
    # Created Date: 12/30/2019
    # Description: Contains methods to create test data used for test classes

    ---------------------------------------------------------------------------------------------------------------
    # Modification inforamtion
    - Modified By                 Modified Date               Description                     Associated Task (If)
    Alona Zubenko                 16.11.2020                  add methods for creation Badge  task - 63227
    #
    ---------------------------------------------------------------------------------------------------------------
*/

public class CFA_TestDataFactory {
    private static String END_FOR_WRAPPER_CLASS = 'Wrapper';
	public static final String DefaultAccountName = 'TestAccount';
	public static final String DefaultContactName = 'TestContact';

	public static final String CFA_Contact_RT_Name = 'CFA Contact';
    /**
     * @description create sObject
     * Contact con = (Contact) TestDataWrappers.create(new Contact().getSObjectType(),'CFA Contact', null,false);
     * Account acc = (Account) TestDataWrappers.create(new Account().getSObjectType(),null,new Map<String,Object>{'Name' => 'TestName'},false);
     *
     * @param sObjType SObjectType
     * @param recordTypeDeveloperName String
     * @param fieldsWithValues Map<String,Object>
     * @param doInsert Boolean
     *
     * @return SObject
     */
    public static SObject create(SObjectType sObjType, String recordTypeDeveloperName, Map<String, Object> fieldsWithValues, Boolean doInsert) {
        return createList(sObjType, recordTypeDeveloperName, fieldsWithValues, 1, doInsert)[0];
    }

    /**
     * @description Create List of sObjects
     * List<Contact> contactList = (List<Contact>) TestDataWrappers.createList(new Contact().getSObjectType(),'CFA Contact', null,50,false);
     * List<Account> accList = (List<Account>) TestDataWrappers.createList(new Account().getSObjectType(),null,null,50,false);
     *
     * @param sObjType SObjectType
     * @param recordTypeDeveloperName String
     * @param fieldsWithValues Map<String,Object>
     * @param numberOfRecords Integer
     * @param doInsert Boolean
     *
     * @return List<SObject>
     */
    public static List<SObject> createList(SObjectType sObjType, String recordTypeDeveloperName, Map<String, Object> fieldsWithValues, Integer numberOfRecords, Boolean doInsert) {
        String className = 'TestDataWrappers.' + sObjType;

        if (className.endsWith('__c')) {
            className = className.substring(0, className.length() - 3);
        }
        className += END_FOR_WRAPPER_CLASS;
        className = className.replaceAll('_', '');

        Type t = Type.forName(className);
        if (t == null) {
            throw new TestDataWrappers.TestDataFactoryException('This object does not have wrapper in TestDataWrappers. Please add ' + className + '. Thank you!');
        }
        TestDataWrappers.SObjectWrapper newObj = (TestDataWrappers.SObjectWrapper) t.newInstance();

        List<SObject> objectList = newObj.createList(recordTypeDeveloperName, fieldsWithValues, numberOfRecords);
        if (doInsert) {
            RetryForTest.upsertOnUnableToLockRow(objectList);
        }

        return objectList;
    }

    /**
     * @description Create Account Records
     *
     * @param accountName String
     * @param recordTypeName String
     * @param count Integer
     * @param isInsert Boolean
     *
     * @return List<Account>
     */
    public static List<Account> createAccountRecords(String accountName, String recordTypeName, Map<String, Object> fieldsWithValues,Integer count, Boolean isInsert) {
        if (fieldsWithValues.isEmpty()) {
            fieldsWithValues = new Map<String, Object>{'Name' => accountName};
        } else if (!fieldsWithValues.containsKey('Name')) {
            fieldsWithValues.put('Name', accountName);
        }
        List<Account> accountList = (List<Account>) CFA_TestDataFactory.createList(new Account().getSObjectType(), recordTypeName, fieldsWithValues, count, isInsert);
        return accountList;
    }

    /**
     * @description Create Account Records
     *
     * @param accountName String
     * @param recordTypeName String
     * @param count Integer
     * @param isInsert Boolean
     *
     * @return List<Account>
     */
    public static List<Account> createAccountRecords(String accountName, String recordTypeName, Integer count, Boolean isInsert) {
        List<Account> accountList = (List<Account>) CFA_TestDataFactory.createList(new Account().getSObjectType(), recordTypeName, new Map<String, Object>{'Name' => accountName}, count, isInsert);
        return accountList;
    }

    /**
     * @param accountName  String
     * @param recordTypeName String
     * @param isInsert  Boolean
     *
     * @return Account
     */
    public static Account createAccount(String accountName, String recordTypeName, Boolean isInsert) {
        Account accountRecord = (Account) CFA_TestDataFactory.create(new Account().getSObjectType(), recordTypeName, null, false);
        accountRecord.Name = accountName;

        if (isInsert) {
            RetryForTest.upsertOnUnableToLockRow(accountRecord);
        }
        return accountRecord;
    }

    /**
    * @description 
    * @param accId Id
    * @param conId Id
    * @param role String
    * @param startDate Date
    * @param endDate Date
    * @param type String
	* @param isInsert Boolean
    * @return 
    */ 
    public static Affiliation__c createAffiliations(Id accId, Id conId, String role, Date startDate, Date endDate, String type, Boolean isInsert){
        
        Affiliation__c objAff = new Affiliation__c(Account__c = accId, Contact__c = conId, Role__c = role, Start_Date__c = startDate, End_Date__c = endDate, Type__c = type);
		if (isInsert) {
			insert objAff;
		}
        return objAff;
    }

    /**
     * @param name  with type String - last name
     * @param recordType with type String
     * @param isInsert with type Boolean
     *
     * @return Lead
     */
    public static Lead createLead(String name, String recordType, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>();
        Lead obj = (Lead) CFA_TestDataFactory.create(new Lead().getSObjectType(), recordType, fieldsWithValue, false);
        obj.LastName = 'Tester';
		obj.company = 'Testers';
		obj.LastName = name;
		obj.Phone = '5555555';
        if (isInsert) {
            insert obj;
        }
        return obj;
    }

    /**
     * @param recordType String
     * @param name String
     * @param count Integer
     * @param isInsert Boolean
     *
     * @return List<Contact>
     */
    public static List<Contact> createContactRecords(String recordType, String name, Integer count, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{'LastName' => name};
        List<Contact> contacts = (List<Contact>) CFA_TestDataFactory.createList(new Contact().getSObjectType(), recordType, fieldsWithValue, count, isInsert);

        return contacts;
    }

    /**
     * @param name  with type String - last name
     * @param accId  with type Id
     * @param recordType with type String
     * @param isInsert with type Boolean
     *
     * @return Contact
     */
    public static Contact createContact(String name, Id accId, String recordType, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{'AccountId' => accId};
        Contact obj = (Contact) CFA_TestDataFactory.create(new Contact().getSObjectType(), recordType, fieldsWithValue, false);
        obj.LastName = name;
		obj.GDPR_Consent__c = true;
		obj.Phone = '5555555';
		obj.Contact_Data_Source__c = 'Business Card';
        if (isInsert) {
            RetryForTest.upsertOnUnableToLockRow(obj);
        }
        return obj;
    }

    /**
     * @param name  with type String - last name
     * @param accId  with type Id
     * @param isInsert with type Boolean
     *
     * @return Contact
     */
    public static Contact createCFAContact(String name, Id accId, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{'AccountId' => accId};
        Contact obj = (Contact) CFA_TestDataFactory.create(new Contact().getSObjectType(), CFA_TestDataFactory.CFA_Contact_RT_Name, fieldsWithValue, false);
        obj.LastName = name;
		obj.GDPR_Consent__c = true;
		obj.Phone = '5555555';
		obj.Contact_Data_Source__c = 'Business Card';
        if (isInsert) {
            RetryForTest.upsertOnUnableToLockRow(obj);
        }
        return obj;
    }

    /**
     * @description Create Case Records
     *
     * @param recordType String
     * @param count Integer
     * @param isInsert Boolean
     *
     * @return List<Case>
     */
    public static List<Case> createCaseRecords(String recordType, Integer count, Boolean isInsert) {
        return (List<Case>) CFA_TestDataFactory.createList(new Case().getSObjectType(), recordType, null, count, isInsert);
    }

    /**
     * @description Create Case Record
     *
     * @param recordType String
     * @param isInsert Boolean
     *
     * @return List<Case>
     */
    public static Case createCaseRecord(String recordType, Boolean isInsert) {
        Map<String,Object> fieldsWithValue = new Map<String,Object> {
            'Status'=>'New',
            'Subject'=>'Test Case'
        };
        return (Case) CFA_TestDataFactory.create(new Case().getSObjectType(), recordType, fieldsWithValue, isInsert);
    }

    /**
     * @description Create ContentVersion Record
     *
     * @param recordType String
     * @param isInsert Boolean
     *
     * @return List<Case>
     */
    public static ContentVersion createFirstContentVersionRecord(Id locationId, Boolean isInsert) {
        Map<String,Object> fieldsWithValue = new Map<String,Object> {
            'FirstPublishLocationId' => locationId,
            'Title' => 'First Content Title',
            'PathOnClient' => 'FirstContent.pdf',
            'VersionData' =>  Blob.valueOf('First Content'),
            'IsMajorVersion' => true,
            'SharingPrivacy' => 'N'
        };
        return (ContentVersion) CFA_TestDataFactory.create(new ContentVersion().getSObjectType(), null, fieldsWithValue, isInsert);
    }

    /**
     * @description Create emailMessage Record
     *
     * @param recordType String
     * @param isInsert Boolean
     *
     * @return List<Case>
     */
    public static EmailMessage createEmailMessageRecord(Id RelatedToId, Boolean isInsert) {
        Map<String,Object> fieldsWithValue = new Map<String,Object> {
            'RelatedToId' => RelatedToId,
            'FromAddress'=>'cfa-sender@cfai.com',
            'ToAddress'=>'cfa-toReceiver@cfai.com',
            'FromName'=>'CFAI Sender',
            'Subject'=>'Hello',
            'HtmlBody'=>'<h1>ABC</h1>'
        };
        return (EmailMessage) CFA_TestDataFactory.create(new EmailMessage().getSObjectType(), null, fieldsWithValue, isInsert);
    }

    /**
     * @description Create Group Record
     *
     * @param groupName String
     * @param isInsert Boolean
     *
     * @return Group
     */
    public static Group createGroup(String groupName, Boolean isInsert){
        Group newGroup = new Group(Name = groupName);
		if (isInsert) {
			insert newGroup;
		}
		return newGroup;
    }

    /**
    * @description 
    * @param groupName String
    * @param groupId 
    * @param count 
    * @param isInsert 
    * @return List<OrderApi__Business_Group__c> 
    */ 
    public static List<OrderApi__Business_Group__c> createBusinessGroups (String groupName, Id groupId, Integer count, Boolean isInsert) {
        
        List<OrderApi__Business_Group__c> lstBusinessGroup = new List<OrderApi__Business_Group__c>();
        for(Integer i=0; i < count; i++){
			lstBusinessGroup.add(
				 new OrderApi__Business_Group__c(
					CFAMN__SystemPublicGroupId__c = groupId,
					Name = groupName + ' - ' + i
				)
			);
        }
		if (isInsert) {
			insert lstBusinessGroup;
		}
        return lstBusinessGroup;
    }

    /**
     * @description Create Group Record
     *
     * @param groupName String
	 * @param groupId Id
     * @param isInsert Boolean
     *
     * @return Group
     */
    public static OrderApi__Business_Group__c createBusinessGroup(String groupName, Id groupId, Boolean isInsert) {
        OrderApi__Business_Group__c newBusinessGroup = new OrderApi__Business_Group__c(
			Name = groupName,
			CFAMN__SystemPublicGroupId__c = groupId
		);
		if (isInsert) {
			insert newBusinessGroup;
		}
		return newBusinessGroup;
    }

    /**
     * @param recordType  with type String
     * @param relatedId  with type Id
     * @param userId  with type Id
     * @param count  with type Integer
     * @param isInsert  with type Boolean
     *
     * @return List<Task>
     */
    public static List<Task> createTaskList(String recordType, Id relatedId, Id userId, Integer count, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'OwnerId' => userId, 'WhatId' => relatedId
        };
        return (List<Task>) CFA_TestDataFactory.createList(new Task().getSObjectType(), recordType, fieldsWithValue, count, isInsert);
    }

    /**
     * @param contactIds List<Id>
     * @param accId with type Id
     * @param transactionType with type String
     * @param isInsert with type Boolean
     *
     * @return List<Membership__c>
     */
    public static List<Membership__c> createMemberships(Set<Id> contactIds, Id accId, String transactionType, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'Account_lookup__c' => accId, 'Transaction_Type__c' => transactionType
        };
        List<Membership__c> memberships = new List<Membership__c>();
        for (Id conId : contactIds) {
            fieldsWithValue.put('Contact_lookup__c', conId);
            Membership__c obj = (Membership__c) CFA_TestDataFactory.create(new Membership__c().getSObjectType(), null, fieldsWithValue, false);
            memberships.add(obj);
        }
        if (isInsert) {
            insert memberships;
        }
        return memberships;
    }

    /**
     * @param name with type String - Campaign name
     * @param isInsert with type Boolean
     *
     * @return Campaign
     */
    public static Campaign createCampaign(String name, Boolean isInsert) {
        return (Campaign) CFA_TestDataFactory.create(new Campaign().getSObjectType(), null, new Map<String, Object>{
                'Name' => name
        }, isInsert);
    }

    /**
     * @param campId with type Id - Campaign Id
     * @param conId with type Id - Contact Id
     * @param ldId with type Id - Lead Id
     * @param isInsert with type Boolean
     *
     * @return CampaignMember
     */
    public static CampaignMember createCampaignMember(Id campId, Id conId, Id ldId, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'CampaignId' => campId
                , 'ContactId' => conId
                , 'LeadId' => ldId
        };
        return (CampaignMember) CFA_TestDataFactory.create(new CampaignMember().getSObjectType(), null, fieldsWithValue, isInsert);
    }
    /**
     * @param campId with type Id - Campaign Id
     * @param conIds with type Set<Id> - Contact Ids
     * @param ldId with type Id - Lead Id
     * @param isInsert with type Boolean
     *
     * @return List<CampaignMember>
     */
    public static List<CampaignMember> createCampaignMembers(Id campId, Set<Id> conIds, Id ldId, Boolean isInsert) {
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        for (Id conId : conIds) {
            campaignMembers.add(createCampaignMember(campId, conId, ldId, false));
        }
        if (isInsert) {
            insert campaignMembers;
        }
        return campaignMembers;
    }

    /**
     * @param name Name of the test user
     * @param profileName Profile of the test user
     * @param count Number of users to to create
     * @param isInsert with type Boolean
     *
     * @return List<User>
     */
    public static List<User> createUsers(String name, String profileName, Integer count, Boolean isInsert) {
        Profile p = [SELECT Id FROM Profile WHERE Name = :profileName];
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'LastName' => name, 'ProfileId' => p.Id, 'UserName' => name + '@gmail.com'
        };

        return (List<User>) CFA_TestDataFactory.createList(new User().getSObjectType(), null, fieldsWithValue, count, isInsert);
    }

    /**
     * @param name Name of the test user
     * @param profileName Profile of the test user
     * @param isInsert with type Boolean
     * @return User
     */
    public static User createUser(String name, String profileName, Boolean isInsert) {
        Profile p = [SELECT Id FROM Profile WHERE Name = :profileName];
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'LastName' => name, 'ProfileId' => p.Id, 'UserName' => name + '@gmail.com'
        };
		return (User) CFA_TestDataFactory.create(new User().getSObjectType(), null, fieldsWithValue, isInsert);
    }

    /**
     * @param name Name of test queue
     * @param sObjectType sObject Type for the queue
     * @param count Number of queues to to create
     * @param isInsert with type Boolean
     *
     * @return List<Group>
     */
    public static List<Group> createQueues(String name, String sObjectType, Integer count, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'Name' => name, 'Type' => 'Queue'
        };
        Map<String, Object> fieldsWithValueForQueue = new Map<String, Object>{
                'sObjectType' => sObjectType
        };
        List<Group> groups = (List<Group>) CFA_TestDataFactory.createList(new Group().getSObjectType(), null, fieldsWithValue, count, false);

        System.runAs(new User(Id = UserInfo.getUserId())) {
            if (isInsert) {
                insert groups;

                List<QueueSobject> queueObjects = new List<QueueSobject>();
                for (Group g : groups) {
                    fieldsWithValueForQueue.put('QueueId', g.Id);
                    queueObjects.add((QueueSobject) CFA_TestDataFactory.create(new QueueSobject().getSObjectType(), null, fieldsWithValueForQueue, false));
                }
                insert queueObjects;
            }
        }

        return groups;
    }

    /**
     * @param policyId Id
     * @param countryName String
     * @param state String
     * @param lowZipCode String
     * @param highZipCode String
     * @param isInsert Boolean
     *
     * @return NSA_Geographic_Rule__c
     */
    public static NSA_Geographic_Rule__c createNsaGeographicRule(Id policyId, String countryName, String state, String lowZipCode, String highZipCode, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'NSA_Policy__c' => policyId
                , 'Country_Name__c' => countryName
                , 'State__c' => state
                , 'Zip_Code_Low__c' => lowZipCode
                , 'Zip_Code_High__c' => highZipCode
        };
        
        return (NSA_Geographic_Rule__c) CFA_TestDataFactory.create(new NSA_Geographic_Rule__c().getSObjectType(), null, fieldsWithValue, isInsert);
    }

    /**
     * @param accId Id
     * @param contactSocietyFieldNameApi String
     * @param allCampaign Id
     * @param memberCampaign Id
     * @param territoryGroupName String
     * @param isInsert Boolean
     *
     * @return NSA_Policy__c
     */
    public static NSA_Policy__c createNsaPolicy(Id accId, String contactSocietyFieldNameApi, Id allCampaign,
            Id memberCampaign, String territoryGroupName, Boolean isInsert) {

        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'Contact_Society_Field_API_Name__c' => contactSocietyFieldNameApi
                , 'Society_Account__c' => accId
                , 'All_Campaign__c' => allCampaign
                , 'Member_Campaign__c' => memberCampaign
                , 'Policy_Status__c' => 'Approved'
        };

        if(territoryGroupName != null){
            fieldsWithValue.put('Territory_Group_Name__c',territoryGroupName);
        }

        return (NSA_Policy__c) CFA_TestDataFactory.create(new NSA_Policy__c().getSObjectType(), null, fieldsWithValue, isInsert);
    }

    /**
     * @param accId Id
     * @param conId Id
     * @param isInsert Boolean
     *
     * @return Membership_Application__c
     */
    public static Membership_Application__c createMembershipApplication(Id accId, Id conId, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'Account__c' => accId
                , 'Contact__c' => conId
        };

        return (Membership_Application__c) CFA_TestDataFactory.create(new Membership_Application__c().getSObjectType(), null, fieldsWithValue, isInsert);
    }

    /**
     * @param accId Id
     * @param contacts List<Contact>
     * @param isInsert Boolean
     *
     * @return List<Membership_Application__c>
     */
    public static List<Membership_Application__c> createMembershipApplications(Id accId, List<Contact> contacts, Boolean isInsert) {
        Map<String, Object> fieldsWithValue = new Map<String, Object>{
                'Account__c' => accId
        };
        List<Membership_Application__c> membershipApplications = new List<Membership_Application__c>();

        for (Contact con : contacts) {
            fieldsWithValue.put('Contact__c', con.Id);
            membershipApplications.add((Membership_Application__c) CFA_TestDataFactory.create(new Membership_Application__c().getSObjectType(), null, fieldsWithValue, false));
        }

        if (isInsert) {
            insert membershipApplications;
        }

        return membershipApplications;
    }

    /**
     * @description Generate Random String
     *
     * @param size Integer
     *
     * @return String
     */
    public static String generateRandomString(Integer size) {
        final String chars = '1234567890ABCDEFGJKLMNOPRSTabcdefjgklmncawertgfdwDEFGH';
        String newRandomValue = 'A';
        while (newRandomValue.length() < size) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            newRandomValue += chars.substring(idx, idx + 1);
        }
        return newRandomValue;
    }

    /**
     * @description Get User With Permission
     *
     * @return User
     */
    public static User getUserWithPermission(String profileName, String permissionName) {
        User userAdmin = createUsers('Admin', 'System Administrator', 1, true)[0];
        User userWithPermission = createUsers('UserWithPermission', profileName, 1, true)[0];
        //Assign Permission set
        List<PermissionSet> permissionSets = [SELECT Id FROM PermissionSet WHERE Name = :permissionName];
        System.runAs(userAdmin) {
            PermissionSetAssignment psa = new PermissionSetAssignment(
                    AssigneeId = userWithPermission.Id,
                    PermissionSetId = permissionSets[0].Id
            );
            insert psa;
        }
        return userWithPermission;
    }

    /**
     * @description Create Partner Users
     *
     * @param contacts List<Contact>
     * @param partnerProfileName String
     * @param numberOfRecords Integer
     *
     * @return List<User>
     */
    public static List<User> createPartnerUsers(List<Contact> contacts, String partnerProfileName, Integer numberOfRecords) {
        return createPartnerUsers(null,contacts,partnerProfileName,numberOfRecords,true);
    }

    /**
     * @description Create Partner Users
     *
     * @param fieldsWithValue Map<String,Object>
     * @param contacts List<Contact>
     * @param partnerProfileName String
     * @param numberOfRecords Integer
     * @param isInsert Boolean
     *
     * @return List<User>
     */
    public static List<User> createPartnerUsers(Map<String,Object> fieldsWithValue, List<Contact> contacts, String partnerProfileName, Integer numberOfRecords, Boolean isInsert) {
		UserRole portalRole = [SELECT Id FROM UserRole WHERE PortalType = 'None' LIMIT 1];

		User adminUser = createUser('TestFactoryAdmin' + Crypto.getRandomInteger(), 'System Administrator', false);

		User partnerAccountOwner = createUser('partnerAccountOwner' + Crypto.getRandomInteger(), 'System Administrator', false);

		List<User> partnerCommunityUsers = new List<User>();

        System.runAs(adminUser) {
			partnerAccountOwner.UserRoleId = portalRole.Id;
            RetryForTest.upsertOnUnableToLockRow(partnerAccountOwner);
        }

		System.runAs(getUserWithPermission('Integration profile', 'Edit_Account_Name')) {
			List<Account> partnerAccounts = createAccountRecords('CFA Institute', null, contacts.size(), false);
			for (Account partnerAccount : partnerAccounts) {
				partnerAccount.Name = 'TDF-' + Crypto.getRandomInteger();
				partnerAccount.OwnerId = partnerAccountOwner.Id;
			}
			RetryForTest.upsertOnUnableToLockRow(partnerAccounts);

			Integer i = 0;
			for (Account partnerAccount : partnerAccounts) {
				contacts[i].AccountId = partnerAccount.Id;
				i++;
			}
			RetryForTest.upsertOnUnableToLockRow(contacts);
		}
		Profile p = [SELECT Id FROM Profile WHERE Name = :partnerProfileName];
		for (Contact partnerContact : contacts) {
			User partnerUser = (User)CFA_TestDataFactory.create(new User().getSObjectType(), null, fieldsWithValue, false);
			partnerUser.ContactId = partnerContact.Id;
			partnerUser.LastName = 'PartnUser' + Crypto.getRandomInteger();
			partnerUser.ProfileId = p.Id;
			partnerUser.UserName = 'PartnUser' + Crypto.getRandomInteger() + '@gmail.com';
			partnerAccountOwner.UserRoleId = portalRole.Id;
			partnerCommunityUsers.add(partnerUser);
		}
		RetryForTest.upsertOnUnableToLockRow(partnerCommunityUsers);
        

        return partnerCommunityUsers;
    }

    /**
     * @description Create Partner Users
     *
     * @param contacts List<Contact>
     * @param partnerProfileName String
     * @param numberOfRecords Integer
     *
     * @return List<User>
     */
    public static User createPartnerUser(Contact contact, String partnerProfileName) {
        return createPartnerUsers(null, New List<Contact> {contact}, partnerProfileName, 1, true)[0];
    }
}