@isTest
public class CFA_UpdateAffiliationInvoke_Test { 

    private static testMethod void ClientMemberAndDirectoryUpdate() {
        Account defAcct = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
    	Contact cn = CFA_TestDataFactory.createContact('Test Contact',defAcct.Id,'CFA Contact', false);
        cn.Person_Id__c = cfa_ApexUtilities.generateRandomString(5);
        cn.Partner_Id__c = cfa_ApexUtilities.generateRandomString(5);
        insert cn;
        
        List<Affiliation__c> affList = new List<Affiliation__c>();
        
        for(Integer i= 0; i < 5; i++) {
            Affiliation__c affiliation = new Affiliation__c();
            affiliation.Contact__c = cn.Id;
            affiliation.Account__c = defAcct.Id;
            affiliation.Start_Date__c = Date.today()+i;
            affiliation.End_Date__c = Date.today()+i;
            affiliation.Active_Affiliation__c = true;
            affiliation.MDM_Primary__c = true;
            affiliation.Role__c = 'Employee';
            affiliation.recordtypeId = Schema.SObjectType.Affiliation__c.getRecordTypeInfosByDeveloperName().get('Access_Provisioning').getRecordTypeId();
            affList.add(affiliation);
        }
        
        for(Integer i= 0; i < 2; i++) {
            Affiliation__c affiliation = new Affiliation__c();
            affiliation.Contact__c = cn.Id;
            affiliation.Account__c = defAcct.Id;
            affiliation.Start_Date__c = null;
            affiliation.Active_Affiliation__c = true;
            affiliation.MDM_Primary__c = true;
            affiliation.Role__c = 'Employee';
            affiliation.recordtypeId = Schema.SObjectType.Affiliation__c.getRecordTypeInfosByDeveloperName().get('Access_Provisioning').getRecordTypeId();
            affList.add(affiliation);
        }
        
        for(Integer i= 1; i < 5; i++) {
            Affiliation__c affiliation = new Affiliation__c();
            affiliation.Contact__c = cn.Id;
            affiliation.Account__c = defAcct.Id;
            affiliation.Start_Date__c = Date.today()-i;
            if(math.mod(i, 2) == 0){
            	affiliation.End_Date__c = Date.today()-i;
            	affiliation.Active_Affiliation__c = true;
            }else{
            	affiliation.End_Date__c = Date.today()+5;
                affiliation.Active_Affiliation__c = false;
            }
            affiliation.MDM_Primary__c = true;
            affiliation.Role__c = 'Employee';
            affiliation.recordtypeId = Schema.SObjectType.Affiliation__c.getRecordTypeInfosByDeveloperName().get('Access_Provisioning').getRecordTypeId();
            affList.add(affiliation);
        }
        
        insert affList;
        
        Test.startTest();
                
        List<CFA_UpdateAffiliationInvoke.affiliationIterator> lstDel = new List<CFA_UpdateAffiliationInvoke.affiliationIterator>();
        for(Affiliation__c affiliation: affList){
            CFA_UpdateAffiliationInvoke.affiliationIterator affUpdate = new CFA_UpdateAffiliationInvoke.affiliationIterator();
            affUpdate.AffliationId = affiliation.id; 
            lstDel.add(affUpdate);
        }        
        
        CFA_UpdateAffiliationInvoke.myInvocableMethod(lstDel);
        Test.stopTest(); 
        Affiliation__c affRec = [SELECT Id, Active_Affiliation__c FROM Affiliation__c WHERE Id =: affList[0].id];
        
    }
}