/* 
        # Created By: Shubham Dadhich
        # Created Date: 03/18/2020
        # Description: Test Class For Update DA Request Status Button
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        # Dhana Prasad               04/20/2020                  made updateCaseStatus  method to return to response
        ---------------------------------------------------------------------------------------------------------------
    */
@isTest
public class CFA_UpdateDARequestCaseStatus_Test {
    /* 
        # Created By: Shubham Dadhich
        # Created Date: 03/18/2020
        # Description: Method to test updateCaseStatus method of CFA_UpdateDARequestCaseStatus_Handler class
        ---------------------------------------------------------------------------------------------------------------
        # Modification inforamtion
        - Modified By                 Modified Date               Description                     Associated Task (If)
        # Dhana Prasad               04/20/2020                  made updateCaseStatus  method to return to response
        ---------------------------------------------------------------------------------------------------------------
    */
    @isTest
    static void UpdateButtonTest(){
        CaseTriggerHandler.TriggerDisabled = true;
        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.Name= 'trigger-details';
        societyOps.No_reply_email__c = 'noreply@cfainstitute.org';
        societyOps.CaseComment_TemplateName__c = 'SocietyOps-New Comment Notification';
        societyOps.CaseReassignment_TemplateName__c = 'SocietyOps-Case Reassignment';
        insert societyOps;
        List<Account> accList =  CFA_TestDataFactory.createAccountRecords('TestAccount', null , 1 ,true);
        List<Contact> conList = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestContact', 1, false);
        conList[0].AccountId = accList[0].Id;
        insert conList;
        List<Case> caseList = CFA_TestDataFactory.createCaseRecords(Label.CFA_ADALabel,1,false);
        caseList[0].ContactId = conList[0].Id;
        insert caseList;
        

        Test.startTest();
        Case caseRecord = new Case();
        string status =CFA_UpdateDARequestCaseStatus_Handler.updateCaseStatus(caseList[0].Id);
        caseRecord = [SELECT Id, Status FROM Case WHERE Id =: caseList[0].Id];
        System.assert(caseRecord.Status=='Submitted');

        CFA_UpdateDARequestCaseStatus_Handler.checkStatus(caseList[0].Id);
        System.assert(caseRecord.Status!=null);
        Test.stopTest();

    }    
}