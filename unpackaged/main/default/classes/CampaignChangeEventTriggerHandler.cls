public with sharing class CampaignChangeEventTriggerHandler extends BaseChangeEventTriggerHandler {
    public static Boolean triggerDisabled = false;
    private static final String TRIGGER_NAME = 'CampaignChangeEventTrigger';
    
    public override Boolean isDisabled() {
        if(Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return triggerDisabled || super.isDisabled();
        }
    }
    
    /**
    * @description Get Trigger Name
    *
    * @return String
    */
    public override String getTriggerName() {
        return TRIGGER_NAME;
    }
    
    /**
     * @description Handle Create Change Events
     *
     * @param changeEvents List<SObject>
     */
    public override void handleAllCreate(List<SObject> changeEvents) {
        CampaignChangeEventTriggerHelper.applyCampaignTerritoryVisibility(getRecordIds(changeEvents), false);
    }
    
    /**
     * @description Handle Update Change Events
     *
     * @param changeEvents List<SObject>
     */
    public override void handleAllUpdate(List<SObject> changeEvents) {
        Set<String> recordIds = new Set<String>();
        for (sObject event : changeEvents) {
            EventBus.ChangeEventHeader header = (EventBus.ChangeEventHeader) event.get('ChangeEventHeader');
            if(header.changedfields.contains('Account_Name__c')) {
                recordIds.addAll(header.getRecordIds());
            }
        }
        if(!recordIds.isEmpty()) {
            CampaignChangeEventTriggerHelper.applyCampaignTerritoryVisibility(recordIds, true);
        }
    }
        

}