/*****************************************************************
Name: CampaignMemberChangeEventTriggerHelper
Copyright © 2020 ITC
============================================================
Purpose: Helper for Trigger CampaignMember Change Event
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   09.12.2020   Created   Update Society Related Contact Fields
*****************************************************************/

public with sharing class CampaignMemberChangeEventTriggerHelper {

    /**
     * @description Update Related Contact Society_Member__c and Campaign_Relations__c Fields
     *
     * @param campaignMembers List<CampaignMember>
     */
    public static void updateSocietyRelatedContactFields(List<CampaignMember> campaignMembers) {
        try {
            List<Contact> updatedContacts = SocietyCampaignMemberService.getUpdatedRelatedContacts(campaignMembers);
            update updatedContacts;
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'Update Society Related Contact Fields', 'CampaignMemberChangeEventTriggerHelper', DateTime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }
}