/*****************************************************************
Name: CampaignMemberPlatEventTriggerHTest
Copyright © 2020 ITC
============================================================
Purpose: Unit test on CampaignMemberPlatEventTriggerHandler class
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   10.12.2020   Created   cover update Society Related Contact Fields logic
*****************************************************************/

@IsTest
private class CampaignMemberPlatEventTriggerHTest {

    @IsTest
    private static void updateSocietyRelatedContactFieldsTest(){
        Test.enableChangeDataCapture();
        Test.startTest();
        Contact con = createContact();
        CampaignMember cmToronto = createCampaignMember('CFA Society Toronto Members', con.Id);
        CampaignMember cmUkraine = createCampaignMember('CFA Society Ukraine Members',con.Id);
        CampaignMember cmNY = createCampaignMember('CFA Society New York',con.Id);
        Test.getEventBus().deliver();

        delete cmToronto;
        Test.getEventBus().deliver();

        List<CampaignMember> cmTorontoExisted = [SELECT Id FROM CampaignMember WHERE Id =: cmToronto.Id];
        System.assertEquals(0, cmTorontoExisted.size());

        List<CFA_Integration_Log__c> afterInsertList = [SELECT Id FROM CFA_Integration_Log__c];
        System.assert(afterInsertList.size() == 0);
        List<Contact> conAfterInsertList = [SELECT Society_Member__c, Campaign_Relations__c FROM Contact];
        System.assertEquals(true, conAfterInsertList[0].Society_Member__c);
        System.assert(!conAfterInsertList[0].Campaign_Relations__c.contains('CFA Society Toronto Members'));
        System.assert( conAfterInsertList[0].Campaign_Relations__c.contains('CFA Society Ukraine Members'));
        System.assert( !conAfterInsertList[0].Campaign_Relations__c.contains('CFA Society New York'));

        delete cmUkraine;
        Test.getEventBus().deliver();
        Test.stopTest();

        List<Contact> conAfterInsertList2 = [SELECT Society_Member__c, Campaign_Relations__c FROM Contact];
        System.assertEquals(false, conAfterInsertList2[0].Society_Member__c);
        System.assertEquals(null, conAfterInsertList2[0].Campaign_Relations__c);
    }

    private static CampaignMember createCampaignMember(String campaignName,Id conId) {
        Campaign camp = CFA_TestDataFactory.createCampaign(campaignName, false);
        camp.Type = 'Society';
        insert camp;
        CampaignMember cm = CFA_TestDataFactory.createCampaignMember(camp.Id, conId, null, true);
        return cm;
    }

    private static Contact createContact(){
        List<Account> accList = CFA_TestDataFactory.createAccountRecords('TestAccount', null, 1, true);
        Contact con = CFA_TestDataFactory.createContact('Test', accList[0].Id, 'CFA Contact', true);
        return con;
    }
}