/*****************************************************************
Name: CampaignMemberPlatEventTriggerHandler
Copyright © 2020 ITC
============================================================
Purpose: Handler fpr Trigger on 'CampaignMemberPlatformEvent' platform event
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   09.12.2020   Created   Update Society Related Contact Fields
*****************************************************************/

public with sharing class CampaignMemberPlatEventTriggerHandler {
    public static final String DELETE_CHANGE_TYPE = 'DELETE';

    /**
    * @description Handle CampaignMember Platform Events
    *
    * @param campaignMemberPlatformEvents List<CampaignMemberPlatformEvent__e>
    */
    public static void handleCampaignMemberPlatformEvents(List<CampaignMemberPlatformEvent__e> campaignMemberPlatformEvents) {
        try {
            handlePlatformEventByChangeType(campaignMemberPlatformEvents);
        } catch (Exception ex) {
            System.debug('Exception ex = ' + ex);
            insert CFA_IntegrationLogException.logError(ex, 'Handle Campaign Member Platform Events ', 'CampaignMemberPlatformEventTriggerHelper', DateTime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }

    /**
     * @description Handle PlatformEvents By ChangeTypes
     * - handleDelete - on delete CampaignMember the CampaignMemberEvent__e record is created and this method
     *                  contains logic for updating Contacts after deleting CampaignMember record
     * @param campaignMemberPlatformEvents List<CampaignMemberPlatformEvent__e>
     */
    private static void handlePlatformEventByChangeType(List<CampaignMemberPlatformEvent__e> campaignMemberPlatformEvents) {
        Map<String, List<CampaignMemberPlatformEvent__e>> changeEventsByChangeType = new Map<String, List<CampaignMemberPlatformEvent__e>>();
        for (CampaignMemberPlatformEvent__e campaignMemberPlatformEvent : campaignMemberPlatformEvents) {
            if (!changeEventsByChangeType.containsKey(campaignMemberPlatformEvent.ChangeType__c)) {
                changeEventsByChangeType.put(campaignMemberPlatformEvent.ChangeType__c, new List<CampaignMemberPlatformEvent__e>());
            }
            if (campaignMemberPlatformEvent.ChangeType__c == DELETE_CHANGE_TYPE) {
                changeEventsByChangeType.get(campaignMemberPlatformEvent.ChangeType__c).add(campaignMemberPlatformEvent);
            }
        }

        for (String action : changeEventsByChangeType.keySet()) {
            List<CampaignMemberPlatformEvent__e> changeEvents = changeEventsByChangeType.get(action);
            if (changeEvents.size() > 0) {
                if (DELETE_CHANGE_TYPE.equalsIgnoreCase(action)) {
                    handleDelete(changeEventsByChangeType.get(action));
                }
            }
        }
    }

    /**
     * @description Handle Update of CampaignMember
     *
     * @param events List<CampaignMemberEvent__e>
     */
    public static void handleDelete(List<CampaignMemberPlatformEvent__e> events) {
        List<CampaignMember> campaignMembers = SocietyCampaignMemberService.getCampaignMembers(events);

        updateSocietyRelatedContactFields(campaignMembers);
    }

    /**
    * @description Update Related Contact Society_Member__c and Campaign_Relations__c Fields
    *
    * @param campaignMembers List<CampaignMember>
    */
    private static void updateSocietyRelatedContactFields(List<CampaignMember> campaignMembers) {
        List<Contact> updatedContacts = SocietyCampaignMemberService.getUpdatedRelatedContacts(campaignMembers);
        update updatedContacts;
    }
}