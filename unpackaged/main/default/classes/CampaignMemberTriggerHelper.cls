/**
 * Created by ariabchenko on 24.11.2020.
 */
/*****************************************************************
Name: CampaignMemberTriggerHelper
Copyright © 2020 ITC
============================================================
Purpose: Handle DML operations in the trigger
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   24.11.2020   Created   Implementation handling Campaign Member On After Delete
*****************************************************************/

public with sharing class CampaignMemberTriggerHelper implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean triggerDisabled = false;

    //Checks to see if the trigger has been disabled either by custom metadata or by running code
    public Boolean IsDisabled() {
        if (Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('CampaignMemberTrigger') || triggerDisabled;
        }
    }

    //  On before insert event
    public void BeforeInsert(List<SObject> newItems) {}

    //  on before update event
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    //  on after insert event
    public void AfterInsert(Map<Id, SObject> newItems) {}

    // On after update event
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    //  On after delete event
    public void AfterDelete(Map<Id, SObject> oldItems) {
        Map<Id, CampaignMember> oldMap = (Map<Id, CampaignMember>) oldItems;
        SocietyCampaignMemberService.createPlatformEvent(oldMap);
    }

    // On after undelete event
    public void AfterUndelete(Map<Id, SObject> oldItems) {}

    //  On before delete event
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
}