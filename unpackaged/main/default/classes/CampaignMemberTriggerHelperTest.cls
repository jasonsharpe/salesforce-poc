/*****************************************************************
Name: CampaignMemberTriggerHelperTest
Copyright © 2020 ITC
============================================================
Purpose:  Unit test on CampaignMemberTriggerHelper class
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   10.12.2020   Created   default tests
*****************************************************************/

@IsTest
private class CampaignMemberTriggerHelperTest {
	@TestSetup
	private static void createCampaignMember() {
		Account newAccount = CFA_TestDataFactory.createAccount('TestAccount', null, true);
        
		Contact con = CFA_TestDataFactory.createContact('Test', newAccount.Id, 'CFA Contact', true);
		Campaign camp = CFA_TestDataFactory.createCampaign('CFA Society Toronto Members', false);
		camp.Type = 'Society';
		insert camp;

		CampaignMember cm = CFA_TestDataFactory.createCampaignMember(camp.Id, con.Id, null, true);
	}

	@IsTest
	private static void campaignMemberTriggerHandlerUpdateTest() {
		CampaignMember member = [SELECT Id FROM CampaignMember LIMIT 1];

		Test.startTest();
		member.Status = 'Sent';
		update member;
		Test.stopTest();

		CampaignMember memberCheck = [SELECT Id, Status FROM CampaignMember LIMIT 1];
		System.assertEquals(member.Status, memberCheck.Status);
	}

	@IsTest
	private static void campaignMemberTriggerHandlerDeleteTest() {
		CampaignMember member = [SELECT Id, ContactId, CampaignId FROM CampaignMember LIMIT 1];
		Test.startTest();
		delete member;
		Test.stopTest();

		List<CampaignMember> memberCheck = new List<CampaignMember>();
		memberCheck = [SELECT Id FROM CampaignMember WHERE Id = :member.Id];
		System.assertEquals(0, memberCheck.size());
	}
}