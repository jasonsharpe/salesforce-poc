/* 
  # Created By: Dhana Prasad
  # Created Date: 04/17/2020
  # Description: Test Class for CaseTriggerHandler and Helper
  ---------------------------------------------------------------------------------------------------------------
  # Modification inforamtion
  - Modified By                 Modified Date               Description                     Associated Task (If)
  #
  ---------------------------------------------------------------------------------------------------------------
*/

@isTest
public class CaseTriggerHandler_Test {

	public static final String DefaultAccountName = 'CFA Institute';
	public static final String DefaultContactName = 'TestContact';

	static List<Case> caseList = null;

	/* 
	  # Created By: Dhana Prasad
	  # Created Date: 04/17/2020
	  # Description: Method to create test data
	  ---------------------------------------------------------------------------------------------------------------
	  # Modification inforamtion
	  - Modified By                 Modified Date               Description                     Associated Task (If)
	  #
	  ---------------------------------------------------------------------------------------------------------------
	 */
	@TestSetup
	static void testSetup() {
		SocietyOps_Backoffice__c societyOps = new SocietyOps_Backoffice__c();
		societyOps.Name = 'trigger-details';
		societyOps.No_reply_email__c = 'noreply@cfainstitute.org';
		societyOps.CaseComment_TemplateName__c = 'SocietyOps-New Comment Notification';
		societyOps.CaseReassignment_TemplateName__c = 'SocietyOps-Case Reassignment';
		insert societyOps;

		Account newAccount = CFA_TestDataFactory.createAccount(CaseTriggerHandler_Test.DefaultAccountName, null, true);

		Contact newContact = CFA_TestDataFactory.createCFAContact(CaseTriggerHandler_Test.DefaultContactName, newAccount.Id, true);

		User adminUser = CFA_TestDataFactory.createUser('CaseTriggerAdmin', 'System Administrator', true);

		System.runAs(adminUser) {
			User userCustomerCommunity = CFA_TestDataFactory.createPartnerUser(newContact, 'CFA_Customer_Community');
			PermissionSet ps = [SELECT Id, Name FROM PermissionSet WHERE Name = 'Medical_Reviewer' LIMIT 1];
			PermissionSetAssignment psAssignment = new PermissionSetAssignment(PermissionSetId = ps.Id, AssigneeId = userCustomerCommunity.Id);
			insert psAssignment;
		}
	}
	/* 
	  # Created By: Dhana Prasad
	  # Created Date: 04/17/2020
	  # Description: Method to test insert and update
	  ---------------------------------------------------------------------------------------------------------------
	  # Modification inforamtion
	  - Modified By                 Modified Date               Description                     Associated Task (If)
	  #
	  ---------------------------------------------------------------------------------------------------------------
	 */
	@isTest
	static void getTestInsertUpdate()
	{
		CaseTriggerHelper.testSwitch = createTimeSwtich();

		Contact testContact = [SELECT Id FROM Contact WHERE LastName = :CaseTriggerHandler_Test.DefaultContactName LIMIT 1];

		RecursiveTriggerHandler.isFirstTime = true;
		Test.startTest();
		caseList = CFA_TestDataFactory.createCaseRecords('DA Request', 2, false);
		caseList[0].ContactId = testContact.Id;
		caseList[0].Status = 'Submitted';
		caseList[0].Other__c = true;
		caseList[1].ContactId = testContact.Id;
		caseList[1].Status = 'Submitted';
		caseList[1].Other__c = true;
		insert caseList;
		System.debug('caseList' + [SELECT Id, Status FROM Case]);

		caseList[0].Additional_Time__c = false;
		caseList[0].Large_Print_Exam__c = false;
		caseList[0].Reader__c = false;
		caseList[0].Scribe__c = true;
		caseList[0].Semi_Private_Room__c = false;
		caseList[0].Other__c = true;
		caseList[0].Status = 'Review Completed';
		caseList[0].Exam_Type__c = 'PBT';
		caseList[0].Other_Status__c = 'Granted with Modifications';
		caseList[0].Scribe_Status__c = 'Not Granted';
		caseList[1].Additional_Time__c = false;
		caseList[1].Large_Print_Exam__c = false;
		caseList[1].Reader__c = false;
		caseList[1].Scribe__c = false;
		caseList[1].Semi_Private_Room__c = false;
		caseList[1].Other__c = true;
		caseList[1].Status = 'Review Completed';
		caseList[1].Exam_Type__c = 'PBT';
		caseList[1].Other_Status__c = 'Not Granted';
		caseList[1].Scribe_Status__c = 'Granted as Requested';
		update caseList;
		CaseTriggerHandler cfaCase = new CaseTriggerHandler();
		cfaCase.AfterDelete(null);
		cfaCase.AfterUndelete(null);
		Test.stopTest();
		List<case> caseUpdatedList = [SELECT Id, Integration_Status__c FROM Case WHERE Integration_Status__c = 'Processed'];
		System.assert(caseUpdatedList != null, true);
	}
	@isTest
	static void getTestInsertUpdate2()
	{
		CaseTriggerHelper.testSwitch = createTimeSwtich();

		RecursiveTriggerHandler.isFirstTime = true;
		Contact testContact = [SELECT Id FROM Contact WHERE LastName = :CaseTriggerHandler_Test.DefaultContactName LIMIT 1];
		Test.startTest();
		caseList = CFA_TestDataFactory.createCaseRecords('DA Request', 2, false);
		caseList[0].ContactId = testContact.Id;
		caseList[0].Status = 'Submitted';
		caseList[0].Other__c = true;
		caseList[1].ContactId = testContact.Id;
		caseList[1].Status = 'Submitted';
		caseList[1].Other__c = true;
		insert caseList;

		System.debug('caseList' + [SELECT Id, Status FROM Case]);
		caseList[0].Additional_Time__c = false;
		caseList[0].Large_Print_Exam__c = false;
		caseList[0].Reader__c = false;
		caseList[0].Scribe__c = true;
		caseList[0].Semi_Private_Room__c = false;
		caseList[0].Other__c = true;
		caseList[0].Status = 'Review Completed';
		caseList[0].Exam_Type__c = 'PBT';
		caseList[0].Other_Status__c = 'Granted with Modifications';
		caseList[0].Scribe_Status__c = 'Not Granted';
		caseList[1].Additional_Time__c = false;
		caseList[1].Large_Print_Exam__c = false;
		caseList[1].Reader__c = false;
		caseList[1].Scribe__c = false;
		caseList[1].Semi_Private_Room__c = false;
		caseList[1].Other__c = true;
		caseList[1].Status = 'Withdrawn';
		caseList[1].Exam_Type__c = 'PBT';
		caseList[1].Other_Status__c = 'Not Granted';
		caseList[1].Scribe_Status__c = 'Granted as Requested';
		update caseList;
		CaseTriggerHelper.createADAEvent(caseList);
		Test.stopTest();
		List<case> caseUpdatedList = [SELECT Id, Integration_Status__c FROM Case WHERE Integration_Status__c = 'Processed'];
		System.assert(caseUpdatedList != null, true);
	}

	@IsTest
	public static void testCBTCase() {
		CaseTriggerHelper.testSwitch = createTimeSwtich();

		Contact testContact = [SELECT Id FROM Contact WHERE LastName = :CaseTriggerHandler_Test.DefaultContactName LIMIT 1];

		RecursiveTriggerHandler.isFirstTime = true;
		caseList = CFA_TestDataFactory.createCaseRecords('CBT DA Request', 2, false);

		caseList[0].ContactId = testContact.Id;
		caseList[0].Status = 'Submitted';
		caseList[1].ContactId = testContact.Id;
		caseList[1].Status = 'Submitted';

		insert caseList;

		caseList[0].Other_Additional_Time__c = true;
		caseList[0].Other_Additional_Time_Status__c = 'Granted as Requested';
		caseList[0].Other_Additional_Time_Detail__c = 'Lorem Ipsum';
		caseList[0].Status = 'Review Completed';
		caseList[0].Exam_Type__c = 'CBT';

		caseList[1].Other_Additional_Time__c = true;
		caseList[1].Other_Additional_Time_Status__c = 'Granted as Requested';
		caseList[1].Other_Additional_Time_Detail__c = 'Lorem Ipsum';
		caseList[1].Status = 'Withdrawn';
		caseList[1].Exam_Type__c = 'CBT';

		update caseList;

		Test.startTest();
		CaseTriggerHelper.createADAEvent(caseList);
		Test.stopTest();

		System.assertNotEquals(0, [SELECT COUNT() FROM Case WHERE Integration_Status__c = 'Processed']);

	}

	static DA_Event_Swith__mdt createTimeSwtich() {
		return new DA_Event_Swith__mdt(timeFrameActive__c = true);
	}

	/* 
	  # Created By: Dhana Prasad
	  # Created Date: 06/08/2020
	  # Description: Method to test createContentDocumentLink method  CFA_ContentVersionTrigger class
	  ---------------------------------------------------------------------------------------------------------------
	  # Modification inforamtion
	  - Modified By                 Modified Date               Description                     Associated Task (If)
	  #
	  ---------------------------------------------------------------------------------------------------------------
	 */
	@isTest
	static void createContentDocumentLinkTest()
	{
		CaseTriggerHandler.TriggerDisabled = true;

		SocietyOps_Backoffice__c societyOps = new SocietyOps_Backoffice__c();

		Contact testContact = [SELECT Id FROM Contact WHERE LastName = :CaseTriggerHandler_Test.DefaultContactName LIMIT 1];

		List<Case> caseList = CFA_TestDataFactory.createCaseRecords(Label.CFA_ADALabel, 2, false);
		caseList[0].ContactId = testContact.Id;
		caseList[1].ContactId = testContact.Id;
		RetryForTest.upsertOnUnableToLockRow(caseList);

		Id testRecordType = caseList[0].RecordTypeId;
		String recordName = CFA_RecordTypeUtility.getRecordTypeName(Case.getSObjectType(), testRecordType, CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
		ContentVersion contentVersion = new ContentVersion(
		                                                   Title = 'Penguins',
		                                                   PathOnClient = 'Penguins.jpg',
		                                                   VersionData = Blob.valueOf('Test Content'),
		                                                   IsMajorVersion = true
		);
		RetryForTest.upsertOnUnableToLockRow(contentVersion);

		List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

		//create ContentDocumentLink  record 
		ContentDocumentLink cdl = New ContentDocumentLink();
		cdl.LinkedEntityId = caseList[0].Id;
		cdl.ContentDocumentId = documents[0].Id;
		cdl.shareType = 'V';
		cdl.Visibility = 'AllUsers';
		RetryForTest.upsertOnUnableToLockRow(cdl);

		List<ContentDocumentLink> testContentDocumentLinkList = new List<ContentDocumentLink> ();
		testContentDocumentLinkList.add(cdl);
		CaseTriggerHelper.byPassQuery = true;
		CaseTriggerHelper.testDocumentLink = testContentDocumentLinkList;
		System.debug('Case::' + caseList);

		// List<ContentVersion> contVerList = [SELECT Id, ContentDocumentId, VersionNumber FROM ContentVersion WHERE Id =: contentVersion.Id];

		Test.startTest();
		List<Case> caseList1 = CFA_TestDataFactory.createCaseRecords(Label.CFA_ADALabel, 1, false);
		caseList1[0].ContactId = testContact.Id;
		RetryForTest.upsertOnUnableToLockRow(caseList1);
		
		Test.stopTest();
		List<ContentDocumentLink> cdList = [SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = :caseList[0].Id];
		System.assertEquals(cdList.size() > 0, true);
	}


}