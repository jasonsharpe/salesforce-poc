public class CaseTriggerHelper {
    
    public static DA_Event_Swith__mdt testSwitch;
    public static Boolean byPassQuery =  false;
    public static List<ContentDocumentLink> testDocumentLink;

                        
/*******************************************************************************************
* @description: handle accommodations and send platform events when applicable
*
* @param: caseList - The list of cases that might be applicable for a platform event
*
*******************************************************************************************/
                        public static void createADAEvent(List<Case> caseList){
                            List<DA_Event__e> listOfDAEventsToPublish = new List<DA_Event__e>();
                            Map<Id,List<String>> mapOfIdsToAccommodationsRequested = new Map<Id,List<String>>();
                            Map<String, List<ADA_AccommodationManager__mdt>> managersByExamType = getManagersByExamType();
                            List<Case> caseListToUpdate = new List<Case>();
                            
                            DA_Event_Swith__mdt timeFrameSwitch;
                            if(Test.isRunningTest()){
                                timeFrameSwitch = testSwitch;
                            }else{
                                timeFrameSwitch = [SELECT Id,DeveloperName,timeFrameActive__c,MasterLabel,NamespacePrefix,Label,QualifiedApiName from  DA_Event_Swith__mdt LIMIT 1];
                            }
                            
                            //queue cases if they fall outside of time frame
                            if(timeFrameSwitch != null && timeFrameSwitch.timeFrameActive__c == false){
                                //System.debug('Timeframe not open, cases queued');
                                for(Case caseObj:caseList){
                                    if(caseObj.Integration_status__c != 'Queued'){
                                        caseObj.Integration_status__c = 'Queued';
                                        caseListToUpdate.add(caseObj);
                                    }
                                }
                            } else if(timeFrameSwitch != null && timeFrameSwitch.timeFrameActive__c ==true){ //only handle if time frame is enabled                                
                                //evaluate accommodations to see if CAT events need to be created
                                for(Case caseVar:caseList){
                                    if(caseVar.Exam_Type__c =='PBT'){
                                        List<ADA_AccommodationManager__mdt> accommodationsRequested = getRequestedAccommodations(caseVar, managersByExamType.get('PBT'));
                    
                                        switch on caseVar.Status {
                                            when 'Withdrawn' {
                                                listOfDAEventsToPublish.add(createDaEvent('No',caseVar.Month_Year_of_Exam__c,
                                                        'PBT',caseVar.Contact.Partner_Id__c, caseVar.ExamCycleCode__c, caseVar.ExamYearCode__c, caseVar.ProgramCode__c, caseVar.ProgramLevelCode__c));
                                                caseVar.Integration_Status__c = 'Processed';
                                                caseListToUpdate.add(caseVar);
                                            }
                                            when 'Review Completed' {
                                                if(!accommodationsRequested.isEmpty()) {
                                                    DA_Event__e event = createDaEvent('No', caseVar.Month_Year_of_Exam__c, 'PBT', caseVar.Contact.Partner_Id__c, caseVar.ExamCycleCode__c, caseVar.ExamYearCode__c, caseVar.ProgramCode__c, caseVar.ProgramLevelCode__c);
                    
                                                    // Loop through all of the accommodation managers that are approved, and map the fields
                                                    // onto the DA Event. There should be a one to one correspondence between all three objects
                                                    // and the field names
                                                    for (ADA_AccommodationManager__mdt manager : accommodationsRequested) {
                                                        event.put(manager.requestedField__c, caseVar.get(manager.requestedField__c));
                                                        if(!String.isBlank(manager.detailsField__c)){
                                                            event.put(manager.detailsField__c, caseVar.get(manager.detailsField__c));

                                                        }
                                                        event.put(manager.statusField__c, caseVar.get(manager.statusField__c));
                                                        if(caseVar.get(manager.statusField__c) == 'Granted as Requested'||caseVar.get(manager.statusField__c) == 'Granted with Modifications'){
                                                            event.DA__c = 'Yes';
                                                        }
                                                       
                                                    }
                    
                                                    listOfDAEventsToPublish.add(event);
                                                    caseVar.Integration_Status__c = 'Processed';
                                                    caseListToUpdate.add(caseVar);
                                                } 
                                            }
                                        }
                                    }else if(caseVar.Exam_Type__c == 'CBT'){
                                        List<ADA_AccommodationManager__mdt> accommodationsRequested = getRequestedAccommodations(caseVar, managersByExamType.get('CBT'));
                                        
                                        switch on caseVar.Status {
                                            when 'Withdrawn' {
                                                listOfDAEventsToPublish.add(createDaEvent('No', caseVar.Month_Year_of_Exam__c, 'CBT', caseVar.Contact.Partner_Id__c, caseVar.ExamCycleCode__c, caseVar.ExamYearCode__c, caseVar.ProgramCode__c, caseVar.ProgramLevelCode__c));
                                                caseVar.Integration_Status__c = 'Processed';
                                                caseListToUpdate.add(caseVar);
                                            }
                                            when 'Review Completed' {
                                                
                                                if(accommodationsRequested.isEmpty()) {
                                                    listOfDAEventsToPublish.add(createDaEvent('No',caseVar.Month_Year_of_Exam__c,'CBT',caseVar.Contact.Partner_Id__c, caseVar.ExamCycleCode__c, caseVar.ExamYearCode__c, caseVar.ProgramCode__c, caseVar.ProgramLevelCode__c));
                                                    caseVar.Integration_Status__c = 'Processed';
                                                    caseListToUpdate.add(caseVar);
                                                } else {
                                                    DA_Event__e event = createDaEvent('Yes', caseVar.Month_Year_of_Exam__c, 'CBT', caseVar.Contact.Partner_Id__c, caseVar.ExamCycleCode__c, caseVar.ExamYearCode__c, caseVar.ProgramCode__c, caseVar.ProgramLevelCode__c);
                                                    
                                                    // Loop through all of the accommodation managers that are approved, and map the fields
                                                    // onto the DA Event. There should be a one to one correspondence between all three objects
                                                    // and the field names
                                                    System.debug('from casen: ' + caseVar);
                                                    for (ADA_AccommodationManager__mdt manager : accommodationsRequested) {
                                                        System.debug('adding accommodation: ' + manager.requestedField__c);
                                                        event.put(manager.requestedField__c, caseVar.get(manager.requestedField__c));
                                                        if(String.isNotBlank(manager.detailsField__c)) {
                                                            event.put(manager.detailsField__c, caseVar.get(manager.detailsField__c));
                                                        }if(String.isNotBlank(manager.statusField__c)) {
                                                            event.put(manager.statusField__c, caseVar.get(manager.statusField__c));
                                                        }if(String.isNotBlank(manager.descriptionField__c)) {
                                                            event.put(manager.descriptionField__c, caseVar.get(manager.descriptionField__c));
                                                        }
                                                    }
                                                    
                                                    event.Flexible_Break_Maximum_Break_Duration__c = caseVar.Flexible_Breaks_Max_Break_Duration__c;
                                                    event.Flexible_Break_Total_Break_Time_Exam__c = caseVar.Flexible_Breaks_Total_Break_Time__c;
                                                    event.Flexible_Break_Maximum_Breaks_Section__c = String.valueOf(caseVar.Flexible_Breaks_Max_Section_Breaks__c);
                                                    event.Flexible_Break_Maximum_Breaks_Exam__c = String.valueOf(caseVar.Flexible_Breaks_Max_Exam_Breaks__c);
                                                    event.Scheduled_Break_Maximum_Break_Duration__c = caseVar.Scheduled_Breaks_Max_Break_Duration__c;
                                                    event.Scheduled_Break_Total_Break_Time_Exam__c = caseVar.Scheduled_Breaks_Total_Break_Time__c;
                                                    event.Scheduled_Break_Maximum_Breaks_Exam__c = String.valueOf(caseVar.Scheduled_Breaks_Maximum_Exam_Breaks__c);
                                                    
                                                    
                                                    listOfDAEventsToPublish.add(event);
                                                    caseVar.Integration_Status__c = 'Processed';
                                                    caseListToUpdate.add(caseVar);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if(listOfDAEventsToPublish.size()>0){
                                System.debug('Publishing events: '+listOfDAEventsToPublish);
                                List<Database.SaveResult> results = EventBus.publish(listOfDAEventsToPublish);
                                for (Database.SaveResult sr : results) {
                                    if (sr.isSuccess()) {
                                        System.debug('Successfully published event.');
                                        //System.debug('sr:'+sr);
                                    } else {
                                        for(Database.Error err : sr.getErrors()) {
                                            System.debug('Error returned: ' +
                                                         err.getStatusCode() +
                                                         ' - ' +
                                                         err.getMessage());
                                        }
                                    }
                                }
                            }
                            if(caseListToUpdate.size()>0)
                                    update caseListToUpdate;
                        }
    /*******************************************************************
*@description To create DA Event platform events
*
* @param DA - Were accommodations granted
* @param examPeriod - Exam month/year
* @param examType - Computer based/ Paper Based
* @param partnerID - Partner ID of contact that is taking exam
*
* @return - the Platform event record to publish
*/
    public static DA_Event__e createDaEvent(String DA,String examPeriod,String examType,String partnerID, String examCode, String yearCode, String programCode, String levelCode){
        DA_Event__e evt = new DA_Event__e(
            DA__c = DA,
            Exam_Period__c = examPeriod,
            Exam_Type__c = examType,
            Partner_ID__c = partnerID,
            ExamCycleCode__c = examCode,
            ExamYearCode__c = yearCode,
            ProgramCode__c = programCode,
            ProgramLevelCode__c = levelCode
        );
        return evt;
    }
    
    /**
* @param caseVar The case currently being processed
* @param managers List of ADA Accommodation Managers we which to check the accommodation fields against
*
* @return List of accommodation managers where the requested fields are true
*/
    private static List<ADA_AccommodationManager__mdt> getRequestedAccommodations(Case caseVar, List<ADA_AccommodationManager__mdt> managers) {
        List<ADA_AccommodationManager__mdt> requestedAccommodations = new List<ADA_AccommodationManager__mdt>();
        
        for(ADA_AccommodationManager__mdt manager : managers) {
            Boolean requestedTrue = ((Boolean)caseVar.get(manager.requestedField__c) == true);
            
            if(requestedTrue) {
                System.debug('adding manager: ' + manager);
                requestedAccommodations.add(manager);
            }
        }
        
        return requestedAccommodations;
    }
    
    /**
* This method retrieves the ADA Accommodation Manager custom metadata types, and creates a mapping between their
* exam types and the managers.
*
* @return Map of managers by their exam types
*/
    private static Map<String, List<ADA_AccommodationManager__mdt>> getManagersByExamType() {
        List<ADA_AccommodationManager__mdt> managers = [SELECT Label, detailsField__c, descriptionField__c, Exam_Type__c, statusField__c, requestedField__c FROM ADA_AccommodationManager__mdt];
        Map<String, List<ADA_AccommodationManager__mdt>> managersByExamType = new Map<String, List<ADA_AccommodationManager__mdt>>();
        
        for(ADA_AccommodationManager__mdt manager : managers) {
            // If the exam type doesn't exist yet in the mapping, add it to the mapping with an empty list of managers
            if(!managersByExamType.containsKey(manager.Exam_Type__c)) {
                managersByExamType.put(manager.Exam_Type__c, new List<ADA_AccommodationManager__mdt>());
            }
            
            managersByExamType.get(manager.Exam_Type__c).add(manager);
        }
        
        return managersByExamType;
    }
    
    /* 
# Created By: Dhana Prasad
# Created Date:07/02/2020
# Description: Method to create content document link for case contact related case of DA requet and CBT DA request cases,

---------------------------------------------------------------------------------------------------------------
# Modification information
- Modified By                 Modified Date               Description                        Associated Task (If)
# 
---------------------------------------------------------------------------------------------------------------
*/
    public static void createContentDocumentLinks(List<Case> caseList){
        Map<Id,Id> caseIdContactIdMap =  new  Map<Id,Id>();
        Map<Id,Id> contactIdCaseIdMap = new Map<Id,Id>();
        List<Case> relatedCaseList = new List<Case>();
        Id recordTypeIdForDARequest = CFA_RecordTypeUtility.getRecordTypeId(Case.getSObjectType(), Label.CFA_ADALabel, CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
        Id recordTypeIdForCBTDARequest = CFA_RecordTypeUtility.getRecordTypeId(Case.getSObjectType(), 'CBT DA Request' , CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
        Map<Id,Set<Id>> caseIdContentDocumentIdSetMap =  new Map<Id,Set<Id>>();
        List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink>();
        List<ContentDocumentLink> newContentDocumentLinks = new List<ContentDocumentLink>();
        for(Case newCase : caseList) {
            
            if(newCase.ContactId != null){
                
                caseIdContactIdMap.put(newCase.Id, newCase.ContactId);
            }
        }
        if(caseIdContactIdMap.size() != 0){
          
            relatedCaseList = new List<Case>([SELECT Id, ContactId  FROM Case WHERE Id NOT IN : caseIdContactIdMap.keySet() AND ContactId =: caseIdContactIdMap.values() AND (RecordTypeId =: recordTypeIdForDARequest OR RecordTypeId =: recordTypeIdForCBTDARequest)]);
        }
        
        if(!relatedCaseList.isEmpty()){
            
            for(Case relatedCase : relatedCaseList) {
                
                if(!caseIdContactIdMap.containsKey(relatedCase.ContactId)) {
                    
                    contactIdCaseIdMap.put(relatedCase.ContactId,relatedCase.Id);
                }
            }
        }
        
        
        if(contactIdCaseIdMap.size() != 0) {
           
            contentDocumentLinkList = new List<ContentDocumentLink>([SELECT Id, ContentDocumentId , LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId =: contactIdCaseIdMap.values()]);
            if(byPassQuery == true) {
                ContentVersion contentVersion = new ContentVersion(
                    Title = 'Penguins',
                    PathOnClient = 'Penguins.jpg',
                    VersionData = Blob.valueOf('Test Content'),
                    IsMajorVersion = true
                );
                insert contentVersion;    
                List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
                ContentDocumentLink newCdl = new ContentDocumentLink();
                newCdl.LinkedEntityId = contactIdCaseIdMap.values()[0];
                newCdl.ContentDocumentId = documents[0].Id;
                newCdl.shareType = 'V';
                contentDocumentLinkList.add(newCdl);
            }
        }
        
        if(!contentDocumentLinkList.isEmpty()) {
            
            for(ContentDocumentLink relatedContentDocumentLink : contentDocumentLinkList){
               
                if(!caseIdContentDocumentIdSetMap.containsKey(relatedContentDocumentLink.LinkedEntityId)) {
                   
                    caseIdContentDocumentIdSetMap.put(relatedContentDocumentLink.LinkedEntityId , new Set<Id>());
                }
                caseIdContentDocumentIdSetMap.get(relatedContentDocumentLink.LinkedEntityId).add(relatedContentDocumentLink.ContentDocumentId);
            } 
        }
        
        if(caseIdContentDocumentIdSetMap.size() != 0) {
           
            for(Case newCase : caseList) {
                
                Set<Id> contentDocumentIdSet = new Set<Id>();
                if(caseIdContentDocumentIdSetMap.get(contactIdCaseIdMap.get(newCase.ContactId)).size()!=0)
                contentDocumentIdSet = caseIdContentDocumentIdSetMap.get(contactIdCaseIdMap.get(newCase.ContactId));
                if(contentDocumentIdSet.size()!=0) {
                    
                    for (Id  contentDocumentId : contentDocumentIdSet) {
                       
                        ContentDocumentLink newCdl = new ContentDocumentLink();
                        newCdl.LinkedEntityId = newCase.Id;
                        newCdl.ContentDocumentId = contentDocumentId;
                        newCdl.shareType = 'V';
                        newContentDocumentLinks.add(newCdl);
                    }
                }
            }
        }
        
        if(newContentDocumentLinks.size()!=0) {
            insert newContentDocumentLinks;
        }
        
        
    }
}