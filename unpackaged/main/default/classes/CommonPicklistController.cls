public with sharing class CommonPicklistController {    
    @AuraEnabled
    public static List<picklistWrapper> getPicklistvalues(String objectName, String field_apiname,Boolean nullRequired){
        List<picklistWrapper> optionlist = new List<picklistWrapper>();       
        Map<String,Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Map<String, Schema.SObjectField> field_map = gd.get(objectName.toLowerCase()).getDescribe().fields.getMap();        
        List<Schema.PicklistEntry> picklistValues = field_map.get(field_apiname).getDescribe().getPickListValues();       
        if(nullRequired == true){
            picklistWrapper tempObj = new picklistWrapper();
            tempObj.label = '--None--';
            tempObj.value = '';
            optionlist.add(tempObj);
        }       
        for (Schema.PicklistEntry pv : picklistValues) {
            picklistWrapper tempWrpObj = new picklistWrapper();
            tempWrpObj.label = pv.getLabel();
            tempWrpObj.value = pv.getValue();
            optionlist.add(tempWrpObj);
        }
        return optionlist;
    }
    public class picklistWrapper{
        @AuraEnabled public string value{get;set;}
        @AuraEnabled public string label{get;set;}
    }
}