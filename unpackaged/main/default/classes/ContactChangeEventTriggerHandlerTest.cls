/*****************************************************************
Name: ContactChangeEventTriggerHandlerTest
Copyright © 2020 ITC
============================================================
Purpose: Test for ContactChangeEventTriggerHandler
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan               Create    Test for Apply Contact Society Campaigns Changes
2.0      Alona Zubenko   08.02.2021   Update    Update Tests, add NSA Policy in logic
3.0      Alona Zubenko   15.02.2021   Update    Update Tests, add creation of Relationship With Employer
4.0      Alona Zubenko   15.03.2021   Update    Update logic for creation Relationship With Employer
*****************************************************************/
@IsTest
private class ContactChangeEventTriggerHandlerTest {
    private static final Integer CONTACT_COUNT = 50;
    private static final String RECORD_TYPE_CFA_CONTACT = 'CFA Contact';
    private static final String ALL_CAMPAIGN_CHINA = 'CFA China All';
    private static final String ALL_CAMPAIGN_GERMANY = 'CFA Society Germany All';
    private static final String CONTACT_FIELD_API_NAME_CHINA = 'CFA_China__c';
    private static final String CONTACT_FIELD_API_NAME_GERMANY = 'CFA_Society_Germany__c';

    @TestSetup
    private static void setup() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        Account accEmployer1 = CFA_TestDataFactory.createAccount('Employer1', 'Curation', true);
        Account accEmployer2 = CFA_TestDataFactory.createAccount('Employer2', 'Curation', true);

        Campaign campChina = CFA_TestDataFactory.createCampaign(ALL_CAMPAIGN_CHINA,true);
        Campaign campGermany = CFA_TestDataFactory.createCampaign(ALL_CAMPAIGN_GERMANY,true);

        Map<String, Account> accountByNameMap = new Map<String, Account>{
                'CFA China' => CFA_TestDataFactory.createAccount('CFA China', 'Society', false)
                , 'CFA Germany' => CFA_TestDataFactory.createAccount('CFA Germany', 'Society', false)
        };
        insert accountByNameMap.values();

        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA China').Id, CONTACT_FIELD_API_NAME_CHINA, campChina.Id, null, null, false)
                , 'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Germany').Id, CONTACT_FIELD_API_NAME_GERMANY, campGermany.Id, null, null, false)
        };
        insert policyByNameMap.values();

        Contact con = CFA_TestDataFactory.createContact('Test', accountByNameMap.get('CFA China').Id,RECORD_TYPE_CFA_CONTACT,true);

        Test.enableChangeDataCapture();
    }
    
    @IsTest
    private static void getTriggerNameTest() {
        String result;
        Test.startTest();
            ContactChangeEventTriggerHandler handler = new ContactChangeEventTriggerHandler();
        	result = handler.getTriggerName();
        Test.stopTest();

        System.assertEquals('ContactChangeEventTrigger', result);
    }

    @IsTest
    private static void whenContactSocietyCheckboxIsChangedToTrueThenContactShouldBeAddedToCampaignTest() {
        Campaign allChinaCampaign = [SELECT Id FROM Campaign WHERE Name = :ALL_CAMPAIGN_CHINA LIMIT 1];

        List<Contact> conList = createDefContact(RECORD_TYPE_CFA_CONTACT, null, new List<String>());
        Test.startTest();
        for(Contact con : conList) {
            con.put(CONTACT_FIELD_API_NAME_CHINA,true);
        }
        update conList;
        Test.getEventBus().deliver();
        Test.stopTest();
        System.assertEquals(CONTACT_COUNT, [SELECT COUNT() FROM CampaignMember WHERE CampaignId = :allChinaCampaign.Id]);
    }

    @IsTest
    private static void whenContactSocietyCheckboxIsChangedToFalseThenContactShouldBeRemovedFromCampaignTest() {
        Campaign allChinaCampaign = [SELECT Id FROM Campaign WHERE Name = :ALL_CAMPAIGN_CHINA LIMIT 1];

        List<Contact>conList = createDefContact(RECORD_TYPE_CFA_CONTACT, null, new List<String>{CONTACT_FIELD_API_NAME_CHINA,CONTACT_FIELD_API_NAME_GERMANY});
        Test.getEventBus().deliver();

        Test.startTest();
        for(Contact con : conList) {
            con.put(CONTACT_FIELD_API_NAME_CHINA,false);
        }
        update conList;
        Test.getEventBus().deliver();
        Test.stopTest();
        System.assertEquals(0, [SELECT COUNT() FROM CampaignMember WHERE CampaignId = :allChinaCampaign.Id]);
    }

    @IsTest
    private static void createRelationshipWithEmployerNewRelationTest() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'Employer1' LIMIT 1];

        Test.startTest();
        List<Contact> conList = createDefContact(RECORD_TYPE_CFA_CONTACT, acc.Id, new List<String>());
        Test.getEventBus().deliver();

        Test.stopTest();

        System.assertEquals(CONTACT_COUNT, [
                SELECT COUNT()
                FROM AccountContactRelation
                WHERE ContactId = :(new Map<Id,Contact>(conList)).keySet()
                AND AccountId = :acc.Id
                AND IsActive = TRUE
                AND Roles = 'Employer'
        ]);
    }

    @IsTest
    private static void createRelationshipWithEmployerUpdateExistedRelationTest() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'Employer1' LIMIT 1];

        Test.startTest();
        List<Contact> conList = createDefContact(RECORD_TYPE_CFA_CONTACT, null, new List<String>());
        Test.getEventBus().deliver();

        for(Contact con : conList){
            con.Employer_Account__c = acc.Id;
        }

        update conList;
        Test.getEventBus().deliver();

        Test.stopTest();

        System.assertEquals(CONTACT_COUNT, [
                SELECT COUNT()
                FROM AccountContactRelation
                WHERE ContactId = :(new Map<Id,Contact>(conList)).keySet()
                AND AccountId = :acc.Id
                AND IsActive = TRUE
                AND Roles = 'Employer'
        ]);
    }

    @IsTest
    private static void createRelationshipWithEmployerUpdateOldCreateNewRelationTest() {
        Account acc1 = [SELECT Id FROM Account WHERE Name = 'Employer1' LIMIT 1];
        Account acc2 = [SELECT Id FROM Account WHERE Name = 'Employer2' LIMIT 1];

        Test.startTest();
        List<Contact> conList = createDefContact(RECORD_TYPE_CFA_CONTACT, acc1.Id, new List<String>());
        Test.getEventBus().deliver();

        System.assertEquals(CONTACT_COUNT, [
                SELECT COUNT()
                FROM AccountContactRelation
                WHERE ContactId = :(new Map<Id,Contact>(conList)).keySet()
                AND AccountId = :acc1.Id
                AND IsActive = TRUE
                AND Roles = 'Employer'
        ]);

        for(Contact con : conList){
            con.Employer_Account__c = acc2.Id;
        }

        update conList;
        Test.getEventBus().deliver();
        Test.stopTest();

        System.assertEquals(CONTACT_COUNT, [
                SELECT COUNT()
                FROM AccountContactRelation
                WHERE ContactId = :(new Map<Id,Contact>(conList)).keySet()
                AND AccountId = :acc2.Id
                AND IsActive = TRUE
                AND Roles = 'Employer'
        ]);
        System.assertEquals(CONTACT_COUNT, [
                SELECT COUNT()
                FROM AccountContactRelation
                WHERE ContactId = :(new Map<Id,Contact>(conList)).keySet()
                AND AccountId = :acc1.Id
                AND IsActive = FALSE
                AND Roles = 'Employer'
        ]);
    }

    @IsTest
    private static void createRelationshipWithEmployerUpdateExistedRelationToNullTest() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'Employer1' LIMIT 1];

        Test.startTest();
        List<Contact> conList = createDefContact(RECORD_TYPE_CFA_CONTACT, acc.Id, new List<String>());
        Test.getEventBus().deliver();

        for(Contact con : conList){
            con.Employer_Account__c = null;
        }

        update conList;
        Test.getEventBus().deliver();

        Test.stopTest();

        System.assertEquals(CONTACT_COUNT, [
                SELECT COUNT()
                FROM AccountContactRelation
                WHERE ContactId = :(new Map<Id,Contact>(conList)).keySet()
                AND AccountId = :acc.Id
                AND IsActive = FALSE
                AND Roles = 'Employer'
        ]);
    }

    @IsTest
    private static void createRelationshipWithEmployerUpdateExistedRelationToNull2Test() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'Employer1' LIMIT 1];

        Test.startTest();
        List<Contact> conList = createDefContact(RECORD_TYPE_CFA_CONTACT, acc.Id, new List<String>());
        Test.getEventBus().deliver();

        for(Contact con : conList){
            con.Employer_Account__c = null;
        }

        update conList;
        Test.getEventBus().deliver();

        for(Contact con : conList){
            con.Employer_Account__c = acc.Id;
        }

        update conList;
        Test.getEventBus().deliver();

        Test.stopTest();

        System.assertEquals(CONTACT_COUNT, [
                SELECT COUNT()
                FROM AccountContactRelation
                WHERE ContactId = :(new Map<Id,Contact>(conList)).keySet()
                AND AccountId = :acc.Id
                AND IsActive = TRUE
                AND Roles = 'Employer'
        ]);
    }

    private static List<Contact> createDefContact(String recordTypeName, Id employerAccountId, List<String> societyContactFieldNames) {
        Account acc = [SELECT Id FROM Account WHERE Name = 'CFA Institute' LIMIT 1];
        List<Contact> conList = CFA_TestDataFactory.createContactRecords(recordTypeName, 'TestContact', CONTACT_COUNT, false);
        for(Contact con : conList) {
            if(employerAccountId != null){
                con.Employer_Account__c = employerAccountId;
            }
            con.AccountId = acc.Id;
            for(String contactFieldName : societyContactFieldNames){
                con.put(contactFieldName,true);
            }
        }
        insert conList;
        return conList;
    }
}