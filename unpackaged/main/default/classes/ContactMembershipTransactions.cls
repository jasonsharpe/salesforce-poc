/**************************************************************************************************
* Apex Class Name   : ContactMembershipTransactions
* Purpose           : This class is used for making API Callout to BERT System to display read Membership Transactions
* Version           : 1.0 Initial Version
* Created Date      : 09-Sept-2018  
* Modified Date     : 28-Sept-2018; Lightning Team -- Vijay Vemuru and Geetha Chadipiralla 
***************************************************************************************************/

public class ContactMembershipTransactions {
    
    private String MembershipEP;
    private String contactId;
    private String cfaPersonID;
    public List<MembershipResponse> membershipResponse{get;set;}
    public PageReference getMembershipTransactions(){
    
        contactId = apexpages.currentPage().getparameters().get('contactId');
        cfaPersonID = apexpages.currentPage().getparameters().get('cfaPersonID');
        String customerId = apexpages.currentPage().getparameters().get('customerId');
        String orgPartnerId = apexpages.currentPage().getparameters().get('orgPartnerId');
        String orgtype = apexpages.currentPage().getparameters().get('orgtype');
        if(String.isnotblank(orgtype))
        {
            if(!orgtype.equalsignorecase('Society'))
            
            orgPartnerId = orgtype.substring(0,4);  
             
        }
        if(orgPartnerId == null || orgPartnerId == '')
        orgPartnerId = Label.CFAOrganizationPartnerID;
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();

        Http http = new Http();
        MembershipEP = System.Label.CustomerCareMembershipLink;
        request.setEndpoint(MembershipEP+'/'+customerId+'/history/'+orgPartnerId);
        
        request.setClientCertificateName(Label.MDM_Swagger_Certificate);
        request.setMethod('GET');
        response = http.send(request);
        System.debug('Response'+ MembershipEP+'/'+customerId+'/history/'+orgPartnerId);
        membershipResponse = (List<MembershipResponse>)JSON.deserialize(response.getBody(), List<MembershipResponse>.class);
        return null;
    }
    
    public PageReference goBack(){
        PageReference pg = new PageReference('/apex/CustomerCare_ContactTabs?id='+contactId+'&cfaPersonID='+cfaPersonID+'&currentTab=Membership&tabName=Membership');
        pg.setRedirect(true);
        return pg;
    }
    
    public class MembershipResponse{
        public String TransactionDate{get;set;}
        public String MemberYear{get;set;}
        public String TransactionTypeDisplayName{get;set;}
        public String ReasonCode{get;set;}
        public String SalesOrderId{get;set;}
        public String WasAffiliate{get;set;}
        public String WasOnProfessionalLeave{get;set;}
    }
}