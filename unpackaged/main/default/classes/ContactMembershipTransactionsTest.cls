@isTest
public class ContactMembershipTransactionsTest {
    @testSetup public static void setup(){
        Account defAcct = new Account();
        defAcct.Name = 'Geetha';
        defAcct.CurrencyIsoCode = 'AED';
        defAcct.type = 'Default';
        insert defAcct;
    }
    @isTest
    public static void goBackTest(){
        ContactMembershipTransactions contactMembershipTransactions = new ContactMembershipTransactions(); 
        contactMembershipTransactions.goBack();
    }
    @isTest
    public static void getMembershipTransactionsTest(){
        ContactMembershipTransactions contactMembershipTransactions = new ContactMembershipTransactions();
        Contact cnt = new Contact();
        cnt.FirstName = 'Geetha';
        cnt.LastName = 'Ch';
        // cnt.Person_Id__c = '387796';
        insert cnt;
        ApexPages.currentPage().getParameters().put('contactId', String.valueOf(cnt.Id));
        ApexPages.currentPage().getParameters().put('cfaPersonID', String.valueOf('387796'));
        ApexPages.currentPage().getParameters().put('customerId', String.valueOf('387796'));
        ApexPages.currentPage().getParameters().put('orgPartnerId', String.valueOf(''));
        try{
            contactMembershipTransactions.getMembershipTransactions();
        }
        catch(Exception e){
            
        }
    }
    
    @isTest
    public static void membershipResponseTest(){
        ContactMembershipTransactions contactMembershipTransactions = new ContactMembershipTransactions(); 
        ContactMembershipTransactions.MembershipResponse mr = new ContactMembershipTransactions.MembershipResponse();
        contactMembershipTransactions.membershipResponse = null;
        mr.TransactionDate = '';
        mr.MemberYear = '';
        mr.TransactionTypeDisplayName = '';
        mr.ReasonCode = '';
        mr.SalesOrderId = '';
        mr.WasAffiliate = '';
        mr.WasOnProfessionalLeave = '';
        
    }
}