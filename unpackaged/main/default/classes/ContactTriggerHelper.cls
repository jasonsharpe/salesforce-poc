/*****************************************************************
Name: ContactTriggerHelper
Copyright © 2021 ITC
============================================================
Purpose: Helper for Contact Trigger
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   29.01.2021   Created   Logic for Assigning Contacts To Societies By Address
*****************************************************************/

public with sharing class ContactTriggerHelper {
    private final static Id CFA_CONTACT_RECORD_TYPE_ID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CFA_Contact').getRecordTypeId();
    private final static Id LOCAL_MEMBER_RECORD_TYPE_ID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Local_Member').getRecordTypeId();
    private final static String CFA_INSTITUTE = 'CFA Institute';
    private final static String SOCIETY_STAFF_PERMISSION_SET_GROUP = 'Society_Staff_Permission_Set_Group';
    private final static Map<String,String> CVENT_FIELDS = new Map<String, String>{
            'FirstName' => 'First Name'
            ,'LastName' => 'Last Name'
            ,'Email' => 'Email Address'
            ,'Employer_Name__c' => 'Company'
            ,'MDM_Job_Title__c' => 'Title'
            ,'Phone' => 'Home Phone'
            ,'Person_Id__c' => 'CFA Institute Person ID'
            , 'HasOptedOutOfEmail' => 'CFA Institute Do Not Email'
            ,'Is_CFA_Charter_Holder__c' => 'CFA Charterholder'
            , 'CFA_Program_Candidate__c' => 'CFA Program Candidate'
            ,'CFA_Institute_Member__c' => 'CFA Institute Member'
            ,'CFA_Charter_Award_Date__c' => 'Charter Award Date'
            ,'CFA_Institute_Designations__c' => 'CFA Institute Designations'
            ,'Society_Member__c' => 'Society Member'
            , 'Campaign_Relations__c' => 'Campaign Relations'
    };

    /**
     * @description Assign Contacts To Societies By Address
     *
     * @param oldContacts List<Contact>
     * @param newContacts List<Contact>
     */
    public static void assignContactsToSocietiesByPolicies(List<Contact> oldContacts, List<Contact> newContacts) {
        try {
            Boolean isInsert = false;
            if (oldContacts == null) {
                isInsert = true;
            }
            List<Contact> contactsWithNewAddress = findContactsWithNewAddress(oldContacts, newContacts);
            if (!contactsWithNewAddress.isEmpty()) {
                if (!isInsert) {
                    clearExistingCFAFlags(contactsWithNewAddress);
                }
                List<NSA_Policy__c> policiesByCountryAndState = getNSAPolicies(contactsWithNewAddress);
                NSAPolicyRunner policyRunner = new NSAPolicyRunner();
                if (!policiesByCountryAndState.isEmpty()) {
                    policyRunner = new NSAPolicyRunner(policiesByCountryAndState);
                }
                if (isInsert) {
                    policyRunner.run(contactsWithNewAddress, new List<INSAPolicyHandler>{
                            new AddressPolicyHandler()
                    }, false);
                } else {
                    policyRunner.run(contactsWithNewAddress, new List<INSAPolicyHandler>{
                            new AddressPolicyHandler(), new MembershipPolicyHandler(), new MembershipApplicationPolicyHandler()
                    }, false);
                }
            }
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'ContactTriggerHelper', 'Assign Contacts To Societies By Address', Datetime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }

    /**
     * @description Handle Deceased Contacts
     *
     * @param newContacts List<Contact>
     */
    public static void handleDeceasedContacts(List<Contact> newContacts) {
        (
                new NSAPolicyRunner(
                [
                        SELECT Advanced_Assignment__c, Advanced_Assignment_Handler__c, Contact_Society_Field_API_Name__c
                        FROM NSA_Policy__c
                        WHERE Is_Active__c = true
                        AND Policy_Status__c = 'Approved'
                ]
                )
        ).run(newContacts, new List<INSAPolicyHandler>{
                new DeceasedPolicyHandler()
        }, false);
    }

    /**
     * Handle community permissions for community profile users
     *
     * @param newItems Map<ID, Contact> from trigger.newMap
     * @param oldItems Map<ID, Contact> from trigger.oldMap
     */
    public static void handleCommunityPermissions(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, Contact> newItemsMap = new Map<Id, Contact>();
        for (contact contact : (List<Contact>) newItems.values()) {
            if (contact.RecordTypeId == CFA_CONTACT_RECORD_TYPE_ID) {
                newItemsMap.put(contact.Id, contact);
            }
        }
        Id communityProfId = [SELECT Id, Name FROM Profile WHERE Name = 'CFA_Customer_Community' LIMIT 1].Id;
        List<User> communityUsers = new List<User>();
        Map<Id, User> contactUserMap = new Map<Id, User>();
        List<User> usersToClean = new List<User>();

        for (User currentUser : [SELECT Id, ContactId, ProfileId, Provision_Self_Service__c FROM User WHERE ContactId IN :newItems.keySet()]) {
            contactUserMap.put(currentUser.ContactId, currentUser);
        }
        List<Contact> allContacts = newItemsMap.values();
        for (Contact currentContact : allContacts) {
            Contact oldContact = (Contact) oldItems.get(currentContact.Id);
            User currentUser = contactUserMap.get(currentContact.Id);
            Boolean requiredFieldChange = false;
            Boolean needsPermissionSetClean = false;
            if (currentUser != null) {
                if (currentContact.CFA_Program_Candidate__c != oldContact.CFA_Program_Candidate__c) {
                    requiredFieldChange = true;
                    if (currentContact.CFA_Program_Candidate__c != true) {
                        needsPermissionSetClean = true;
                    }
                }
                if (currentContact.CFA_Institute_Member__c != oldContact.CFA_Institute_Member__c) {
                    requiredFieldChange = true;
                    if (currentContact.CFA_Institute_Member__c != true) {
                        needsPermissionSetClean = true;
                    }
                }
            }
            if (requiredFieldChange == true && currentUser.ProfileId == communityProfId) {
                communityUsers.add(currentUser);
            }
            if (needsPermissionSetClean == true && currentUser.ProfileId == communityProfId) {
                usersToClean.add(currentUser);
            }
        }
        if (communityUsers.size() > 0) {
            CFA_CommunityuserTriggerLogic.handleSelfServicePermissionSets(communityUsers);
        }
        if (usersToClean.size() > 0) {
            CFA_CommunityuserTriggerLogic.cleanPermissionSets(usersToClean);
        }
    }

    public static void handleUndelete(Map<Id, SObject> newMap) {
        List<Contact> contactList = new List<Contact>();
        contactList = getContactForPlatFormEvent(newMap.values());
        createCventEvents(contactList);
    }

    public static void contactCventCall(List<Contact> newContactList, Map<Id, Contact> oldContactMap) {
        List<Contact> contacts = new List<Contact>();
        List<Contact> contactList = new List<Contact>();

        try {
            Contact_Cvent_Trigger__mdt cctMTD = [SELECT Id, DeveloperName, Contact_Field_Name__c FROM Contact_Cvent_Trigger__mdt LIMIT 1];
            // Iterate over contact
            for (Contact con : newContactList) {

                // Check for record type(CFA or Local Contact)
                if (con.RecordTypeId == CFA_CONTACT_RECORD_TYPE_ID || con.RecordTypeId == LOCAL_MEMBER_RECORD_TYPE_ID) {
                    for (String fld : cctMTD.Contact_Field_Name__c.split(',')) {
                        if (con.get(fld.trim()) != oldContactMap.get(con.Id).get(fld.trim())) {
                            contacts.add(con);
                            break;
                        }
                    }
                }
            }
			System.debug(contacts);
            if (!contacts.isEmpty()) {
                contactList = getContactForPlatFormEvent(contacts);
                System.debug(contactList);
                createCventEvents(contactList);
            }
        } catch (Exception ex) {
            //    Message when any exception
            String msg = 'contactCventCall method exception = ' + ex.getMessage() + ', # ' + ex.getLineNumber();
            cfa_ApexUtilities.log(msg);
        }
    }


    /**
     * Updates society checkboxes on local contacts. No dml performed
     *
     * @param newItems List<SObject> from trigger.newMap
     */
    public static void handleLocalContacts(List<SObject> newItems) {
        Set<Id> accIds = new Set<Id>();
        for (Contact contact : (List<Contact>) newItems) {
            if (contact.RecordTypeId == LOCAL_MEMBER_RECORD_TYPE_ID && contact.AccountId != null) {
                accIds.add(contact.AccountId);
            }
        }
        if (!accIds.isEmpty()) {
            Map<Id, List<NSA_Policy__c>> mapAccIdToPolicies = NSAPolicyService.groupPoliciesByAccountId(NSAPolicyService.getPoliciesByAccIds(accIds));
            Set<String> societyFieldsApiNames = NSAPolicyService.getContactSocietyFieldAPINamesFromPolicies();
            for (Contact contact : (List<Contact>) newItems) {
                if (contact.RecordTypeId == LOCAL_MEMBER_RECORD_TYPE_ID && contact.AccountId != null) {
                    for (String fieldName : societyFieldsApiNames) {
                        if (!String.isBlank(fieldName)) {
                            contact.put(fieldName, false);
                        }
                    }
                    List<NSA_Policy__c> accPolicies = mapAccIdToPolicies.get(contact.AccountId);
                    if (accPolicies != null && !accPolicies.isEmpty()) {
                        NSAPolicyService.changeContactCFACheckboxes(contact, accPolicies, true);
                    }
                }
            }
        }
    }

    /* *********************** HELPERS ************************ */

    /**
     * @description Create Cvent Event
     *
     * @param contacts List<Contact>
     */
    private static void createCventEvents(List<Contact> contacts){
        ACCC_Log_Event__e event = new ACCC_Log_Event__e();
        event.Total_Number__c = 1;
        event.MessageId__c = EncodingUtil.convertToHex(Crypto.generateAesKey(128));
        event.UserId__c =  UserInfo.getUserId();
        event.Action__c =  'ContactLogEntry';
        event.Object_Name__c =  'Contact';
        event.Version__c =  '1.0.0.0';
        event.TransactionId__c =  EncodingUtil.convertToHex(Crypto.generateAesKey(128));
        event.Payload__c =  createPayloads(contacts);
        event.Exception__c =  null;
        event.DateTime__c =  System.now();
        event.Sequence_Number__c =  1;
        event.External_Id__c =  EncodingUtil.convertToHex(Crypto.generateAesKey(128));

        try {
            EventBus.publish(event);
        } catch (Exception ex){
            insert CFA_IntegrationLogException.logError(ex, 'ContactTriggerHelper', 'Create Cvent Events', DateTime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }

    /**
     * @description Create Payloads
     *
     * @param items List<SObject>
     *
     * @return String
     */
    public static String createPayloads(List<SObject> items){
        JSONGenerator jsGen = JSON.createGenerator(true);
        jsGen.writeStartObject();
        jsGen.writeFieldName('Payload');
        jsGen.writeStartArray();

        for(SObject item : items) {
            jsGen.writeStartObject();

            for (String apiName : new List<String>(CVENT_FIELDS.keySet())) {
                String name = CVENT_FIELDS.get(apiName);
                Object data = (Object) item.get(apiName);

                if (data == null) {
                    jsGen.writeNullField(name);
                    break;
                }

                if (data instanceof String) {
                    jsGen.writeStringField(name, String.valueOf(data));
                } else if (data instanceof Boolean) {
                    jsGen.writeBooleanField(name, Boolean.valueOf(data));
                } else if (data instanceof Integer) {
                    jsGen.writeNumberField(name, ((Decimal) data).intValue());
                } else if (data instanceof Long) {
                    jsGen.writeNumberField(name, ((Decimal) data).longValue());
                } else if (data instanceof Double) {
                    jsGen.writeNumberField(name, ((Decimal) data).doubleValue());
                } else if (data instanceof Date) {
                    jsGen.writeDateField(name, Date.valueOf(data));
                } else if (data instanceof Datetime) {
                    if (name == 'Charter Award Date') {
                        jsGen.writeDateField(name, Date.valueOf(data));
                    } else {
                        jsGen.writeDateTimeField(name, Datetime.valueOf(data));
                    }
                } else if (data instanceof Id) {
                    jsGen.writeStringField(name, String.valueOf(data));
                } else {
                    jsGen.writeStringField(name, String.valueOf(data));
                }
            }

            jsGen.writeEndObject();
        }
        jsGen.writeEndArray();
        jsGen.writeEndObject();

        return jsGen.getAsString().replace('\n','');
    }

    /**
     * @description  Get Contact Information on the basis of Contacts
     *
     * @param contacts List<Contact>
     *
     * @return  List<Contact>
     */
    private static List<Contact> getContactForPlatFormEvent(List<Contact> contacts) {
        Set<Id> societyConId = new Set<Id>();
        Map<String, Set<Id>> mapSocietyFNameToConIds = new Map<String, Set<Id>>();
        Set<String> cfaFieldNames = NSAPolicyService.getContactSocietyFieldAPINamesFromPolicies();
        Set<String> trueCfaFieldNames = new Set<String>();
        for (Contact contact : contacts) {
            for (String fieldName : cfaFieldNames) {
                if (contact.get(fieldName) instanceof Boolean && (Boolean) contact.get(fieldName)) {
                    trueCfaFieldNames.add(fieldName);
                    Set<Id> contactIds = mapSocietyFNameToConIds.get(fieldName);
                    if (contactIds == null) {
                        contactIds = new Set<Id>();
                    }
                    contactIds.add(contact.Id);
                    mapSocietyFNameToConIds.put(fieldName, contactIds);
                }
            }
        }
        Map<Id, String> mapAccIdToSocietyField = new Map<Id, String>();
        if (!trueCfaFieldNames.isEmpty()) {
            for (NSA_Policy__c policy : [SELECT Society_Account__c,Contact_Society_Field_API_Name__c FROM NSA_Policy__c WHERE Contact_Society_Field_API_Name__c IN :trueCfaFieldNames AND is_Active__c = true AND Policy_Status__c = 'Approved']) {
                mapAccIdToSocietyField.put(policy.Society_Account__c, policy.Contact_Society_Field_API_Name__c);
            }
        }

        for (Account acc : [SELECT Id, Name, Cvent_User__c FROM Account WHERE ID IN :mapAccIdToSocietyField.keySet() AND Cvent_User__c = true]) {
            Set<Id> contactIds = mapSocietyFNameToConIds.get(mapAccIdToSocietyField.get(acc.Id));
            if (contactIds != null && !contactIds.isEmpty()) {
                societyConId.addAll(contactIds);
            }
        }

        List<Contact> conList = new List<Contact>();

        if (!societyConId.isEmpty()) {
            String query = 'SELECT Id, ' + String.join(new List<String>(CVENT_FIELDS.keySet()),',') + ' FROM Contact WHERE Id = :societyConId';
            conList = (List<Contact>) Database.query(query);

        }

        return conList;
    }

    /**
     * @description Set Temp Ids To Records
     *
     * @param newContacts List<Contact>
     */
    public static void setTempIdsToRecords(List<Contact> newContacts) {
        Set<String> usedRandomValue = new Set<String>();
        for (Contact con : newContacts) {
            String randomVal = createTempId();
            while (usedRandomValue.contains(randomVal)) {
                randomVal = createTempId();
            }
            usedRandomValue.add(randomVal);

            con.Id = randomVal;
        }
    }

    /**
     * @description Remove Temp Ids To Records
     *
     * @param newContacts List<Contact>
     */
    public static void removeTempIdsToRecords(List<Contact> newContacts) {
        for (Contact con : newContacts) {
            con.Id = null;
        }
    }

    /**
     * @description Get contacts that this user has an access to edit
     *
     * @param oldContacts List<Contact>
     * @param newContacts List<Contact>
     *
     * @return ContactTriggerHelper.ContactListWrapper
     */
    public static ContactTriggerHelper.ContactListWrapper getContactsWithEditAccess(List<Contact> oldContacts, Map<Id, Contact> newContacts) {
        ContactTriggerHelper.ContactListWrapper wrapper = new ContactTriggerHelper.ContactListWrapper();
        List<PermissionSetAssignment> permissionSetAssignments = [
                SELECT Id
                FROM PermissionSetAssignment
                WHERE AssigneeId = :UserInfo.getUserId()
                AND PermissionSetGroup.DeveloperName =  :SOCIETY_STAFF_PERMISSION_SET_GROUP
        ];

        if (permissionSetAssignments.size() == 0) {
            wrapper.oldValues = oldContacts;
            wrapper.newValues = newContacts.values();
            return wrapper;
        }

        List<Contact> contactsForEdit = new List<Contact>();
        List<Id> contactIdsWithAccessError = new List<Id>();
        for (Contact con : oldContacts) {
            if (con.RecordTypeId != CFA_CONTACT_RECORD_TYPE_ID) {
                contactsForEdit.add(con);
            } else {
                contactIdsWithAccessError.add(con.Id);
            }
        }
        wrapper.oldValues = contactsForEdit;
        wrapper.newValues = new List<Contact>();

        for(Id conId : contactIdsWithAccessError){
            newContacts.get(conId).addError(Label.SF_Society_Staff_NotHaveAccessEditContact);
        }

        for (Contact con : contactsForEdit) {
            wrapper.newValues.add(newContacts.get(con.Id));
        }

        return wrapper;
    }


    /**
     * @description Create Temp Id
     *
     * @return Id
     */
    private static Id createTempId() {
        final String chars = '123456789ABCecwDEFGH';
        String tempId = '003010000';
        while (tempId.length() < 15) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            tempId += chars.substring(idx, idx + 1);
        }
        return Id.valueOf(tempId);
    }

    /**
     * @description Find Contacts With New Address
     *
     * @param oldContacts List<Contact>
     * @param newContacts List<Contact>
     *
     * @return List<Contact>
     */
    private static List<Contact> findContactsWithNewAddress(List<Contact> oldContacts, List<Contact> newContacts) {
        List<Contact> contactsWithNewAddress = new List<Contact>();
        Map<Id, Contact> oldContactMap;
        if (oldContacts != null) {
            oldContactMap = new Map<Id, Contact>(oldContacts);
        }
        for (Contact con : newContacts) {
            if (con.RecordTypeId == CFA_CONTACT_RECORD_TYPE_ID) {
                if (oldContacts == null || oldContacts.isEmpty()) {
                    contactsWithNewAddress.add(con);
                } else {
                    if (con.Country_ISO_Code__c != oldContactMap.get(con.Id).Country_ISO_Code__c
                            || con.State_Province_ISO_Code__c != oldContactMap.get(con.Id).State_Province_ISO_Code__c
                            || con.MailingPostalCode != oldContactMap.get(con.Id).MailingPostalCode) {
                        contactsWithNewAddress.add(con);
                    }
                }
            }
        }
        return contactsWithNewAddress;
    }

    /**
     * @description Get NSA Policies
     *
     * @param changedContacts List<Contact>
     *
     * @return List<NSA_Policy__c>
     */
    private static List<NSA_Policy__c> getNSAPolicies(List<Contact> changedContacts) {
        Set<String> states = new Set<String>();
        Set<String> countries = new Set<String>();
        for (Contact con : changedContacts) {
            states.add(con.State_Province_ISO_Code__c);
            countries.add(con.Country_ISO_Code__c);
        }
        // find all policies by country and state
        List<NSA_Policy__c> policies = [
                SELECT Id
                        , Advanced_Assignment__c
                        , Advanced_Assignment_Handler__c
                        , Society_Account__c
                        , Contact_Society_Field_API_Name__c
                        , (
                        SELECT Country_Name__c
                                , State__c
                                , Zip_Code_High__c
                                , Zip_Code_Low__c
                                , NSA_Policy__c
                        FROM NSA_Geograhpic_Rules__r
                )
                FROM NSA_Policy__c
                WHERE Id IN (
                        SELECT NSA_Policy__c
                        FROM NSA_Geographic_Rule__c
                )
                AND Is_Active__c = TRUE
                AND Policy_Status__c = 'Approved'
        ];
        return policies;
    }

    /**
     * @description Set all Society fields on Contact as False
     *
     * @param changedContacts List<Contact>
     */
    private static void clearExistingCFAFlags(List<Contact> changedContacts) {
        List<NSA_Policy__c> policies = [SELECT Contact_Society_Field_API_Name__c FROM NSA_Policy__c WHERE is_Active__c = true AND Policy_Status__c = 'Approved'];
        Set<String> societyFieldNames = new Set<String>();

        for (NSA_Policy__c policy : policies) {
            societyFieldNames.add(policy.Contact_Society_Field_API_Name__c);
        }

        for (Contact con : changedContacts) {
            for (String societyField : societyFieldNames) {
                con.put(societyField, false);
            }
        }
    }

    /**
     * @description Get CFA Institute Account Id
     *
     * @return String
     */
    public static String getCFAInstituteAccountId() {
        List<Account> cfaInstituteAccounts;
        if (Test.isRunningTest()) {
            cfaInstituteAccounts = [SELECT Id FROM Account WHERE Name = :CFA_INSTITUTE] ;
        } else {
            cfaInstituteAccounts = [SELECT Id FROM Account WHERE Partner_Id__c = :Label.CFA_Institute] ;
        }

        if (cfaInstituteAccounts != null && !cfaInstituteAccounts.isEmpty()) {
            return cfaInstituteAccounts[0].Id;
        }
        return null;
    }
    /**
     * Copies contact fields if source field not empty. No dml performed.
     *
     * @param   newList from trigger
     */
    public static void copyContactFields(List<Sobject> newList) {
        for (Contact contact : (List<Contact>) newList) {
            if (contact.RecordTypeId == CFA_CONTACT_RECORD_TYPE_ID) {
                contact.CFAMN__PersonID__c = contact.Person_Id__c;
                contact.DonorApi__Suffix__c = contact.Suffix;
                contact.CFAMN__Gender__c = contact.Gender__c;
                contact.CFAMN__Employer__c = contact.Employer_Name__c;
                contact.CFAMN__CFAClassCodeList__c = contact.CFA_Class_Code__c;
                contact.CFAMN__CFACandidateCode__c = contact.CFA_Candidate_Code__c;
                contact.CFAMN__CFAMemberCode__c = contact.CFA_Member_Code__c;
                contact.CFAMN__CharterAwardDate__c = contact.CFA_Charter_Award_Date__c?.dateGMT();
                contact.CFAMN__ClaritasCertificateAwardDate__c = contact.Investment_Foundations_Certificate_Award__c?.dateGMT();
                contact.CFAMN__ClaritasCandidateCode__c = contact.Investment_Foundations_Candidate_Code__c;
                contact.CFAMN__CIPMMemberCode__c = contact.CIPM_Member_Code__c;
                contact.CFAMN__CIPMCandidateCode__c = contact.CIPM_Candidate_Code__c;
                contact.CFAMN__CIPMCertificateDate__c = contact.CIPM_Certificate_Award_Date__c?.dateGMT();
                contact.CFAMN__CFAPREP__c = contact.CFA_Program_Prep_Allow_Contact__c;
                contact.CFAMN__NMAIL__c = contact.HasOptedOutOfMail__c;
                contact.CFAMN__NEML__c = contact.HasOptedOutOfEmail;
                contact.CFAMN__CLASOC__c = contact.Investment_Foundations_Candidate_SOC__c;
                contact.CFA__c = contact.Is_CFA_Charter_Holder__c;
                contact.CIPM__c = contact.Is_CIPM_Certificate_Holder__c;
            }
        }
    }

    /**
 * Updates contact AccountId field to CFA Institute account for
 * CFA_Contact record type
 *
 * @param   newList from trigger
 */
    public static void linkCFAContactsToCFAInsituteAccount(List<Sobject> newList) {
        String cfaInstituteAccountId = getCFAInstituteAccountId();
        if (!String.isEmpty(cfaInstituteAccountId)) {
            for (Contact contact : (List<Contact>) newList) {
                if (contact.RecordTypeId == CFA_CONTACT_RECORD_TYPE_ID) {
                    contact.AccountId = cfaInstituteAccountId;
                }
            }
        }
    }

    /**
 * Copies contact fields if source field not empty. No dml performed.
 *
 * @param   newList from trigger
 */
    public static void copyContactFields2(List<Sobject> newList) {
        for (Contact contact : (List<Contact>) newList) {
            if (contact.RecordTypeId == CFA_CONTACT_RECORD_TYPE_ID) {
                contact.Person_Id__c = contact.CFAMN__PersonID__c;
                contact.Suffix = contact.DonorApi__Suffix__c;
                contact.Gender__c = contact.CFAMN__Gender__c;
                contact.Employer_Name__c = contact.CFAMN__Employer__c;
                contact.CFA_Class_Code__c = contact.CFAMN__CFAClassCodeList__c;
                contact.CFA_Candidate_Code__c = contact.CFAMN__CFACandidateCode__c;
                contact.CFA_Member_Code__c = contact.CFAMN__CFAMemberCode__c;
                if (contact.CFAMN__CharterAwardDate__c != null) {
                    contact.CFA_Charter_Award_Date__c = DateTime.newInstanceGMT(contact.CFAMN__CharterAwardDate__c, Time.newInstance(0, 0, 0, 0));
                }
                if (contact.CFAMN__ClaritasCertificateAwardDate__c != null) {
                    contact.Investment_Foundations_Certificate_Award__c = DateTime.newInstanceGMT(contact.CFAMN__ClaritasCertificateAwardDate__c, Time.newInstance(0, 0, 0, 0));
                }
                contact.Investment_Foundations_Candidate_Code__c = contact.CFAMN__ClaritasCandidateCode__c;
                contact.CIPM_Member_Code__c = contact.CFAMN__CIPMMemberCode__c;
                contact.CIPM_Candidate_Code__c = contact.CFAMN__CIPMCandidateCode__c;
                if (contact.CFAMN__CIPMCertificateDate__c != null) {
                    contact.CIPM_Certificate_Award_Date__c = DateTime.newInstanceGMT(contact.CFAMN__CIPMCertificateDate__c, Time.newInstance(0, 0, 0, 0));
                }
                contact.CFA_Program_Prep_Allow_Contact__c = contact.CFAMN__CFAPREP__c;
                contact.HasOptedOutOfMail__c = contact.CFAMN__NMAIL__c;
                contact.HasOptedOutOfEmail = contact.CFAMN__NEML__c;
                contact.Investment_Foundations_Candidate_SOC__c = contact.CFAMN__CLASOC__c;
                contact.Is_CFA_Charter_Holder__c = contact.CFA__c;
                contact.Is_CIPM_Certificate_Holder__c = contact.CIPM__c;
            }
        }
    }

    public class ContactListWrapper {
        public List<Contact> oldValues { set; get; }
        public List<Contact> newValues { set; get; }
    }
}