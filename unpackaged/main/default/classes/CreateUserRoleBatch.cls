global class CreateUserRoleBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([select Id, Name from Account WHERE CreatedDate = TODAY AND HOUR_IN_DAY(CreatedDate) > 1]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts) {
        List<UserRole> userRoleLst = new List<UserRole>();
        UserRole role = new UserRole();
        for(Account acct :accounts){
            role = createUserRole(acct);
            if(role != null)
            userRoleLst.add(role);
        }
        try{
        upsert userRoleLst;
        }
        catch(Exception e){
            
        }
        
    }
    
    global void finish(Database.BatchableContext bc) {
        // send out an email or something to the user
    }
    
     private UserRole createUserRole(Account account){

        Integer roleCnt = [Select count() from UserRole where PortalAccountId = :account.Id];
        UserRole role;
        //Execute create logic if less than 3 roles exist as we can't add 4th role and system will throw an error
        if(roleCnt < 1) {

            role = new UserRole();
           // role.Name = account.Name+' Partner User';
           	role.portalType = 'Partner'; // use 'CustomerPortal' for customer portal roles
            role.PortalAccountId = account.Id;

            role.CaseAccessForAccountOwner = 'Edit'; //Modify as needed
            role.OpportunityAccessForAccountOwner = 'Read'; //Modify as needed

            
            //if(!Test.isRunningTest())
           // insert role;
        }
         return role;
    }
    
}