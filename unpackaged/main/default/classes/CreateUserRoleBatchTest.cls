@isTest
public class CreateUserRoleBatchTest {
    @testSetup public static void setup(){
    Account defAcct = new Account();
        defAcct.Name = 'Geetha';
        defAcct.CurrencyIsoCode = 'AED';
        defAcct.type = 'Default';
        insert defAcct;
    }
    @isTest
    public static void batchTest(){
        List<Account> lstAccount= new List<Account>();
        List<Contact> lstContact = new List<Contact>();
        for(Integer i=0 ;i <20;i++) {
            Account acc = new Account();
            acc.Name ='Name'+i;
            acc.Type = 'Default';
            acc.OwnerId = UserInfo.getUserId();
            lstAccount.add(acc);
        }
        insert lstAccount;
        for(Integer i=0 ;i <20;i++) {
            Contact cnt = new Contact();
            cnt.LastName = 'Name'+i;
            cnt.AccountId = lstAccount[i].Id;
            lstContact.add(cnt);
        }
        //insert lstContact;
        Account defAcct = new Account();
        defAcct.Name = 'Geetha';
        defAcct.CurrencyIsoCode = 'AED';
        defAcct.type = 'Default';
        insert defAcct;
        Test.startTest();
            System.debug(lstAccount);
            CreateUserRoleBatch obj = new CreateUserRoleBatch();
        try{
            DataBase.executeBatch(obj); 
        }
        catch(Exception e){
            System.debug('Exception'+e);
        }
        Test.stopTest();
    }
}