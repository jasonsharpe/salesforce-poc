/**************************************************************************************************
* Apex Class Name   : CustomerCareChangeRecordType
* Purpose           : This class allows the Case Owner and Supervisor profile users to change case record type   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 08-Feb-2018  
***************************************************************************************************/

global without sharing class CustomerCareChangeRecordType{
    /*This method used to check profile of current user is CustomerCare Supervisor*/
    global ID currentCaseID;
    global static String queuename;
    global static String defaultQueueId;
    global static string cfa_RT;
    global static String message {get;set;}
    global static String errorMsg{get; set;}
    global static Map<String,ID> queueMap = new Map<String,ID>();
    global static Map<String,ID> caseRecordTypeMap = new Map<String,ID>();
    global static List<Case> caseupdateLst = new List<Case>();
    
    /* This method check current user details if user is case owner or Supervisor profile users and returns True*/ 
    webservice static String validateUser(List<id> caseids){ 
        
        List<Case> caseOwnerLst = [Select ID,Status,Area__c,Sub_Area__c,RecordTypeId,RecordType.Name,RecordType.DeveloperName,ByPassAreaValidation__c,OwnerId from Case where ID IN :caseids];
        Id loggedInUserProfileId = UserInfo.getProfileId();
        Id currentOwnerId = UserInfo.getUserId();
        System.debug('#currentOwnerId##'+currentOwnerId+'#####caseOwnerLst[0].OwnerId'+caseOwnerLst[0].OwnerId);
        Profile profile;
        if(loggedInUserProfileId!=null){
            profile = [Select Name from Profile where Id=:loggedInUserProfileId limit 1];
        }
        String profileName;
        if(profile!=null)
            profileName=profile.Name;
        
        if(!caseOwnerLst.isEmpty()){
            if(profileName==CustomerCareUtility.supervisorProfile || currentOwnerId == caseOwnerLst[0].OwnerId){
                return 'True';
            }else{
                return 'False';
            }
        }
            return '';
    }
    
   /* This method is used to check case recordtype id*/ 
   webservice static String fetchRecordType(List<id> caseids){ 
      List<Case> caseLst = [Select ID,Status,Area__c,Sub_Area__c,RecordTypeId,RecordType.Name,RecordType.DeveloperName,ByPassAreaValidation__c from Case where ID IN :caseids]; 
       if(!caseLst.isEmpty()){
            if(caseLst[0].RecordType.DeveloperName == CustomerCareUtility.FrontofficeRtdevelopername){
                return CustomerCareUtility.FrontofficeRtdevelopername;
            }else if (caseLst[0].RecordType.DeveloperName == CustomerCareUtility.BackofficeRtdevelopername){
                return CustomerCareUtility.BackofficeRtdevelopername;
            } 
       }
       return '';
   }  
   
    /* This method is used to change case recordtype id*/  
    webservice static String switchRecordType(List<id> caseids){ 
        queuename = '%' + 'CustomerCare' + '%';
        List<RecordType> lstRecordType = [SELECT DeveloperName,Id,Name,SobjectType FROM RecordType where sobjectType = 'Case'];
        if(!lstRecordType.isEmpty()){
            for(RecordType rt : lstRecordType){
                caseRecordTypeMap.put(rt.DeveloperName,rt.ID);
            }
        }
        
        List<QueueSobject> queueLst = [SELECT Id,QueueId,SobjectType,Queue.Name FROM QueueSobject where SobjectType = 'Case' and Queue.Name like :queuename];
        if(!queueLst.isEmpty()){
            for(QueueSobject qq : queueLst){
                queueMap.put(qq.Queue.Name,qq.QueueId);
            }
        }
        
        try{
            
            System.debug('$$$$$Case ID'+caseids);
            List<Case> caseLst = [Select ID,Status,Area__c,Sub_Area__c,RecordTypeId,RecordType.Name,RecordType.DeveloperName,ByPassAreaValidation__c from Case where ID IN :caseids];
            //if(!caseLst.isEmpty() && caseRecordTypeMap.size() > 0){
              if(!caseLst.isEmpty() && !caseRecordTypeMap.isEmpty()){
                if(caseLst[0].RecordType.DeveloperName == CustomerCareUtility.FrontofficeRtdevelopername){
                    caseLst[0].RecordTypeID = caseRecordTypeMap.get(CustomerCareUtility.BackofficeRtdevelopername);
                    caseLst[0].OwnerID = queueMap.get(CustomerCareUtility.QueueBoEmailWeb); 
                    caseLst[0].ByPassAreaValidation__c = true;
                    caseupdateLst.add(caseLst[0]);
                }else if (caseLst[0].RecordType.DeveloperName == CustomerCareUtility.BackofficeRtdevelopername){
                    caseLst[0].RecordTypeID = caseRecordTypeMap.get(CustomerCareUtility.FrontofficeRtdevelopername);
                    caseLst[0].OwnerID = queueMap.get(CustomerCareUtility.QueueFoEmailWeb); 
                    caseLst[0].ByPassAreaValidation__c = true;
                    caseupdateLst.add(caseLst[0]);
                }else if (caseLst[0].RecordType.DeveloperName == CustomerCareUtility.CfainstitueRtdevelopername){
                    cfa_RT=CustomerCareUtility.CfainstitueRtdevelopername;
                    return cfa_RT;
                }
                
            } 
            if(!caseupdateLst.isEmpty()){
                Database.SaveResult[] srList = Database.Update(caseupdateLst); 
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()){
                        system.debug('%%testing'+sr.id);
                        updatecasevalue(sr.id);
                        System.debug('####inside success');
                        return 'True'; 
                    }else {
                        if(!sr.getErrors().isEmpty()){
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Case fields that affected this error: ' + err.getFields());
                        }
                            return 'False'; 
                        }
                    }
                }
            }
            
        }catch(Exception e){
            System.debug('###Inside Exception'+e.getMessage());
            return 'False';
        }
        
        return '';  
   }
    
    public static void updatecasevalue(id caseid){
        
        system.debug('%%caseid'+caseid);
        List<Case> caseLst = [Select ID,Status,Area__c,Sub_Area__c,RecordTypeId,RecordType.Name,RecordType.DeveloperName,ByPassAreaValidation__c from Case where ID = :caseid];
        if(!caseLst.isEmpty()){
            caseLst[0].ByPassAreaValidation__c = false;
            try{
                Database.SaveResult[] srList = Database.Update(caseLst);
            }catch(Exception e){
                System.debug('###Inside Exception'+e.getMessage());
            }
        }
    }
}