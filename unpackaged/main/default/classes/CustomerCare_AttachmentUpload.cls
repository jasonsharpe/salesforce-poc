/**************************************************************************************************
* Apex Class Name   : CustomerCare_AttachmentUpload
* Purpose           : Custom code to upload files to attachments from cases  
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 12-Dec-2017  
***************************************************************************************************/

public with sharing class CustomerCare_AttachmentUpload{

     public Id parentId{get;set;}
     public list<Attachment> lstAttach{get;set;}
     public Case CurrentCase{get;set;}
     //@TestVisible
     //private set<id> ids = new set<id>();
     
     public Attachment attachment {
      get {
          if (attachment == null)
            attachment = new Attachment();
          return attachment;
        }
      set;
      }
    
    //Contructor
    public CustomerCare_AttachmentUpload(ApexPages.StandardController controller) {

         this.CurrentCase = (Case)controller.getRecord(); 
         Refresh();
           
    }
    
    //Refresh the Attachement list
    public PageReference Refresh(){
    
        lstAttach = new list<Attachment>([select id, name, BodyLength , CreatedDate, Owner.Name, LastModifiedDate, OwnerId from Attachment where parentid =:this.CurrentCase.id Order by CreatedDate DESC]);
         return null;
    }
    
    //Write the file to the database
    public PageReference upload() {

    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = this.CurrentCase.id; // the record the file is attached to
    attachment.Description = 'Manual Upload';
    attachment.IsPrivate = false;

    try {
      system.debug('-----------------------Attachment'+attachment);
      insert attachment;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      attachment = new Attachment(); 
    }

    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
    
  }
  
   //Preview the file attached to the case
   public PageReference viewAttach()
    {
        ID lid=apexpages.currentpage().getparameters().get('ViewAttach');
        return new Pagereference ('/servlet/servlet.FileDownload?file='+lid);


    }

}