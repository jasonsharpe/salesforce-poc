/**************************************************************************************************
* Apex Class Name   : CustomerCare_Attachment_Handler_Test
* Purpose           : This test class is used for validating CustomerCare_Attachment_TriggerHandler   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCare_Attachment_Handler_Test {
    static testMethod void testEmailMessage() {
        Test.startTest();
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        
        User usr = CustomerCare_CommonTestData.testDataUser();
        Id RecordTypeIdAcc= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Society').getRecordTypeId();
        Account testacc = new Account(Name='Testacc1',RecordTypeId=RecordTypeIdAcc);
        insert testacc;
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        Contact contacts = new Contact(AccountID=testacc.id,FirstName='test',CFAMN__CFAMemberCode__c='AOA',Last_Survey_Date__c= System.TODAY()-32,Email='testrawcc@test.com', LastName='Contact222',RecordTypeId=ContactCCRecordType);
        insert contacts;
        
        //Case testEmailCase = CustomerCare_CommonTestData.TestDataEmailCase(); 
        
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        Case emailCase = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType, Priority='Medium', Subject = 'TestEmailTOCaseVerification22', Description='Testing Email To case222, CFA Institute Membership 47856985256314');
        insert emailCase;
        System.debug('%testEmailCase%%'+emailCase);
        
            EmailMessage newEmail = new EmailMessage();
            newEmail.FromAddress = 'test@abc.org';
            newEmail.Incoming = True;
            newEmail.ToAddress= 'hello@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
            newEmail.Subject = 'Test email';
            newEmail.TextBody = '23456';
            newEmail.ParentId = emailCase.Id;            
            insert newEmail;
            newEmail.Approver_status__c = 'Approved';
            update newEmail;
        
            Attachment attach=new Attachment();     
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId=newEmail.id;
            insert attach;
        Test.stopTest();            
        
        
    }
    
}