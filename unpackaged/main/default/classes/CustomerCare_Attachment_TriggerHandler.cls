/**************************************************************************************************
* Apex Class Name   : CustomerCare_Attachment_TriggerHandler
* Purpose           :   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 07-Dec-2017  
***************************************************************************************************/

public class CustomerCare_Attachment_TriggerHandler {
    public static void cc_changeParentId(List<Attachment> listAttachments){
        Map<Id,List<Attachment>> mapEmailMsgIdAttach=new Map<Id,List<Attachment>>();
        for(Attachment attachObj :listAttachments){
            String attachObjParentId=attachObj.parentId;
            String preFix=attachObjParentId.substring(0,3);
            if(prefix=='02s'){
                List<Attachment> tempListAttach=new List<Attachment>();
                if(mapEmailMsgIdAttach.containsKey(attachObj.parentId)){
                    tempListAttach=mapEmailMsgIdAttach.get(attachObj.parentId);                     
                }  
                tempListAttach.add(attachObj);
                mapEmailMsgIdAttach.put(attachObj.parentId,tempListAttach);   
            }             
        } 
        
        Map<Id,Id> mapEmailMsgIdCaseId=new Map<Id,Id>();
        List<EmailMessage> listEmailMessages=new List<EmailMessage>();
        listEmailMessages=[select id,parentid from EmailMessage where id IN:mapEmailMsgIdAttach.keyset()];
        for(EmailMessage emailMsgObj:listEmailMessages){
            mapEmailMsgIdCaseId.put(emailMsgObj.id,emailMsgObj.parentId);
        }
        
        for(Id emailMsgId :mapEmailMsgIdAttach.keySet()){
            List<Attachment> listAttachObj=mapEmailMsgIdAttach.get(emailMsgId);
            Id caseId;
            if(listAttachObj!=null){
                for(Attachment attachObj :listAttachObj){
                    caseId=mapEmailMsgIdCaseId.get(emailMsgId);
                    if(caseId!=null)
                        attachObj.parentId=caseId;
                }                
            }
        }
    }
}