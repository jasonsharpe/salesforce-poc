/**************************************************************************************************
* Apex Class Name   : CustomerCare_Case_TriggerHandler
* Purpose           : This class is used for validating Escalated cases,handling Email and Web cases,
deleting SocialMedia contacts and finding contact for web cases.    
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 29-Sept-2017  
***************************************************************************************************/
public with sharing class CustomerCare_Case_TriggerHandler{
      string Exceptiondetails='Exception details:';
 private CustomerCare_Case_TriggerHandler(){}
    /*This method extracts Area and SubArea keywords from Subject and Description of the Email origin Cases
and sets Area__c,Sub_Area__c,Priority,Front_Office__c and Back_Office__c fields of Case object 
using CustomerCare_Email2Case__c*/
    public static Boolean check = true;
    public static final Id CCFORecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
    public static final Id CCBORecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Back Office Record Type').getRecordTypeId();
    
    
    public static List<CustomerCare_Email2Case__c> getEmailToCaseData(){
        //List<CustomerCare_Email2Case__c> emailtocasesettings = CustomerCare_Email2Case__c.getall().values();
        return CustomerCare_Email2Case__c.getall().values();
    }
    
    public static void ccHandleEmailCases(List<Case> listNewCases){
        string Exceptiondetails='Exception details:';
         string ccHandleEmailCasescatchmsg='CustomerCare_Case_TriggerHandler::ccHandleEmailCases';
        try{
            List<Case> listEmailOriginCases = new List<Case>();
            for(case caseObj : listNewCases){
                if(caseObj.Origin == CustomerCareUtility.emailOrigin 
                   && (caseObj.Subject != Null || caseObj.Description!=Null)){
                       listEmailOriginCases.add(caseObj);
                   }
            }
            
           // if(listEmailOriginCases!=null && listEmailOriginCases.size()>0){
           if(listEmailOriginCases!=null && !listEmailOriginCases.isempty()){
                List<CustomerCare_Email2Case__c> listCustSettings = new List<CustomerCare_Email2Case__c>();
                listCustSettings = CustomerCare_Case_TriggerHandler.getEmailToCaseData();
                System.debug('$$listCustSettings$$'+listCustSettings);
                /*listCustSettings= [SELECT id,Area__c,Sub_Area__c,Priority__c,Back_Office__c,Search_Word__c,
Front_Office__c From CustomerCare_Email2Case__c];*/
                
                
                //if(listCustSettings!=null && listCustSettings.size()>0){
                if(listCustSettings!=null && !listCustSettings.isempty()){    
                    //List<Case> listClosedCases=new List<Case>();
                    Map<Id,Integer> mapCaseObjIdToCount=new Map<Id,Integer>();
                    Map<Id,CustomerCare_Email2Case__c> mapCaseObjIdToCustSet=new Map<Id,CustomerCare_Email2Case__c>();
                    
                    for(Case caseObj :listEmailOriginCases){
                        Integer counter=0;
                        String combSubjectDesc=caseObj.subject+'*'+caseObj.description;
                        combSubjectDesc = combSubjectDesc.toLowerCase();
                        system.debug('***listCustSettings***'+listCustSettings.size());
                        for(CustomerCare_Email2Case__c custSetObj :listCustSettings){
                            String searchWords=custSetObj.Search_Word__c;
                            system.debug('***searchWords***'+searchWords);
                            boolean boolAreaCheck=isContain(combSubjectDesc,custSetObj.Area__c);
                            boolean boolSubAreaCheck=isContain(combSubjectDesc,custSetObj.Sub_Area__c);
                            system.debug('***boolAreaCheck***'+boolAreaCheck);
                            system.debug('***custSetObj.Area__c***'+custSetObj.Area__c);
                            system.debug('***boolSubAreaCheck***'+boolSubAreaCheck);
                            system.debug('***custSetObj.Sub_Area__c***'+custSetObj.Sub_Area__c);
                            if(boolAreaCheck && boolSubAreaCheck){
                                counter++;
                                system.debug('***counter***'+counter);
                                mapCaseObjIdToCustSet.put(caseObj.Id, custSetObj); 
                                mapCaseObjIdToCount.put(caseObj.Id, counter);                                
                            }
                            if(searchWords!=null && !(boolAreaCheck && boolSubAreaCheck)){
                                List<String> listSearchWords = searchWords.split(',');
                                system.debug('***listSearchWords***'+listSearchWords);
                                for(String searchWord :listSearchWords){
                                    system.debug('***searchWord***'+searchWord);
                                    /*Integer check=combSubjectDesc.indexOf(searchWord);
Integer boolAreaCheck=combSubjectDesc.indexOf(custSetObj.Area__c);
Integer boolSubAreaCheck=combSubjectDesc.indexOf(custSetObj.Sub_Area__c);*/
                                    boolean check=isContain(combSubjectDesc,searchWord);
                                    system.debug('***combSubjectDesc***'+combSubjectDesc);
                                    system.debug('***searchWord***'+searchWord);
                                    system.debug('***check***'+check);
                                    if(check){
                                        counter++;
                                        system.debug('***counter for search keyword***'+counter);
                                        mapCaseObjIdToCustSet.put(caseObj.Id, custSetObj); 
                                        mapCaseObjIdToCount.put(caseObj.Id, counter);
                                        break;
                                    }
                                }
                            }
                        }
                        if(!mapCaseObjIdToCount.containsKey(caseObj.Id)){
                            mapCaseObjIdToCount.put(caseObj.Id, 0);                              
                        }
                        if(!mapCaseObjIdToCustSet.containsKey(caseObj.Id)){
                            mapCaseObjIdToCustSet.put(caseObj.Id, null);
                        }
                    }
                    system.debug('***'+mapCaseObjIdToCount);
                    system.debug('***'+mapCaseObjIdToCustSet);
                    
                    List<Case> listUpdateCases = new List<Case>();
                    if(mapCaseObjIdToCustSet.keySet().size()>0){
                        listUpdateCases=[Select id from Case where Id IN:mapCaseObjIdToCustSet.keySet()];
                        for(Case caseObj :listUpdateCases){
                            CustomerCare_Email2Case__c custSetObj=mapCaseObjIdToCustSet.get(caseObj.Id);
                            Integer matchingCounter=mapCaseObjIdToCount.get(caseObj.Id);
                            if(matchingCounter!=null && matchingCounter==1){
                                if(custSetObj!=null && custSetObj.Area__c!=null)
                                    caseObj.Area__c=custSetObj.Area__c;
                                if(custSetObj!=null && custSetObj.Sub_Area__c!=null)
                                    caseObj.Sub_Area__c=custSetObj.Sub_Area__c;
                                if(custSetObj!=null && custSetObj.Priority__c!=null)
                                    caseObj.Priority=custSetObj.Priority__c; 
                                if(custSetObj!=null && custSetObj.Front_Office__c)
                                    caseObj.RecordTypeId = CCFORecordType;
                                if(custSetObj!=null && custSetObj.Back_Office__c)
                                    caseObj.RecordTypeId =  CCBORecordType;                                                       
                            }else{
                                caseObj.Default_Queue__c=true;
                            }
                            
                        }
                        //if(listUpdateCases!=null && listUpdateCases.size()>0){
                        if(listUpdateCases!=null && !listUpdateCases.isempty()){
                            try{
                                update listUpdateCases;
                            }catch(Exception e){
                                //system.debug('CustomerCare_Case_TriggerHandler::ccHandleEmailCases'+e);
                                system.debug(ccHandleEmailCasescatchmsg+e);
                            }
                        }
                    }
                }
                
            }
            //Added for Encryption Credit Card Details starts here
            List<Case> listUpdateCasesCreditCard=new List<Case>();
            User userObj;
            userObj =[Select Id, ByPass_Rule__c from User where Id=: UserInfo.getUserId()];
            //if(listEmailOriginCases!=null && listEmailOriginCases.size()>0){
            if(listEmailOriginCases!=null && !listEmailOriginCases.isempty()){
                listUpdateCasesCreditCard=[Select id, Subject, Description, Encrypted_Description__c 
                                           from Case where Id IN:listEmailOriginCases];
            }
            
            //if(listUpdateCasesCreditCard!=null && listUpdateCasesCreditCard.size()>0){
            if(listUpdateCasesCreditCard!=null && !listUpdateCasesCreditCard.isEmpty()){
                userObj.ByPass_Rule__c=true;
                try{
                    update userObj;
                }catch(Exception e){
                    //system.debug('CustomerCare_Case_TriggerHandler::ccHandleEmailCases'+e);
                    system.debug(ccHandleEmailCasescatchmsg+e);
                }
                for(Case caseObj:listUpdateCasesCreditCard){
                        if(caseObj.Subject !=null)
                            caseObj.Subject = EmailMessageTriggerHelper.creditCardPattern(caseObj.Subject );
                        if(caseObj.description!=null)
                            caseObj.description= EmailMessageTriggerHelper.creditCardPattern(caseObj.description);
                }
                //if(listUpdateCasesCreditCard!=null && listUpdateCasesCreditCard.size()>0){
                if(listUpdateCasesCreditCard!=null && !listUpdateCasesCreditCard.isEmpty()){
                    try{
                        update listUpdateCasesCreditCard;
                        userObj.ByPass_Rule__c=false;
                        update userObj;
                    }catch(Exception e){
                        //system.debug('CustomerCare_Case_TriggerHandler::ccHandleEmailCases'+e);
                        system.debug(ccHandleEmailCasescatchmsg+e);
                    }
                }
            }
            //Added for Encryption Credit Card Details ends here
            
        }Catch(Exception e){
            system.debug('Exception occured in ccHandleEmailCases method of CustomerCare_Case_TriggerHandler class. '+
                         Exceptiondetails+e);
        }
    }
    
    /*This method sets Priority,Front_Office__c and Back_Office__c fields of Case object 
using CustomerCare_Web2Case__c*/
    public static void ccHandleWebCases(List<Case> listNewCases){
        System.debug('in handle web cases');
        string Exceptiondetails='Exception details:';
        string ccHandleEmailCasescatchmsg='CustomerCare_Case_TriggerHandler::ccHandleEmailCases';
        List<Case> listWebOriginCases = new List<Case>();
        List<String> listAreas=new List<String>();
        List<String> listSubAreas=new List<String>();
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.AssignmentRuleHeader.useDefaultRule = true;
        for(case caseObj : listNewCases){

            if(caseObj.Origin == CustomerCareUtility.webOrigin || caseObj.Origin == CustomerCareUtility.selfServiceCommunityOrigin){
                listWebOriginCases.add(caseObj); 
                listAreas.add(caseObj.Area__c);
                listSubAreas.add(caseObj.Sub_Area__c);
            }
        }
        List<CustomerCare_Web2Case__c> listCustSettings = new List<CustomerCare_Web2Case__c>();
        listCustSettings= [SELECT id,Area__c,Sub_Area__c,Priority__c,Back_Office__c,
                           Front_Office__c From CustomerCare_Web2Case__c where Area__c IN:listAreas
                           and Sub_Area__c IN:listSubAreas];  // fetching data for custom setting
        if(!listCustSettings.isEmpty()){
            Map<Id,CustomerCare_Web2Case__c> mapCaseObjIdToCustSet=new Map<Id,CustomerCare_Web2Case__c>();
            for(Case caseObj :listWebOriginCases){
                for(CustomerCare_Web2Case__c custSetObj :listCustSettings){
                    boolean boolAreaCheck=caseObj.Area__c==custSetObj.Area__c;
                    boolean boolSubAreaCheck=caseObj.Sub_Area__c==custSetObj.Sub_Area__c;
                    if(boolAreaCheck && boolSubAreaCheck){
                        mapCaseObjIdToCustSet.put(caseObj.Id, custSetObj); 
                        break;
                    }
                }
            }
            List<Case> listUpdateCases=new List<Case>();
            
            if(mapCaseObjIdToCustSet.keySet().size()>0){
                listUpdateCases=[Select id from Case where Id IN:mapCaseObjIdToCustSet.keySet()];
                for(Case caseObj :listUpdateCases){
                    caseObj.setOptions(dmo);
                    CustomerCare_Web2Case__c custSetObj=mapCaseObjIdToCustSet.get(caseObj.Id);
                    if(custSetObj!=null && custSetObj.Priority__c!=null)
                        caseObj.Priority=custSetObj.Priority__c; 
                    if(custSetObj!=null && custSetObj.Front_Office__c)
                        caseObj.RecordTypeId = CCFORecordType;
                    if(custSetObj!=null && custSetObj.Back_Office__c)
                        caseObj.RecordTypeId = CCBORecordType;
                    
                }
                if(listUpdateCases!=null && !listUpdateCases.isEmpty()){
                try{
                        update listUpdateCases;
                    }Catch(Exception e){
                        system.debug('Exception occured in ccHandleWebCases method of CustomerCare_Case_TriggerHandler class. '+
                                     Exceptiondetails+e);
                    } 
                }
            }
            
        }
        //Added for Encryption Credit Card Details starts here
        List<Case> listUpdateCasesCreditCard=new List<Case>();
        User userObj;
        userObj =[Select Id, ByPass_Rule__c from User where Id=: UserInfo.getUserId()];
        //if(listWebOriginCases!=null && listWebOriginCases.size()>0){
        if(listWebOriginCases!=null && !listWebOriginCases.isEmpty()){
            listUpdateCasesCreditCard=[Select id,Subject, Description, Encrypted_Description__c 
                                       from Case where Id IN:listWebOriginCases];
        }
        
        //if(listUpdateCasesCreditCard!=null && listUpdateCasesCreditCard.size()>0){
        if(listUpdateCasesCreditCard!=null && !listUpdateCasesCreditCard.isEmpty()){    
            userObj.ByPass_Rule__c=true;
            try{
                update userObj;
            }catch(Exception e){
                //system.debug('CustomerCare_Case_TriggerHandler::ccHandleEmailCases'+e);
                system.debug(ccHandleEmailCasescatchmsg+e);
            }
            for(Case caseObj:listUpdateCasesCreditCard){
                if(caseObj.description!=null || caseObj.Subject != null ){
                    if(caseObj.Subject !=null)
                        caseObj.Subject = EmailMessageTriggerHelper.creditCardPattern(caseObj.Subject);
                    if(caseObj.description !=null) 
                        caseObj.description = EmailMessageTriggerHelper.creditCardPattern(caseObj.description);
                }
            }
            //if(listUpdateCasesCreditCard!=null && listUpdateCasesCreditCard.size()>0){
            if(listUpdateCasesCreditCard!=null && !listUpdateCasesCreditCard.isEmpty()){ 
                try{
                    update listUpdateCasesCreditCard;
                    userObj.ByPass_Rule__c=false;
                    update userObj;
                }catch(Exception e){
                    //system.debug('CustomerCare_Case_TriggerHandler::ccHandleEmailCases'+e);
                    system.debug(ccHandleEmailCasescatchmsg+e);
                }
            }
        }
        //Added for Encryption Credit Card Details ends here
        
    }
    
    /*This method validates Escalation path when case status is Escalated*/
    public static void ccValidatedEscalatedCases(List<Case> listNewCases){
        string Exceptiondetails='Exception details:';
        try{
            for(Case caseObj :listNewCases){
                if(caseObj.status==CustomerCareUtility.escalatedStatus && caseObj.Escalation_Path__c==null
                   && caseObj.Escalation_Department__c==null){
                       caseObj.addError('Please enter Escalation Path and Department as Status is Escalated');
                   }
                if(caseObj.status==CustomerCareUtility.escalatedStatus && caseObj.Escalation_Path__c==null){
                    caseObj.addError('Please enter Escalation Path as Status is Escalated');
                }
                if(caseObj.status==CustomerCareUtility.escalatedStatus && caseObj.Escalation_Department__c==null){
                    caseObj.addError('Please enter Escalation Department as Status is Escalated');
                }
            }        
        }Catch(Exception e){
            system.debug('Exception occured in ccValidatedEscalatedCases method of CC_Case_TriggerHandler class. '+
                         Exceptiondetails+e);
        }
    }
    
    // This is used to update Customer_Type_when_Case_closed field when case is closed
    public static void updateCustomerTypeWhenCaseClosed(List<Case> listNewCases, Map<Id,Case> mapOldCases){
        for(Case cobj : listNewCases){
            //if(!mapOldCases.isEmpty()){
                //if(mapOldCases.containsKey(cobj.id) && cobj.Status == 'Closed' && mapOldCases.get(cobj.id).Status != 'Closed'){
                if(!mapOldCases.isEmpty() && mapOldCases.containsKey(cobj.id) && cobj.Status == 'Closed' && mapOldCases.get(cobj.id).Status != 'Closed'){
                    cobj.Customer_Type_when_Case_closed__c = cobj.Customer_Type__c;
                }
            } 
        }
    
    /*This method updates case recordTypeid when case status is ReOpened*/
    public static void ccSetRecordTypeIdOnReopenCase(List<Case> listNewCases,Map<Id,Case> mapOldCases){
      List<CaseHistory> HistoryCases = [SELECT CaseId, CreatedById, CreatedDate, Field, Id, IsDeleted, NewValue, OldValue FROM CaseHistory WHERE Field = 'RecordType' and CaseId IN : listNewCases ORDER BY CreatedDate DESC];
     
       Map<id,id> mapCaseHistory =  new Map<id,id>();
       for(CaseHistory cH : HistoryCases){
          string strOldValue = (string) cH.OldValue;
           if(cH.OldValue instanceof id && !mapCaseHistory.containsKey(cH.CaseId)){
               mapCaseHistory.put(cH.CaseId,Id.valueOf(strOldValue));
           }
       }
      for(Case Cs : listNewCases){
          system.debug('~~ccSetRecordTypeIdOnReopenCase~~Case~'+Cs);
          //update recordtype only when case status is changed to reopen
          if(Cs.Status == 'Reopen' && mapOldCases.get(Cs.id).Status != Cs.Status && mapCaseHistory.size()>0 && mapCaseHistory.containsKey(Cs.id)){
              cs.recordTypeid = mapCaseHistory.get(Cs.id);
          }
      }
    }
     
    
    /*This method finds contact for web origin cases*/
    public static void ccFindContactforEmailWebCases(List<Case> listNewCases){ 
        string Exceptiondetails='Exception details:';
        List<Case> listCases=new List<Case>();
        listCases=[Select id,Origin,SuppliedEmail From Case where Id IN:listNewCases];
        
        Map<String,Case> mapWebEmailToCaseObj = new Map<String, Case>();
        for(Case caseObj : listCases){
            if((caseObj.Origin==CustomerCareUtility.webOrigin || 
               caseObj.Origin==CustomerCareUtility.emailOrigin) &&
               caseObj.SuppliedEmail!=null)
                mapWebEmailToCaseObj.put(caseObj.SuppliedEmail, caseObj);
        }
        
        List<Contact> listContacts = new List<Contact>();
        if(mapWebEmailToCaseObj!=null && mapWebEmailToCaseObj.keySet().size()>0)
            listContacts=[SELECT id,email FROM Contact WHERE Email IN : mapWebEmailToCaseObj.keySet()
                          and RecordType.Name ='Local Member'];
        
        Map<String, Id> mapContactEmailToId = new Map<String, Id>();
        //if(listContacts!=null && listContacts.size()>0){
        if(listContacts!=null && !listContacts.isEmpty()){  
        for(Contact contactObj : listContacts){
                mapContactEmailToId.put(contactObj.Email, contactObj.id);
            }
        }
        
        for(String emailId : mapWebEmailToCaseObj.keySet()){
            Case caseObj=mapWebEmailToCaseObj.get(emailId);
            Id contactId=mapContactEmailToId.get(emailId);
            if(caseObj!=null && contactId!=null){
                caseObj.contactId=contactId;                    
            }
        }
        try{
            if(mapWebEmailToCaseObj.values().size()>0)
                update mapWebEmailToCaseObj.values();
        }Catch(Exception e){
            system.debug('Exception occured in ccFindContactforWebCases method of CustomerCare_Case_TriggerHandler class. '+
                         Exceptiondetails+e);
        }
    }
    
    public class SpamJunkEmailException extends Exception {}
    
    /*This method finds and avoids Junk emails and cases*/
    public static void ccHandleJunkEmailCases(List<Case> listNewCases){
        string Exceptiondetails='Exception details:';
        List<Case> listCases=new List<Case>();
        for(Case caseObj :listNewcases){
            if(caseObj.SuppliedEmail!=null && caseObj.Origin == CustomerCareUtility.emailOrigin){
                listCases.add(caseObj);
            }
        }
        
       // if(listCases.size()>0){
           if(!listCases.isEmpty()){
            List<CustomerCare_Email_Domains__c> listEmailDomains = new List<CustomerCare_Email_Domains__c>();
            listEmailDomains= CustomerCare_Email_Domains__c.getAll().values(); // fetching all the custom setting values 
            map<String,String> emaildomainMap = new Map<String,String>();
            if(!listEmailDomains.isEmpty()){
                for(CustomerCare_Email_Domains__c esobj : listEmailDomains){
                    emaildomainMap.put(esobj.Domain_Name__c, esobj.Domain_Name__c);
                }
            }
            //if(listEmailDomains.size()>0){
            if(!listEmailDomains.isEmpty()){
                for(Case caseObj :listCases){
                    if(!emaildomainMap.isEmpty() && emaildomainMap.containsKey(caseObj.SuppliedEmail)){
                        caseObj.CustomerCare_Spam_Case__c = True;
                        caseObj.Case_Closed_w_o_Agent_Intervention__c = True;
                        //caseObj.Status = 'Closed';
                        caseObj.Resolution_Type__c = 'Spam';
                        System.debug('Inside our scenario'+caseObj);
                    }                        
                }  
            }            
        }   
    }
    
    // handle CHILD cases
    public static void ccHandleChildCases(List<Case> listNewCases, List<Id> setConId)
    {
        List<Case> listParentCases=new List<Case>();
        Map<Id,Id> mapContactCase = new Map<Id,Id>();
       
        if(!setConId.isEmpty())
        {
            
            listParentCases=[Select Id,contactId from case where contactId IN:setConId AND parentId=NULL AND Status='New' AND (RecordType.Id =: CCFORecordType OR RecordType.Id =:CCBORecordType) AND Id NOT IN :listNewCases];
            System.debug('***before trigger parentcase size***'+listParentCases.size());   
            
            for(Case c:listParentCases)
            {
                mapContactCase.put(c.ContactId, c.Id);
                
            }
            
            for(Case c:listNewCases)
            {
                
                if(mapContactCase.containsKey(c.contactId))
                {
                    Id parentCaseId = mapContactCase.get(c.ContactId);
                    c.Status='Closed';
                    c.Resolution_Type__c='Duplicate';
                    c.ParentId=parentCaseId;
                    c.OwnerId=Label.CustomerCareSystemUser;
                    c.Case_Closed_w_o_Agent_Intervention__c=true;
                    c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName()
    .get('CustomerCare Case Reopen Record Type').getRecordTypeId();
                    System.debug('***Case updated if Contact Matches***');
                }
            }
        }
    }    
    
    
    // handle PARENT cases
    public static void ccHandleParentCases(List<Case> listNewCases)
    {
        List<Case> listUpdatedParent=new List<Case>();
        List<Id> pIds=new List<Id>();
        for(Case c:listNewCases)
        {
            pIds.add(c.ParentId);
        }
        System.debug('***ParentIds*** = '+pIds);
        
        if(!pIds.isEmpty())
        {
            List<Case> getCases=[Select Id from Case where Id IN:pIds];
            
            System.debug('***getCases Size*** '+getCases.size()+'\n'+getCases);
            
            for(Case c:getCases)
            {
               
                c.ConsolidatedCase__c=true;
                listUpdatedParent.add(c);
                System.debug('***Updating Parent***listUpdatedParent'+listUpdatedParent);
            }        
            
            /* if(listUpdatedParent!=null && !listUpdatedParent.isempty())*/
            
            if(!listUpdatedParent.isempty())
            {
                try{
                    update listUpdatedParent;
                    System.debug('***Parent Updated*** listUpdatedParent'+listUpdatedParent);
                }
                catch(Exception e)
                {
                    
                    system.debug(e);
                }
                
                
                
            }    
        }
    }    
    

  
    public static void ccemailMessageCreation(List<Case> listNewCases){   
        string Exceptiondetails='Exception details:';
        //if(listNewCases.size()>0){ 
          if(!listNewCases.isempty()){
            List<EmailMessage> listEmailMessages=new List<EmailMessage>();
            List<Case> caseDetails=[select ID,Subject,Description,SuppliedEmail,origin from Case where ID IN:listNewCases];         
            for(Case caseDetail:caseDetails){
                if(caseDetail.Origin==CustomerCareUtility.webOrigin){
                    EmailMessage eMessage=new EmailMessage();
                    eMessage.ParentId=caseDetail.ID;
                    eMessage.subject=caseDetail.Subject;
                    eMessage.TextBody=caseDetail.Description;
                    eMessage.FromAddress=caseDetail.SuppliedEmail; 
                    eMessage.ToAddress = caseDetail.SuppliedEmail; 
                    listEmailMessages.add(eMessage);     
                }    
            }
            if(!listEmailMessages.isEmpty()){
                try{
                    Database.saveResult [] insertListEmailMessages = Database.insert(listEmailMessages,false);  
                }catch(Exception e){
                    system.debug('Exception occured in ccFindContactforWebCases method of CustomerCare_Case_TriggerHandler class. '+
                                 Exceptiondetails+e);
                }
            }
        }     
    } 
    public static boolean isContain(String source, String subItem){
        string source1;
        string subItem1;
        source1 = source.toLowerCase();
        subItem1 = subItem.toLowerCase();
        String patterns = '\\b'+subItem1+'\\b';
        Pattern p=Pattern.compile(patterns);
        Matcher m=p.matcher(source1);
        return m.find();
    }
}