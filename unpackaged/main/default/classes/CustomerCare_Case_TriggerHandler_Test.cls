/**************************************************************************************************
* Apex Class Name   : CustomerCare_Case_TriggerHandler_Test 
* Purpose           : This test class is used for validating CustomerCare_Case_TriggerHandler class    
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 29-Sept-2017  
***************************************************************************************************/
@isTest(seeAllData=false)
public class CustomerCare_Case_TriggerHandler_Test {
    
    @testSetup
    static void testSetup(){
        insert new CustomerCare_Email_Domains__c(Name = 'test',Domain_Name__c='test@test.com');
    }
    static testMethod void testCCHandleEmailCases() {
        
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        
        CustomerCare_CommonTestData.testDatawebToCaseSettings();
        CustomerCare_CommonTestData.testDataEmailToCaseSettings();
        CustomerCare_CommonTestData.testdataListOfcase();
        test.startTest();
        CustomerCare_CommonTestData.TestDataWebcase();
        //CustomerCare_CommonTestData.testdataListOfcase();
        test.stopTest();
        
    }
    
      static testmethod  void testCCHandleOtherCasesChildCases()
    {
          system.debug('contact Id from test data');
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        
         Contact cnt=CustomerCare_CommonTestData.testDataContact();
        
         Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
         List<Case> webCase4 = new List<Case>();
        Case webCase1 = new Case(Status = 'New', Origin = 'Web',RecordTypeId=CCRecordType, Priority='Medium', Area__c='Investment Foundations',contactId= cnt.id, Sub_Area__c='Results', SuppliedEmail='testweb@webt.com',Description='Testing Web To case 4521785496865896');
        webCase1.parentId=null;
        //webCase4.add(webCase1);
        insert webCase1;
        
        Case webCase2 = new Case(Status = 'New', Origin = 'Web',ParentId =webCase1.Id ,RecordTypeId=CCRecordType, Priority='Medium', Area__c='Investment Foundations',contactId= cnt.id, Sub_Area__c='Results', SuppliedEmail='testweb@webt.com',Description='Testing Web To case 4521785496865896');
        
        webCase4.add(webCase2);
        insert webCase4;
        
        system.debug('contact Id from test data'+webCase4[0].ContactId);
            Test.startTest();
            List<Id> contactIdLst=new List<Id>();
            contactIdLst.add(cnt.Id);
            CustomerCare_Case_TriggerHandler.ccHandleChildCases(webCase4,contactIdLst);
            Test.stopTest();
        
    }
    
    
}