/**************************************************************************************************
* Apex Class Name   : CustomerCare_CommonTestData 
* Purpose           : This test class is used for test data for test classes   
* Version           : 1.0 Initial Version
* Organization      : Cognizant**
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCare_CommonTestData{
    
    public static User testDataUser(){
        Profile p=[Select Id from Profile where name='CustomerCare Supervisor'];
        User usr = new User (alias = 'newUsert', Email='newusertest@testorg.com',
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US', ProfileId = p.Id,
                             TimeZoneSidKey='America/Los_Angeles', UserName='Testnewuser123@testorg.com',
                             ByPassValidation__c=true);
        insert usr;
        return Usr;
    }
    public static User testDataBOUser(){
        Profile p=[Select Id from Profile where name='Customer Care Back Office Agent'];
        User usr = new User (alias = 'newUsert', Email='newusertest@testorg.com',
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US', ProfileId = p.Id,
                             TimeZoneSidKey='America/Los_Angeles', UserName='Testnewuser123@testorg.com',
                             ByPassValidation__c=true);
        insert usr;
        return Usr;
    }
    public static User testDataFOUser(){
        Profile p=[Select Id from Profile where name='Customer Care Front Office Agent'];
        User usr = new User (alias = 'newUsert', Email='newusertest@testorg.com',
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US', ProfileId = p.Id,
                             TimeZoneSidKey='America/Los_Angeles', UserName='Testnewuser123@testorg.com',
                             ByPassValidation__c=true);
        insert usr;
        return Usr;
    }
    
    public static User testDataCiscoUser(){
        Profile p=[Select Id from Profile where name='CustomerCare Supervisor'];
        system.debug('@@@'+p);
        User usrcisco = new User (alias = 'newCisco', Email='newuserciscotest@testorg.com',
                                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                  LocaleSidKey='en_US', ProfileId = p.Id,Cisco_User_Name__c ='Test_agent55',
                                  TimeZoneSidKey='America/Los_Angeles', UserName='Testnewusercisco123@testorg.com',
                                  ByPassValidation__c=true);
        insert usrcisco;
        System.assertEquals(usrcisco.LastName,'Testing');
        return usrcisco;
    }
    
    public static User testDataCreateContact1(){
        Profile p=[Select Id from Profile where name='Integration Profile'];
        system.debug('@@@'+p);
        User usrcisco = new User (alias = 'newCisco', Email='newuserciscotest@testorg.com',
                                  EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                  LocaleSidKey='en_US', ProfileId = p.Id,Cisco_User_Name__c ='Test_agent55',
                                  TimeZoneSidKey='America/Los_Angeles', UserName='Testnewusercisco123@testorg.com',
                                  ByPassValidation__c=true);
        
        insert usrcisco;
        
        return usrcisco;
    }
    

    public static contact testDataContact(){
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        String AcctId = testDataAccount()[0].id;
        Contact contacts = new Contact(FirstName='test',AccountID=AcctId  ,CFAMN__CFAMemberCode__c='AOA',Partner_Id__c='123456',Last_Survey_Date__c= System.TODAY()-32,Email='test@test.com', LastName='Contact',RecordTypeId=ContactCCRecordType,GDPR_Consent__c=false);
        try{
            insert contacts;
            System.assertEquals(contacts.LastName,'Contact');
        }catch(exception e){
            system.debug('exception'+e);
        }
        return contacts;
    }
    
    public static contact testCFADataContact(){
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('CFA Contact').getRecordTypeId();
        Contact contactcfas = new Contact(FirstName='test25',LastName='Contact1',RecordTypeId=ContactCCRecordType,Partner_Id__c='56891245', CFAMN__PersonID__c='698745', CFAMN__CFAMemberCode__c='AOA',Last_Survey_Date__c= System.TODAY()-32,Email='test125@test123.com');
        try{
            insert contactcfas;
        }catch(exception e){
            system.debug('exception'+e);
        }
        return contactcfas;
    } 
    
    public static contact testRawCFAContact(){
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        Contact rowCFAContact = new Contact(FirstName='test',CFAMN__PersonID__c='698745',CFAMN__CFAMemberCode__c='AOA',Last_Survey_Date__c= System.TODAY()-32,Email='rawemailcfa@test.com', LastName='Contact',RecordTypeId=ContactCCRecordType);
        try{
            insert rowCFAContact;
        }catch(exception e){
            system.debug('exception'+e);
        }
        return rowCFAContact; 
        
    }
    
    public static contact testDataContact2(){
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        Contact contacts = new Contact(FirstName='test11',CFAMN__CFACandidateCode__c='2C',Email='test11@test.com', LastName='Contact11',RecordTypeId=ContactCCRecordType,GDPR_Consent__c=false);
        try{
            insert contacts;
            System.assertEquals(contacts.LastName, ' Contact11');
        }catch(exception e){
            system.debug('exception'+e);
        }
        return contacts;
    }
    public static list<account> testDataAccount(){
        Id RecordTypeIdAcc= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Society').getRecordTypeId();
        List<Account> testacc = new List<Account>();
        testacc.add(new Account(Name=(Math.random() * 1000)+'Testacc1'+System.now(),RecordTypeId=RecordTypeIdAcc));
        insert testacc;
        return testacc;
    }
    public static list<Affiliation__c> testDataAffiliation(){        
        String AccId = testDataAccount()[0].id;
        string ContactId=testDataContact().id; 
        List<Affiliation__c> testaff = new List<Affiliation__c>();
        testaff.add(new Affiliation__c(Account__c=AccId, Type__c='Society',Active_Affiliation__c=true,
                                       Contact__c=ContactId, Role__c='Member'));      
        insert testaff;
        return testaff;
    }
    Public static CustomerCare_ImageURL__c testDataImageurl(){
        CustomerCare_ImageURL__c Testcustmsetting = new CustomerCare_ImageURL__c();
        Testcustmsetting.Name = 'test';
        Testcustmsetting.Header_URl__c = 'https://test.com';
        insert Testcustmsetting;
        return Testcustmsetting;
    }
    public static CustomerCare_Email2Case__c testDataEmailToCaseSettings(){
        CustomerCare_Email2Case__c emailSetting = new CustomerCare_Email2Case__c();
        emailSetting.Name = 'Test Email Setting';
        emailSetting.Area__c = 'CFA Program';
        emailSetting.Sub_Area__c = 'Verification';
        emailSetting.Back_Office__c = true ;
        emailSetting.Front_Office__c = true;
        emailSetting.Priority__c = 'Medium';
        emailSetting.Search_Word__c = 'Medium   confirmation,verify';
        insert emailSetting ;
        System.assertEquals(emailSetting.NAme, 'Test Email Setting');
        return emailsetting;
    } 
     public static CustomerCare_Email_Domains__c testDataEmailDomainSettings(){
        CustomerCare_Email_Domains__c emailDomainSetting = new CustomerCare_Email_Domains__c();
        emailDomainSetting.Name = 'Dev domain';
        emailDomainSetting.Domain_Name__c = 'test@test.com';
        insert emailDomainSetting ;
        System.assertEquals(emailDomainSetting.Name, 'Dev domain');
        return emailDomainSetting;
    } 
      public static CustomerCare_SurveyEmails__c testDataSurveyEmailSettings(){
        CustomerCare_SurveyEmails__c SurveyEmailSetting = new CustomerCare_SurveyEmails__c();
        SurveyEmailSetting.Name = '1';
        SurveyEmailSetting.Customer_Type__c = 'Member';
        SurveyEmailSetting.Email_Template__c = 'CustomerCare_SurveyLink_for_Member';
        insert SurveyEmailSetting ;
        System.assertEquals(SurveyEmailSetting.Customer_Type__c, 'Member');
        return SurveyEmailSetting;
    } 
    
    public static CustomerCareCiscoPlayPause__c testCiscoSettings(){
        CustomerCareCiscoPlayPause__c ciscoSetting = new CustomerCareCiscoPlayPause__c();
        ciscoSetting.Cisco_Auth_Header__c = 'Basic VGVzdF9BZ2VudDE6VGghbmtUQG5r';
        ciscoSetting.Cisco_Pause__c = 'https://prdcalabrioqm.aimr.org/recordingcontrols/rest/pause';
        ciscoSetting.Cisco_Play__c = 'https://prdcalabrioqm.aimr.org/recordingcontrols/rest/resume';
        ciscoSetting.Domain__c = 'aimr';
        ciscoSetting.Name = 'Test Cisco';
        insert ciscoSetting ;
        System.assertEquals(ciscoSetting.Name, 'Test Cisco');
        return ciscoSetting;
    } 
    
    
    Public static CustomerCare_Web2Case__c testDatawebToCaseSettings(){
        CustomerCare_Web2Case__c webSetting = new CustomerCare_Web2Case__c();
        webSetting.Name = 'Test Web Setting';
        webSetting.Area__c = 'Investment Foundations';
        webSetting.Sub_Area__c = 'Results';
        webSetting.Back_Office__c = true;
        webSetting.Front_Office__c = true ;
        webSetting.Priority__c = 'Medium';
        insert webSetting;
        //System.assertEquals(webSetting.NAme, 'Test Web Setting');
        return webSetting;
        
    }
    public static List<Case> testdataListOfcase(){        
        string ContactId=testDataContact().id; 
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        List<Case>  NewEmailCaseList = new List<Case>();
        Case emailCase = new Case(Status = 'New', Origin = 'Email',sub_area__c='Deferral', RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership 47856985256314');
        Case emailCase1 = new Case(Status = 'New', Origin = 'Email',ContactId=ContactId,RecordTypeId=CCRecordType, Priority='Medium', Subject = 'TestEmailTOCaseVerification',sub_area__c='testSubArea');
        Case emailCase2 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTO Case Verification verify', Description='Testing Email To case,CFA Program, CFA Institute Membership 1452365247856985');
        Case emailCase3 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTO Case Verification', Description='Testing Email To case,CFA Program,verify, CFA Institute Membership passport A12 32452');
        Case emailCase4 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification verify', Description='Testing Email To case, CFA Institute Membership');
        Case emailCase5 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', SuppliedEmail='test@test.com', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership Passport A12 34567554 ');
        Case emailCase6 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification verify', Description='Testing Email To case, CFA Institute Membership test@test.com pwd Akanksha@0434343434');
        Case emailCase7 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification verify', Description='Testing Email To case, CFA Institute Membership test@test.com password Akanksha@04 ');
        Case emailCase8 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership test@test.com Password Akanksha@04 ');
        Case emailCase9 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership test@test.com PASSWORD Akanksha@04 ');
        Case emailCase10 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership test@test.com PWD Akanksha@043434343 ');
        Case emailCase11 = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', SuppliedEmail='test@test.com', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership PASSPORT A12 34567525 ');
        NewEmailCaseList.add(emailCase);
        NewEmailCaseList.add(emailCase1);
        NewEmailCaseList.add(emailCase2);
        NewEmailCaseList.add(emailCase3);
        NewEmailCaseList.add(emailCase4);
        NewEmailCaseList.add(emailCase5);
        NewEmailCaseList.add(emailCase6);
        NewEmailCaseList.add(emailCase7);
        NewEmailCaseList.add(emailCase8);
        NewEmailCaseList.add(emailCase9);
        NewEmailCaseList.add(emailCase10);
        NewEmailCaseList.add(emailCase11);
        insert NewEmailCaseList;
        return NewEmailCaseList;
    }
 

    
    Public static Case TestDataEmailCase(){
        string ContactId=testDataContact().id;
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        BusinessHours bh = [select id from businesshours where IsDefault=true];
        Case emailCase = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership 47856985256314',Description_Short__c='test', Area__c='Miscellaneous',Sub_Area__c='Miscellaneous', BusinessHours=bh );
        insert emailCase;
            //System.assertEquals(emailCase.Origin, 'Email');
            return emailCase;
    }  
    Public static Case TestDataEmailCaseBO(){
        string ContactId=testDataContact().id;
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Back Office Record Type').getRecordTypeId();
        Case emailCase = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerificationbo', Description='Testing Email To case bo, CFA Institute Membership 47856985256314');
        insert emailCase;
        return emailCase;
    } 
    
    Public static Case TestDataCFAEmailCase(){
        string ContactId=testCFADataContact().id;
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        Case emailCFACase = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='',Description_Short__c='test');
        insert emailCFACase;
        return emailCFACase;
    }  
    
    Public static Case TestRawCFADataEmailCase(){
        string ContactId=testRawCFAContact().id;
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        Case emailCase = new Case(Status = 'New', Origin = 'Email',RecordTypeId=CCRecordType,ContactId=ContactId, Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership 47856985256314');
        insert emailCase;
        return emailCase;
    }
    
    public static List<Case> TestDataWebcase(){
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        List<Case> newWebCaseList= new List<Case>();
        Case webCase1 = new Case(Status = 'New', Origin = 'Web',RecordTypeId=CCRecordType, Priority='Medium', Area__c='Investment Foundations',contactId= CustomerCare_CommonTestData.testDataContact().id, Sub_Area__c='Results', SuppliedEmail='testweb@webt.com',Description='Testing Web To case 4521785496865896');
        Case webCase2 = new Case(Status = 'New', Origin = 'Web',RecordTypeId=CCRecordType, Priority='Medium', Area__c='Investment Foundations',contactId= CustomerCare_CommonTestData.testDataContact().id, Sub_Area__c='Results', SuppliedEmail='testweb@webt.com',Description='Testing Web To case 452178549686589');
        Case webCase3 = new Case(Status = 'New', Origin = 'Web',RecordTypeId=CCRecordType, Priority='Medium', Area__c='Investment Foundations',contactId= CustomerCare_CommonTestData.testDataContact().id, Sub_Area__c='Results', SuppliedEmail='testweb@webt.com',Description='Testing Web To case 45217854968658');
        newWebCaseList.add(webCase1);
        newWebCaseList.add(webCase2);
        newWebCaseList.add(webCase3);
        insert newWebCaseList;
        return newWebCaseList;        
    }
    Public static list<LiveChatTranscript> TestDataForTranscript(){
        list<LiveChatTranscript> testlist = new list<LiveChatTranscript>();
        LiveChatVisitor visitor = new LiveChatVisitor();
        visitor.CurrencyIsoCode ='USD';
        insert visitor; 
        LiveChatTranscript trnscript = new LiveChatTranscript();
        trnscript.LiveChatVisitorId=visitor.id;
        trnscript.caseid= TestDataEmailCase().id;
        insert trnscript;
        
        testlist.add(trnscript);
        return testlist;
        
    }
    Public static List<SocialPost> TestDataSocailPost(){
        List<SocialPost> listNewPosts = new List<SocialPost>();        
        SocialPost Post = new SocialPost();
        Post.Name= 'test';
        List<Case> webCase = TestDataWebcase();
        Post.ParentId = webCase[0].id;
        listNewPosts.add(Post);
        SocialPost Post1 = new SocialPost();
        Post1.Name= 'test';
        List<Case> webCase1 = TestDataWebcase();
        Post1.ParentId = webCase1[0].id;
        Post1.MessageType = 'Reply';
        listNewPosts.add(Post1);
        insert listNewPosts;
        return listNewPosts;
    }
    public static case TestDataCaseOwnerGroup(){
        Id ContactCCRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        Contact contact1 = new Contact(FirstName='test',Email='test@test.com', LastName='Contact',RecordTypeId=ContactCCRecordType);
        insert contact1;
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        Group grp =[Select Id, Name from Group where Type ='Queue' and Name='CustomerCare - Default Queue'];
        Case case1 = new Case(Status = 'New', Origin = 'Email',ContactId=contact1.id ,RecordTypeId=CCRecordType, Priority='High', OwnerId=grp.Id );
        insert case1;
        System.assertEquals(case1.Origin, 'Email');
        return Case1;
    }
    
    public static EmailTemplate TestDataForETOfOpenCases(){
        Folder FId= [SELECT Id,Name FROM Folder WHERE Name = 'CustomerCare Email Templates'];
        EmailTemplate et = new EmailTemplate (developerName = 'test', FolderId = FId.id, TemplateType= 'Text', 
                                              Name = 'customerCare_Open Email Template',isActive=true);
        insert et;
        return et;
    }
    public static EmailTemplate TestDataForETOfSruveyLink(){
        Folder FId= [SELECT Id,Name FROM Folder WHERE Name = 'CustomerCare Email Templates'];
        EmailTemplate et = new EmailTemplate (developerName = 'test' + System.now().millisecond(), FolderId = FId.id, TemplateType= 'Text',
                                              Name = 'CustomerCare SurveyLink for Member',isActive=true);
        insert et;
        return et;
    }
    
    public static EmailMessage TestDataForEmailMessage(){
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test@abc.org';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hello@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.TextBody = '23456';
        newEmail.ParentId = TestDataEmailCase().Id;            
        insert newEmail;
        newEmail.Approver_status__c = 'Approved';
        update newEmail;
        EmailMessage newEmail1 = new EmailMessage();
        newEmail1.FromAddress = 'test@abc.org';
        newEmail1.Incoming = True;
        newEmail1.ToAddress= 'hello@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail1.Subject = 'Test email';
        newEmail1.TextBody = '23456';
        newEmail1.ParentId = TestDataEmailCase().Id;            
        insert newEmail1;
        newEmail1.Approver_status__c = 'Rejected';
        update newEmail1;
        return newEmail;
    }
      
    public static EmailMessage TestRawCFADataForEmailMessage(){
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'rawemailcfa@test.com';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hellorawcfa@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.TextBody = '23456';
        newEmail.ParentId = TestRawCFADataEmailCase().Id;            
        insert newEmail;
        newEmail.Approver_status__c = 'Approved';
        update newEmail;
        EmailMessage newEmail1 = new EmailMessage();
        newEmail1.FromAddress = 'rawemailcfa@test.com';
        newEmail1.Incoming = True;
        newEmail1.ToAddress= 'hellorawcfa@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail1.Subject = 'Test email';
        newEmail1.TextBody = '23456';
        newEmail1.ParentId = TestRawCFADataEmailCase().Id;            
        insert newEmail1;
        newEmail1.Approver_status__c = 'Rejected';
        update newEmail1;
        return newEmail;
    }
   
    public static EmailMessage TestDataForApprovedEmailMessage(){
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test4@abc.org';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hello4@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.TextBody = '23456';
        newEmail.ParentId = TestDataEmailCase().Id;            
        insert newEmail;
        newEmail.Approver_status__c = 'Approved';
        update newEmail;
        return newEmail;
    }
    
    public static EmailMessage TestDataForRejectedEmailMessage(){
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = 'test5@abc.org';
        newEmail.Incoming = True;
        newEmail.ToAddress= 'hello5@670ocglw7xh4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com';
        newEmail.Subject = 'Test email';
        newEmail.TextBody = '23456';
        newEmail.ParentId = TestDataEmailCase().Id;            
        insert newEmail;
        newEmail.Approver_status__c = 'Rejected';
        update newEmail;
        return newEmail;
    }

    Public static case CreatePhonecase(){
        Id CCRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CustomerCare Front Office Record Type').getRecordTypeId();
        Case phoneCase = new Case(Status = 'New', Origin = 'Phone',RecordTypeId=CCRecordType,Priority='Medium', Subject = 'TestEmailTOCaseVerification', Description='Testing Email To case, CFA Institute Membership 47856985256314');
        insert phoneCase;
        phoneCase.status ='closed';
        update phoneCase;
        return phonecase;
    }
    
    public static void createCustomSettingData(){
        CustomerCare_ApiCodeMessages__c codeMe = new CustomerCare_ApiCodeMessages__c();
        codeMe.Name = 'Test500';
        codeMe.CustomerCare_Error_Code__c = 500;
        codeMe.CustomerCare_ErrorMessage__c = '500 - Server down';
        insert codeMe;
    }
}