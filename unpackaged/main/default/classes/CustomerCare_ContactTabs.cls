/*
 * Apex Class Name : CustomerCare_ContactTabs
 * Purpose : Loading data from Swagger for various tabs(Enrollment, Exams etc)
 *           Makes callouts to Swagger
*/
public with sharing class CustomerCare_ContactTabs{
    
    /*getData receives request from lwc and fetch URL from custom meta data and passes URL to callWebService method*/
    @AuraEnabled
    public static Object getData(String tabName, String tabLabel, String personId){
        Map<String,String> tabMap = getTabMap();
        String  orgURLMetadata = DynamicEnvVariableService.genericdatafetcher(tabMap.get(tabLabel));
        System.debug('URL: '+orgURLMetadata);
        System.debug('URL: '+tabLabel);
        orgURLMetadata = orgURLMetadata+ '/' + personId;
        if(tabName == 'History'){
            orgURLMetadata = orgURLMetadata+ '/'+ tabName;
        }
        try{
            return callWebService(orgURLMetadata);
        }
        catch(Exception e){
            throw new AuraException(e.getMessage()); 
        }

    }
    
    @AuraEnabled
    public static Object getMembershipTransactions(String customerId, String orgPartnerId, String orgtype){
        Map<String,String> tabMap = getTabMap();
        String  orgURLMetadata = DynamicEnvVariableService.genericdatafetcher(tabMap.get('Memberships'));
        if(String.isnotblank(orgtype)){
            if(!orgtype.equalsignorecase('Society'))
                orgPartnerId = orgtype.substring(0,4);
        }
        if(orgPartnerId == null || orgPartnerId == '')
            orgPartnerId = Label.CFAOrganizationPartnerID;
        orgURLMetadata = orgURLMetadata+'/'+customerId+'/history/'+orgPartnerId;
        try{
            return callWebService(orgURLMetadata);
        }
        catch(Exception e){
            throw new AuraException(e.getMessage()); 
        }
    }
    
    @AuraEnabled
    public static Object getMemebershipApplication(String personId, String submitDate){
        String  orgURLMetadata = DynamicEnvVariableService.genericdatafetcher(getTabMap().get('Membership Application Reference'));
        orgURLMetadata = orgURLMetadata+'/'+personId+'?submittedDate='+submitDate;
        try{
            return callWebService(orgURLMetadata);
        }
        catch(Exception e){
            throw new AuraException(e.getMessage()); 
        }
    }
    
    /* callWebService methods makes callout to Swagger to get data */
    public static Object callWebService(String endPoint){
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        request.setClientCertificateName(Label.MDM_Swagger_Certificate);
        try{
            response = http.send(request);
            System.debug('Response: '+endPoint);
            System.debug('Response: '+response.getBody());
            return (Object)response.getBody();
        }
        catch(Exception e){
            System.debug('Exception '+e);
           throw new AuraException(e.getMessage());
        }
    }
    
    /* getTabMap returns the map to getData method to fetch URL from custom meta data */
    public static Map<String, String> getTabMap(){
        Map<String,String> tabMap = new Map<String,String>();
        tabMap.put('Enrollment','CustomerCareEnrollmentLink');
        tabMap.put('Exam Result','CustomerCareEnrollmentLink');
        tabMap.put('Scholarship','CustomerCareScholarshipLink');
        tabMap.put('Login History','CustomerCareLoginHistoryLink');
        tabMap.put('Memberships','CustomerCareMembershipLink');
        tabMap.put('Customer History','CustomerCareCustomerHistory');
        tabMap.put('Membership Application','CustomerCareMembershipApplicationLink');
        tabMap.put('Membership Application Reference','CustomCareMembershipAppReference');
        return tabMap;
    }
    
}