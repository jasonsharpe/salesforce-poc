/**************************************************************************************************
* Apex Class Name   : CustomerCare_EmailToOpenCase_Test
* Purpose           : This test class is used for validating CustomerCare_CreateEmailForContact   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCare_EmailToOpenCase_Test {
    static testMethod void testEmailopencase() {
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        User usr = CustomerCare_CommonTestData.testDataUser();
         SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        system.runAs(usr)
        {  
            List<Case> testCases= CustomerCare_CommonTestData.testdataListOfcase();   
            List<id> caseIds = new List<id>();
            caseids.add(testCases[0].id);
            //System.assertEquals(testCases,null);
           /* for(Case cases:testCases){
                caseIds.add(cases.id);
            }  */
            CustomerCare_EmailToOpenCaseContacts.validateUser();
            string result=CustomerCare_EmailToOpenCaseContacts.emailToOpenCaseContacts(caseIds);
            //system.assertEquals(result,'True');
        }
    }
}