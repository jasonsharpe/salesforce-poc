/**************************************************************************************************
* Apex Class Name   : CustomerCare_SocialPost_Handler_Test
* Purpose           : This test class is used for validating CustomerCare_CreateEmailForContact   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCare_SocialPost_Handler_Test {
    static testMethod void EmailTemplateImageURLTest(){ 
     boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }    
        insert obj;
        List<SocialPost> listSocialPosts = new List<SocialPost>();
         List<SocialPost> listNewPosts = new List<SocialPost>();        
        SocialPost Post = new SocialPost();
        Post.Name= 'test';
        List<Case> webCase = CustomerCare_CommonTestData.TestDataWebcase();
        webCase[0].contactid = null;
        update webCase;
        Post.ParentId = webCase[0].id;
        listSocialPosts.add(Post);
        SocialPost Post1 = new SocialPost();
        Post1.Name= 'test';
        //List<Case> webCase1 = TestDataWebcase();
        Post1.ParentId = webCase[0].id;
        Post1.MessageType = 'Reply';
        listSocialPosts.add(Post1);
        insert listSocialPosts;
        
        
        //List<SocialPost> listSocialPosts= CustomerCare_CommonTestData.TestDataSocailPost();
         test.startTest();
         
        //System.assertEquals(listSocialPosts,null);
        CustomerCare_SocialPost_TriggerHandler.ccClearSocialPersonaId(listSocialPosts); 
        test.stopTest();
        CustomerCare_SocialPost_TriggerHandler.ccSendNotificationToCaseOwner(listSocialPosts);
       
    }
      static testMethod void EmailTemplateImageURLTest1(){ 
         boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        List<SocialPost> listSocialPosts = new List<SocialPost>();
         List<SocialPost> listNewPosts = new List<SocialPost>();        
        SocialPost Post = new SocialPost();
        Post.Name= 'test';
        List<Case> webCase = CustomerCare_CommonTestData.TestDataWebcase();
        Post.ParentId = webCase[0].id;
        listSocialPosts.add(Post);
        
        insert listSocialPosts;
        
        
        //List<SocialPost> listSocialPosts= CustomerCare_CommonTestData.TestDataSocailPost();
         test.startTest();
         
        //System.assertEquals(listSocialPosts,null);
       CustomerCare_SocialPost_TriggerHandler.ccSendNotificationToCaseOwner(listSocialPosts);
        test.stopTest();
        
       
    }
}