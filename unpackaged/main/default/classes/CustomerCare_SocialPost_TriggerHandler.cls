/**************************************************************************************************
* Apex Class Name   : CustomerCare_SocialPost_TriggerHandler
* Purpose           : This class is used for clearing SocialPersonaId in SocialPost record.     
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 08-Oct-2017  
***************************************************************************************************/

public class CustomerCare_SocialPost_TriggerHandler {
    /*This method clears SocialPersonaId in SocialPost record*/
    public static void ccClearSocialPersonaId(List<SocialPost> listNewPosts){
        
        List<Id> listCaseIds=new List<Id>();
        for(SocialPost postObj :listNewPosts){
            postObj.personaId=null; 
            postObj.whoId=null;
            listCaseIds.add(postObj.ParentId);
        }
        
        if(listCaseIds.size()>0){
            List<Case> listCases=new List<Case>();
            listCases=[Select id,ContactId from Case where id IN:listCaseIds];
            
            List<Id> listContactIds=new List<Id>();
            if(listCases.size()>0){
                for(Case caseObj :listCases){
                    if(caseObj.ContactId!=null)
                        listContactIds.add(caseObj.ContactId);
                    caseObj.contactId=null;
                    caseObj.AccountId = null;
                }
                try{
                    update listCases;
                }catch (exception e){
                    system.debug('Exception occured in Update Of social Media Cases Exception details:'+e); 
                }
                if(listContactIds.size()>0){
                    List<Contact> listContacts=new List<Contact>();
                    listContacts=[Select id from Contact where Id IN:listContactIds];
                    
                    if(listContacts.size()>0){
                        try{
                            delete listContacts;
                        }catch (exception e){
                            system.debug('Exception occured in Deletion Of social Media Cases related contact Exception details:'+e); 
                        }
                    }
                }
            }
        }
    } 
    
    /*This method clears SocialPersonaId in SocialPost record*/
    public static void ccSendNotificationToCaseOwner(List<SocialPost> listNewPosts){
        List<SocialPost> listNewSocialPosts=[Select id,MessageType,parentId  
                                             from SocialPost where Id IN:listNewPosts];
        
        List<Id> listCaseIds=new List<Id>();
        Map<Id,Id> mapCaseIdToOwnerId=new Map<Id,Id>();
        for(SocialPost socialPostObj :listNewSocialPosts){
            if(socialPostObj.MessageType=='Reply')
                listCaseIds.add(socialPostObj.ParentId);
        }
        List<Case> listCases=new List<Case>();
        listCases=[Select id,OwnerId from Case where id IN:listCaseIds];
        for(Case caseObj :listCases){
            if(caseObj.OwnerId!=null && string.valueOf(caseObj.OwnerId).startsWith('005'))
                mapCaseIdToOwnerId.put(caseObj.Id,caseObj.OwnerId);
        }
        
        EmailTemplate emailTemplate=[SELECT Id FROM EmailTemplate 
                                     WHERE Name = 'Email Notification when customer replied on Social Media case' limit 1];
        
        for(Id caseId :mapCaseIdToOwnerId.keySet()){
            Id ownerId=mapCaseIdToOwnerId.get(caseId);
            if(ownerId!=null && emailTemplate!=null){
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                msg.setTargetObjectId(ownerId);
                msg.setTemplateId(emailTemplate.Id);
                msg.SaveAsActivity = false;
                try{
                    Messaging.sendEmail(new Messaging.singleEmailMessage[] { msg });
                }Catch(Exception e){
                    system.debug('Exception occured in Sending mail in  social Media trigger handler  Exception details:'+e); 
                }
            }            
        }
    }    
}