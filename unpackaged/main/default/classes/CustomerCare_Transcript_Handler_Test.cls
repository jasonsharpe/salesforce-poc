/**************************************************************************************************
* Apex Class Name   : CustomerCare_Transcript_Handler_Test
* Purpose           : This test class is used for validating CustomerCare_Transcript_TriggerHandler
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 15-Nov-2017  
***************************************************************************************************/
@isTest
public class CustomerCare_Transcript_Handler_Test {
    static testMethod void LiveTranscriptTest() {
          boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        
          SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        List<LiveChatTranscript> listLiveChatTranscripts =CustomerCare_CommonTestData.TestDataForTranscript();
        CustomerCare_Transcript_TriggerHandler.ccUpdateCaseowner(listLiveChatTranscripts);

        LiveChatTranscript actualtrnscript=listLiveChatTranscripts[0];
        Case actualCase=[select ID,ownerID from Case where ID=:actualtrnscript.caseID];
        //system.assertEquals(actualcase.OwnerId,actualtrnscript.ownerID);
    }
}