/**************************************************************************************************
* Apex Class Name   : CustomerCare_LiveChatTranscript_TriggerHandler
* Purpose           : This class is used for updating case owner through transcript.    
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 07-Nov-2017  
***************************************************************************************************/

public class CustomerCare_Transcript_TriggerHandler{
    
    public static void ccUpdateCaseowner(List<LiveChatTranscript> listTranscripts){
        List<LiveChatTranscript> listNewTranscripts =[SELECT Id,CaseId,OwnerId
                                                      FROM LiveChatTranscript WHERE Id IN :listTranscripts];
        map<String,string> mapCaseIdToOwnerId= new map<String,string>();
        for(LiveChatTranscript liveChatTranscriptObj :listNewTranscripts){
            mapCaseIdToOwnerId.put(liveChatTranscriptObj.Caseid,liveChatTranscriptObj.ownerID);   
        }
        
        List<case> listCases = [Select id from Case where Id IN :mapCaseIdToOwnerId.keySet()];
        for(Case caseObj : listCases){
            caseObj.ownerId = mapCaseIdToOwnerId.get(caseObj.id);            
        }
        try{
            Update listCases;
        }catch(exception e){
            system.debug('Exception occured in Updating Case owner during livechat'+e);
        }
    }
}