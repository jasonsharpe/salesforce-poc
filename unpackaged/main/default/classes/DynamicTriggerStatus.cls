global  class DynamicTriggerStatus{
    
      /**
     * Method which will return status of trigger 
     * @param triggerName : Name of the trigger which is to be disabled
     **/
    public static Boolean getTriggerStatus(String triggerName) {
        Boolean checkTriggerstatus = false;
        List<Trigger_Management__mdt> trigManagmnt = new List<Trigger_Management__mdt>();
        //    fetching data from custom metadata "Trigger_Management__mdt "
        trigManagmnt = [SELECT Trigger_Name__c, Is_Disable__c from Trigger_Management__mdt WHERE Trigger_Name__c =: triggerName];
        
        if(!trigManagmnt.isEmpty()) {
            checkTriggerstatus = trigManagmnt[0].Is_Disable__c; 
        }
        return checkTriggerstatus;
    }
}