public with sharing class EmailMessageTriggerHelper {

    public static void checkCreditCardNumber(List<EmailMessage> newItems) {
        for(EmailMessage each: newItems) {
            if(each.TextBody != null) {
                each.TextBody = creditCardPattern(each.TextBody); //check if credit card number present in email body
            }
            if(each.Subject != null) {
                each.Subject = creditCardPattern(each.Subject); //check if credit card number present in email subject
            }
        }
    }

    public static void sendMessageBasedOnApproverStatus(List<EmailMessage> newItems, Map<Id, EmailMessage> oldItems) {
        Map<Id,EmailMessage> idToApproveMessagemap = new Map<Id,EmailMessage>();
        Map<Id,EmailMessage> idToRejectMessagemap = new Map<Id,EmailMessage>();

        for(EmailMessage em : newItems) {
            if(em.Approver_status__c == 'Approved' && oldItems.get(em.Id).Approver_status__c != 'Approved' && String.valueOf(em.ParentId).startswith('500')){
                idToApproveMessagemap.put(em.ParentId,em);
            }
            if(em.Approver_status__c == 'Rejected' && oldItems.get(em.Id).Approver_status__c != 'Rejected' && String.valueOf(em.ParentId).startswith('500')){
                idToRejectMessagemap.put(em.ParentId,em);
            }
        }
        if(!idToApproveMessagemap.isEmpty()) {
             CustomerCareEmailMessageEmail.sendApproval(idToApproveMessagemap);   
        }
        if(!idToRejectMessagemap.isEmpty()) {
            CustomerCareEmailMessageEmail.sendRejection(idToRejectMessagemap);   
        }
    }

    public static string creditCardPattern(String description) {
        List<String> strArrInit= new List<String>();
        if(description.contains('\n')) { 
            strArrInit.addAll(description.split('\n')); //split the string with new Line
        } else {
            strArrInit.add(description);
        }
        
        List<String> strArr = new List<String>();
        for(String each: strArrInit) {
            if(each.contains(' ')) {
                strArr.addAll(each.split(' ')); //split the string with space
            } else {
                strArr.add(each);
            }
        }
        
        description = ' '+ description + ' ';
        description = patternMatcher(description,'\\s\\d{16}\\s');
        description = patternMatcher(description,'\\s\\d{15}\\s');
        description = patternMatcher(description,'\\s\\d{14}\\s');
        
        description = patternMatcher(description,'\\s[-#@:]\\d{16}\\s');
        description = patternMatcher(description,'\\s[-#@:]\\d{15}\\s');
        description = patternMatcher(description,'\\s[-#@:]\\d{14}\\s');
        
        description = patternMatcher(description,'\\s\\d{16}[.,]\\s');
        description = patternMatcher(description,'\\s\\d{15}[.,]\\s');
        description = patternMatcher(description,'\\s\\d{14}[.,]\\s');
        
        description = patternMatcher(description,'\\s[-#@:]\\d{16}[.,]\\s');
        description = patternMatcher(description,'\\s[-#@:]\\d{15}[.,]\\s');
        description = patternMatcher(description,'\\s[{(]\\d{14}[)}]\\s');
        
        description = patternMatcher(description,'\\s\\d{4} *\\d{4} *\\d{4} *\\d{4}\\s');
        description = patternMatcher(description,'\\s\\d{4} *\\d{6} *\\d{5}\\s');
        description = patternMatcher(description,'\\s\\d{4} *\\d{6} *\\d{4}\\s');
        
        description = patternMatcher(description,'\\s[-#@:]\\d{4} *\\d{4} *\\d{4} *\\d{4}\\s');
        description = patternMatcher(description,'\\s[-#@:]\\d{4} *\\d{6} *\\d{5}\\s');
        description = patternMatcher(description,'\\s[-#@:]\\d{4} *\\d{6} *\\d{4}\\s');
        
        description = patternMatcher(description,'\\s\\d{4} *\\d{4} *\\d{4} *\\d{4}[.,]\\s');
        description = patternMatcher(description,'\\s\\d{4} *\\d{6} *\\d{5}[.,]\\s');
        description = patternMatcher(description,'\\s\\d{4} *\\d{6} *\\d{4}[.,]\\s');
        
        description = patternMatcher(description,'\\s[\\[\'\"{(]\\d{4} *\\d{4} *\\d{4} *\\d{4}[\\]\'\")}]\\s');
        description = patternMatcher(description,'\\s[\\[\'\"{(]\\d{4} *\\d{6} *\\d{5}[\\]\'\")}]\\s');
        description = patternMatcher(description,'\\s[\\[\'\"{(]\\d{4} *\\d{6} *\\d{4}[\\]\'\")}]\\s');
        
        description = patternMatcher(description,'\\s\\d{4}-*\\d{4}-*\\d{4}-*\\d{4}\\s');
        description = patternMatcher(description,'\\s\\d{4}-*\\d{6}-*\\d{5}\\s');
        description = patternMatcher(description,'\\s\\d{4}-*\\d{6}-*\\d{4}\\s');
        
        description = patternMatcher(description,'\\s[-#@:]\\d{4}-*\\d{4}-*\\d{4}-*\\d{4}\\s');
        description = patternMatcher(description,'\\s[-#@:]\\d{4}-*\\d{6}-*\\d{5}\\s');
        description = patternMatcher(description,'\\s[-#@:]\\d{4}-*\\d{6}-*\\d{4}\\s');
        
        description = patternMatcher(description,'\\s\\d{4}-*\\d{4}-*\\d{4}-*\\d{4}[.,]\\s');
        description = patternMatcher(description,'\\s\\d{4}-*\\d{6}-*\\d{5}[.,]\\s');
        description = patternMatcher(description,'\\s\\d{4}-*\\d{6}-*\\d{4}[.,]\\s');
        
        description = patternMatcher(description,'\\s[{(]\\d{4}-*\\d{4}-*\\d{4}-*\\d{4}[)}]\\s');
        description = patternMatcher(description,'\\s[{(]\\d{4}-*\\d{6}-*\\d{5}[)}]\\s');
        description = patternMatcher(description,'\\s[{(]\\d{4}-*\\d{6}-*\\d{4}[)}]\\s');
        
        description = patternMatcher(description,'\\s[\\[\'\"{(]\\d{4}-*\\d{4}-*\\d{4}-*\\d{4}[\\]\'\")}]\\s');
        description = patternMatcher(description,'\\s[\\[\'\"{(]\\d{4}-*\\d{6}-*\\d{5}[\\]\'\")}]\\s');
        description = patternMatcher(description,'\\s[\\[\'\"{(]\\d{4}-*\\d{6}-*\\d{4}[\\]\'\")}]\\s');
        
        return description;
    }

    private static string patternMatcher(String message,String regex) {
        Pattern regexPattern = Pattern.compile(regex);
        Matcher regexMatcher = regexPattern.matcher(message);
        String replacedMessage;

        while (regexMatcher.find()) {
            String matched =regexMatcher.group() ;
            String og = matched.replaceAll('[0-9]','x');
            message = message.replace(matched, og);
        }

        return message;
    }
}