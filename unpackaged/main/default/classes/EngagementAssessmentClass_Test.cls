@IsTest
public class EngagementAssessmentClass_Test {
    
    
    private static testMethod void testSave()
    {
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest3@gmail.com','RMTest1','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        eng.Engagement_Type__c='Committee';
        RetryForTest.upsertOnUnableToLockRow(eng);
        Engagement__c engm=[select Id,name,Business_Objective__c from Engagement__c where Id =: eng.Id];
        Assessment__c assess = new Assessment__c();
        
        
        pagereference prf = ApexPages.currentPage();
        prf.getParameters().put('eng',eng.Id);
        //Create a new instance of standard controller
        ApexPages.StandardController sc1 = new ApexPages.standardController(assess);
        
        EngagementAssessmentClass controller1 = new EngagementAssessmentClass(sc1);
        System.assertNotEquals(null,controller1.Save());
        controller1.cancel();
        
    }
}