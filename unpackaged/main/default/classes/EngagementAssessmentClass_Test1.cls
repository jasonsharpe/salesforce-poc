@IsTest
public class EngagementAssessmentClass_Test1 {
    
    
    private static testMethod void testSave()
    {
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest3@gmail.com','RMTest3','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        insert eng;
        
        PageReference pageRef = Page.AssessVF;
        pageRef.getParameters().put('eng', String.valueOf(eng.Id));
        Test.setCurrentPage(pageRef);
        Assessment__c a= new Assessment__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        EngagementAssessmentClass testAccPlan = new EngagementAssessmentClass(sc);
        System.assertNotEquals(null,testAccPlan.Save());
        
        
    }
    private static testMethod void testSave1()
    {
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest3@gmail.com','RMTest3','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        try
        {  
            insert eng;
            
            system.debug('Successful Exe'+eng);
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        
        
        PageReference pageRef = Page.AssessVF;
        pageRef.getParameters().put('eng', String.valueOf(eng.Id));
        Test.setCurrentPage(pageRef);
        Assessment__c a= new Assessment__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(a);
        EngagementAssessmentClass testAccPlan = new EngagementAssessmentClass(sc);
        a.Engagement__c=null;
        try
        {
            testAccPlan.Save();
        }catch(Exception ex)
        {
            System.debug(ex.getMessage());
        }
        
    }
}