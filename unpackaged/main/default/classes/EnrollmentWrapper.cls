/**************************************************************************************************
* Apex Class Name   : EnrollmentWrapper
* Purpose           : This is the wrapper class which is used for mapping Individual key of Enrollment Json Response into seperate field
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 14-Jan-2018  
***************************************************************************************************/
public class EnrollmentWrapper{
    public String Program{get; set;}
    public String EnrollmentStatus{get; set;}
    public DateTime EnrollmentDate{get; set;}
    public String RegistrationStatus{get; set;}
    public String TestAreaCode{get; set;}
    public Boolean IsRAD{get; set;}
    public String TestCenterCode{get; set;}
    public String TestAreaName{get; set;}
    public String QualificationReason{get;set;}
    public String Level{get;set;}
    public String Cycle{get;set;}
    public Integer Year{get;set;}
    public DateTime RegistrationDate{get;set;}
    public DateTime CompletedDate{get;set;}
    public DateTime CancellationDate{get; set;}
    public String ExamAttendanceStatus{get; set;}
    public String ExamResultsStatus{get; set;}
    public String ExamResultsVisibilityStatus{get;set;}
    
}