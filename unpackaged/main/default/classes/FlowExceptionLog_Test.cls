@isTest
public class FlowExceptionLog_Test {
    @isTest
    public static void flowFaultLog(){
        FlowExceptionLog.FlowErrorLog flowErrorLog = new FlowExceptionLog.FlowErrorLog();
        flowErrorLog.flowName = 'test';
        flowErrorLog.errorMsg = 'test';
        flowErrorLog.errorDateTime = System.now();
        flowErrorLog.currentStage = 'test';
        Test.startTest();
        FlowExceptionLog.flowFaultLog(new List<FlowExceptionLog.FlowErrorLog>{flowErrorLog});
        Test.stopTest();
    }

}