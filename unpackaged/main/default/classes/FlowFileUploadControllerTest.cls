@isTest
public class FlowFileUploadControllerTest {
    @testSetup static void createData(){
        Account accObj = new Account(name = 'TestAccount');
        insert accObj;
    }
    static testMethod void saveTheFileTest(){
        List<Account> accList = [SELECT Id from Account LIMIT 1];
        if(accList != null && !accList.isEmpty()){
            test.startTest();
            Blob data = Blob.valueof('Some random String');          
            FlowFileUploadController.saveTheFile(accList[0].id,'test.txt',EncodingUtil.base64Encode(data),'text/plain'); 
            test.stopTest();
        }
    }
}