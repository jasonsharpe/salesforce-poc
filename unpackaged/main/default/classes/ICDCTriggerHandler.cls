public interface ICDCTriggerHandler {
    void handleCreate(List<sObject> changeEvents);
    void handleGAPCreate(List<sObject> changeEvents);
    void handleAllCreate(List<sObject> changeEvents);
    
    void handleUpdate(List<sObject> changeEvents);
    void handleGAPUpdate(List<sObject> changeEvents);
    void handleAllUpdate(List<sObject> changeEvents);
    
    void handleDelete(List<sObject> changeEvents);
    void handleGAPDelete(List<sObject> changeEvents);
    void handleAllDelete(List<sObject> changeEvents);
    
    void handleUndelete(List<sObject> changeEvents);
    void handleGAPUndelete(List<sObject> changeEvents);
    void handleAllUndelete(List<sObject> changeEvents);
    
    void handleOverflow(List<sObject> changeEvents);
	
    Boolean isDisabled();
    String getTriggerName();
}