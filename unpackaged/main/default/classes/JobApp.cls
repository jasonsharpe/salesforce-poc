public without sharing class JobApp {

public Id parentId; 
    public JobApp(ApexPages.StandardController controller) {

        Job_Application__c record = (Job_Application__c)controller.getRecord();
        parentId = record.Position__c;
        system.debug('Parent Id'+parentId);
    }
    
    public Pagereference MyFWithCustomSettingNew(){
       
       Id uid=UserInfo.getUserId();
       String url='';
       String environment='';
       String roleNameVal='';
       User u=new User();
       u=[select id,name,contactid from User where id=:uid];
               
       List<JobApplicationRecordTypeSelection__c> jobselectionlst= JobApplicationRecordTypeSelection__c.getall().values();
       //List<URLSetting__c> urlselectionlst=URLSetting__c.getall().values();
       Role__c roleN=[select id,name,Engagement_name__r.Engagement_Type__c,Role_Type__c,JARecordType__c from Role__c where ID=:parentId limit 1];
       
       
       if(u.contactid==null){
           
           environment=URLSetting__c.getInstance('InternalURLSetting').EnvironmentVariable__c;
           if(environment=='1'){
               
               url=URLSetting__c.getInstance('InternalURLSetting').DevURL__c;
               roleNameVal=RoleURL__c.getInstance('RoleFieldValue').DevURL__c;
           }
           if(environment=='2'){
               
               url=URLSetting__c.getInstance('InternalURLSetting').TestURL__c;
               roleNameVal=RoleURL__c.getInstance('RoleFieldValue').TestURL__c;

           }
           if(environment=='3'){
               
               url=URLSetting__c.getInstance('InternalURLSetting').ProdURL__c;
               roleNameVal=RoleURL__c.getInstance('RoleFieldValue').ProdURL__c;
           }
           for(JobApplicationRecordTypeSelection__c jobselect:jobselectionlst){
               
               system.debug('Eng Type ac'+roleN.Engagement_name__r.Engagement_Type__c);
               system.debug('Eng Type cus'+jobselect.Engagement_Type__c);
               system.debug('Role Type ac'+roleN.Role_Type__c);
               system.debug('Role Type cus'+jobselect.Role_Type__c);
               
			   //String recordTypeId =Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get(jobselect.JobApplication_RecordType__c).getRecordTypeId();
               if(roleN.JARecordType__c!=null){
                       
                        PageReference pr=new PageReference(url+'e?RecordType='+roleN.JARecordType__c+'&nooverride=1');       

                        pr.getParameters().put(roleNameVal,roleN.Name);
                        pr.getParameters().put('retURL',parentId); //30-Jan
                        return pr; 
                   }          
           
           }
           //system.debug('recordTypeId' +Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Default').getRecordTypeId());
           //String recordTypeId =Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
           //system.debug('recordTypeId' +Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Default').getRecordTypeId());
           PageReference pr=new PageReference(url+'e?RecordType='+roleN.JARecordType__c+'&nooverride=1'); 
           pr.getParameters().put(roleNameVal,roleN.Name);
           pr.getParameters().put('retURL',parentId);
           return pr;
       }
       	   PageReference pr=new PageReference(url); 
           pr.getParameters().put('retURL',parentId); //30-Jan
		   return pr;
    }


}