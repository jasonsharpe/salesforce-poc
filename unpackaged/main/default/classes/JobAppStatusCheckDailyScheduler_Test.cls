@isTest
public class JobAppStatusCheckDailyScheduler_Test {

    public static testmethod void first1(){
         User RMOwner=VM_TestDataFactory.createUser('RMTest1@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='t FY20';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        
         try
        {
            
            insert eng;
            system.debug('Role inserted');
            
        }
        catch(DMLException ex){
            System.debug(ex) ;
        }
    
        
         Role__c rl = VM_TestDataFactory.createRole(eng) ;
        
        try
        {
            
            insert rl;
            system.debug('Role inserted');
            
        }
        catch(DMLException ex){
            System.debug(ex) ;
        }
        Account acc = VM_TestDataFactory.createAccountRecord() ;
            Contact con = VM_TestDataFactory.createContactRecord(acc) ;
        Job_Application__c job_app = VM_TestDataFactory.createJobApplication(rl,con);
        
        try
        {
           //job_app.JobAppStatusDate__c=date.today().adddays(-30);
            job_app.Status__c = 'Submitted';
            insert job_app;
            job_app.JobAppStatusDate__c=date.today().adddays(-30);
            update job_app;
            system.debug('Job application inserted');
            
        }
        catch(DMLException ex){
            System.debug(ex) ;
        }
        
        
    Test.startTest();
    JobApplicationStatusCheckDailyScheduler scheUpdateAccount = new JobApplicationStatusCheckDailyScheduler();
String sch ='0 48 * * * ?'; 
System.schedule('Schedule to update Account Name', sch,scheUpdateAccount);
Test.stopTest();
    }   
        
}