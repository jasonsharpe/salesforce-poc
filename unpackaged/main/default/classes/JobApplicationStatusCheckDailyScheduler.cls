global without sharing class JobApplicationStatusCheckDailyScheduler Implements Schedulable
{
        global void execute(SchedulableContext sc)
        {
            checkStatus();
        }

        public void checkStatus()
        {
             List<Job_Application__c> jbLst=new List<Job_Application__c>();
             List<Id> roleId=new List<Id>();
             List<Task> tskLst=new List<Task>();
             List<RecordType> rtypes;
             Map<String,String> taskRecordTypes;
             rtypes=new List<RecordType>();
             taskRecordTypes = new Map<String,String>{};
             rtypes=[Select Name, Id From RecordType where sObjectType='Task' and isActive=true];
              for (RecordType rt : rtypes) {
                    taskRecordTypes.put(rt.Name,rt.Id);
              }
             Date dt=system.today().adddays(-30);
             
             //Date dt=system.today();
             jbLst=[select id,Status__c,JobAppStatusDate__c,Position__c from Job_Application__c where JobAppStatusDate__c=:dt and (Status__c<>'Accepted and Invited' or Status__c<>'RM Rejected')];
             for(Job_Application__c j:jbLst)
             {
                 //if(j.Status__c<>'Accepted and invited'||j.Status__c<>'Rejected'){
                 
                     roleId.add(j.Position__c);
                // }
             }
             Map<Id,Role__c> roleMap=new Map<Id,Role__c>([select id,Engagement_name__r.Id,Engagement_name__r.Business_Owner__c,Engagement_name__r.name from Role__c where Id in:roleId]);
                
                for(Job_Application__c jb:jbLst)
                {
                    if(jb.JobAppStatusDate__c.adddays(30)==system.today())
                    //if(jb.JobAppStatusDate__c==system.today())
                    {
                        Task t=new Task();
                        t.ownerid=roleMap.get(jb.Position__c).Engagement_name__r.Business_Owner__c;
                        t.Subject = 'Status Unchanged';
                        t.Description = 'Job Application Status:'+jb.Status__c+' is Unchanged for 30 days';
                        t.Status = 'Open';
                        t.Priority = 'High';
                        t.WhatId = jb.Id;
                        t.recordtypeid=taskRecordTypes.get('TaskRecordInternal');                
                        tskLst.add(t);
                    }
                }
                try
                {
                    if(tskLst.size()>0)
                    insert tskLst;
                }catch(System.DMLException ex)
                {
                    system.debug('Error in Creating task for JobApplicationStatusCheckDailyScheduler  '+ex.getDMLMessage(0));
                }
        }
}