/**************************************************************************************************
* Apex Class Name   : LoginHistoryWrapper_Test
* Purpose           : This is the test class for LoginHistoryWrapper wrapper class
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 30-April-2018  
***************************************************************************************************/
@isTest
public class LoginHistoryWrapper_Test{

    public static testMethod void LoginHistoryWrapper_Test1(){
        LoginHistoryWrapper LoginWrapper = new LoginHistoryWrapper();
        LoginWrapper.PersonId = 'TestPr1';
        LoginWrapper.LoginStatus = 'TestES1';
        LoginWrapper.LoginAttemptDate = Date.today();
        system.assert(LoginWrapper != null);
    }
}