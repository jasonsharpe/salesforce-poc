/**************************************************************************************************************************************************
Name:  MDM_Contact_Triggerhandler
Copyright © 2017  ITC
=====================================================================
Purpose: 1. If New Contact with No employer created/updated then Create Affiliation with 'Unknown Employer' and set it as Primary Affiliation.
         2. If Contact created with Employer created/updated then create Affiliation with Employer by setting it as 
                                                           primary and removing existing primary association.                                                                                                            
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0      Anju           10/12/2017     Created         Added methods onAfterInsert and onAfterUpdate
2.0      Smitha         10/18/2017     Updated         1. Added method onBeforeInsert and assignDefaultAccount
                                                       2. Updated onAfterUpdate to onBeforeUpdate
                                                       3. Removed recursion handler
3.0      Anupama        01/11/2018     updated         Added method onAfterInsertUpdate                                               
4.0      Nikhil         14/1/2018      Optimzed        1. Optimized the code upto some extent
5.0      Anupama        25/01/2018     updated         
****************************************************************************************************************************************************/
public with sharing class MDM_Contact_Triggerhandler {
	
    public static Account defaultAcc = [SELECT Id,Name,RecordType.Name FROM Account WHERE type =: Label.MDM_Default_Account];
    public static Date dtToday = Date.today();
    final String OR_CONTACT_ROLE = 'General Outreach';

    /****************************************************************************************************************************
       Purpose:  This method is called on before insert of Contact, to assign Default Account to Contact inserted with no Account                                                      
       Parameters: List<Contact> newContactList
       Returns: void
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Smitha         10/18/2017     Created         Added this to have assigning of Default Account in before insert case 
                                                              rather than after insert.
    ******************************************************************************************************************************/
    public void onBeforeInsert(List<Contact> newContactList) {
 
    }
     /****************************************************************************************************************************
       Purpose:  This method is called on after insert of Contact, to create Affiliation with Account                                                     
       Parameters: List<Contact> newContactList
       Returns: void
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR       DATE           DETAIL          Description
       1.0      Anju         10/12/2017     Created         To create affiliation between Account and Contact, when contact is created
    *****************************************************************************************************************************/
    public void onAfterInsert(List<Contact> newContactList) {
        List<Affiliation__c> lstAffToUpsert = new List<Affiliation__c>();
        for(Contact cnt : newContactList){
            Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Contact.getRecordTypeInfosById();
            String recordTypeName =rtMap.get(cnt.RecordTypeId).getName();
            if(recordTypeName == 'Outreach Contact' && cnt.AccountId == defaultAcc.Id){
              Affiliation__c affl =   new Affiliation__c(Account__c = defaultAcc.Id, 
                                                            Contact__c=cnt.Id, 
                                                            Role__c=OR_CONTACT_ROLE, 
                                                            Start_Date__c = dtToday, 
                                                            MDM_Primary__c=true,
                                                            Type__c = defaultAcc.RecordType.Name);
                lstAffToUpsert.add(affl);
            }
        }
        upsertAffliations(lstAffToUpsert);
    }
    /****************************************************************************************************************************
       Purpose:  This method is called on before update of Contact, to create Affiliation with Account if Account is changed. 
       Parameters: map<Id, Contact> mapOldContacts, map<Id, Contact> mapNewContacts
       Returns: void
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR       DATE           DETAIL          Description
       1.0      Anju         10/12/2017     Created         To create affiliation between Account and Contact, when Contact is updated
       1.1      Smitha       10/18/2017     Updated         Changed method to onBeforeUpdate
    *****************************************************************************************************************************/
    public void onBeforeUpdate(map<Id, Contact> mapOldContacts, map<Id, Contact> mapNewContacts) {
        
        checkUpdatedContact(mapOldContacts, mapNewContacts);
        
    }
        
   /****************************************************************************************************************************
       Purpose:  This method will check if the Account on contact is changed after update. Affiliation is created only if the Account is changed. 
                 If Account is changed to blank then default Account is assigned
       Parameters: map<Id, Contact> mapOldContacts, map<Id, Contact> mapNewContacts
       Returns: void
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR       DATE           DETAIL          Description
       1.0      Smitha       10/18/2017     Created         Created a seperate method to check for change of Account on Contact.
    *****************************************************************************************************************************/ 
    private void checkUpdatedContact(map<Id, Contact> oldContacts, map<Id, Contact> newContacts) {
        
        list<Contact> lstContacts = new list<Contact>();
        
        // collecting Contacts whose Account is changed
        for(Contact objCon : newContacts.values() ) {
            
            if(objCon.AccountId != oldContacts.get(objCon.Id).AccountId){
                
                lstContacts.add(objCon);
            }
        }
        if( !lstContacts.isEmpty()) {
            
             }
    }
    
   /****************************************************************************************************************************
       Purpose:  This method will create Affiliation of Conatct with Account when Contact is either created or updated.
                 The newly created Affiliation is marked as primary and any existing primary affiliation will be reset.
                 For the newly created affiliation started will be current date and for previous affiliation, end date will be current date
       Parameters: list<Contact> lstContacts
       Returns: void
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR     DATE           DETAIL          Description
       1.0      Anju       10/12/2017     Created         Added a method to create Affiliation.
       1.1      Smitha     10/18/2017     Updated         Moved default account assignment to seperate method
    *****************************************************************************************************************************/  
    
    public void partnerEnableAccount(list<Contact> lstContacts,map<Id, Contact> mapOldContacts, map<Id, Contact> mapNewContacts)
    {
        
       List<Account> accLst=new List<Account>();
        
        for(Contact con:lstContacts)
        {
            
            system.debug('Partner Enabled new value '+con.accountId);
            system.debug('Partner Enabled old value '+mapOldContacts);
            if(con.accountId!=mapOldContacts.get(con.id).accountId)
            {
                system.debug('inside the loop'); 
                Account a=new Account();
                a.id=con.accountId;
                a.isPartner=true;
                accLst.add(a);   
            }
        }
        if(!accLst.isEmpty())
        {
            update accLst;
        }
    
    }
    
    public void upsertAffliations(List<Affiliation__c> lstAffToUpsert){
         if( !lstAffToUpsert.isEmpty() ) {
                if ((Schema.sObjectType.Affiliation__c.isCreateable())&& (Schema.sObjectType.Affiliation__c.isUpdateable())){
                    Database.UpsertResult[] saveResults = Database.upsert(lstAffToUpsert, false);
                }
            }
    }
    
}