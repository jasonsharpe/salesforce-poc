/**************************************************************************************************************************************************
Purpose: This is a controller class for vf page MDM_ORContacts_Affilaitions, which will display Contacts and Affiliation related to the Account 
from where the page is launched. It will filter only Contacts which has active affiliation. And affilaitons whose role is present in custom label
MDM_Affiliation_Roles will be displayed. Also, it provides methods to redirect to edit/create Affiliation and Contact and delete Contacts.
==============================================
Reference : US - 8035
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL                                   Description
1.0      Smitha                        Created MDM_ORContacts_Affilaitions       Created this class 

****************************************************************************************************************************************************/
public with sharing class MDM_ORContacts_Affilaitions {
    
     private final Id idRecordId ;
     // list for Affiliations displayed on Account Detail page
     public list<Affiliation__c> lstAffiliations { get; set; }
     // list for Contacts displayed on Account Detail page
     public list<Contact> lstContactsToDisplay   { get; set; } 
     
     public MDM_ORContacts_Affilaitions(ApexPages.StandardController stdController) {
         idRecordId = Id.valueOf(stdController.getId());
         lstAffiliations = new list<Affiliation__c>();
         lstContactsToDisplay = new list<Contact>();
         displayContactsAndAffiliation(idRecordId);
     }
     
     /****************************************************************************************************************************
           Purpose:  This method collects only those Contacts which has active affilaiton related to the Account from where the page is launched
           Parameters: Id accountId
           Returns: none
        
           History                                                            
           --------                                                           
           VERSION  AUTHOR         DATE           DETAIL          Description
           1.0      Smitha                        Created         To display Contacts and affiliations on vf page
        ******************************************************************************************************************************/
     private void displayContactsAndAffiliation(Id accountId){
         
        map<Id, Affiliation__c> mapContactIdToActiveAffiliation = new map<Id, Affiliation__c>();
        //Retreive roles from custom label MDM_Affiliation_Roles
        String setOutreach = Label.MDM_Affiliation_Roles;
        String[] arrOutreach = setOutreach.split(';');
        Set<String> setOutreachRoles = new Set<String>();
        for(String str :  arrOutreach ){
            setOutreachRoles.add(str);    
        }
        
        // check for active affiliations and whose roles in CL
        for(Affiliation__c affCheck :[ SELECT Id, Type__c, Name, Affiliation_Name__c, Role__c, Account__c, Contact__c, Active_Affiliation__c, Start_Date__c, End_Date__c, Contact__r.Name
                                       FROM Affiliation__c 
                                      WHERE Role__c IN: setOutreachRoles AND (Account__c =: idRecordId OR MDM_Mapped_Account__c =: idRecordId)]){
             lstAffiliations.add(affCheck);
            // active affiliation check for OR layout                               
            if(affCheck.Active_Affiliation__c){
                mapContactIdToActiveAffiliation.put(affCheck.Contact__c, affCheck);
            }
        }
        
        // store the contacts in list who has active affiliation to display in vf page 
        for(Contact objCon : [ SELECT MDM_Outreach_Lookup__c, AccountId, RecordType.Name, Phone, Name
                               FROM Contact 
                               WHERE Id IN : mapContactIdToActiveAffiliation.keyset()]){
            
            lstContactsToDisplay.add(objCon);
        }
     }
     
     /****************************************************************************************************************************
           Purpose:  This method is called on click of edit button on the related list of Contacts to edit a particular record
           Parameters: 
           Returns: PageReference
        
           History                                                            
           --------                                                           
           VERSION  AUTHOR         DATE           DETAIL          Description
           1.0      Smitha                        Created         This method will allow to edit the particular contact record
        ******************************************************************************************************************************/
        public PageReference editContactRecord(){
            
            String strContactId = ApexPages.currentPage().getParameters().get('strContactId'); 
            return new PageReference('/003/e?id='+strContactId+'&retURL='+idRecordId);
        }
      
        /****************************************************************************************************************************
           Purpose:  This method is called on click of edit button on the related list of Affiliations to edit a particular record
           Parameters: 
           Returns: PageReference
        
           History                                                            
           --------                                                           
           VERSION  AUTHOR         DATE           DETAIL          Description
           1.0      Smitha                        Created         This method will allow to edit the particular affiliation record
        ******************************************************************************************************************************/
        public PageReference editAffiliationRecord(){
            
            String strAffiliationId = ApexPages.currentPage().getParameters().get('strAffiliationId');  
            return new PageReference('/a1f/e?id='+strAffiliationId+'&retURL='+idRecordId);
        }
        
      /****************************************************************************************************************************
           Purpose:  This method is called on click of new button on the related list of Contact to cretae a new contact.
           Parameters: 
           Returns: PageReference
        
           History                                                            
           --------                                                           
           VERSION  AUTHOR         DATE           DETAIL          Description
           1.0      Smitha                        Created         This method will allow to create a new contact record
        ******************************************************************************************************************************/
        public PageReference newContact(){   
            
           PageReference redirectLink = new PageReference('/setup/ui/recordtypeselect.jsp?ent=Contact&save_new_url=%2F003%2Fe%3F&retURL=/'+idRecordId+'&con4_lkid='+idRecordId);         
           return redirectLink; 
        }
        /****************************************************************************************************************************
           Purpose:  This method is called on click of new button on the related list of Affiliations to create a new record
           Parameters: 
           Returns: PageReference
        
           History                                                            
           --------                                                           
           VERSION  AUTHOR         DATE           DETAIL          Description
           1.0      Smitha                        Created         This method will allow to create a new affiliation record
        ******************************************************************************************************************************/
        public PageReference newAffiliation(){   
            
            String acctName = [SELECT Name FROM Account where Id=: idRecordId].Name;
            String strRecordTypeName = [SELECT RecordType.Name FROM Account WHERE Id=: idRecordId].RecordType.Name;
         
            return new PageReference('/a1f/e?ent=CF00Nj0000009ePPQ&CF00Nj0000009ePPQ='+EncodingUtil.urlEncode(acctName,'UTF-8')+'&retURL=/'+idRecordId+'&00Nj0000009ePQM='+strRecordTypeName+'&CF00Nj0000009ePPQ_lkid='+idRecordId+'&save_new_url=/'+idRecordId);
        }
        /****************************************************************************************************************************
           Purpose:  This method is called on click of delete button on the related list of Affiliations to delete record
           Parameters: 
           Returns: PageReference
        
           History                                                            
           --------                                                           
           VERSION  AUTHOR         DATE           DETAIL          Description
           1.0      Smitha                        Created         This method will allow to delete an affiliation record
        ******************************************************************************************************************************/
        public PageReference delAffiliationRecord(){
            String strAffiliationId = ApexPages.currentPage().getParameters().get('strAffiliationId');  
            // delete affiliation
            delete [Select Id From Affiliation__c Where Id =: strAffiliationId];
            return new PageReference('/'+idRecordId);
        }
}