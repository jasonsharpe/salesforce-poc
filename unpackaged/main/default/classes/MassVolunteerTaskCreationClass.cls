public class MassVolunteerTaskCreationClass {

    public List<Engagement_Volunteer__c> engVolList{get;set;}
    public List<EngagementVolunteerWrapper> engVolWrapperList{get;set;}
    public List<EngagementVolunteerWrapper> selectedVolunteers{get;set;}
    public List<User> userLst;
    public Map<Id,User> userMap;
    public List<Id> conIdLst;
    public List<Task> tskList;
    public Task tsk{get;set;}
    public boolean checkSelect{get;set;}
    List<RecordType> rtypes;
    Map<String,String> taskRecordTypes;
    public String engId='';
    Public Integer size{get;set;}
    Public Integer noOfRecords{get; set;}
    public List<SelectOption> paginationSizeOptions{get;set;}
    public static final Integer QUERY_LIMIT = 10000;
    public static final Integer PAGE_SIZE = 50;
    Map<Id,EngagementVolunteerWrapper> mapHoldingSelectedRecords{get;set;}

    public MassVolunteerTaskCreationClass(ApexPages.StandardController controller) {
        
        /*taskRecordTypes = new Map<String,String>{};
        rtypes=new List<RecordType>();
        tsk=(Task)controller.getRecord();
        engVolList = new List<Engagement_Volunteer__c>();
        engVolWrapperList = new List<EngagementVolunteerWrapper>();
        engId=ApexPages.currentPage().getParameters().get('eng');
        engVolList = [Select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name from Engagement_Volunteer__c where Engagement__c=:engId and Engagement_Volunteer__c.Status__c!='Off-boarded'];
        if(engVolList==null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There are no Volunteers on Engagement for Task Creation'));    
        }
        else
        {
            for(Engagement_Volunteer__c ev:engVolList)         
            {                                              
                engVolWrapperList.add(new EngagementVolunteerWrapper(ev,false));                                    
            } 
        }*/
        
    }
       public MassVolunteerTaskCreationClass(){
           selectedVolunteers = new List<EngagementVolunteerWrapper>(); 
        	userLst=new List<User>();
    	tskList=new List<Task>();
    	conIdLst=new List<Id>();  
    	userMap=new Map<Id,User>();
        mapHoldingSelectedRecords = new Map<Id, EngagementVolunteerWrapper>();
        rtypes=new List<RecordType>();
        taskRecordTypes = new Map<String,String>{};
        init();
        tsk=new Task();
    }
    public void init() {
         engVolWrapperList = new List<EngagementVolunteerWrapper>();
         for (Engagement_Volunteer__c vol: (List<Engagement_Volunteer__c>)setVol.getRecords()) {
             if(mapHoldingSelectedRecords != null && mapHoldingSelectedRecords.containsKey(vol.id)){
                 engVolWrapperList.add(mapHoldingSelectedRecords.get(vol.id));
            
             }
             else{
               engVolWrapperList.add(new EngagementVolunteerWrapper(vol, false));
             }
          }
    }
       public ApexPages.StandardSetController setVol{
     get {
     if(setVol == null) {
     engId=ApexPages.currentPage().getParameters().get('eng');            
     setVol = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name from Engagement_Volunteer__c where Engagement__c=:engId and Engagement_Volunteer__c.Status__c!='Off-boarded' ORDER BY Contact__r.name ASC NULLS LAST limit 2000]));
    
       // sets the number of records show  each page view
       setVol.setPageSize(PAGE_SIZE);
     }
       return setVol;
     }
     set;
    }
    
    public Boolean hasNext {
    get {
    return setVol.getHasNext();
    }
    set;
    }
     /** indicates whether there are more records before the current page set.*/
 public Boolean hasPrevious {
 get {
   return setVol.getHasPrevious();
 }
 set;
 }

 /** returns the page number of the current page set*/
 public Integer pageNumber {
 get {
   return setVol.getPageNumber();
 }
 set;
 }

 /** return total number of pages for page set*/
   Public Integer getTotalPages(){
     Decimal totalSize = setVol.getResultSize();
     Decimal pageSize = setVol.getPageSize();
     Decimal pages = totalSize/pageSize;
     return (Integer)pages.round(System.RoundingMode.CEILING);
 }

 /** returns the first page of the page set*/
 public void first() {
   updateSearchItemsMap();
   setVol.first();
   init();
 }

 /** returns the last page of the page set*/
 public void last() {
   updateSearchItemsMap();
   setVol.last();
   init();
 }

 /** returns the previous page of the page set*/
 public void previous() {
   updateSearchItemsMap();
   setVol.previous();
   init();
 }

 /** returns the next page of the page set*/
 public void next() {
   updateSearchItemsMap();
   setVol.next();
   init();
 }

 //This is the method which manages to remove the deselected records, and keep the records which are selected in map.
 private void updateSearchItemsMap() {
     for(EngagementVolunteerWrapper wrp : engVolWrapperList){
         
      if(wrp.isSelected){
         mapHoldingSelectedRecords.put(wrp.engVol.id, wrp);

      }
      if(wrp.isSelected == false && mapHoldingSelectedRecords.containsKey(wrp.engVol.id)){
         mapHoldingSelectedRecords.remove(wrp.engVol.id);
      }
     }

 }
 Public pagereference mydata(){
     system.debug('limitstartTaskCreation class' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());
    updateSearchItemsMap();
    String a='';
   
    //rtypes=[Select Name, Id From RecordType where sObjectType='Task' and isActive=true];
    for (RecordType rt : [Select Name, Id From RecordType where sObjectType='Task' and isActive=true]) {
        taskRecordTypes.put(rt.Name,rt.Id);
    }
   /* selectedVolunteers = new List<EngagementVolunteerWrapper>(); 
    userLst=new List<User>();
    tskList=new List<Task>();
    conIdLst=new List<Id>();  
    userMap=new Map<Id,User>(); */

    if(mapHoldingSelectedRecords.size()<=0)
    {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Please Select Atleast 1 Volunteer ')); 
        return null;
    
    }
    for(Id recordID:mapHoldingSelectedRecords.keyset())
    {
        
        if(mapHoldingSelectedRecords.get(recordID).isSelected==true)
        {
            //system.debug('recordid' +mapHoldingSelectedRecords.get(recordID));
            //system.debug('mapdata' +mapHoldingSelectedRecords.get(recordID).engVol.Contact__c);
            conIdLst.add(mapHoldingSelectedRecords.get(recordID).engVol.Contact__c);            
        }
    }
    userLst=[select id,contactid from User where contactid IN:conIdLst and isActive=true];
    /******************************************************************************************/
    Map<Id,Contact> conMap=new Map<Id,Contact>([select id,email,MDM_Preferred_Email_for_Volunteering__c from contact where id IN:conIdLst]);
   /*******************************************************************************************/ 
     if(userLst!=null)
     {
         for(User u:userLst)
         {
             userMap.put(u.contactid,u);    
         }
     }
         for(Id recordID:mapHoldingSelectedRecords.keyset())
         {
                Task t=new Task();
                t.subject=tsk.subject;
                t.ActivityDate = tsk.ActivityDate;
                if(userMap.get(mapHoldingSelectedRecords.get(recordID).engVol.Contact__c)==null)
                t.ownerid=UserInfo.getUserId();
                else
                t.ownerid=userMap.get(mapHoldingSelectedRecords.get(recordID).engVol.Contact__c).id;                
                t.whatid=mapHoldingSelectedRecords.get(recordID).engVol.Id;
                t.whoid=mapHoldingSelectedRecords.get(recordID).engVol.Contact__c;
                t.status=tsk.status;
                t.priority=tsk.priority;
                t.recordtypeid=taskRecordTypes.get('TaskRecordExternal');
                t.Start_Date__c=tsk.Start_Date__c;
                t.Task_type__c=tsk.Task_type__c;
                t.Description__c=tsk.Description__c;
                t.Travel_Hours__c=tsk.Travel_Hours__c;
                t.Working_Hours__c=tsk.Working_Hours__c;
                t.Engagement__c=mapHoldingSelectedRecords.get(recordID).engVol.Engagement__c;
                
                /*
                if(t.Task_type__c=='Feedback')
                {
                    t.Feedback_Link__c=Label.VM_FeedbackURL+'?VM_Engagement_Volunteer__c='+mapHoldingSelectedRecords.get(recordID).engVol.Id+'&VM_Role__c='+engVol.Role_del__c+'&Contact__c='+mapHoldingSelectedRecords.get(recordID).engVol.Contact__c+'&VM_Engagement__c='+mapHoldingSelectedRecords.get(recordID).engVol.Engagement__c;      
                
                }*/
                /****************************************************************************************/
                
                if(conMap.get(t.whoid).email!=null)
                {
                    t.VM_Volunteer_Preferred_Email__c=conMap.get(t.whoid).email;    
                }
                if(conMap.get(t.whoid).MDM_Preferred_Email_for_Volunteering__c!=null)
                {
                    t.VM_Volunteer_Preferred_Email__c=conMap.get(t.whoid).MDM_Preferred_Email_for_Volunteering__c;
                }
           
                
                /******************************************************************************************/
                tskList.add(t);    
         }
         If(!tskList.isEmpty())
         {

                 try
                 {
                     system.debug('limitendTaskCreation class' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime());
                     insert tskList;
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Task successfully created for Volunteers')); 
                     pagereference pr = new pagereference('/' +engId);       
                     return pr;
                 }catch(DMLException e)
                 {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' '+e.getdmlmessage(0))); 
                     return null;    
                 }       
          }   
         else
         {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please Select Atleast 1 Volunteer to create Task'));        
             return null;
         }

    return null;     
     

 }
  
    
    public PageReference goback()
    {
        pagereference pr = new pagereference('/' + engId);
       
        return pr;
        
           
    }
    
   
     public class EngagementVolunteerWrapper    
     {         
         public Boolean isSelected{get; set;}         
         public Engagement_Volunteer__c engVol{get; set;}                  
         public EngagementVolunteerWrapper(Engagement_Volunteer__c v,Boolean flag)         
         {              
             engVol = v;              
             isSelected = flag;         
         } 
     }
}