//@isTest(SeeAllData=true)
@isTest
public class MassVolunteerTaskCreationClass_Test {

    public static testMethod void testMassTask() {
        //create a new user
        User RMOwner=VM_TestDataFactory.createUser('RMTest1@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Lange','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        try
        {  
            insert eng;
            

        }catch(DMLException ex)
        {

        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            insert role;
            

        }catch(DMLException ex)
        {

        }
        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc);

        
        Job_Application__c job=VM_TestDataFactory.createJobApplication(role,con); 
        try
        {  
            insert job;
            

        }catch(DMLException ex)
        {

        }                                                          
                try
        {  
            job.Status__c='Submitted';
            job.Submitted__c = true;
            update job;
            job.status__c='Volunteer Accepted';
            update job;
            

        }catch(DMLException ex)
        {

        }       
        Test.startTest();
            Engagement_Volunteer__c engV=new Engagement_Volunteer__c();
            engV=[select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name from Engagement_Volunteer__c where Engagement__c=:eng.Id];
            PageReference pageRef = Page.MassVolunteerTaskCreationVF;
            pageRef.getParameters().put('eng', String.valueOf(eng.Id));
            Test.setCurrentPage(pageRef);    
            MassVolunteerTaskCreationClass masstsk=new MassVolunteerTaskCreationClass();
            masstsk.mydata();
            Boolean b=masstsk.hasNext;
            b=masstsk.hasPrevious;
            Integer i=masstsk.pageNumber;
            i=masstsk.getTotalPages(); 
            masstsk.mydata();
            masstsk.first();
            masstsk.last();
            masstsk.previous();
            masstsk.next();
            masstsk.mydata();
            MassVolunteerTaskCreationClass.EngagementVolunteerWrapper m=new MassVolunteerTaskCreationClass.EngagementVolunteerWrapper(engV,true);
            masstsk.engVolWrapperList.add(m);
            masstsk.mydata();
            masstsk.goback();              
        Test.stopTest();
    }
     public static testMethod void testMassTaskWithoutVolunteers() {        
        User RMOwner=VM_TestDataFactory.createUser('RMTest1@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Lange','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        try
        {  
            insert eng;
            

        }catch(DMLException ex)
        {
        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            insert role;
           
        }catch(DMLException ex)
        {
        }
        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc);

        Test.startTest();
            PageReference pageRef = Page.MassVolunteerTaskCreationVF;
            pageRef.getParameters().put('eng', String.valueOf(eng.Id));
            Test.setCurrentPage(pageRef);    
            MassVolunteerTaskCreationClass masstsk=new MassVolunteerTaskCreationClass();
            masstsk.mydata();
            masstsk.checkSelect=true;
            masstsk.size=10;
            masstsk.noOfRecords=10;
            Boolean b=masstsk.hasNext;
            b=masstsk.hasPrevious;
            Integer i=masstsk.pageNumber;
            i=masstsk.getTotalPages(); 
            masstsk.mydata();
            masstsk.first();
            masstsk.last();
            masstsk.previous();
            masstsk.next();
            masstsk.mydata();
            masstsk.goback();              
        Test.stopTest();
    }

    
}