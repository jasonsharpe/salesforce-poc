/*****************************************************************
Name: MembershipApplicationPolicyHandler
Copyright © 2021 ITC
============================================================
Purpose: Logic for Assigning Contacts To Societies base on Membership Application records
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   09.02.2021   Created   Logic for Assigning Contacts To Societies
*****************************************************************/

public without sharing class MembershipApplicationPolicyHandler implements INSAPolicyHandler {

    /**
     * @description Run policies for contacts
     *
     * @param policies List<NSA_Policy__c>
     * @param contacts List<Contact>
     *
     */
    public void run(List<NSA_Policy__c> policies, List<Contact> contacts) {
        List<Contact> contactsToUpdate = new List<Contact>();
        if (policies != null && !policies.isEmpty()) {
            Map<Id, Set<Id>> policyIdsByAccountId = getPolicyIdsByAccountId(policies);
            Map<Id, Set<Id>> accIdsByConIdInExistedMembApps = getAccIdsByConIdInExistedMembApps(policyIdsByAccountId.keySet(), (new Map<Id, Contact>(contacts)).keySet());

            for (NSA_Policy__c policy : policies) {
                for (Contact con : contacts) {
                   if (accIdsByConIdInExistedMembApps.containsKey(con.Id)
                            && accIdsByConIdInExistedMembApps.get(con.Id).contains(policy.Society_Account__c)) {

                       contactsToUpdate.add(addVisibility(con, policy.Contact_Society_Field_API_Name__c));
                    }
                }
            }
        }
    }

    //********************* HELPERS ***********************//

    /**
     * @description Get Account Ids By Contact Id Map In Existed Membership Applications
     *
     * @param policyIds Set<Id>
     * @param contactIds  Set<Id>
     *
     * @return Map<Id, Set<Id>>
     */
    private Map<Id, Set<Id>> getAccIdsByConIdInExistedMembApps(Set<Id> policyIds, Set<Id> contactIds) {
        Map<Id, Set<Id>> accIdsByContactIdInExistedMembershipApplications = new Map<Id, Set<Id>>();
        List<Membership_Application__c> membershipApplications = [
                SELECT Id, Account__c, Contact__c
                FROM Membership_Application__c
                WHERE Account__c IN :policyIds
                AND Contact__c IN :contactIds
                AND Date_Submitted__c >= :Date.today().addMonths(-6)
        ];
        for (Membership_Application__c membershipApplication : membershipApplications) {
            if (!accIdsByContactIdInExistedMembershipApplications.containsKey(membershipApplication.Contact__c)) {
                accIdsByContactIdInExistedMembershipApplications.put(membershipApplication.Contact__c, new Set<Id>());
            }
            accIdsByContactIdInExistedMembershipApplications.get(membershipApplication.Contact__c).add(membershipApplication.Account__c);
        }
        return accIdsByContactIdInExistedMembershipApplications;
    }

    /**
     * @description Get Policy Ids By AccountId
     *
     * @param policies List<NSA_Policy__c>
     *
     * @return Map<Id, Set<Id>>ContactChangeEventTriggerHandler
     */
    private Map<Id, Set<Id>> getPolicyIdsByAccountId(List<NSA_Policy__c> policies) {
        Map<Id, Set<Id>> policyIdsByAccountId = new Map<Id, Set<Id>>();

        for (NSA_Policy__c policy : policies) {
            if (!policyIdsByAccountId.containsKey(policy.Society_Account__c)) {
                policyIdsByAccountId.put(policy.Society_Account__c, new Set<Id>());
            }
            policyIdsByAccountId.get(policy.Society_Account__c).add(policy.Id);
        }
        return policyIdsByAccountId;
    }

    /**
     * @param con Contact
     * @param fieldName String
     *
     * @return Contact
     */
    private Contact addVisibility(Contact con, String fieldName) {
        con.put(fieldName, true);
        return con;
    }
}