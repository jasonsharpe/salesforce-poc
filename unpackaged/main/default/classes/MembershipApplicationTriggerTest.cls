/*****************************************************************
Name: MembershipApplicationTriggerTest
Copyright © 2021 ITC
============================================================
Purpose: Unit test for MembershipApplicationTriggerHelper
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   11.02.2021   Created   Cover Assign Society By Membership Application logic
*****************************************************************/

@IsTest
private class MembershipApplicationTriggerTest {
    private static String COUNTRY_ISO_CODE_CHINA = 'CHN';
    private static String COUNTRY_ISO_CODE_GERMANY = 'DEU';
    private static String COUNTRY_ISO_CODE_UKRAINE = 'UKR';
    private static String COUNTRY_ISO_CODE_BELGIUM = 'BEL';
    private static String STATE_CODE_1 = 'CA-AB';
    private static String STATE_CODE_2 = 'CA-BC';
    private static String PROFILE_INTEGRATION_USER = 'Integration profile';
    private static String PERMISSION_SET_EDIT_ACCOUNT_NAME = 'Edit_Account_Name';
    private static Integer NUMBER_OF_CONTACTS = 10;

    @TestSetup
    private static void init() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);

        Map<String, Account> accountByNameMap = new Map<String, Account>{
                'CFA China' => CFA_TestDataFactory.createAccount('CFA China', 'Society', false)
                , 'CFA Germany' => CFA_TestDataFactory.createAccount('CFA Germany', 'Society', false)
                , 'CFA Ukraine' => CFA_TestDataFactory.createAccount('CFA Ukraine', 'Society', false)
                , 'CFA Belgium' => CFA_TestDataFactory.createAccount('CFA Belgium', 'Society', false)
        };
        insert accountByNameMap.values();

        Map<String, NSA_Policy__c> policyByNameMap = new Map<String, NSA_Policy__c>{
                'NSA China' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA China').Id, 'CFA_China__c', null, null, null, false)
                , 'NSA Germany' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Germany').Id, 'CFA_Society_Germany__c', null, null, null, false)
                , 'NSA Ukraine' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Ukraine').Id, 'CFA_Society_Ukraine__c', null, null, null, false)
                , 'NSA Belgium' => CFA_TestDataFactory.createNsaPolicy(accountByNameMap.get('CFA Belgium').Id, 'CFA_Society_Belgium__c', null, null, null, false)
        };
        insert policyByNameMap.values();


        List<NSA_Geographic_Rule__c> geographicRules = new List<NSA_Geographic_Rule__c>();

        //China
        Integer i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '000' + 1 : '00' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_1, '0000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA China').Id, COUNTRY_ISO_CODE_CHINA, STATE_CODE_2, '10000', '12000', false));

        //Toronto
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '1400' + 1 : '140' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, STATE_CODE_1, '13000', highZipCode, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Germany').Id, COUNTRY_ISO_CODE_GERMANY, null, '14100', '15000', false));

        //Ukraine
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '1510' + 1 : '151' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_1, '15100', '16000', false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Ukraine').Id, COUNTRY_ISO_CODE_UKRAINE, STATE_CODE_2, null, null, false));

        //Atlanta
        i = 0;
        while (i < 100) {
            String highZipCode = (i < 10) ? '7000' + 1 : '700' + i;
            geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, STATE_CODE_1, '70000', null, false));
            i++;
        }
        geographicRules.add(CFA_TestDataFactory.createNsaGeographicRule(policyByNameMap.get('NSA Belgium').Id, COUNTRY_ISO_CODE_BELGIUM, null, null, null, false));

        insert geographicRules;

        List<Contact> contacts = CFA_TestDataFactory.createContactRecords('CFA Contact', 'TestSet', NUMBER_OF_CONTACTS * 4, true);
    }

    @IsTest
    static void assignSocietyByMembershipApplicationOnInsertTest() {
        List<NSA_Policy__c> policies = [
                SELECT Id
                        , Contact_Society_Field_API_Name__c
                        , Advanced_Assignment__c
                        , Advanced_Assignment_Handler__c
                        , Society_Account__c
                FROM NSA_Policy__c
                WHERE Is_Active__c = TRUE
            	AND Policy_Status__c = 'Approved'
        ];
        List<Membership_Application__c> membershipApplications = new List<Membership_Application__c>();
        List<Contact> contacts = [SELECT Id, CFA_China__c, CFA_Society_Germany__c, CFA_Society_Ukraine__c, CFA_Society_Belgium__c FROM Contact];

        Set<String> usedRandomValue = new Set<String>();
        String randomVal;
        Id accountId;
        Integer i = 1;
        for (Contact con : contacts) {
            while (randomVal == null || usedRandomValue.contains(randomVal)) {
                randomVal = CFA_TestDataFactory.generateRandomString(40);
            }
            usedRandomValue.add(randomVal);
            accountId = null;
            if (i <= NUMBER_OF_CONTACTS) {
                accountId = policies[0].Society_Account__c;
            } else if (i > NUMBER_OF_CONTACTS && i <= NUMBER_OF_CONTACTS * 2) {
                accountId = policies[1].Society_Account__c;
            } else if (i > NUMBER_OF_CONTACTS * 2 && i <= NUMBER_OF_CONTACTS * 3) {
                accountId = policies[2].Society_Account__c;
            } else if (i > NUMBER_OF_CONTACTS * 3 && i <= NUMBER_OF_CONTACTS * 4) {
                accountId = policies[3].Society_Account__c;
            }
            Membership_Application__c membershipApplication = CFA_TestDataFactory.createMembershipApplication(accountId, con.Id, false);
            membershipApplication.Membership_Application_Number__c = randomVal;
            membershipApplications.add(membershipApplication);

            i++;
        }

        Test.startTest();
        insert membershipApplications;
        Test.stopTest();

        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaUkraineAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumAccount);
    }

    @IsTest
    static void assignSocietyByMembershipApplicationOnDeleteTest() {
        System.runAs(CFA_TestDataFactory.getUserWithPermission(PROFILE_INTEGRATION_USER, PERMISSION_SET_EDIT_ACCOUNT_NAME)) {
            List<NSA_Policy__c> policies = [
                    SELECT Id
                            , Advanced_Assignment__c
                            , Advanced_Assignment_Handler__c
                            , Society_Account__c
                            , Society_Account__r.Name
                            , Contact_Society_Field_API_Name__c
                            , (
                            SELECT Country_Name__c
                                    , State__c
                                    , Zip_Code_High__c
                                    , Zip_Code_Low__c
                                    , NSA_Policy__c
                            FROM NSA_Geograhpic_Rules__r
                    )
                    FROM NSA_Policy__c
                    WHERE Id IN (
                            SELECT NSA_Policy__c
                            FROM NSA_Geographic_Rule__c
                    )
                    AND Is_Active__c = TRUE
                	AND Policy_Status__c = 'Approved'
            ];

            Map<String, NSA_Policy__c> policiesByAccountName = new Map<String, NSA_Policy__c>();
            for (NSA_Policy__c policy : policies) {
                policiesByAccountName.put(policy.Society_Account__r.Name, policy);
            }
            List<Membership_Application__c> membershipApplications = new List<Membership_Application__c>();
            List<Contact> contacts = [SELECT Id, CFA_China__c, CFA_Society_Germany__c, CFA_Society_Ukraine__c, CFA_Society_Belgium__c FROM Contact];

            Set<String> usedRandomValue = new Set<String>();
            String randomVal;
            Id accountId;
            Integer i = 1;
            for (Contact con : contacts) {
                while (randomVal == null || usedRandomValue.contains(randomVal)) {
                    randomVal = CFA_TestDataFactory.generateRandomString(40);
                }
                usedRandomValue.add(randomVal);
                accountId = null;
                if (i <= NUMBER_OF_CONTACTS) {
                    accountId = policiesByAccountName.get('CFA China').Society_Account__c;
                } else if (i > NUMBER_OF_CONTACTS && i <= NUMBER_OF_CONTACTS * 2) {
                    accountId = policiesByAccountName.get('CFA Belgium').Society_Account__c;
                } else if (i > NUMBER_OF_CONTACTS * 2 && i <= NUMBER_OF_CONTACTS * 3) {
                    accountId = policiesByAccountName.get('CFA Germany').Society_Account__c;
                    con.Country_ISO_Code__c = COUNTRY_ISO_CODE_GERMANY;
                    con.State_Province_ISO_Code__c = STATE_CODE_1;
                    con.MailingPostalCode = '13001';
                } else if (i > NUMBER_OF_CONTACTS * 3 && i <= NUMBER_OF_CONTACTS * 4) {
                    accountId = policiesByAccountName.get('CFA Ukraine').Society_Account__c;
                    con.Country_ISO_Code__c = COUNTRY_ISO_CODE_UKRAINE;
                    con.State_Province_ISO_Code__c = STATE_CODE_1;
                    con.MailingPostalCode = '15101';
                }

                Membership_Application__c membershipApplication = CFA_TestDataFactory.createMembershipApplication(accountId, con.Id, false);
                membershipApplication.Membership_Application_Number__c = randomVal;
                membershipApplications.add(membershipApplication);

                i++;
            }

            ContactTriggerHandler.triggerDisabled = true;
            update contacts;

            Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
            Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
            Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
            Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

            System.assertEquals(0, numberOfCfaChinaAccount);
            System.assertEquals(0, numberOfCfaBelgiumAccount);
            System.assertEquals(0, numberOfCfaGermanyAccount);
            System.assertEquals(0, numberOfCfaUkraineAccount);

            insert membershipApplications;
            
            Integer numberOfCfaChinaAccountAfterInsert = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
            Integer numberOfCfaGermanyAccountAfterInsert = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
            Integer numberOfCfaUkraineAccountAfterInsert = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
            Integer numberOfCfaBelgiumAccountAfterInsert = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

            System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaChinaAccountAfterInsert);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaBelgiumAccountAfterInsert);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccountAfterInsert);
            System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaUkraineAccountAfterInsert);

            Test.startTest();
            delete membershipApplications;
            Test.stopTest();
        }

        Integer numberOfCfaChinaAccount = [SELECT COUNT() FROM Contact WHERE CFA_China__c = TRUE];
        Integer numberOfCfaGermanyAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Germany__c = TRUE];
        Integer numberOfCfaUkraineAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Ukraine__c = TRUE];
        Integer numberOfCfaBelgiumAccount = [SELECT COUNT() FROM Contact WHERE CFA_Society_Belgium__c = TRUE];

        System.assertEquals(0, numberOfCfaChinaAccount);
        System.assertEquals(0, numberOfCfaBelgiumAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaGermanyAccount);
        System.assertEquals(NUMBER_OF_CONTACTS, numberOfCfaUkraineAccount);
    }

}