/*****************************************************************
Name: MembershipChangeEventTriggerHelper
Copyright © 2020 ITC
============================================================
Purpose: This trigger helper is responsible for handling Membership Change Event
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan               Create    Apply Member Society Campaigns Changes
2.0      Alona Zubenko   10.12.2020   Update    Update Badge Visibility Dates
3.0      Alona Zubenko   15.02.2021   Update    Change Society mtd to Policies
*****************************************************************/

public with sharing class MembershipChangeEventTriggerHelper {
    public static final String LAPSE_TRANSACTION_TYPE = 'Lapse';
    public static final String RENEW_TRANSACTION_TYPE = 'Renew';
    public static final String JOIN_TRANSACTION_TYPE = 'Join';
    public static final String CANCEL_TRANSACTION_TYPE = 'Cancel';


    /**
     * Add/Remove Contact to Member Campaigns based on society checkboxes.
     * When Membership record is created/updated
     *
     * @param changedMembershipList List<Membership__c>	changedMembershipList
     *
     */
    public static void applyMemberSocietyCampaignsChanges(List<Membership__c> changedMembershipList) {
        try {
            List<CampaignMember> campaignMembersToCreate = new List<CampaignMember>();
            List<CampaignMember> campaignMembersToRemove = new List<CampaignMember>();
            Set<Id> societiesIds = new Set<Id>();
            Set<Id> contactIds = new Set<Id>();
            Map<Id, Set<Id>> accIdsByConId = new Map<Id, Set<Id>>();

            for (Membership__c membership : changedMembershipList) {
                societiesIds.add(membership.Account_lookup__c);
                contactIds.add(membership.Contact_lookup__c);

                if (!accIdsByConId.containsKey(membership.Contact_lookup__c)) {
                    accIdsByConId.put(membership.Contact_lookup__c, new Set<Id>());
                }
                accIdsByConId.get(membership.Contact_lookup__c).add(membership.Account_lookup__c);
            }
            Map<Id, Set<Id>> allCampaignIdsByAccIds = SocietyMembershipService.getCampaignIdsByAccIds();
            List<CampaignMember> campaignMembers = SocietyMembershipService.getCampaignMembers(societiesIds,contactIds,allCampaignIdsByAccIds);
            Map<Id, Set<Id>> existedCampaignIdsByContactId = SocietyMembershipService.groupCampaignIdByContactId(campaignMembers);
            Map<Id,Map<Id,CampaignMember>> campaignMembersByContactId = SocietyMembershipService.groupCampaignMembersByContactId(campaignMembers);

            for (Membership__c membership : changedMembershipList) {
                Set<Id> existedCampaignIds = existedCampaignIdsByContactId.get(membership.Contact_lookup__c);
                Set<Id> allCampIdsForThisAccountId = allCampaignIdsByAccIds.get(membership.Account_lookup__c);
                if (allCampIdsForThisAccountId != null 
                    && (membership.Transaction_Type__c == JOIN_TRANSACTION_TYPE || membership.Transaction_Type__c == RENEW_TRANSACTION_TYPE)
                   ) {
                    for (Id campId : allCampIdsForThisAccountId) {
                        if (existedCampaignIds == null || !existedCampaignIds.contains(campId)) {
                            campaignMembersToCreate.add(new CampaignMember(ContactId = membership.Contact_lookup__c, CampaignId = campId));
                        }
                    }
                } else if (allCampIdsForThisAccountId != null 
                           && (membership.Transaction_Type__c == LAPSE_TRANSACTION_TYPE || membership.Transaction_Type__c == CANCEL_TRANSACTION_TYPE)
                		   ) {
                    for (Id campId : allCampIdsForThisAccountId) {
                        if (existedCampaignIds != null && existedCampaignIds.contains(campId)) {
                            campaignMembersToRemove.add(campaignMembersByContactId.get(membership.Contact_lookup__c).get(campId));
                        }
                    }
                }
            }
            if (!campaignMembersToCreate.isEmpty()) {
                insert campaignMembersToCreate;
            }
            if (!campaignMembersToRemove.isEmpty()) {
                delete campaignMembersToRemove;
            }
        } catch (Exception ex) {
            insert CFA_IntegrationLogException.logError(ex, 'Apply Member Society Campaigns Changes', 'MembershipChangeEventTriggerHelper', DateTime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }
}