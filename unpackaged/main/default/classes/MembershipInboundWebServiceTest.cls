@isTest
public class MembershipInboundWebServiceTest {
    public static String reqBody = '{"Action":"MembershipUpdate","TransactionId":"655b1b43-821f-4611-8026-f3653b130ae9","Code":null,"Message":null,"Memberships":[{"MembershipType":"Society","TransactionType":"Cancel","JoinDate":"1981-09-15 04:00:00.000","MemberYear":"2018","TransactionDate":"2014-07-22 11:52:26.000","ReasonOther":"This is uesd fir tedtimgg","Reason":"Products/services offered by new society","CustomerPartnerId":"100906195","IsOnProfessionalLeave":false,"IsAffiliate":true,"SocietyPartnerId":"1151075","Success":null,"ErrorMessage":null}]}';
    public static String reqBody2 = '';
    @isTest
    public static void membershipTransactionTest(){
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = 'services/apexrest/membershiptransaction/v1.0/*';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(reqBody);
        RestContext.request = request;
        RestContext.response = response;
        MembershipInboundWebService.membershipTransaction();
        request.requestBody = Blob.valueOf(reqBody2);
        RestContext.request = request;
        RestContext.response = response;
        MembershipInboundWebService.membershipTransaction();
        try{
            request.requestBody = null;
        request.requestURI = 'services/apexrest/membershiptransaction/v1.0/*';
        request.httpMethod = 'GET';
        RestContext.request = request;
        RestContext.response = response;
        MembershipInboundWebService.membershipTransaction();
        }catch(Exception e){
            System.debug('Exception::'+e);
            System.debug('Line::'+e.getLineNumber());
        }
    }
}