//tests in membershipTriggerTest
public with sharing class MembershipTriggerHandler implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean triggerDisabled = false;

    @TestVisible 
    private static Boolean disableNSALogic = false;

    //Checks to see if the trigger has been disabled either by custom metadata or by running code
    public Boolean isDisabled() {
        if (Test.isRunningTest()) {
            return triggerDisabled;
        } else {
            return DynamicTriggerStatus.getTriggerStatus('MembershipTrigger') || triggerDisabled;
        }
    }
    
    private Boolean isDisabledNSALogic {
        get {
            if(Test.isRunningTest()) {
                return disableNSALogic;
            } else {
                return DynamicTriggerStatus.getTriggerStatus('NSALogic');
            }
        }
    }

    //  On before insert event
    public void beforeInsert(List<SObject> newItems) {}

    //  on before update event
    public void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    //  on after insert event
    public void afterInsert(Map<Id, SObject> newItems) {
        if(!isDisabledNSALogic) {
    	    MembershipTriggerHelper.assignContactsToSocietiesByPolicies(newItems);
        }
    }

    // On after update event
    public void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        if(!isDisabledNSALogic) {
            MembershipTriggerHelper.assignContactsToSocietiesByPolicies(newItems);
        }
    }

    //  On after delete event
    public void afterDelete(Map<Id, SObject> oldItems) {
        if(!isDisabledNSALogic) {
            MembershipTriggerHelper.removeContactSocietyVisibility(oldItems);
        }
    }

    // On after undelete event
    public void afterUndelete(Map<Id, SObject> oldItems) {}

    //  On before delete event
    public void beforeDelete(Map<Id, SObject> oldItems) {}

}