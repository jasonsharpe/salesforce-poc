public with sharing class MembershipTriggerHelper {
    
    /**
     * Change CFA society checkboxes based on type of membership
     * 
     * @param	newMembershipsMap new map from triggers
     * */
    public static void assignContactsToSocietiesByPolicies(Map<Id, Sobject> newMembershipsMap) {
        if(newMembershipsMap != null && !newMembershipsMap.isEmpty()) {
            Set<Id> contactIds = new Set<Id>();
            Set<Id> accountIds = new Set<Id>();
            for(Membership__c membership: (List<Membership__c>) newMembershipsMap.values()) {
                contactIds.add(membership.Contact_lookup__c);
                accountIds.add(membership.Account_lookup__c);
            }
            List<NSA_Policy__c> policies = NSAPolicyService.getPoliciesByAccIds(accountIds);
            Set<String> fieldNames = SObjectService.getFieldValues(policies, 'Contact_Society_Field_API_Name__c');
            fieldNames.add('Id');
            fieldNames.add('RecordTypeId');
            String query = 'SELECT ' + String.join(new List<String>(fieldNames), ',');
            query += ' FROM Contact WHERE ID IN :contactIds';
            List<Contact> contacts = Database.query(query);
            for(Contact contact: contacts) {
                for(String fieldApiName : fieldNames) {
                    if(contact.get(fieldApiName) instanceof Boolean) {
                        contact.put(fieldApiName, false);
                    }
                }
            }

            (new NSAPolicyRunner(policies))
            .run(
                contacts,
                new List<INSAPolicyHandler> {
                    new MembershipPolicyHandler(),
                    new MembershipApplicationPolicyHandler(),
                    new AddressPolicyHandler(false)
                },
                true
            );
        }
    }
    
    /**
     * Chnage CFA society checkboxes to false based on deleted memberships
     * 
     * @param	membershipsMap membership map from trigger
     * */
    public static void removeContactSocietyVisibility(Map<Id, Sobject> membershipsMap) {
        if(membershipsMap != null && !membershipsMap.isEmpty()) {
            Set<Id> contactIds = new Set<Id>();
            Set<Id> accountIds = new Set<Id>();
            for(Membership__c membership: (List<Membership__c>) membershipsMap.values()) {
                contactIds.add(membership.Contact_lookup__c);
                accountIds.add(membership.Account_lookup__c);
            }
            Set<String> fieldNames = NSAPolicyService.getContactSocietyFieldAPINamesFromPolicies();
            fieldNames.add('Id');
            fieldNames.add('RecordTypeId');
            String query = 'SELECT ' + String.join(new List<String>(fieldNames), ',');
            query += ' FROM Contact WHERE ID IN :contactIds';
            List<Contact> contacts = Database.query(query);
            Map<Id,Contact> mapIdToContact = new Map<Id,Contact>(contacts);

            List<NSA_Policy__c> policies = NSAPolicyService.getPoliciesByAccIds(accountIds);
            Map<Id, List<NSA_Policy__c>> mapAccIdToPolicies = NSAPolicyService.groupPoliciesByAccountId(policies);
            List<Contact> contactsToUpdate = new List<Contact>();
            for(Membership__c membership: (List<Membership__c>) membershipsMap.values()) {
                if(mapAccIdToPolicies.containsKey(membership.Account_lookup__c)) {
                    Contact contact = mapIdToContact.get(membership.Contact_lookup__c);
                    List<NSA_Policy__c> policiesList = mapAccIdToPolicies.get(membership.Account_lookup__c);

                    if(NSAPolicyService.changeContactCFACheckboxes(contact, policiesList, false)) {
                        contactsToUpdate.add(contact);
                    }
                }
            }

            (new NSAPolicyRunner(policies))
            .run(
                contacts,
                new List<INSAPolicyHandler> {
                    new MembershipApplicationPolicyHandler(),
                    new AddressPolicyHandler(false)
                },
                true
            );
        }
    }
}