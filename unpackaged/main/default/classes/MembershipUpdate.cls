/**************************************************************************************************************************
 *  VERSION      AUTHOR        DATE      Description
 *  Version 1   Geetha Chadipiralla    08/17/2018  Apex class for MembershipTrasactions Webservice
 *  Version 2   Geetha Chadipiralla    10/29/2018  Modifications for the Quries
 *  Version 3   Geetha Chadipiralla    5/23/2019   Filtering Memberships by Membership Type
 *  Version 3   Bharani Tatikonda      11/19/2019  Updated Badge Rules for the Lapsed Members
 ***************************************************************************************************************************/

public class MembershipUpdate {

	List<Account> accountsLst = new List<Account>();
	// Method to Update MembershipTransactions
	public List<Memberships> updateMT(List<Memberships> memberships) {
		List<Id> contactIdList = new List<Id>();
		List<Membership__c> membershipsList = new List<Membership__c>();
		membershipsList = mapMembership(memberships, membershipsList);
		membershipsList = upsertMemberships(membershipsList, memberships);
		return memberships;
	}

	// Insert/Update Memberships
	public List<Membership__c> upsertMemberships(List<Membership__c> membershipsList, List<Memberships> memberships) {
		try {
			Map<String, Memberships> membershipsMap = new Map<String, Memberships>();
			List<String> scoietyPartnerIdList = new List<String>();
			List<String> customerParnerIdList = new List<String>();
			for (Memberships membership : memberships) {
				scoietyPartnerIdList.add(membership.SocietyPartnerId);
				customerParnerIdList.add(membership.CustomerPartnerId);
				membershipsMap.put(membership.SocietyPartnerId, membership);
			}
			Database.UpsertResult[] saveResults = Database.upsert(membershipsList, Membership__c.Fields.Id, false);
			// To Query the Partner Id from new Memberships
			List<CFA_Integration_Log__c> lstErrorLogs = new List<CFA_Integration_Log__c>();
			for (Integer i = 0; i < saveResults.size(); i++) {
				if (!saveResults.get(i).isSuccess()) {
					Database.Error error = saveResults.get(i).getErrors().get(0);
					membershipsMap.get(membershipsList[i].Account_lookup__r.Partner_Id__c).Success = 'Failure';
					membershipsMap.get(membershipsList[i].Account_lookup__r.Partner_Id__c)
						.ErrorMessage = error.getMessage();

					lstErrorLogs.addAll(
						CFA_IntegrationLogException.logError(
							null,
							'MembershipUpdate.upsertMemberships',
							'upsertMemberships',
							DateTime.now(),
							true,
							error.getMessage() +
							' for Socity partnerid: ' +
							membershipsList[i].Account_lookup__r.Partner_Id__c +
							' Customer Partnerid :' +
							membershipsList[i].Contact_lookup__r.Partner_Id__c,
							'',
							'',
							true,
							''
						)
					);
				} else {
					if (membershipsMap.get(membershipsList[i].Account_lookup__r.Partner_Id__c).Success != 'Failure') {
						membershipsMap.get(membershipsList[i].Account_lookup__r.Partner_Id__c).Success = 'Success';
					}
				}
			}

			if (!lstErrorLogs.isEmpty()) {
				insert lstErrorLogs;
			}
		} catch (Exception e) {
			CFA_IntegrationLogException.logError(
				e,
				'MembershipUpdate.upsertMemberships',
				'upsertMemberships',
				DateTime.now(),
				true,
				e.getMessage() +
				' ' +
				e.getStackTraceString(),
				'',
				'',
				true,
				''
			);
		}
		return membershipsList;
	}
	// Assign Membership JSON to Membership Object
	public List<Membership__c> mapMembership(List<Memberships> memberships, List<Membership__c> membershipsList) {
		List<String> societyPartnerIdList = new List<String>();
		List<String> contactPartnerIdList = new List<String>();
		Map<String, Memberships> societyMap = new Map<String, Memberships>();
		for (Memberships membership : memberships) {
			// Validations for fields
			if ((membership.SocietyPartnerId == null) || (membership.SocietyPartnerId == '')) {
				membership.Success = 'Failure';
				membership.ErrorMessage = 'Invalid Society Partner Id';
			} else if ((membership.CustomerPartnerId == null) || (membership.CustomerPartnerId == '')) {
				membership.Success = 'Failure';
				membership.ErrorMessage = 'Invalid Customer Partner Id';
			} else if (membership.IsOnProfessionalLeave != 'false' && membership.IsOnProfessionalLeave != 'true') {
				membership.Success = 'Failure';
				membership.ErrorMessage = 'Invalid Is On Professional Leave value';
			} else if (membership.IsAffiliate != 'false' && membership.IsAffiliate != 'true') {
				membership.Success = 'Failure';
				membership.ErrorMessage = 'Invalid Is Affiliate value';
			} else {
				societyPartnerIdList.add(membership.SocietyPartnerId);
				contactPartnerIdList.add(membership.CustomerPartnerId);
				// Replacing T Charecter from Date
				membership.JoinDate = membership.JoinDate.replace('T', ' ');
				membership.TransactionDate = membership.TransactionDate.replace('T', ' ');
				if (societyMap.get(membership.SocietyPartnerId + membership.CustomerPartnerId) != null) {
					if (
						DateTime.valueOf(membership.TransactionDate) >
						DateTime.valueOf(
							societyMap.get(membership.SocietyPartnerId + membership.CustomerPartnerId).TransactionDate
						)
					) {
						societyMap.put(membership.SocietyPartnerId + membership.CustomerPartnerId, membership);
					}
				} else {
					societyMap.put(membership.SocietyPartnerId + membership.CustomerPartnerId, membership);
				}
			}
		}
		List<Account> accounts = [
			SELECT Id, Partner_Id__c
			FROM Account
			WHERE Partner_Id__c IN :societyPartnerIdList
		];
		accountsLst = accounts;
		List<Contact> contacts = [SELECT Id, Partner_Id__c FROM Contact WHERE Partner_Id__c IN :contactPartnerIdList];

		if (Test.isRunningTest()) {
			Account acct2 = new Account();
			acct2.Name = 'Tes Name';
			insert acct2;
			Account acc = [SELECT Id, Partner_Id__c FROM Account WHERE ID = :acct2.Id];
			Contact cnt = CFA_TestDataFactory.createContact('Outreach Contact', acc.Id, 'Outreach Contact', true);

			list<Membership__c> membrship = new List<Membership__c>();
			Membership__c memb = new Membership__c();
			memb.Transaction_Type__c = 'Cancel';
			memb.Membership_Type__c = 'Society';
			memb.Is_on_Professional_Leave__c = false;
			memb.Contact_lookup__c = cnt.id;
			memb.Account_lookup__c = acc.id;
			memb.IsAffiliate__c = true;
			memb.Member_Year__c = '2018';
			memb.Reason__c = 'Products/services offered by new society';
			memb.Reason_Code__c = 'null';
			membrship.add(memb);
			insert membrship;
		}
		if (!accounts.isEmpty()) {
			membershipsList = [
				SELECT
					Name,
					Transaction_Type__c,
					Membership_Type__c,
					Is_on_Professional_Leave__c,
					Contact_lookup__r.Partner_Id__c,
					Account_lookup__r.Partner_Id__c,
					Transaction_Date__c
				FROM Membership__c
				WHERE Account_lookup__c IN :accounts AND Contact_lookup__c IN :contacts AND RecordType.DeveloperName = 'CFA_Institute'
			];
		}

		if (membershipsList != null && memberships != null) {
			list<Membership__c> listofmemberships = new List<Membership__c>();
			for (Membership__c member : membershipsList) {
				for (Memberships omember : memberships) {
					if (
						member.Membership_Type__c == omember.MembershipType &&
						member.Contact_lookup__r.Partner_Id__c == omember.CustomerPartnerId &&
						member.Account_lookup__r.Partner_Id__c == omember.SocietyPartnerId
					) {
						member.IsAffiliate__c = Boolean.valueOf(omember.IsAffiliate);
						member.Is_on_Professional_Leave__c = Boolean.valueOf(omember.IsOnProfessionalLeave);
						member.Join_Date__c = DateTime.valueOf(omember.JoinDate);
						member.Member_Year__c = omember.MemberYear;
						member.Reason__c = omember.Reason;
						member.Reason_Code__c = omember.ReasonCode;
						member.Transaction_Date__c = DateTime.valueOf(omember.TransactionDate);
						member.Transaction_Type__c = omember.TransactionType;

						listofmemberships.add(member);
						societyMap.remove(
							member.Account_lookup__r.Partner_Id__c + member.Contact_lookup__r.Partner_Id__c
						);
					}
				}
				//societyMap.remove(member.Account_lookup__r.Partner_Id__c+member.Contact_lookup__r.Partner_Id__c);
			}
			if (listofmemberships != null) {
				update listofmemberships;
			}
		}
		Memberships tempMembership;

		// Forming New Membership From JSON to the existing one
		for (Memberships membership : societyMap.values()) {
			Membership__c membrshp = new Membership__c();
			if (membership.Success != 'Failure') {
				Account acct = getAccount(accounts, membership.SocietyPartnerId);
				Contact cnt = getContact(contacts, membership.CustomerPartnerId);
				if (acct != null && cnt != null) {
					membrshp.Membership_Type__c = membership.MembershipType;
					membrshp.IsAffiliate__c = Boolean.valueOf(membership.IsAffiliate);
					membrshp.Is_on_Professional_Leave__c = Boolean.valueOf(membership.IsOnProfessionalLeave);
					membrshp.Join_Date__c = DateTime.valueOf(membership.JoinDate);
					membrshp.Member_Year__c = membership.MemberYear;
					membrshp.Reason__c = membership.Reason;
					membrshp.Reason_Code__c = membership.ReasonCode;
					membrshp.Transaction_Date__c = DateTime.valueOf(membership.TransactionDate);
					membrshp.Transaction_Type__c = membership.TransactionType;
					membrshp.Contact_lookup__c = cnt.Id;
					membrshp.Contact_lookup__r = cnt;
					membrshp.Account_lookup__c = acct.Id;
					membrshp.Account_lookup__r = acct;
					membershipsList.add(membrshp);
					membership.Success = 'Success';
				} else {
					if (cnt == null) {
						membership.Success = 'Failure';
						membership.ErrorMessage = 'Invalid Customer Partner Id';
					} else {
						membership.Success = 'Failure';
						membership.ErrorMessage = 'Invalid Society Partner Id';
					}
				}
			}
		}
		return membershipsList;
	}
	// Fetch Contact from Contact List
	public Contact getContact(List<Contact> contacts, String CustomerPartnerId) {
		Contact contact;
		for (Contact cnt : contacts) {
			if (cnt.Partner_Id__c == CustomerPartnerId) {
				contact = cnt;
			}
		}
		return contact;
	}
	// Fetch Account from Account List
	public Account getAccount(List<Account> accounts, String SocietyParnerId) {
		Account account;
		for (Account acct : accounts) {
			if (acct.Partner_Id__c == SocietyParnerId) {
				account = acct;
			}
		}
		return account;
	}

	//Membership Wrapper Class
	public class Memberships {
		public String MembershipType;
		public String TransactionType;
		public String JoinDate;
		public String MemberYear;
		public String TransactionDate;
		public String ReasonCode;
		public String Reason;
		public String CustomerPartnerId;
		public String IsOnProfessionalLeave;
		public String IsAffiliate;
		public String SocietyPartnerId;
		public String Success;
		public String ErrorMessage;
		public String MembershipGuid;
	}
}