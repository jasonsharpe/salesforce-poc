/**************************************************************************************************
* Apex Class Name   : Memberships_Test
* Purpose           : This is the test class for Memberships class
* Version           : 1.0 Initial Version
* Organization      : Lightning Team -- Vijay Vemuru
* Created Date      : 19-July-2018  
* Modified Date   : September 28th 2018

***************************************************************************************************/
@isTest
public class Memberships_Test{

    public static testMethod void Membership_Test1(){
        Memberships MBclass = new Memberships();
        MBclass.CurrentStatus = 'Testst1';
        MBclass.JoinDate = Date.today();
        MBclass.ExpirationDate = Date.today();
        MBclass.MembershipType = 'Testmb2';
        MBclass.Organization = 'GBX';
        MBclass.IsCFAMembership = True;
        MBclass.IsAffiliate = False;
        MBclass.OnProfessionalLeave= True;
     
    }
}