public with sharing class MoveCasesFromRrAccountsBatch implements Database.Batchable<SObject>, Database.Stateful {
    private static String queryForCases = 'SELECT Id, AccountId, Contact.AccountId FROM Case WHERE Contact.AccountId != NULL AND (Account.Name LIKE \'* RR%\' OR Account.Name LIKE \'*RR%\')';
    /**
     * @description Start
     *
     * @param context Database.BatchableContext
     *
     * @return Database.QueryLocator
     */
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(queryForCases);
    }

    /**
     * @description Execute
     *
     * @param context Database.BatchableContext
     * @param emailSends List<Contact>
     */
    public void execute(Database.BatchableContext context, List<Case> cases) {
        for (Case caseObject : cases) {
            caseObject.AccountId = caseObject.Contact.AccountId;
        }
        update cases;
    }

    /**
     * @description Finish
     *
     * @param param1 Database.BatchableContext
     */
    public void finish(Database.BatchableContext param1) {
    }

}