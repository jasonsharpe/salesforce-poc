/*****************************************************************
Name: NSAAssignmentBatchScheduler
Copyright © 2021 ITC
============================================================
Purpose: Scheduler for NSA Assignment Batch
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   15.02.2021   Created   Schedule job for NSA Assignment Batch
*****************************************************************/

public with sharing class NSAAssignmentBatchScheduler implements Schedulable{
    private static final List<INSAPolicyHandler> DECEASED_HANDLER = new List<INSAPolicyHandler>{
            new DeceasedPolicyHandler()
    };
    private static final List<INSAPolicyHandler> HANDLERS = new List<INSAPolicyHandler>{
            new AddressPolicyHandler()
            , new MembershipPolicyHandler()
            , new MembershipApplicationPolicyHandler()
    };
    private static final String DECEASED_DATE_CONDITIONS = 'WHERE Deceased_Date__c < ' + Datetime.now().addYears(-2).format('yyyy-MM-dd') + ' AND (RecordType.DeveloperName = \'CFA_Contact\' OR RecordType.DeveloperName = \'Local_Member\') AND (' + getSocietyCondition() + ')';
    private static final String TRANSACTION_TYPE_CONDITIONS_MEMBERSHIPS = 'WHERE (Deceased_Date__c = NULL OR Deceased_Date__c >= ' + Datetime.now().addYears(-2).format('yyyy-MM-dd')  + ') AND Id IN (SELECT Contact_lookup__c FROM Membership__c WHERE Transaction_Date__c < ' + Datetime.now().addYears(-2).format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + ' AND (Transaction_Type__c = \'Lapse\' OR Transaction_Type__c = \'Cancel\')) AND (RecordType.DeveloperName = \'CFA_Contact\' OR RecordType.DeveloperName = \'Local_Member\') AND (' + getSocietyCondition() + ')';
    private static final String TRANSACTION_TYPE_CONDITIONS_MEMBERSHIP_APPS = 'WHERE (Deceased_Date__c = NULL OR Deceased_Date__c >= ' + Datetime.now().addYears(-2).format('yyyy-MM-dd')  + ') AND Id IN (SELECT Contact__c FROM Membership_Application__c WHERE Date_Submitted__c < ' + Datetime.now().addMonths(-6).format('yyyy-MM-dd') + ') AND (RecordType.DeveloperName = \'CFA_Contact\' OR RecordType.DeveloperName = \'Local_Member\') AND (' + getSocietyCondition() + ')';

    /**
     * @description Run Schedule job for NSA Assignment Batch daily at midnight
     */
    public static void run(){
        List<CronTrigger> jobs = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'NSA Assignment Batch Schedule Job'];
        if(jobs.isEmpty()){
            NSAAssignmentBatchScheduler m = new NSAAssignmentBatchScheduler();
            String sch = '0 0 0 * * ?';
            String jobID = System.schedule('NSA Assignment Batch Schedule Job', sch, m);
        }
    }

    /**
     * @description Stop Schedule job for NSA Assignment Batch
     */
    public static void stop(){
        List<CronTrigger> jobs = [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name = 'NSA Assignment Batch Schedule Job'];
        if(!jobs.isEmpty()){
            System.abortJob(jobs[0].Id);
        }
    }

    /**
     * @description Execute schedule job
     *
     * @param sc SchedulableContext
     */
    public void execute(SchedulableContext sc) {
        NSAAssignmentBatch nsaAssignmentBatch = new NSAAssignmentBatch(DECEASED_HANDLER, DECEASED_DATE_CONDITIONS, true);
        Database.executeBatch(nsaAssignmentBatch,200);

        NSAAssignmentBatch nsaAssignmentBatch2 = new NSAAssignmentBatch(HANDLERS, TRANSACTION_TYPE_CONDITIONS_MEMBERSHIPS, true);
        Database.executeBatch(nsaAssignmentBatch2,200);

        NSAAssignmentBatch nsaAssignmentBatch3 = new NSAAssignmentBatch(HANDLERS, TRANSACTION_TYPE_CONDITIONS_MEMBERSHIP_APPS, true);
        Database.executeBatch(nsaAssignmentBatch3,200);
    }

    private static String getSocietyCondition() {
        String result = String.join(new List<String>(NSAPolicyService.getContactSocietyFieldAPINamesFromPolicies()), ' = true OR ').removeEnd('OR');
        result += ' = true ';
        
        return result;
    }
}