public without sharing class NSAPolicyRunner {
    private List<NSA_Policy__c> policies;

    /**
     *  @description Constructor for empty List of NSA_Policy__c
     */
    public NSAPolicyRunner() {
    }

    /**
     * @description Constructor for single NSA_Policy__c
     *
     * @param policy NSA_Policy__c
     */
    public NSAPolicyRunner(NSA_Policy__c policy) {
        if (policy != null) {
            this.policies = new List<NSA_Policy__c>{
                    policy
            };
        }
    }

    /**
     * @description Constructor for not empty List of NSA_Policy__c
     *
     * @param policies List<NSA_Policy__c>
     */
    public NSAPolicyRunner(List<NSA_Policy__c> policies) {
        this.policies = policies;
    }

    /**
     * @description  Run
     *
     * @param contacts List<Contact>
     * @param handlers List<INSAPolicyHandler>
     * @param doUpdate Boolean
     */
    public void run(List<Contact> contacts, List<INSAPolicyHandler> handlers, Boolean doUpdate) {
        Map<String, List<NSA_Policy__c>> policiesByAdvancedAssignments = new Map<String, List<NSA_Policy__c>>();
        List<NSA_Policy__c> policiesWithoutAdvancedAssignments = new List<NSA_Policy__c>();
        if (policies != null) {
            for (NSA_Policy__c policy : policies) {
                if (policy.Advanced_Assignment__c) {
                    if (!policiesByAdvancedAssignments.containsKey(policy.Advanced_Assignment_Handler__c)) {
                        policiesByAdvancedAssignments.put(policy.Advanced_Assignment_Handler__c, new List<NSA_Policy__c>());
                    }
                    policiesByAdvancedAssignments.get(policy.Advanced_Assignment_Handler__c).add(policy);
                } else {
                    policiesWithoutAdvancedAssignments.add(policy);
                }
            }
        }

        for (String advancedPolicyHandler : policiesByAdvancedAssignments.keySet()) {
            INSAPolicyHandler policyHandler = (INSAPolicyHandler) Type.forName(advancedPolicyHandler).newInstance();
        }

        for (INSAPolicyHandler handler : handlers) {
            handler.run(policies, contacts);
        }
        
        if (doUpdate) {
            List<CFA_Integration_Log__c> exceptions = new List<CFA_Integration_Log__c>();
            ContactTriggerHandler.cfaContactValidationDisabled  = true;
            List<Database.SaveResult> updateResults = Database.update(contacts, false);

            Integer i = 0;
            for (Database.SaveResult result : updateResults) {
                Contact origRecord = contacts[i];
                if (!result.isSuccess()) {
                    for (Database.Error error : result.getErrors()) {
                        exceptions.addAll(CFA_IntegrationLogException.logError(new NSAPolicyRunnerException(origRecord.Id + ' : ' + error.getMessage()), 'NSAPolicyRunner', 'Update Contacts', Datetime.now(), true, '', '', '', true, ''));
                    }
                }
                i++;
            }
            if(!exceptions.isEmpty()){
                insert exceptions;
            }
        }
    }

    private class NSAPolicyRunnerException extends Exception {}
}