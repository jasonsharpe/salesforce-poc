/**************************************************************************************************
* Apex Class Name   : OutreachContactCreation
* Purpose           : Custom code to create outreach contacts and affiliations
* Version           : 1.0 Initial Version
* Organization      : CFA Institute
* Created Date      : 05-June-2019
***************************************************************************************************/

public class OutreachContactCreation{
    id conId = null;
    public contact con;
    public Affiliation__c aff{get;set;}
    //static final cfa_RoundRobinAccountService roundRobinService = new cfa_RoundRobinAccountService();
    
    public OutreachContactCreation(ApexPages.StandardController controller) {
        conId=controller.getId();
        con = new contact();
        con = (contact)controller.getRecord();
        aff = new Affiliation__c();
               
    }

    public pagereference redirectpage(){
        pagereference page = null;
        String selRecId= ApexPages.currentPage().getParameters().get('RecordType');
        id recid = [select id from recordtype where developername=:'Outreach_Contact'].id;
        if(con.recordtypeID != null ){
            selRecId = con.recordtypeID;
        } else{ 
            Schema.DescribeSObjectResult dsr = contact.SObjectType.getDescribe();
            Schema.RecordTypeInfo defaultRecordType;
            for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    defaultRecordType = rti;
                }
            }
            
            selRecId  =  defaultRecordType.getRecordTypeId();
        }
        if(selRecId != recId){
            if(conid == null){
                page = new PageReference('/003/e?nooverride=1');
            } else{
                page = new PageReference('/'+conID+'/e?nooverride=1');
            }
            
        }
         return page;
        
    }
    

    public pagereference save(){
        try{
            /*if(con.id == null){
                Account  acc = new cfa_RoundRobinAccountService().getAccount(1);
                con.accountID =acc.id;
            }*/
            upsert con;
        }catch(Exception excp){
            System.debug('************'+excp.getmessage());
            ApexPages.addMessages(excp) ;
            return null;
        }
        try{
            aff = [select id from Affiliation__c where Contact__c =: con.id limit 1];
        }catch(exception excp){
            System.debug('******aff******'+excp.getmessage());
        }
        if(aff.id == null){
            aff.Role__c = 'General Outreach';
            //aff.Account__c = con.accountId;
            aff.Start_Date__c = system.today();
            aff.Active_Affiliation__c = true;
            aff.recordtypeid = [SELECT ID from recordtype where developername=:'Relationship_Management_Non_Access' limit 1].id;
            aff.Contact__c = con.id;
        }
        
        aff.Email__c =con.email;
        
        try{
            upsert aff;
        }catch(Exception excp){
            System.debug('******aff2******'+excp.getmessage());
            ApexPages.addMessages(excp) ;
            return null;
        }
        return new pagereference('/'+con.id);
    }


}