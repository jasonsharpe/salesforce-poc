/*****************************************************************
Name: PopulateIntegrationFieldsBatch
Copyright © 2021 ITC
============================================================
Purpose: Populate Integration Fields Batch
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan  12.04.2021   Created   Batch to copy data to new integration fields
*****************************************************************/
public without sharing class PopulateIntegrationFieldsBatch implements Database.Batchable<SObject> {
    
    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(
        	'SELECT Id,RecordTypeId,CFAMN__PersonID__c , Person_Id__c,DonorApi__Suffix__c , Suffix,CFAMN__Gender__c , '
                 +'Gender__c,CFAMN__Employer__c , Employer_Name__c,CFAMN__CFAClassCodeList__c , '
                 +'CFA_Class_Code__c, CFAMN__CFACandidateCode__c , CFA_Candidate_Code__c,'
                 +'CFAMN__CFAPREP__c , CFA_Program_Prep_Allow_Contact__c, CFAMN__CFAMemberCode__c , '
                 +'CFA_Member_Code__c, CFAMN__CharterAwardDate__c , CFA_Charter_Award_Date__c,'
                 +'CFAMN__NMAIL__c , HasOptedOutOfMail__c,CFAMN__NEML__c , HasOptedOutOfEmail,'
                 +'CFAMN__CLASOC__c , Investment_Foundations_Candidate_SOC__c,CFAMN__ClaritasCertificateAwardDate__c ,' 
                 +'Investment_Foundations_Certificate_Award__c, CFAMN__ClaritasCandidateCode__c , '
                 +'Investment_Foundations_Candidate_Code__c,CFAMN__CIPMMemberCode__c , CIPM_Member_Code__c,'
                 +'CFAMN__CIPMCandidateCode__c , CIPM_Candidate_Code__c, CFAMN__CIPMCertificateDate__c, '
                 +'CIPM_Certificate_Award_Date__c, CFA__c , Is_CFA_Charter_Holder__c,CIPM__c , '
                 +'Is_CIPM_Certificate_Holder__c'
                 +' FROM Contact'
            	 +' WHERE RecordType.DeveloperName = \'CFA_Contact\' OR RecordType.DeveloperName = \'Local_Member\''
        );
    }
    
    public void execute(Database.BatchableContext context, List<Contact> contacts) {
        for(Contact contact: contacts) {
            contact.Person_Id__c = contact.CFAMN__PersonID__c;
            contact.Suffix = contact.DonorApi__Suffix__c;
            contact.Gender__c = contact.CFAMN__Gender__c;
            contact.Employer_Name__c = contact.CFAMN__Employer__c;
            contact.CFA_Class_Code__c = contact.CFAMN__CFAClassCodeList__c;
            contact.CFA_Candidate_Code__c = contact.CFAMN__CFACandidateCode__c;                
            contact.CFA_Member_Code__c = contact.CFAMN__CFAMemberCode__c;
            if(contact.CFAMN__CharterAwardDate__c != null) {
            	contact.CFA_Charter_Award_Date__c = DateTime.newInstanceGMT(contact.CFAMN__CharterAwardDate__c, Time.newInstance(0, 0, 0, 0));    
            }
            if(contact.CFAMN__ClaritasCertificateAwardDate__c != null) {
                contact.Investment_Foundations_Certificate_Award__c = DateTime.newInstanceGMT(contact.CFAMN__ClaritasCertificateAwardDate__c, Time.newInstance(0, 0, 0, 0));
            }    
            contact.Investment_Foundations_Candidate_Code__c = contact.CFAMN__ClaritasCandidateCode__c;
            contact.CIPM_Member_Code__c = contact.CFAMN__CIPMMemberCode__c;
            contact.CIPM_Candidate_Code__c = contact.CFAMN__CIPMCandidateCode__c;
            if(contact.CFAMN__CIPMCertificateDate__c  != null) {
                contact.CIPM_Certificate_Award_Date__c = DateTime.newInstanceGMT(contact.CFAMN__CIPMCertificateDate__c, Time.newInstance(0, 0, 0, 0));
            }
            contact.CFA_Program_Prep_Allow_Contact__c = contact.CFAMN__CFAPREP__c;
            contact.HasOptedOutOfMail__c = contact.CFAMN__NMAIL__c;
            contact.HasOptedOutOfEmail = contact.CFAMN__NEML__c;
            contact.Investment_Foundations_Candidate_SOC__c = contact.CFAMN__CLASOC__c;
            contact.Is_CFA_Charter_Holder__c = contact.CFA__c;
            contact.Is_CIPM_Certificate_Holder__c = contact.CIPM__c;            
        }
        Database.update(contacts, false);
    }
    
    public void finish(Database.BatchableContext BC) {}

}