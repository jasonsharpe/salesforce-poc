public without sharing class RandomCodeGenerator {

  @AuraEnabled
  public static RandomCodeWrapper generateCode(String userId){
      try {
      Integer len = 5;
      String str = string.valueof(Math.abs(Crypto.getRandomLong()));
      String randomNumber = str.substring(0, len);

      List<String> codeList = new List<String>();
      codeList.add(randomNumber);
      
      RandomCodeWrapper ob = new RandomCodeWrapper();
      
      ob.code = randomNumber;
      ob.CurrentDateTime = string.valueof(System.now());
      User u = [Select id,FirstName,Name,email,ContactID, Profile.UserLicense.Name,FederationIdentifier from User where id=:userId];
      
      if(u.Profile.UserLicense.Name == 'Salesforce' && u.FederationIdentifier != null && u.FederationIdentifier.contains('@cfainstitute.org')) {
          String partnerId = u.FederationIdentifier.SubStringBefore('@');
          contact con = [Select id,Email,Partner_Id__c from Contact where Partner_Id__c =:partnerId];
          ob.userEmail = con.email;
      } else if(u.Profile.UserLicense.Name == 'Salesforce' && u.FederationIdentifier != null && u.FederationIdentifier.contains('@aimr.org')) {      
          ob.userEmail = u.email;
      } else if(u.Profile.UserLicense.Name == 'Partner Community'){
          contact con = [Select id,Email from Contact where id=:u.ContactID];
          ob.userEmail = con.email;
      } else {
          ob.userEmail = u.email;
      }
      
      
      ob.ContactEmail = DynamicEnvVariableService.genericdatafetcher('TwoFactorAuthSupport');
      
      OrgWideEmailAddress[] obOrgAddress = [select id,DisplayName,Address from OrgWideEmailAddress where Address='noreply.societyoperations@cfainstitute.org'];
      
      EmailTemplate emailTempExec = [select Id, Name, Subject, Markup, HtmlValue, Body from EmailTemplate where Name = '2FA_LoginCodeEmail'];
      String subject, body;
      subject = emailTempExec.Subject;

      emailTempExec.HtmlValue = emailTempExec.HtmlValue.replace('userName', u.FirstName);
      emailTempExec.HtmlValue = emailTempExec.HtmlValue.replace('authCode', ob.code);
      emailTempExec.HtmlValue = emailTempExec.HtmlValue.replace('contactEmail', ob.ContactEmail);
      emailTempExec.HtmlValue = emailTempExec.HtmlValue.replace('currentYear', String.valueOf(Date.Today().Year()));

      body = emailTempExec.HtmlValue;
      List<Messaging.SingleEmailMessage> reminderMails = new List<Messaging.SingleEmailMessage>();  
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
      mail.setSubject(subject);        
      mail.setHtmlBody(body);
      mail.setOrgWideEmailAddressId(obOrgAddress.get(0).Id);
      mail.setSaveAsActivity(false);      
      String[] toEmails = new String[]{};
      toEmails.add(ob.userEmail);
      if(toEmails.size()>0)
          mail.setToAddresses(toEmails);
      reminderMails.add(mail);
      if(reminderMails.size()>0 && !Test.isRunningTest())
          Messaging.sendEmail(reminderMails,false);
      return ob;
      } catch(Exception e) {
          System.debug(e.getStackTraceString());
          System.debug('Exception e'+ e.getMessage());
          return null;
      }
  }

  public class RandomCodeWrapper{
    @AuraEnabled public String Code;
    @AuraEnabled public String CurrentDateTime;
    @AuraEnabled public String userEmail;
    @AuraEnabled public String ContactEmail;
   
  }
  
  @AuraEnabled
  public static boolean checkTimeDifference(String OriginalTime){
    System.debug(OriginalTime);
    Datetime OriginalTimeDiff = Datetime.valueOf(OriginalTime);
    Long milliseconds =   ((System.now().getTime() - OriginalTimeDiff.getTime()));
    Long seconds = milliseconds / 1000;
    Long minutes = seconds / 60;
    Long hours = minutes / 60;

      if(minutes > 10 && seconds >0) {
        return false;
      } else {
        return true;
      }
    
  }
  
}