@isTest
public class RandomCodeGeneratorTest {
	public static final String SocietyPartnerUserPorfileName = 'Society Partner User Admin';
	public static final String AdminUserPorfileName = 'System Administrator';

	@TestSetup
	static void testSetup() {
		Account newAccount = CFA_TestDataFactory.createAccount(CaseTriggerHandler_Test.DefaultAccountName, null, true);

		Contact newContact = CFA_TestDataFactory.createCFAContact(CaseTriggerHandler_Test.DefaultContactName, newAccount.Id, true);

		CFA_TestDataFactory.createPartnerUser(newContact, RandomCodeGeneratorTest.SocietyPartnerUserPorfileName);
	}

	static testMethod void MainWrapperTest() {
		RandomCodeGenerator.generateCode(userInfo.getUserId());
		Boolean isTimeDiff = RandomCodeGenerator.checkTimeDifference(String.valueOf(DateTime.now()));
		System.assertEquals(isTimeDiff, true);
	}

	static testMethod void UserCreationCommunity() {
		User societyUser = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = :RandomCodeGeneratorTest.SocietyPartnerUserPorfileName LIMIT 1];
		System.runAs(societyUser) {
			RandomCodeGenerator.generateCode(userInfo.getUserId());
			Boolean isTimeDiff = RandomCodeGenerator.checkTimeDifference(String.valueOf(DateTime.now()));
			System.assertEquals(isTimeDiff, true);
		}
	}

	static testMethod void UserCreationCommunity1() {
		User adminUser = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = :RandomCodeGeneratorTest.AdminUserPorfileName LIMIT 1];

		System.runAs(adminUser) {
			RandomCodeGenerator.generateCode(userInfo.getUserId());
			Boolean isTimeDiff = RandomCodeGenerator.checkTimeDifference(String.valueOf(DateTime.now()));
			System.assertEquals(isTimeDiff, true);
		}
	}
}