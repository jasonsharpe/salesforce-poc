/**************************************************************************************************
 * Apex Class Name   : ResearchChallengeCalloutControllerTest
 * Purpose           : This is the test class which is used for ResearchChallengeCalloutController class.
 * Version           : 1.0 Initial Version
 * Organization      : Vijay Vemuru -- Lightning Team
 * Created Date      : 08-August-2018
 * Created Date      : 13-Nov-2018
 * Updated Date      : 17-July-2019 <removed debug statements>
 ***************************************************************************************************/
@isTest
private class ResearchChallengeCalloutControllerTest {
	public static Account defAcct;

	private static String UNIVERSITY_ACCOUNT_RT = 'University';
	private static String LOCAL_RESEARCH_CHALLENGE_RT_ID = Schema.SObjectType.Research_Challenge__c.getRecordTypeInfosByName()
		.get('Local Research Challenge')
		.getRecordTypeId();
	private static String CONTACTNAME = 'Test Contact for Callout';

	@testSetup
	static void setup() {
		Test.setMock(HttpCalloutMock.class, new ResearchChallengeMockClass());
		Account newAccount = CFA_TestDataFactory.createAccount('Test Account', UNIVERSITY_ACCOUNT_RT, true);
		Contact newContact = CFA_TestDataFactory.createCFAContact(CONTACTNAME, newAccount.Id, true);

		List<MDM_Default_Account__c> lstDflt = new List<MDM_Default_Account__c>();
		lstDflt.add(new MDM_Default_Account__c(Name = 'Unknown Employer', MDM_Account_ID__c = newAccount.Id));
		RetryForTest.upsertOnUnableToLockRow(lstDflt);

		CustomerCare_ApiCodeMessages__c testcodes = new CustomerCare_ApiCodeMessages__c(
			name = 'testsetting',
			CustomerCare_Error_Code__c = 500,
			CustomerCare_ErrorMessage__c = 'Test Error'
		);
		RetryForTest.upsertOnUnableToLockRow(testcodes);

		Research_Challenge__c researchChallenge = new Research_Challenge__c();
		researchChallenge.Name = 'Local Challenge';
		researchChallenge.Competition_Date__c = Date.today();
		researchChallenge.Location__c = 'Test Location';
		researchChallenge.RecordTypeId = LOCAL_RESEARCH_CHALLENGE_RT_ID;
		RetryForTest.upsertOnUnableToLockRow(researchChallenge);

		RC_Participant__c testParticipant = new RC_Participant__c(
			Email__c = 'testemail@gmail.com',
			Challenge_Status__c = 'Invited',
			Faculty_University_Name__c = newAccount.Id,
			Faculty_Advisor__c = newContact.Id,
			Local_Challenge__c = researchChallenge.Id,
			Participant_First_Name__c = 'TestFirstName',
			Participant_Last_Name__c = 'TestLastName',
			Challenge_Name__c = 'Test Challenge'
		);
		RetryForTest.upsertOnUnableToLockRow(testParticipant);
	}

	private static testMethod void testResearchChallengeCallouts() {
		Test.setMock(HttpCalloutMock.class, new ResearchChallengeMockClass());
		RC_Participant__c testParticipant = [SELECT Id FROM RC_Participant__c LIMIT 1];

		Test.startTest();
		
		ResearchChallengeCalloutController.ParticipatUpdateCallout(new List<Id>{ testParticipant.Id });
		
		Test.stopTest();
	}
}