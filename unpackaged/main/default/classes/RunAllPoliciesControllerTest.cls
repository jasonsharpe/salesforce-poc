@isTest
private class RunAllPoliciesControllerTest {

    @isTest
    private static void whenCallRunAllPolciesThenRunBatch() {
        Account accCfaInstitute = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        Test.startTest();
        RunAllPoliciesController.runAllPolicies();
        Test.stopTest();
        System.assertEquals(0, [SELECT count() FROM CFA_Integration_Log__c]);
    }
}