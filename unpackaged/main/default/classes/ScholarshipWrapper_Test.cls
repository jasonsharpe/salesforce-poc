/**************************************************************************************************
* Apex Class Name   : ScholarshipWrapper_Test
* Purpose           : This is the test class for ScholarshipWrapper wrapper class
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 30-April-2018  
***************************************************************************************************/
@isTest
public class ScholarshipWrapper_Test{

    public static testMethod void ScholarshipWrapper_Test1(){
        ScholarshipWrapper ScWrapper = new ScholarshipWrapper();
        ScWrapper.Program = 'TestPr1';
        ScWrapper.Type = 'TestES1';
        ScWrapper.ProgramLevel = 'TestLv1';
        ScWrapper.ExamCycle = 'TestCy1';
        ScWrapper.Year = 1;
        ScWrapper.ScholarshipApplicationDate = Date.today();
        ScWrapper.CommunicationDate = Date.today();
        ScWrapper.ScholarshipApplicationDeadline = Date.today();
        ScWrapper.Status = 'TestRS1';
        ScWrapper.Reason = 'Test EAT1';
        ScWrapper.Society = 'Test ERS1';
    
    }
}