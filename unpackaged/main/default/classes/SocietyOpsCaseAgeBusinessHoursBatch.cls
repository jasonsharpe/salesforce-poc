/**
* @author 7Summits Inc
* @date 07-03-2019
* @description The batch job updates SOC_Age_Working_Status_Hours__c & SOC_Age_New_Status_Hours__c fields
* to calculate the business hours of cases in new and working statuses.
* This is only applicable for Society Tech Case record type
*/

global class  SocietyOpsCaseAgeBusinessHoursBatch implements Database.Batchable<sObject>,Schedulable  {

    /**
  *  Selects records for batch processing based on all the open statuses
  */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        cfa_ApexUtilities.log('Starting Batch Job');

        Id societyTechCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Society Tech Case').getRecordTypeId();

        Set<String> caseStatusSet= new Set<String> {'New','In-Progress', 'Waiting on Internal', 'Waiting on Vendor', 'Waiting on Customer'};
        String caseQuery = 'SELECT Id, SOC_Age_New_Status_Hours__c, SOC_Age_Working_Status_Hours__c, SOC_LastModified_Working_Status_Time__c, ' +
                'SOC_Total_Time_hours__c, Status, CreatedDate ' +
                'FROM Case ' +
                'WHERE Status IN: caseStatusSet'+
                ' AND RecordTypeId= :societyTechCaseRecordTypeId';

        cfa_ApexUtilities.log('Printing the Case Query: ' + caseQuery);

        return Database.getQueryLocator(caseQuery);
    }
    /**
     *
     */
    global void execute(Database.BatchableContext bc, List<sObject> cases) {
        cfa_ApexUtilities.log('Executing batch job');

        // This is a test
        Datetime currentTime = Datetime.NOW();
        //reads the business hours defined in Society-Ops-BusinessHours
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE name='Society-Ops-BusinessHours'];
        cfa_ApexUtilities.log('business hours id: ' + bh.id);
        // calculate business hours and run update on SOC_Age_New_Status_Hours

        List<Case> casesToUpdate = new List<Case>();
        for(Case c : (List<Case>)cases){
            if(c.Status == 'New') {
                // process new logic: update SOC Age New Status Hours to be calculated business hours
                long businessSec=BusinessHours.diff(bh.id, c.createdDate, currentTime)/1000;
                Decimal busHours=(decimal)businessSec/3600;

                cfa_ApexUtilities.log('business hours for new case: ' + busHours + ' for Case Id: ' + c.Id);
                c.SOC_Age_New_Status_Hours__c = busHours;
            } else if(c.SOC_LastModified_Working_Status_Time__c!=null) {
                // status is a working status. Process working status logic

                long businessSec=BusinessHours.diff(bh.id, c.SOC_LastModified_Working_Status_Time__c, currentTime)/1000;
                Decimal busHours=(decimal)businessSec/3600;
                cfa_ApexUtilities.log('business hours for working status case: ' + busHours + ' for Case Id: ' + c.Id);

                c.SOC_Age_Working_Status_Hours__c = busHours;
                long businessNewStatusSec=BusinessHours.diff(bh.id, c.createdDate,c.SOC_LastModified_Working_Status_Time__c)/1000;
                Decimal busNewHours=(decimal)businessNewStatusSec/3600;
                cfa_ApexUtilities.log('business New hours for working status case: ' + busNewHours + ' for Case Id: ' + c.Id);

                c.SOC_Age_New_Status_Hours__c=busNewHours;


            }
            casesToUpdate.add(c);
        }

        List<Database.SaveResult> results = database.update(casesToUpdate, false);
        for (Database.SaveResult result : results) {
            if (!result.isSuccess()){
                for (Database.Error err : result.getErrors()){
                    cfa_ApexUtilities.log('Error: '+ err.getStatusCode() + ' ' + err.getMessage());
                }
            }
        }
    }

    global void execute(SchedulableContext ctx) {

        SocietyOpsCaseAgeBusinessHoursBatch societyOpsCaseAgeBusinessHoursBatch=
                new SocietyOpsCaseAgeBusinessHoursBatch();
        database.executebatch(societyOpsCaseAgeBusinessHoursBatch);


    }
    /**
     *
     */
    global void finish(Database.BatchableContext bc) {
        cfa_ApexUtilities.log('Finishing batch job.');
    }


}