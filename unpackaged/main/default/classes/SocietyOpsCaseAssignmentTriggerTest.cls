/**
* @author 7Summits Inc
* @date 06-21-2019
* @group Test Layer
* @description This test class checks the trigger functionality of SocietyOpsCaseAssignmentTrigger
*/

@isTest
private  class SocietyOpsCaseAssignmentTriggerTest {
    private static final String TEST_USER_NAME = 'testSocOpsCaseAss';
    private static final String SYSTEM_ADMIN = 'System Administrator';

    @IsTest
    static void testCaseOwnershipChange() {

        Id societyTechCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Society Tech Case').getRecordTypeId();

        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.Name='trigger-details';
        societyOps.CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment';
       // societyOps.No_reply_email__c='noreply@cfainstitute.org';
        societyOps.No_reply_email__c=Label.Society_Ops_NoReply;

        insert societyOps;

        User initialCaseOwner = CFA_TestDataFactory.createUsers(TEST_USER_NAME+'Init', SYSTEM_ADMIN, 1, true)[0];
        User newCaseOwner = CFA_TestDataFactory.createUsers(TEST_USER_NAME+'Owner', SYSTEM_ADMIN, 1, true)[0];

        Case caseObject = new Case();

        System.runAs(initialCaseOwner) {
            caseObject.Subject = 'Test Subject';
            caseObject.society_Issue_Sub_Type__c = 'Access Request / Issue';
            caseObject.Priority = 'Medium';
            caseObject.Origin = 'Email';
            caseObject.Status = 'New';
            caseObject.society_Impacted_Functional_Area__c = 'Domain Changes';
            caseObject.society_Case_Close_Comments__c='Test comments';
            caseObject.SuppliedEmail = 'test@test.com';
            caseObject.RecordTypeId = societyTechCaseRecordTypeId;

            insert caseObject;
        }

        System.runAs(newCaseOwner){

            caseObject.OwnerId=newCaseOwner.Id;
            Test.startTest();
            update caseObject;
            Integer emailInvocations=Limits.getEmailInvocations();
            Test.stopTest();
            System.assertEquals(1,emailInvocations,'An email invocation occurred');


        }


    }
}