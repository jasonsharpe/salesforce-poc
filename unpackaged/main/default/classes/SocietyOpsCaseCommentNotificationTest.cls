/**
* @author 7Summits Inc
* @date 06-21-2019
* @group Test Layer
* @description This test class checks the trigger functionality of SocietyOpsCaseCommentNotificationTrigger.
*/
@IsTest
private class SocietyOpsCaseCommentNotificationTest {

    @IsTest
    static void testcommentCreationByNonOwner() {
        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.name='trigger-details';
        societyOps.CaseComment_TemplateName__c='SocietyOps-New Comment Notification';
        societyOps.No_reply_email__c=Label.Society_Ops_NoReply;
        insert societyOps;

        Id societyTechCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Society Tech Case').getRecordTypeId();

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

        User caseCommentuser = new User(Alias = 'testUser', Email='ccUser@testorg.com',
                EmailEncodingKey='UTF-8', LastName='TestUser', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='caseCommentUser@cfainstitute.org');
        insert caseCommentuser;


        User recordOwneruser = new User(Alias = 'testUser', Email='recordOwner@cfainstitute.org',
                EmailEncodingKey='UTF-8', LastName='TestUser', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id,
                TimeZoneSidKey='America/Los_Angeles', UserName='caseOwner@cfainstitute.org');
        insert recordOwneruser;

        Case caseObject = new Case();

        System.runAs(recordOwneruser) {
            caseObject.subject = 'Test Subject';
            caseObject.society_Issue_Sub_Type__c = 'Access Request / Issue';
            caseObject.priority = 'Medium';
            caseObject.Origin = 'Email';
            caseObject.Status = 'New';
            caseObject.SuppliedEmail = 'test@test.com';
            caseObject.society_Impacted_Functional_Area__c = 'Domain Changes';
            caseObject.RecordTypeId = societyTechCaseRecordTypeId;
            caseObject.society_Case_Close_Comments__c='Test comments';

            insert caseObject;
        }

        System.runAs(caseCommentuser){
            CaseComment caseCommentObject= new CaseComment();
            caseCommentObject.CommentBody='Test Comment';
            caseCommentObject.ParentId=caseObject.id;
            Test.startTest();
            insert caseCommentObject;
            Integer emailInvocations=Limits.getEmailInvocations();
            Test.stopTest();
            System.assertEquals(1,emailInvocations,'An email invocation occurred');
        }
    }
}