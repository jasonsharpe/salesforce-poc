public with sharing class TaskTriggerHelper {
    
    private static Boolean isFirstTime = true;
    private static OrgWideEmailAddress cfaInstituteVolunteersEmail = [ SELECT Id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'CFA Institute Volunteers' LIMIT 1 ];
    private static OrgWideEmailAddress cfaInstituteCustomerServiceEmail = [ SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'CFA Institute Customer Service (Do Not Reply)' LIMIT 1 ]; 
    
    public static void handleCompletedTasks(Map<Id,SObject> newMap, Map<Id,SObject> oldMap) {
        List<Task> tasksToProcess = new List<Task>();
        for(Task currentTask: (List<Task>)newMap.values()) {
            Task oldTask = (Task) oldMap.get(currentTask.Id);
            if(currentTask.Status == 'Completed' && oldTask.Status != 'Completed'){
                tasksToProcess.add(currentTask);
            }
        }
        handleCompletedTasks(tasksToProcess);
    }
    
    public static void handleVMTaskAssignmenttoVolunteer(list<task> tasks) {
        if(isFirstTime) {
            VMTaskAssignmenttoVolunteer(tasks);
        }
    }
    
    private static void handleCompletedTasks(List<Task> tasks) {
        List<Task> tasksToProcess = getTasksToProcess(tasks);
        if(tasksToProcess.isEmpty()) {
            return;
        }
        Map<Id, List<Task>> tasksByCaseId = getTasksByCaseId(tasks);
        Set<Id> allTasksCompleted = new Set<Id>();
        for(Task t : tasksToProcess) {
            // If the map does not contain any tasks for the case, then
            // all of them are complete
            if(!tasksByCaseId.containsKey(t.WhatId)) {
                allTasksCompleted.add(t.WhatId);
            }
        }
        if(!allTasksCompleted.isEmpty()) {
            sendAllTasksCompletedEmails(allTasksCompleted);
        }
    }
    
    public static void checkAssginedTo(List<Task> newItems, Map<Id,Task> oldMap){
        Set<Id> ownerIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        for(GroupMember g : [ SELECT UserorGroupId FROM GroupMember WHERE Group.Name = 'Exam Admin - General' OR Group.Name = 'Medical Reviewer' ]){
            if(g.UserOrGroupId.getsObjectType() == Schema.User.sObjectType) {
                userIds.add(g.UserOrGroupId);
            }
        }
        for(Task task : newItems){
            if((oldMap == null || task.OwnerId != oldMap.get(task.Id).ownerId) && 
               task.RecordTypeId == Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Medical_Reviewer_Task').getRecordTypeId()) {
                   if(!userIds.contains(task.OwnerId)){
                       task.addError('AssignedTo should be either Exam-Admin or Medical Reviewer');
                   }
            }
        }
    }

    /**
     * The purpose of this method is to send emails for those cases where all tasks have
     * been completed.
     *
     * @param caseIds set of case Ids associated with the processed tasks
    */
    private static void sendAllTasksCompletedEmails(Set<Id> caseIds) {
        // We only want to consider those cases that have the CBT DA Request record type
        List<Case> cases = [SELECT Id, OwnerId, Owner.Email, CaseNumber, Owner.Name FROM Case WHERE Id IN: caseIds AND RecordType.DeveloperName = 'CBT_DA_Request'];

        // If no cases were found, there's reason to proceed
        if(cases.isEmpty()) {
            return;
        }
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        for(Case c : cases) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTargetObjectId(c.OwnerId);
            email.setSaveAsActivity(false);
            email.setSubject('Case Ready for Review');
            email.setPlainTextBody('Hello ' + c.Owner.Name + '\n\nAll Medical/Legal review tasks have been completed for case ' + c.CaseNumber);
            email.setOrgWideEmailAddressId(cfaInstituteCustomerServiceEmail.Id);

            emails.add(email);
        }
        Messaging.sendEmail(emails);
    }

    /**
     * The purpose of this helper method is to gather all those tasks asscoiated with a case,
     * and where their status is completed.
     *
     * @param tasks The list of tasks to consider
     *
     * @return the list of tasks that are completed and associated with a case
    */
    private static List<Task> getTasksToProcess(List<Task> tasks) {
        List<Task> tasksToProcess = new List<Task>();
        for(Task t : tasks) {
            if(t.Status == 'Completed' && t.WhatId.getSobjectType().getDescribe().getLabel() == 'Case') {
                tasksToProcess.add(t);
            }
        }
        return tasksToProcess;
    }

    /**
     * The purpose of this helper method is to query for any other tasks associated with
     * the cases in the list of tasks. These are then placed into a map of lists, with the
     * key being the case id for those tasks.
     *
     * @param tasks The list of tasks for cases
     *
     * @return The map of tasks grouped by their respective case Ids
    */
    private static Map<Id, List<Task>> getTasksByCaseId(List<Task> tasks) {
        Set<Id> caseIds =  new Set<Id>();
        for(Task t : tasks) {
            caseIds.add(t.WhatId);
        }
        List<Task> otherTasks = [ SELECT Id, Status, WhatId FROM Task WHERE WhatId IN: caseIds AND Id NOT IN: tasks AND Status != 'Completed' ];
        Map<Id, List<Task>> tasksByCaseId = new Map<Id, List<Task>>();

        for(Task otherTask : otherTasks) {
            if(!tasksByCaseId.containsKey(otherTask.WhatId)) {
                tasksByCaseId.put(otherTask.WhatId, new List<Task>());
            }
            tasksByCaseId.get(otherTask.WhatId).add(otherTask);
        }
        return tasksByCaseId;
    }
    
    private static void VMTaskAssignmenttoVolunteer(List<Task> tasks) {
        Set<Id> testId = new Set<Id>();
        Map<Id, String> m1 = new Map<Id, String>();
        Map<Id, String> m2 = new Map<Id, String>();
        
        Id tasksordExternalRT = Task.sObjectType.getDescribe().getRecordTypeInfosByDeveloperName().get('TaskRecordExternal').getRecordTypeId();
        List<Messaging.SingleEmailMessage> allmails = new List<Messaging.SingleEmailMessage>();
        //The rule has been created to notify a volunteer with an email when task is assigned.      
        try{
            isFirstTime = false;
            //Fetching the ID of complete list of new task ids
            for(Task t2 : tasks){
                testId.add(t2.id);
            }
            //Fetching the engagement and contact name for complete list of new task ids
            for(Task t1 : [ SELECT Id, Engagement__r.Name, Who.name, Whoid FROM Task WHERE Id = :testId ]){
                m1.put(t1.id, t1.engagement__r.name);
                m2.put(t1.id, t1.who.name);
            }
            for(Task t : tasks){
                if((t.RecordTypeId == tasksordExternalRT) && (t.Status != 'Completed')
                    && (t.VM_Volunteer_Preferred_Email__c != null && t.VM_Volunteer_Preferred_Email__c != '')){

                    String TaskDetails = Label.VMTaskDetails;
                    String ForgotPassword = Label.VMforgotpassword;
                    String ContactUs = Label.VMContactUs;
                    String logo = label.VMlogo;
                    //Structuring the body of email
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setToAddresses (new String[] { t.VM_Volunteer_Preferred_Email__c});
                    mail.setSubject('You have new task');
                    mail.setOrgWideEmailAddressId(cfaInstituteVolunteersEmail.id);
                    
                    String body = '<img src="'+logo+'"/>';
                    body += '<br><br>Hi'+' &nbsp;'+ m2.get(t.Id) + ', ';
                    body += '<br><br><b>You have a new task.</b>';
                    body += '<br>Your task for&nbsp;'+m1.get(t.Id) +' is due.';
                    body += '<br><a href="'+TaskDetails+'">Click here to see your task details.</a>';
                    body += '<br><br>Sincerely,';
                    body += '<br>CFA Institute | Volunteer Management';
                    body += '<br><a href="'+ForgotPassword+'">Forgot your password?</a>'; 
                    body += '<br><a href="'+ContactUs+'">Contact us.</a>'; 
                    mail.setHtmlBody(body);
                    mail.setSaveAsActivity(false);
                    mail.setTreatTargetObjectAsRecipient(false);
                    allmails.add(mail);
                }
            }     
            Messaging.sendEmail(allmails,false);  
        } catch(exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
        }
    }
}