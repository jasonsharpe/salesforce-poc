@isTest 
public class Test_VM_JobApplicationController_Create{
    public static Account VM_Account;
    public static Contact VM_Contact;
    public static Engagement__c VM_Engage;
    public static Role__c VM_Role;
    public static Job_Application__c VM_Job;
    public static Job_Application__c VM_Job1;
    public static Document vm_document;
    Public Static Attachment Attach1;
    Public Static Attachment Attach2;
        Public Static Attachment Att1;
    Public Static Attachment Att2;
    public static integer count=0;
    
    public static String recordtypew;
    public static String roleIdw;
    public static String contactIdw;    
    public String cntctId;
    Public String AttachmentType;
    
    public String url_contactName;
    public String url_futureRole;
    public String url_recordType;
    @IsTest
    public static void TestprepareData(){
        Count =count+1;
        VM_Account = new Account();
        //VM_Account.Name ='Test_VMAcc1';
        //VM_Account.Phone = '222568974';
        //VM_Account.Type = 'Default';
        //insert VM_Account;
        VM_Account =     VM_TestDataFactory.createAccountRecord();
        
        VM_Contact = new Contact();
        //VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        VM_Contact.MDM_Preferred_Email_for_Volunteering__c= 'TestVM_test01@test.com';
        insert VM_Contact;
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest'+count+'@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest'+count+'@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne'+count+'@gmail.com','Lange','Outreach Management User','Relationship Manager');
        
        Engagement__c VM_Engage=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        
        insert VM_Engage;
        
        VM_Role=new Role__c();
        VM_Role.Engagement_name__c=VM_Engage.id;
        VM_Role.Position_Title__c='Test_VMRole';
        VM_Role.Role_Type__c='CIPM Committee Member';
        VM_Role.Conflict_filter__c='Writers';
        VM_Role.Number_of_Positions__c=123;
        VM_Role.Start_date__c=Date.today();
        VM_Role.Recruiting_Start_Date__c=Date.today();
        VM_Role.Recruiting_End_Date__c=Date.today().addDays(10);
        VM_Role.PC_Check_required__c=true;
        VM_Role.Confidential__c=true;
        //VM_Role.ByPassApproval__c=true;
        VM_Role.Volunteer_Impact__c='Test';
        VM_Role.Volunteer_Work_location__c='Test';
        VM_Role.Volunteer_Roles_and_Responsibilities__c='Test';
        VM_Role.Volunteer_Experience__c='Test';
        VM_Role.Volunteer_Compet__c='Chief Financial Officer (CFO)';
        VM_Role.Volunteer_Certifications__c='CFA Charterholder';
        VM_Role.Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited';
        VM_Role.VMApprovalStatus__c=true;
        insert VM_Role;
  //Create job application for positive      
        VM_Job = new Job_Application__c();
        VM_Job.RecordTypeID = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CIPMCommittee').getRecordTypeId();
        VM_Job.Position__c=VM_Role.id;
        VM_Job.Contact__c=VM_Contact.id;
        VM_Job.Status__c='Applied';
        VM_Job.Alternate_E_mail_Address__c='TestVM_test@test.com';
        VM_Job.Professional_Affiliations__c='Test';
        VM_Job.Volunteered_before__c='Yes';
        VM_Job.If_YES_list_experiences__c='Yes';
        VM_Job.Disciplinary_Action_Imposed__c='Yes';
        VM_Job.Disciplinary_action_Explain__c='Test';
        VM_Job.What_interests_you_about_this_role__c='Test';
        VM_Job.How_can_you_contribute_to_this_role__c='Test';
        VM_Job.Why_are_you_best_for_this_role__c='Test';
        VM_Job.Have_you_presented_on_these_topics__c='Test';
        VM_Job.Do_you_have_employer_support__c='Yes';
        
        VM_Job.Select_all_competencies__c='Central Banking 10 plus years';
        VM_Job.Degree1__c='Test';
        VM_Job.Degree2__c='Test';
        VM_Job.Degree3__c='Test';
        VM_Job.Reference_Name1__c='Test';
        VM_Job.Reference_Name2__c='Test';
       VM_Job.Reference_Name3__c='Test';
        VM_Job.Email1__c='TestVM56@test.com';
        VM_Job.Email2__c='TestVM57@test.com';
        VM_Job.Email3__c='TestVM58@test.com';
        VM_Job.Phone1__c='1234567897';
        VM_Job.Phone2__c='1234567897';
        VM_Job.Phone3__c='1234567897';
        VM_Job.Address1__c='Test';
        VM_Job.Address2__c='Test';
        VM_Job.Address3__c='Test';
        VM_Job.Personal_gain_or_benefit__c='Yes';
        VM_Job.Inappropriate_influence__c='Yes';
        VM_Job.Relationship_to_Prep_Provider__c='Yes';
        VM_Job.Relationship_with_Testing_Organization__c='Yes';
        VM_Job.Relationship_with_Candidate__c='Yes';
        VM_Job.I_have_read_the_disclosed_conflicts__c=true;
        VM_Job.I_Agree_to_the_Conflict_Policy__c=true;
        VM_Job.Other_potential_conflicts__c='Test';
        insert VM_Job;
       
        
        
        vm_document = new Document();
        vm_document.AuthorId = UserInfo.getUserId();
        vm_document.name = 'Volunteer Acknowledgement Form';
        vm_document.Body = blob.valueOf('Test Attachment');
        vm_document.FolderId = UserInfo.getUserId();
        insert vm_document;
        
        attach1 = new Attachment();
        attach1.name = 'Test Attachment.msg';
        attach1.Body = blob.valueOf('Test Attachment');
        attach1.parentID = VM_Job.Id;
        insert attach1;

        attach2 = new Attachment();
        attach2.name = 'Test Attachment.docx';
        attach2.Body = blob.valueOf('Test Attachment2');
        attach2.parentID = VM_Job.Id;
        insert attach2;        
    }
 
    @isTest
    public static void testCreate1(){
        TestprepareData();
                  Test.startTest();


        Test.setCurrentPage(Page.VM_VolunteerApplication_Create);
        string str = '?key1='+VM_Contact.Id;
        str = str+'&key2='+VM_Role.Id+'&key3=CMPCCommittee';
        ApexPages.currentPage().getHeaders().put('referer',str);
        
        System.currentPageReference().getParameters().put('key1', VM_Contact.Id);
        System.currentPageReference().getParameters().put('key2', VM_Role.Id);
        System.currentPageReference().getParameters().put('key3', 'CMPCCommittee');
        
 //Check happy path for SaveAndContinue Action   
        VM_JobApplicationController_Create vmController = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController.outreach1 = 'No';
        vmController.commitmentCPMC = 'No';
        vmController.dispactionimp = 'No';
        vmController.thistimecommitmentDRC = 'No';
        vmController.employeersupport = 'No';
        vmController.doyouworkinindustry = 'No';
        vmController.othertestingGroups = 'No';
        vmController.cfainstCode = 'No';
        vmController.rulesandProcedure = 'No';
        vmController.CFAexamcurricula = 'No';
        vmController.relatedTopics = 'No';
        vmController.related_topics1 = 'No';
        vmController.Volunteered_before = 'No';
        vmController.Inappropriate_influence = 'No';
        vmController.teachng_of_CFA = 'No';
        vmController.gain_or_benefit = 'No';
        vmController.Prep_Provider = 'No';
        vmController.Relationship_with_Candidate = 'No';
        vmController.Relationship_with_Testing = 'No';
        vmController.Agree_to_the_Conflict_Policy = 'True';
        vmController.disclosed_conflicts = 'True';
        vmController.cfaDevelopCurricula = 'No';
        vmController.volTimeCommitDRC = 'No';
        vmController.con = VM_Contact;
        vmController.getItems1();
        vmController.getItems();
        VM_JobApplicationController_Create.getPolicy();
        vmController.key3 = 'CMPCCommittee';
        VM_JobApplicationController_Create.getParams('CMPCCommittee','Test','Test');
        vmController.getIndustriesText();
        vmController.getRolesText();
        vmController.getEverServedText();
        vmController.getQuestionsForRecordType();
        vmController.fillInErrorMessage('Testing apps');
        vmController.saveandcontinue();

//Check happy path for SaveForLater

        VM_JobApplicationController_Create vmController5 = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController5.outreach1 = 'No';
        vmController5.commitmentCPMC = 'No';
        vmController5.dispactionimp = 'No';
        vmController5.thistimecommitmentDRC = 'No';
        vmController5.employeersupport = 'No';
        vmController5.doyouworkinindustry = 'No';
        vmController5.othertestingGroups = 'No';
        vmController5.cfainstCode = 'No';
        vmController5.rulesandProcedure = 'No';
        vmController5.CFAexamcurricula = 'No';
        vmController5.relatedTopics = 'No';
        vmController5.related_topics1 = 'No';
        vmController5.Volunteered_before = 'No';
        vmController5.Inappropriate_influence = 'No';
        vmController5.teachng_of_CFA = 'No';
        vmController5.gain_or_benefit = 'No';
        vmController5.Prep_Provider = 'No';
        vmController5.Relationship_with_Candidate = 'No';
        vmController5.Relationship_with_Testing = 'No';
        vmController5.Agree_to_the_Conflict_Policy = 'True';
        vmController5.disclosed_conflicts = 'True';
        vmController5.cfaDevelopCurricula = 'No';
        vmController5.volTimeCommitDRC = 'No';
        vmController5.con = VM_Contact;
        vmController5.getItems1();
        vmController5.getItems();
        VM_JobApplicationController_Create.getPolicy();
        vmController5.key3 = 'CMPCCommittee';
        VM_JobApplicationController_Create.getParams('CMPCCommittee','Test','Test');
        vmController5.getIndustriesText();
        vmController5.getRolesText();
        vmController5.getEverServedText();
        vmController5.getQuestionsForRecordType();
        vmController5.fillInErrorMessage('Testing apps');
        vmController5.saveforlater();
        
        //Check when conflict of intrest answered YES       
        
        
        VM_JobApplicationController_Create vmController1 = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController1.con = VM_Contact;
        vmController1.Agree_to_the_Conflict_Policy = 'True';
        vmController1.disclosed_conflicts = 'True';
        vmController1.Inappropriate_influence = 'Yes';
        vmController1.getItems1();
        vmController1.getItems();
        VM_JobApplicationController_Create.getPolicy();
        vmController1.key3 = 'Exam Related Advisor';
        VM_JobApplicationController_Create.getParams('Exam Related Advisor','Test','Test');
        vmController1.getIndustriesText();
        vmController1.getRolesText();
        vmController1.getEverServedText();
        vmController1.getQuestionsForRecordType();
        vmController1.fillInErrorMessage('Testing apps');
        vmController1.saveandcontinue();
        vmController1.saveforlater();  

 //Disosed conflicts Null        
        VM_JobApplicationController_Create vmController6 = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController6.con = VM_Contact;
        vmController6.Agree_to_the_Conflict_Policy = 'True';
        vmController6.disclosed_conflicts = Null;   
        vmController6.saveandcontinue();
        vmController6.saveforlater();
//One of the agreements set to False
        VM_JobApplicationController_Create vmController7 = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController7.con = VM_Contact;
        vmController7.Agree_to_the_Conflict_Policy = 'False';
        vmController7.disclosed_conflicts = 'True';   
        vmController7.saveandcontinue();
        vmController7.saveforlater();

        Test.stopTest();
        System.assertEquals(2, ApexPages.getMessages().size());
    }
    

    @isTest
    public static void testCreate2() {
        TestprepareData();

        Test.startTest();
        try
        {  
            set<string> setRecordType = new set<String>{'CIPMCommittee','CMPCCommittee','DRCCommittee','EACCommittee','Exam Related Advisor','GIPSCommittee','MemberMicro','Non Exam related Advisory council','SPCCommittee'};
                for(string str1 : setRecordType){
                   
                    VM_Job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get(str1).getRecordTypeId();
                    update VM_Job;
                    
                    Test.setCurrentPage(Page.VM_VolunteerApplication_Create);
                    string str = '?key1='+VM_Contact.Id+'&key2='+VM_Role.Id+'&key3='+str1;
                    ApexPages.currentPage().getHeaders().put('referer',str);
                    
                    System.currentPageReference().getParameters().put('key1', VM_Contact.Id);
                    System.currentPageReference().getParameters().put('key2', VM_Role.Id);
                    System.currentPageReference().getParameters().put('key3', str1);
                    
                    VM_JobApplicationController_Create vmController = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
                    list<string> stquestion = vmController.getQuestionsForRecordType();
                    

                }
            
        }
        catch(DMLException ex)
        {
            system.debug('testCreate'+ex.getDMLMessage(0));
        }
        Test.stopTest();
                System.assertEquals(0, ApexPages.getMessages().size());

    }
 
    @isTest
    public static void testCreate4(){
        TestprepareData();
                  Test.startTest();

        //PageReference vfPage = new PageReference('https://devcfacogn-cfainstitute.cs91.force.com/CFAVolunteer/s/new-application?');
        Test.setCurrentPage(Page.VM_VolunteerApplication_Create);
        string str = '?key1='+VM_Contact.Id;
        str = str+'&key2='+VM_Role.Id+'&key3=Exam%20Related Advisor&';
        ApexPages.currentPage().getHeaders().put('referer',str);
        
        System.currentPageReference().getParameters().put('key1', VM_Contact.Id);
        System.currentPageReference().getParameters().put('key2', VM_Role.Id);
        System.currentPageReference().getParameters().put('key3', 'Exam%20Related Advisor&');
        
 //Check happy path for SaveAndContinue Action   
        VM_JobApplicationController_Create vmController = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController.outreach1 = 'No';
        vmController.commitmentCPMC = 'No';
        vmController.dispactionimp = 'No';
        vmController.thistimecommitmentDRC = 'No';
        vmController.employeersupport = 'No';
        vmController.doyouworkinindustry = 'No';
        vmController.othertestingGroups = 'No';
        vmController.cfainstCode = 'No';
        vmController.rulesandProcedure = 'No';
        vmController.CFAexamcurricula = 'No';
        vmController.relatedTopics = 'No';
        vmController.related_topics1 = 'No';
        vmController.Volunteered_before = 'No';
        vmController.Inappropriate_influence = 'No';
        vmController.teachng_of_CFA = 'No';
        vmController.gain_or_benefit = 'No';
        vmController.Prep_Provider = 'No';
        vmController.Relationship_with_Candidate = 'No';
        vmController.Relationship_with_Testing = 'No';
        vmController.Agree_to_the_Conflict_Policy = 'True';
        vmController.disclosed_conflicts = 'True';
        vmController.cfaDevelopCurricula = 'No';
        vmController.volTimeCommitDRC = 'No';
        vmController.con = VM_Contact;
        vmController.getItems1();
        vmController.getItems();
        VM_JobApplicationController_Create.getPolicy();
        vmController.key3 = 'Exam%20Related Advisor&';
        VM_JobApplicationController_Create.getParams('Exam Related Advisor','Test','Test');
        vmController.getIndustriesText();
        vmController.getRolesText();
        vmController.getEverServedText();
        vmController.getQuestionsForRecordType();
        vmController.fillInErrorMessage('Testing apps');
        vmController.saveandcontinue();

//Check happy path for SaveForLater

        VM_JobApplicationController_Create vmController5 = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController5.outreach1 = 'No';
        vmController5.commitmentCPMC = 'No';
        vmController5.dispactionimp = 'No';
        vmController5.thistimecommitmentDRC = 'No';
        vmController5.employeersupport = 'No';
        vmController5.doyouworkinindustry = 'No';
        vmController5.othertestingGroups = 'No';
        vmController5.cfainstCode = 'No';
        vmController5.rulesandProcedure = 'No';
        vmController5.CFAexamcurricula = 'No';
        vmController5.relatedTopics = 'No';
        vmController5.related_topics1 = 'No';
        vmController5.Volunteered_before = 'No';
        vmController5.Inappropriate_influence = 'No';
        vmController5.teachng_of_CFA = 'No';
        vmController5.gain_or_benefit = 'No';
        vmController5.Prep_Provider = 'No';
        vmController5.Relationship_with_Candidate = 'No';
        vmController5.Relationship_with_Testing = 'No';
        vmController5.Agree_to_the_Conflict_Policy = 'True';
        vmController5.disclosed_conflicts = 'True';
        vmController5.cfaDevelopCurricula = 'No';
        vmController5.volTimeCommitDRC = 'No';
        vmController5.con = VM_Contact;
        vmController5.getItems1();
        vmController5.getItems();
        VM_JobApplicationController_Create.getPolicy();
        vmController5.key3 = 'CMPCCommittee';
        VM_JobApplicationController_Create.getParams('CMPCCommittee','Test','Test');
        vmController5.getIndustriesText();
        vmController5.getRolesText();
        vmController5.getEverServedText();
        vmController5.getQuestionsForRecordType();
        vmController5.fillInErrorMessage('Testing apps');
        vmController5.saveforlater();
        
        //Check when conflict of intrest answered YES       
        
        
        VM_JobApplicationController_Create vmController1 = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController1.con = VM_Contact;
        vmController1.Agree_to_the_Conflict_Policy = 'True';
        vmController1.disclosed_conflicts = 'True';
        vmController1.Inappropriate_influence = 'Yes';
//        vmController1.saveApplication();
        vmController1.getItems1();
        vmController1.getItems();
        VM_JobApplicationController_Create.getPolicy();
        vmController1.key3 = 'Exam Related Advisor';
        VM_JobApplicationController_Create.getParams('Exam Related Advisor','Test','Test');
        vmController1.getIndustriesText();
        vmController1.getRolesText();
        vmController1.getEverServedText();
        vmController1.getQuestionsForRecordType();
        vmController1.fillInErrorMessage('Testing apps');
        vmController1.saveandcontinue();
        vmController1.saveforlater();  

        Test.stopTest();
        //System.assertEquals(1, ApexPages.getMessages().size());
    }
   
    @isTest
    public static void testCreate5(){
        TestprepareData();
                  Test.startTest();


        Test.setCurrentPage(Page.VM_VolunteerApplication_Create);
        string str = '?key1='+VM_Contact.Id;
        str = str+'&key2='+VM_Role.Id+'&key3=&Exam%20Related Advisor';
        ApexPages.currentPage().getHeaders().put('referer',str);
        
        System.currentPageReference().getParameters().put('key1', VM_Contact.Id);
        System.currentPageReference().getParameters().put('key2', VM_Role.Id);
        System.currentPageReference().getParameters().put('key3', '&Exam%20Related Advisor');
        
 //Check happy path for SaveAndContinue Action   
        VM_JobApplicationController_Create vmController = new VM_JobApplicationController_Create(new ApexPages.StandardController(VM_Job));
        vmController.outreach1 = 'No';
        vmController.commitmentCPMC = 'No';
        vmController.dispactionimp = 'No';
        vmController.thistimecommitmentDRC = 'No';
        vmController.employeersupport = 'No';
        vmController.doyouworkinindustry = 'No';
        vmController.othertestingGroups = 'No';
        vmController.cfainstCode = 'No';
        vmController.rulesandProcedure = 'No';
        vmController.CFAexamcurricula = 'No';
        vmController.relatedTopics = 'No';
        vmController.related_topics1 = 'No';
        vmController.Volunteered_before = 'No';
        vmController.Inappropriate_influence = 'No';
        vmController.teachng_of_CFA = 'No';
        vmController.gain_or_benefit = 'No';
        vmController.Prep_Provider = 'No';
        vmController.Relationship_with_Candidate = 'No';
        vmController.Relationship_with_Testing = 'No';
        vmController.Agree_to_the_Conflict_Policy = 'True';
        vmController.disclosed_conflicts = 'True';
        vmController.cfaDevelopCurricula = 'No';
        vmController.volTimeCommitDRC = 'No';
        vmController.con = VM_Contact;
        vmController.getItems1();
        vmController.getItems();
        VM_JobApplicationController_Create.getPolicy();
        vmController.key3 = '&Exam%20Related Advisor';
        VM_JobApplicationController_Create.getParams('Exam Related Advisor','Test','Test');
        vmController.getIndustriesText();
        vmController.getRolesText();
        vmController.getEverServedText();
        vmController.getQuestionsForRecordType();
        vmController.fillInErrorMessage('Testing apps');
        vmController.saveandcontinue();
 
        Test.stopTest();
        System.assertEquals(1, ApexPages.getMessages().size());
    }

  }