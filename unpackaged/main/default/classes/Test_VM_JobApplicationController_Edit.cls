@isTest

public class Test_VM_JobApplicationController_Edit {
    
    public static Account portalAccount;
    public static Contact portalContact;
    public static user portalUser;
    public static Engagement__c eng;
    public static Role__c role;
    public static Job_Application__c job;
    public static ContactShare conShare;
    public static Engagement__Share engShare;
    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    public static final String ENGAGEMENT_RECORDTYPE = 'Non Confidential';
    public static integer count=0;

    public static void setupdata(){
        ContactTriggerHandler.triggerDisabled = true;

        Count =count+1;
        //create Account
        portalAccount = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        portalAccount.IsPartner = true;
        update portalAccount;
        //create contact
        portalContact = VM_TestDataFactory.createContactRecord(portalAccount);        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
        
        //create users
        portalUser = CFA_TestDataFactory.createPartnerUser(portalContact,'CFA Base Partner');
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest'+count+'@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest'+count+'@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne'+count+'@gmail.com','Lange','Outreach Management User','Relationship Manager');
        //create engagement
        eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        Schema.DescribeSObjectResult engSchemaResult = Schema.SObjectType.Engagement__c;
        Map<String,Schema.RecordTypeInfo> mapengTypeInfo = engSchemaResult.getRecordTypeInfosByName();
        Id engRecordTypeId = mapengTypeInfo.get(ENGAGEMENT_RECORDTYPE).getRecordTypeId();
        eng.RecordTypeId = engRecordTypeId;
        eng.Engagement_Type__c='Committee';
        insert eng;

        
        //create role
        role=VM_TestDataFactory.createRole(eng); 
        role.Confidential__c = false;        
        insert role;
        //create job
        job=VM_TestDataFactory.createJobApplication(role,portalContact);
        insert job;
        job.Status__c='Submitted';
        job.Submitted__c = true;
        update job;
        job.status__c='Volunteer Accepted';
        update job;
        
        //create sharing
        ContactShare conShare = new ContactShare();
        conShare.ContactId =portalContact.Id;
        conShare.UserOrGroupId = portalUser.Id;
        conShare.ContactAccessLevel = 'Read';
        insert conShare;
        /*Engagement__Share engShare = new Engagement__Share();
        engShare.ParentId =eng.Id;
        engShare.UserOrGroupId = portalUser.Id;
        engShare.AccessLevel = 'Read';
        insert engShare;*/
        Document doc = new Document(FolderId = UserInfo.getUserId() ,Name='Volunteer Acknowledgement Form', Body = Blob.ValueOf('Hello World'));
        insert doc;

    }
    
    
    public static testMethod void test_JobAppEdit() {
        setupdata();
        //Task tsk = VM_TaskCreateEditDetail.getNewTask();
        
        try
        {   
            job.In_which_of_the_following_industry_areas__c = 'Academic Institution;Alternative Investments';
            job.Current_or_previous_roles_have_you__c='Client Service Relationship Manager;Corporate CFO';
            job.Have_you_ever_served__c='as an Arbitrator;as a juror';
            update job;
            Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
            string str = '/edit-application?key='+job.Id;
            ApexPages.currentPage().getHeaders().put('referer',str);
            VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
            vmController.fillInErrorMessage('abc');
            List<SelectOption> lstsel= vmController.getlevel1Items();
            String str1 = vmController.getEverServedText();
            vmController.saveApplication();
            list<SelectOption> selOp2 = vmController.getItems();
            list<SelectOption> selOp3 = vmController.getItems1();
            list<string> stquestion = vmController.getQuestionsForRecordType();
            
        }
        catch(DMLException ex)
        {
            
        }
                System.assertEquals(1, ApexPages.getMessages().size());
    }
    
    public static testMethod void test_JobAppEdit1() {
        setupdata();

        
        try
        {   
            job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CMPCCommittee').getRecordTypeId();
            update job;
            Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
            string str = '/edit-application?key='+job.Id;
            ApexPages.currentPage().getHeaders().put('referer',str);
            VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
            list<string> stquestion = vmController.getQuestionsForRecordType();
            
        }
        catch(DMLException ex)
        {
            
        }
                System.assertEquals(0, ApexPages.getMessages().size());
    }
    
    public static testMethod void test_JobAppEdit2CIPMCommittee() {
        setupdata();

        Test.startTest();
        try{
            job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CIPMCommittee').getRecordTypeId();
            update job;
            Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
            String str = '/edit-application?key='+job.Id;
            ApexPages.currentPage().getHeaders().put('referer',str);
            VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
            List<String> stquestion = vmController.getQuestionsForRecordType();

        }
        catch(DmlException ex){
            System.assert(ex == null);
        }
        Test.stopTest();

        System.assertEquals(0, ApexPages.getMessages().size());
    }

    public static testMethod void test_JobAppEdit2CMPCCommittee() {
        setupdata();

        Test.startTest();
        try{
            job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('CMPCCommittee').getRecordTypeId();
            update job;
            Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
            String str = '/edit-application?key='+job.Id;
            ApexPages.currentPage().getHeaders().put('referer',str);
            VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
            List<String> stquestion = vmController.getQuestionsForRecordType();

        }
        catch(DmlException ex){
            System.assert(ex == null);
        }
        Test.stopTest();

        System.assertEquals(0, ApexPages.getMessages().size());
    }

    public static testMethod void test_JobAppEdit2DRCCommittee() {
        setupdata();

        Test.startTest();
        try{
            job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('DRCCommittee').getRecordTypeId();
            update job;
            Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
            String str = '/edit-application?key='+job.Id;
            ApexPages.currentPage().getHeaders().put('referer',str);
            VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
            List<String> stquestion = vmController.getQuestionsForRecordType();

        }
        catch(DmlException ex){
            System.assert(ex == null);
        }
        Test.stopTest();

        System.assertEquals(0, ApexPages.getMessages().size());
    }

    public static testMethod void test_JobAppEdit2EACCommittee() {
        setupdata();

        Test.startTest();
        try{
            job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('EACCommittee').getRecordTypeId();
            update job;
            Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
            String str = '/edit-application?key='+job.Id;
            ApexPages.currentPage().getHeaders().put('referer',str);
            VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
            List<String> stquestion = vmController.getQuestionsForRecordType();

        }
        catch(DmlException ex){
            System.assert(ex == null);
        }
        Test.stopTest();

        System.assertEquals(0, ApexPages.getMessages().size());
    }

    public static testMethod void test_JobAppEditEACCommittee() {
        setupdata();
        job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('EACCommittee').getRecordTypeId();
        update job;
        Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
        string str = '/edit-application?key='+job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
        list<string> stquestion = vmController.getQuestionsForRecordType();
        System.assertEquals(0, ApexPages.getMessages().size());
    }
    
    public static testMethod void test_JobAppEditExamRelatedAdvisor() {
        setupdata();
        job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Exam Related Advisor').getRecordTypeId();
        update job;
        Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
        string str = '/edit-application?key='+job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
        list<string> stquestion = vmController.getQuestionsForRecordType();
        System.assertEquals(0, ApexPages.getMessages().size());
    }
    
    public static testMethod void test_JobAppEditGIPSCommittee() {
        setupdata();
        job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('GIPSCommittee').getRecordTypeId();
        update job;
        Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
        string str = '/edit-application?key='+job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
        list<string> stquestion = vmController.getQuestionsForRecordType();
        System.assertEquals(0, ApexPages.getMessages().size());
    }
    
    public static testMethod void test_JobAppEditMemberMicro() {
        setupdata();
        job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('MemberMicro').getRecordTypeId();
        update job;
        Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
        string str = '/edit-application?key='+job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
        list<string> stquestion = vmController.getQuestionsForRecordType();
        System.assertEquals(0, ApexPages.getMessages().size());
    }
    
    public static testMethod void test_JobAppEditNonExamrelatedAdvisorycouncil() {
        setupdata();
        job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Non Exam related Advisory council').getRecordTypeId();
        update job;
        Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
        string str = '/edit-application?key='+job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
        list<string> stquestion = vmController.getQuestionsForRecordType();
        System.assertEquals(0, ApexPages.getMessages().size());
    }
    
	public static testMethod void test_JobAppEditSPCCommittee() {
        setupdata();
        job.RecordTypeId = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('SPCCommittee').getRecordTypeId();
        update job;
        Test.setCurrentPage(Page.VM_VolunteerApplication_Edit1);
        string str = '/edit-application?key='+job.Id;
        ApexPages.currentPage().getHeaders().put('referer',str);
        VM_JobApplicationController_Edit vmController = new VM_JobApplicationController_Edit(new ApexPages.StandardController(job));
        list<string> stquestion = vmController.getQuestionsForRecordType();
        System.assertEquals(0, ApexPages.getMessages().size());
    }
    
}