/**
 * @description Batch class to copy data from sobject to big object
 */
public with sharing class TransferSObjectToBigObjectBatch implements Database.Batchable<sObject> {
    
    private String sobjectApiName;
    private String bigObjectApiName;
    private Map<String, String> mapSobjectFieldToBigObjectField;
    private String sobjectQueryCriteria;
    private Boolean deleteSobjectRecords;
    
    /**
     *  @description Constructor
     * @param   sobjectApiName      String  Sobject Api name for source sobject
     * @param   bigObjectApiName    String  Big Object Api Name for target big object
     * @param   mapSobjectFieldToBigObjectField  Map<String, String> map sobject field names to big object field names
     * @param   sobjectQueryCriteria    String where clause for source sobject
     * @param   deleteSobjectRecords    Boolean if true source sobject records would be deleted
     * 
     */
    public TransferSObjectToBigObjectBatch(String sobjectApiName, String bigObjectApiName, Map<String, String> mapSobjectFieldToBigObjectField, String sobjectQueryCriteria, Boolean deleteSobjectRecords) {
        this.sobjectApiName = sobjectApiName;
        this.bigObjectApiName = bigObjectApiName;
        this.mapSobjectFieldToBigObjectField = mapSobjectFieldToBigObjectField;
        this.sobjectQueryCriteria = sobjectQueryCriteria;
        this.deleteSobjectRecords = deleteSobjectRecords;
        String[] types = new String[]{bigObjectApiName, sobjectApiName};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        if(!results[0].isCreateable()) {
            throw new SecurityException('User with Id ' + UserInfo.getUserId() + ' does not have create access to object ' + bigObjectApiName);
        }
        if(deleteSobjectRecords && !results[1].isDeletable()) {
            throw new SecurityException('User with Id ' + UserInfo.getUserId() + ' does not have delete access to object ' + sobjectApiName);
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT ' + String.join(new List<String>(this.mapSobjectFieldToBigObjectField.keySet()), ',');
        query += ' FROM ' + sobjectApiName;
        if(String.isNotBlank(sobjectQueryCriteria)) {
            query += ' WHERE ' + sobjectQueryCriteria;
        }
        query += ' WITH SECURITY_ENFORCED';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<Sobject> records) {
        Savepoint sp = Database.setSavepoint();
        try {
            String errorMessage = '';
            List<Sobject> bigObjectList = new List<Sobject>();
            for(Sobject sobj: records) {
                Schema.SObjectType sobjType = Schema.getGlobalDescribe().get(this.bigObjectApiName);
                SObject resultBigObject = sobjType.newSObject();
                for(String sobjectField: this.mapSobjectFieldToBigObjectField.keySet()) {
                    Object fieldValue = sobj.get(sobjectField);
                    resultBigObject.put(this.mapSobjectFieldToBigObjectField.get(sobjectField), fieldValue instanceof Boolean ? String.valueOf(fieldValue) : fieldValue);
                }
                bigObjectList.add(resultBigObject);
            }
            List<Database.SaveResult> saveResults = new List<Database.SaveResult>();
            if(!Test.isRunningTest()) saveResults = Database.insertImmediate(bigObjectList);
            Boolean rollbackTransaction = false;
            for(Database.SaveResult saveResult: saveResults) {
                if(!saveResult.isSuccess()) {
                    rollbackTransaction = true;
                    for(Database.Error err : saveResult.getErrors()) {
                        errorMessage += err.getStatusCode() + ': ' + err.getMessage() + ' ';
                    }
                }
            }
            if(rollbackTransaction) throw new TransferSObjectToBigObjectException(errorMessage);
            if(this.deleteSobjectRecords)Database.delete(records);
        } catch (Exception ex) {
            Database.rollback(sp);
            insert CFA_IntegrationLogException.logError(ex, 'Data archival process', 'batch', DateTime.now(), true, '', '', '', true, '');
            throw ex;
        }
    }    

    public void finish(Database.BatchableContext bc) {}

    private class TransferSObjectToBigObjectException extends Exception{}
}