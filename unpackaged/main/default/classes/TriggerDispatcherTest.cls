@isTest
public class TriggerDispatcherTest {

	static testMethod void BeforeInserttest() {
		Account acc = new account(name = 'TestAc');
		insert acc;
		System.assertEquals('TestAc', [SELECT id, name FROM account WHERE id = :acc.id].name);

		list<Research_Challenge__c> lstRC = new List<Research_Challenge__c>();
		Id LoaclRecordTypeId = Schema.SObjectType.Research_Challenge__c
			.getRecordTypeInfosByName()
			.get('Local Research Challenge')
			.getRecordTypeId();
		Id RegionalRecordTypeId = Schema.SObjectType.Research_Challenge__c
			.getRecordTypeInfosByName()
			.get('Regional Research Challenge')
			.getRecordTypeId();
		Id GobalRecordTypeId = Schema.SObjectType.Research_Challenge__c
			.getRecordTypeInfosByName()
			.get('Global Research Challenge')
			.getRecordTypeId();

		Research_Challenge__c objRChallenge = new Research_Challenge__c(name = 'LOCAL - The first Kind', recordtypeid = LoaclRecordTypeId);
		objRChallenge.Location__c = 'abc';
		objRChallenge.Local_Final__c = system.today();
		lstRC.add(objRChallenge);

		Research_Challenge__c objRChallenge1 = new Research_Challenge__c(
			name = 'Regional - The foirst of Kind',
			recordtypeid = RegionalRecordTypeId
		);
		objRChallenge.Location__c = 'abc';
		objRChallenge.Local_Final__c = system.today();
		lstRC.add(objRChallenge1);

		Research_Challenge__c objRChallenge2 = new Research_Challenge__c(name = 'Global Test Challenge', recordtypeid = GobalRecordTypeId);
		objRChallenge.Location__c = 'abc';
		objRChallenge.Local_Final__c = system.today();
		lstRC.add(objRChallenge2);
		insert lstRC;

		System.assertEquals('LOCAL - The first Kind', [SELECT id, name FROM Research_Challenge__c WHERE id = :objRChallenge.id].name);
		System.assertEquals(
			'Regional - The foirst of Kind',
			[SELECT id, name FROM Research_Challenge__c WHERE id = :objRChallenge1.id]
			.name
		);
		System.assertEquals('Global Test Challenge', [SELECT id, name FROM Research_Challenge__c WHERE id = :objRChallenge2.id].name);

		RC_Participant__c objParticipant = new RC_Participant__c();
		objParticipant.Challenge_Status__c = 'Applied';
		objParticipant.Participant_First_Name__c = 'test';
		objParticipant.Participant_Last_Name__c = 'test';
		objParticipant.Email__c = 'abc@gmail.com';
		objParticipant.Faculty_University_Name__c = acc.id;
		objParticipant.Local_Challenge__c = objRChallenge.id;

		insert objParticipant;
		System.assertEquals('abc@gmail.com', [SELECT id, Email__c FROM RC_Participant__c WHERE id = :objParticipant.id].Email__c);

		objParticipant.Email__c = 'ab1c@gmail.com';
		update objParticipant;
		System.assertEquals('ab1c@gmail.com', [SELECT id, Email__c FROM RC_Participant__c WHERE id = :objParticipant.id].Email__c);

		RC_Participant__c objCLone = objParticipant.clone();
		insert objCLone;

		delete objParticipant;
	}
}