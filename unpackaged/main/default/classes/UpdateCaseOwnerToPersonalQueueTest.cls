@isTest
private class UpdateCaseOwnerToPersonalQueueTest {
    private static final Integer NUMBER_OF_AGENTS = 35;
    private static final String QUEUE_NAME = 'Test Personal Group';
    private static final String GCC_PROFILE_NAME = 'Customer Care Front Office Agent';
    private static final String GCC_CASE_RECORDTYPE = 'CustomerCare Back Office Record Type';
    private static final String TEST_USER_NAME = 'testUpdateCaseOwnerUser';

    @TestSetup
    private static void testSetup() {
        SocietyOps_Backoffice__c societyOps= new SocietyOps_Backoffice__c();
        societyOps.Name= 'trigger-details';
        societyOps.No_reply_email__c = 'noreply@cfainstitute.org';
        societyOps.CaseComment_TemplateName__c = 'SocietyOps-New Comment Notification';
        societyOps.CaseReassignment_TemplateName__c = 'SocietyOps-Case Reassignment';
        insert societyOps;

        List<User> testUsers = CFA_TestDataFactory.createUsers(TEST_USER_NAME, GCC_PROFILE_NAME, NUMBER_OF_AGENTS, true);
        List<Group> queues = CFA_TestDataFactory.createQueues(QUEUE_NAME, 'Case', NUMBER_OF_AGENTS, true);
        List<Case> cases = CFA_TestDataFactory.createCaseRecords(GCC_CASE_RECORDTYPE, NUMBER_OF_AGENTS, false);
        for(Integer i = 0; i < NUMBER_OF_AGENTS; i++) {
            cases[i].OwnerId = testUsers[i].Id;
        }
        insert cases;
    }

    @isTest
    private static void updateOwnerTest() {
        updateSettings();

        Map<Id, Case> cases = new Map<Id, Case>([ SELECT Id, Owner.Name FROM Case ]);
        Test.startTest();
            UpdateCaseOwnerToPersonalQueue.updateOwner(new List<Id>(cases.keySet()));
        Test.stopTest();
        
        List<Case> updatedCase = [ SELECT Id, Owner.Name FROM Case ORDER BY Owner.Name];
        for(Integer i = 0; i < NUMBER_OF_AGENTS; i++) {
            System.assert(updatedCase[i].Owner.Name.contains(QUEUE_NAME));
        }
    }

    @isTest
    private static void doNotUpdateOwnerTest() {
        Map<Id, Case> cases = new Map<Id, Case>([ SELECT Id, Owner.Name FROM Case ]);

        Test.startTest();
            UpdateCaseOwnerToPersonalQueue.updateOwner(new List<Id>(cases.keySet()));
        Test.stopTest();
        
        List<Case> updatedCase = [ SELECT Id, Owner.Name FROM Case ORDER BY Id];
        for(Integer i = 0; i < NUMBER_OF_AGENTS; i++) {
            System.assertEquals(cases.get(updatedCase[i].Id).Owner.Name, updatedCase[i].Owner.Name);
        }
    }


    private static void updateSettings() {
        String testUserSearchName = '%' + TEST_USER_NAME + '%';
        List<User> testUsers = [ SELECT Alias FROM User WHERE Username LIKE :testUserSearchName ORDER BY Id ];
        String queueName = QUEUE_NAME + '%';
        List<Group> queues = [ SELECT Id FROM Group WHERE Type = 'Queue' AND Name LIKE :queueName ORDER BY Id ];
        CustomerCare_Private_Queue_Assignment__c settings = CustomerCare_Private_Queue_Assignment__c.getInstance();
        for(Integer i = 0; i < NUMBER_OF_AGENTS; i++) {
            settings.put('Case_Owner_' + (i+1) + '__c', testUsers[i].Alias);
            settings.put('Queue_ID_' + (i+1) + '__c', queues[i].Id);
        }
        upsert settings;
    }
}