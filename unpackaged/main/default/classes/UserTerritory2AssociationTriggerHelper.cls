public with sharing class UserTerritory2AssociationTriggerHelper {
    
    public enum ShareCalculationType {ADD, REMOVE}
    
    /**
     * Removes library shares for user
     * 
     * @param	oldUserTerritory2Associations	deleted items from Trigger.old
     */
    public static void unshareLibrariesFromUsers(List<UserTerritory2Association> oldUserTerritory2Associations) {
        calculateLibrariesUsersShares(oldUserTerritory2Associations, UserTerritory2AssociationTriggerHelper.ShareCalculationType.REMOVE);
    }
    
    /**
     * Adds library shares for user
     * 
     * @param	newUserTerritory2Associations	deleted items from Trigger.new
     */
    public static void shareLibrariesWithUsers(List<UserTerritory2Association> newUserTerritory2Associations) {
        calculateLibrariesUsersShares(newUserTerritory2Associations, UserTerritory2AssociationTriggerHelper.ShareCalculationType.ADD);
    }
    
    /**
     * Adds/removed library shares for user based on ShareCalculationType
     * 
     * @param	userTerritory2Associations	items to count shares for
     * @param	shareCalculationType		type of share. if ADD then new shares would be created
     * 										If REMOVE shares would be deleted
     */
    private static void calculateLibrariesUsersShares(List<UserTerritory2Association> userTerritory2Associations, ShareCalculationType shareCalculationType) {
        Map<Id, UserTerritory2Association> mapIdToAssociation = new Map<Id, UserTerritory2Association>(userTerritory2Associations);
        Set<Id> territoryIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        for(UserTerritory2Association association: mapIdToAssociation.values()) {
            territoryIds.add(association.Territory2Id);
            userIds.add(association.UserId);
        }
        Map<Id, User> mapActiveUsers = new Map<Id, User>([SELECT Id, Name FROM User WHERE ID IN :userIds AND  isActive = true]);
        List<Territory2> territories = TerritoryService.getTerritoriesByIds(territoryIds);
        Map<Id, Territory2> mapIdToTerritory = new Map<Id, Territory2>(territories);
        Map<String, List<NSA_Policy__c>> mapTerritoryNameToPolicies = 
            NSAPolicyService.groupPoliciesByTerritoryGroupName(
                NSAPolicyService.getPoliciesByTerritoryGroupNames(
                	SObjectService.getFieldValues(territories, 'Name')
            	)
            );
        Map<Id, Set<String>> mapUserIdToLibraryIds = new Map<Id, Set<String>>();
        for(UserTerritory2Association association: mapIdToAssociation.values()) {
            Territory2 territory = mapIdToTerritory.get(association.Territory2Id);
            List<NSA_Policy__c> policiesList = mapTerritoryNameToPolicies.get(territory.Name);
            if(policiesList != null && !policiesList.isEmpty()) {
                Set<String> libraryIds = new Set<String>();
                for(String libraryId: SObjectService.getFieldValues(policiesList, 'Library_Id__c')) {
                    if(String.isNotEmpty(libraryId)) {
                        libraryIds.add(libraryId);
                    }
                }
                if(!libraryIds.isEmpty() && mapActiveUsers.containsKey(association.UserId)) {
                    Set<String> userLibraryIds = mapUserIdToLibraryIds.get(association.UserId);
                    if(userLibraryIds == null) {
                        userLibraryIds = new Set<String>();
                    }
                    userLibraryIds.addAll(libraryIds);
                    mapUserIdToLibraryIds.put(association.UserId, userLibraryIds);
                }
            }
        }
        Set<String> allLibraryIds = new Set<String>();
        for(Set<String> ids: mapUserIdToLibraryIds.values()) {
            allLibraryIds.addAll(ids);
        }
        ContentWorkspacePermission authorPermission = [SELECT Id, Name FROM ContentWorkspacePermission WHERE Name = 'Author'];
        Map<String, ContentWorkspaceMember> mapUniqueKeyToexistingMembers = new Map<String, ContentWorkspaceMember>();
        for(ContentWorkspaceMember cwm: [SELECT ID,ContentWorkspaceId,MemberId 
                                           FROM ContentWorkspaceMember 
                                          WHERE MemberId IN :mapUserIdToLibraryIds.keySet() 
                                            AND ContentWorkspaceId IN:allLibraryIds]) {
            mapUniqueKeyToexistingMembers.put(cwm.ContentWorkspaceId + '-' + cwm.MemberId, cwm);
        }
        List<ContentWorkspaceMember> libraryMembers = new List<ContentWorkspaceMember>();
        for(Id userId: mapUserIdToLibraryIds.keySet()) {
            for(String libraryId: mapUserIdToLibraryIds.get(userId)) {
                String uniqueKey = libraryId + '-' + userId;
                if(
                    shareCalculationType == UserTerritory2AssociationTriggerHelper.ShareCalculationType.ADD 
                    && !mapUniqueKeyToexistingMembers.containsKey(uniqueKey)
                ) {
                    libraryMembers.add(
                        new ContentWorkspaceMember(
                        ContentWorkspaceId = libraryId, 
                        ContentWorkspacePermissionId = authorPermission.Id, 
                        MemberId = userId)
                    );
                } else if(
                    shareCalculationType == UserTerritory2AssociationTriggerHelper.ShareCalculationType.REMOVE 
                    && mapUniqueKeyToexistingMembers.containsKey(uniqueKey)
                ) {
                    libraryMembers.add(mapUniqueKeyToexistingMembers.get(uniqueKey));
                }
            }
        }
        if(!libraryMembers.isEmpty()) {
            if(shareCalculationType == UserTerritory2AssociationTriggerHelper.ShareCalculationType.ADD) {
                insert libraryMembers;
            } else if(shareCalculationType == UserTerritory2AssociationTriggerHelper.ShareCalculationType.REMOVE) {
                delete libraryMembers;
            }
            
        }
    }

}