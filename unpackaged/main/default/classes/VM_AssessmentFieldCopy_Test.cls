@isTest
public class VM_AssessmentFieldCopy_Test {
    
     @isTest
    public static void testBeforeInsert(){
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest5@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest1@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne1@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='Test Data FY14';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        
        try{  
            insert eng;
            system.debug('Engagement inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        
        Assessment__c eng_assessment=VM_TestDataFactory.createEngagementAssessment(eng);
        try{
             insert eng_assessment;  
           
            system.debug('role inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        
        Role__c test_role= VM_TestDataFactory.createRole(eng);
        try{
             insert test_role;  
           
            system.debug('role inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        
        
        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc);
        try{
             insert con;  
           
            system.debug('Contact inserted');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }
        
        Engagement_Volunteer__c eng_vol_test=VM_TestDataFactory.createEngagementVolunteer(eng,test_role,con);
        
        Assessment__c vol_assessment=VM_TestDataFactory.createVolunteerAssessment(eng_vol_test);
        try{
             insert con;  
           
            system.debug('eng vol inserted!');
        }catch(DMLException ex)
        {
            System.debug(ex) ;
        }

    }
}