@isTest
public class VM_CreateRoleControllerTest {
    
    public static Account VM_Account;
    public static Contact VM_Contact;
    public static Engagement__c VM_Engage;
    public static Role__c VM_Role;
    public static Job_Application__c VM_Job;
    
    public static void prepareData(){
        
        VM_Account = new Account();
        VM_Account.Name ='Test_VMAcc1';
        VM_Account.Phone = '222568974';
        insert VM_Account;
        
        VM_Contact = new Contact();
        //VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        insert VM_Contact;
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest7788@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest565@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne7787@gmail.com','Huff','Outreach Management User','Relationship Manager');
        
        Engagement__c VM_Engage=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        insert VM_Engage;
        
        VM_Role=VM_TestDataFactory.createRole(VM_Engage);
        VM_Role.VM_JARecordTypeName__c='MemberMicro';
        try{system.Debug('###@'+VM_Role);
            upsert VM_Role;
           }catch(exception e){system.Debug('###'+e);}
        
        VM_Job = VM_TestDataFactory.createJobApplicationMicro(VM_Role,VM_Contact);
         VM_Job.Position__c=VM_Role.Id;
        insert VM_Job; 
        
    }
    public static testMethod void testCreate(){
        prepareData();
        //ApexPages.StandardController controller; 
        PageReference pgref = new PageReference('apex/VM_CreateRole?retURL=%2Fa2x%2Fo&save_new=1&sfdc.override=1');
        Test.setCurrentPageReference(pgref);
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(VM_Role);
        VM_CreateRoleController VMC = New VM_CreateRoleController(sc);
        VMC.getRecordType();
        VMC.getRecordTypes();
        //VMC.recordTypeSelection();
        VMC.dummy();
        VMC.SaveAndNew();
        
        Test.stopTest();
    }
    
}