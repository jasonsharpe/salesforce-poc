public class VM_JobApplicationController_Create{
    public String errorMessage {get;set;}
    public String PolicyDocumentURl {get;set;}
    public string strCharLimit{get;set;}
    public static String recordtypew;
    public static String roleIdw;
    public static String contactIdw;    
    public String cntctId;
    Public String AttachmentType;
    Public String AttachmentType1;
    Public String AttachmentType2;
    Public String AttachmentType3;
    Public Final String InvAttType = 'Invalid';
    Public Final String ValidAttType = 'Valid';
    public String url_contactName;
    public String url_futureRole;
    public String url_recordType;
    
    public String mapKey{get; set;}
    public String outreach1{get; set;}
    public String commitmentCPMC{get; set;}
    public String dispactionimp{get; set;}
    public String thistimecommitmentDRC{get; set;}
    public String thistimecommitmentSPC{get; set;}
    public String employeersupport{get; set;}
    public String doyouworkinindustry{get; set;}
    public String othertestingGroups{get; set;}
    public String cfainstCode{get; set;}
    public String rulesandProcedure{get; set;}
    public String CFAexamcurricula{get; set;}
    public String relatedTopics{get; set;}
    public String related_topics1{get; set;}
    public String Volunteered_before{get; set;}
    public String Inappropriate_influence{get; set;}
    public String teachng_of_CFA{get; set;}
    public String gain_or_benefit{get; set;}
    public String Prep_Provider{get; set;}
    public String Relationship_with_Candidate{get; set;}
    public String Relationship_with_Testing{get; set;}
    public String Agree_to_the_Conflict_Policy{get; set;}
    public String disclosed_conflicts{get; set;}
    public String cfaDevelopCurricula{get; set;}
    public String volTimeCommitDRC{get; set;}
    public boolean isSave = false;
    
    //multi select picklist 
    public String[] Competenciesvalues { get; set; }
    public SelectOption[] CompetenciesvalueList { get; set; }
    public String[] Industryvalues { get; set; }
    public SelectOption[] IndustryvalueList { get; set; }
    public String[] Rollesvalues { get; set; }
    public SelectOption[] RollesvalueList { get; set; }
    public String[] industriesmultiselectvalues { get; set; }
    public SelectOption[] industriesmultiselectvalueList { get; set; }
    public String[] rolesmultiselectvalues { get; set; }
    public SelectOption[] rolesmultiselectvalueList { get; set; }
    public String[] everServedmultiselectvalues { get; set; }
    public SelectOption[] everServedmultiselectvalueList { get; set; }
    
    public String key1=null;
    public String key2=null;
    public String key3=null;
    
    public Job_Application__c job{get;set;}
    public Contact con{get; set;}
    
    public List<Role__c> roleDetails{get;set;}
    public Role__c rolelst{get; set;}
    public List<String> templateString{get; set;}
    public Integer numStr{get; set;}
    public Attachment att1{get; set;}
    public Attachment att2{get; set;}
    public Attachment att3{get; set;}
    public boolean ShowNote{get;set;}
    @TestVisible
    public VM_JobApplicationController_Create(ApexPages.StandardController controller) {
        //changes done by prathamesh
        ShowNote=false;
        strCharLimit ='Each text box is limited to 255 characters.';
        String url= ApexPages.currentPage().getHeaders().get('referer'); 
        String keyId = ApexPages.currentPage().getHeaders().get('key'); 
        att1 = new Attachment();
        att2 = new Attachment();
        att3 = new Attachment();
        //Get Acknowledge document
        Document dr=[Select ID from Document where Name='Volunteer Acknowledgement Form'];
        PolicyDocumentURl= Label.VM_Policydocument+'/servlet/servlet.FileDownload?file='+dr.id;     
        
        //Get values from URL    
        roleDetails = new List<Role__c>();
        rolelst = new Role__c();
        if(!String.isBlank(url) && url.contains('?')){
            Integer questionMarkIndex=url.indexOf('?');
            String relativeURL=url.substring(questionMarkIndex+1);
            set<string> keySet = new set<string>();
            keySet.addAll(relativeURL.split('&'));
            for(string str : keySet){
                if((str.left(4)).equalsIgnoreCase('key1')){
                    key1=str.removeStartIgnoreCase('key1=');
                }
                if((str.left(4)).equalsIgnoreCase('key2')){
                    key2=str.removeStartIgnoreCase('key2=');
                }
                if((str.left(4)).equalsIgnoreCase('key3')){
                    key3=str.removeStartIgnoreCase('key3=');
                }
            }
        }
        //changes done by prathamesh
        if(!String.isBlank(key3) && key3.contains('%20')){
            key3.replace('%20', '');
            
        }
        //Logic for questions visibility 
        if(key3=='CIPMCommittee')
            numStr = 1;
        else if(key3=='CMPCCommittee')
            numStr = 2;
        else if(key3=='Advisory Council')
            numStr = 3;
        else if(key3=='Default')
            numStr = 4;
        else if(key3=='DRCCommittee')
            numStr = 5;
        else if(key3=='EACCommittee')
            numStr = 6;
        else if(key3=='Exam Related Advisor' || key3=='Exam+Related+Advisor'||key3=='Exam%20Related%20Advisor')
        {
            numStr = 7;
            key3='Exam Related Advisor';
        }
        else if(key3=='GIPSCommittee')
           { numStr = 8;
            ShowNote=true;
             strCharLimit ='Each text box is limited to 1000 characters.  It is important that your responses be brief and specific to the volunteer role.  You will have the opportunity to attach additional information, along with your required resume or CV, before submitting the application.';
           }  
        else if(key3=='MemberMicro')
            numStr = 9;
        else if(key3=='SPCCommittee')
            numStr = 10;
        else if(key3=='Non+Exam+related+Advisory+council'||key3=='Non Exam related Advisory Council'||key3=='Non%20Exam%20related%20Advisory%20council')
        {
            numStr = 11;
            key3='Non Exam related Advisory council';
        }
        else
            numStr = 12;
        //Get contact info       
        
        cntctId=[Select ContactId from User where id=:Userinfo.getUserId()].ContactId;
        if(!String.isBlank(cntctId))
            con=[Select FirstName, Lastname,MiddleName,Mailingpostalcode,MailingCity,MailingCountry,MailingAddress,MailingState,MailingStreet, Email ,Account.name,Phone,MDM_Preferred_Email_for_Volunteering__c,MDM_Job_Title__c,CFAMN__Employer__c from Contact where id=:cntctId];
        //Get roles based on contact
        if(!String.isBlank(key2) && key2 != null)
            roleDetails = [Select ID,Name,Engagement_name__r.name,Start_date__c,Volunteer_Impact__c,Relationship_Manager_Formula__c,VM_Conflicts_Specific_To_The_Role__c,Conflicts_Specific_To_The_Role__c from Role__c where ID =:key2];
          if(!roleDetails.isEmpty())
            rolelst = roleDetails[0];
        
        //Initiate Job application
        
        job=new Job_Application__c();
        
        //multi select picklist code
        if(job != null){
            url_contactName = job.Contact__c;
            url_futureRole =  job.Position__c;
            url_recordType = job.RecordTypeId;
            
        }
        CompetenciesvalueList = new SelectOption[0];
        Competenciesvalues = new String[0];
        IndustryvalueList = new SelectOption[0];
        Industryvalues = new String[0];
        RollesvalueList = new SelectOption[0];
        Rollesvalues = new String[0];
        industriesmultiselectvalueList = new SelectOption[0];
        industriesmultiselectvalues = new String[0];
        rolesmultiselectvalueList = new SelectOption[0];
        rolesmultiselectvalues = new String[0];
        everServedmultiselectvalueList = new SelectOption[0];
        everServedmultiselectvalues = new String[0];
        
        Schema.DescribeFieldResult fieldResult = Job_Application__c.Select_all_competencies__c.getDescribe();
        Schema.DescribeFieldResult fieldResult1 = Job_Application__c.In_which_of_the_following_industry_areas__c.getDescribe();
        Schema.DescribeFieldResult fieldResult2 = Job_Application__c.Current_or_previous_roles_have_you__c.getDescribe();
        Schema.DescribeFieldResult fieldResult3 = Job_Application__c.Have_you_ever_served__c.getDescribe();
        Schema.DescribeFieldResult fieldResult4 = Job_Application__c.In_which_of_the_following_industry_areas__c.getDescribe();
        Schema.DescribeFieldResult fieldResult5 = Job_Application__c.Current_or_previous_roles_have_you__c.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Schema.PicklistEntry> ple1 = fieldResult1.getPicklistValues();
        List<Schema.PicklistEntry> ple2 = fieldResult2.getPicklistValues();
        List<Schema.PicklistEntry> ple3 = fieldResult3.getPicklistValues();
        List<Schema.PicklistEntry> ple4 = fieldResult4.getPicklistValues();
        List<Schema.PicklistEntry> ple5 = fieldResult5.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            Competenciesvaluelist.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        for( Schema.PicklistEntry f : ple1)
        {
            industriesmultiselectvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        for( Schema.PicklistEntry f : ple2)
        {
            rolesmultiselectvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        for( Schema.PicklistEntry f : ple3)
        {
            everServedmultiselectvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        for( Schema.PicklistEntry f : ple4)
        {
            IndustryvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
        for( Schema.PicklistEntry f : ple5)
        {
            RollesvalueList.add(new SelectOption(f.getLabel(), f.getValue()));
        }
    }
    
    @AuraEnabled
    public static String getParams(String urlparam1, String urlparam2, String urlparam3){ 
        recordtypew=urlparam3; 
        roleIdw=urlparam2;
        contactIdw=urlparam1;
        return recordtypew;
    }
    
    public string getOutputText() {
        return String.join(Competenciesvalues, ';');
    }
    public string getIndustriesText() {
        return String.join(industriesmultiselectvalues, ';');
    }
    public string getRolesText() {
        return String.join(rolesmultiselectvalues, ';');
    }
    public string getEverServedText() {
        return String.join(everServedmultiselectvalues, ';');
    }
    
    //Get questions based on RecordType
    @TestVisible
    public List<String> getQuestionsForRecordType(){
        templateString = new List<String>();
        
        String recordTypew=null;
        if(key3=='CIPMCommittee'){
            recordTypew='CIPMCommittee__c';
        } else if(key3=='CMPCCommittee'){
            recordTypew='CMPCCommittee__c';
        } else if(key3=='Advisory Council'|| key3=='Advisory+Council'){
            recordTypew='Exam_Related_Advisor__c';
        } else if(key3=='Default'){
            recordTypew='Micro__c';
            
        } else if(key3=='DRCCommittee'){
            recordTypew='DRCCommittee__c';
            
        } else if(key3=='EACCommittee'){
            recordTypew='EAC__c';
            
        } else if(key3=='Exam Related Advisor' || key3=='Exam+Related+Advisor'){
            recordTypew='Exam_Related_Advisor__c';
            
        } else if(key3=='GIPSCommittee'){
            recordTypew='GIPS__c';
            
        } else if(key3=='MemberMicro'){
            recordTypew='Micro__c';
            
        } else if(key3=='SPCCommittee'){
            recordTypew='SPC__c';
        }else if(key3=='Non+Exam+related+Advisory+council'||key3=='Non Exam related Advisory Council'||key3=='Non%20Exam%20related%20Advisory%20council'){
            recordTypew='Non_Exam_Related_Advisor__c';
        }else{
            recordTypew='Micro__c';
        }
        templateString.add(recordTypew);
        mapKey = key3;
        
        String query='Select id,Question__c from VM_QuestionsVisibility__c where '+recordTypew+'=true';
        List<VM_QuestionsVisibility__c> listObjVM_QuestionsVisibility=Database.query(query);
        List<String> listQuestions=new List<String>(); 
        if(listObjVM_QuestionsVisibility!=null && listObjVM_QuestionsVisibility.size()>0){
            for(VM_QuestionsVisibility__c objVM_QuestionsVisibility :listObjVM_QuestionsVisibility){
                listQuestions.add(objVM_QuestionsVisibility.Question__c);
            }
        }
        return listQuestions;
    }
    
    @TestVisible
    private void fillInErrorMessage(String msg){
        errorMessage = msg;
    }
 
//User pressed continue button
    @TestVisible
    public PageReference saveandcontinue(){
        PageReference pg;
         
/* /* Removed as per discussion on 05/14/2019 with Leah
            if(string.isBlank(Agree_to_the_Conflict_Policy)){
                att1 = new attachment();
                att2 = new attachment();
                att3 = new attachment();
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please accept or decline to the Conflicts of Interest Policy'));
                return null;
            }
            
            if(string.isBlank(disclosed_conflicts)){
                att1 = new attachment();
                att2 = new attachment();
                att3 = new attachment();
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please accept or decline to the Role Description'));
                return null;
            } 

*/

//Call method to update fields
            updateFields();

/*/* Removed as per discussion on 05/14/2019 with Leah
//check for attachments if they are valid format
            String aType =checkAttachmentType();
            If (aType=='Invalid'){
                return null;
            }
*/
/* Removed as per discussion on 05/14/2019 with Leah
//check for Agreements
            if(job.I_Agree_to_the_Conflict_Policy__c==false||job.I_have_read_the_disclosed_conflicts__c==false){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Acceptance of the CFA Institute Conflicts Policy and role description are required.</b></font>'));
                return null;
            } 
*/            
//Check for Conflicts of intrest questions
            if(job.Inappropriate_influence__c=='Yes'|| job.Personal_gain_or_benefit__c=='Yes' || job.Relationship_to_Prep_Provider__c=='Yes'||job.Relationship_with_Testing_Organization__c=='Yes'||job.Relationship_with_Candidate__c=='Yes'){
                att1 = new attachment();
                att2 = new attachment();
                att3 = new attachment();
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Please contact the Relationship Manager regarding potential conflicts</b></font> '+roledetails[0].Relationship_Manager_Formula__c));
                return null;
            }
        
 //Insert application to DB           
         String insertSuccess= insertApplication();
        If(insertSuccess=='Success'){   
            pg = new PageReference(Label.VM_CommunityHomepage+'job-application/'+job.id);
            return pg;   
        }else{
            return null;
        }

    }
    
    
    //User pressed on Save For Later button
    public PageReference saveforlater(){       
        PageReference pg;
        isSave = true;

//Update fields
            updateFields();
//Check for Conflicts of intrest questions
            if(job.Inappropriate_influence__c=='Yes'|| job.Personal_gain_or_benefit__c=='Yes' || job.Relationship_to_Prep_Provider__c=='Yes'||job.Relationship_with_Testing_Organization__c=='Yes'||job.Relationship_with_Candidate__c=='Yes'){
                att1 = new attachment();
                att2 = new attachment();
                att3 = new attachment();
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Please contact the Relationship Manager regarding potential conflicts</b></font> '+roledetails[0].Relationship_Manager_Formula__c));
                return null;
            }
//check for attachment format
            String aType =checkAttachmentType();
            If (aType=='Invalid'){
                return null;
            }
//insert application
        String insertSuccess= insertApplication();    
        If(insertSuccess=='Success'){
            pg = new PageReference(Label.VM_CommunityHomepage);
            pg.getParameters().put('message', 'Your Application has been saved!');
            pg.setRedirect(true);
            return pg;    
        }
        else{
            return null;
        }
    }

//This method updates fields to be save in DB
   
    Public void updateFields(){
        job.Position__c=key2;
            job.Contact__c=key1;
            job.Select_all_competencies__c= getOutputText();
            job.Have_you_ever_served__c=getEverServedText();
            job.In_which_of_the_following_industry_areas__c=getIndustriesText();
            job.Current_or_previous_roles_have_you__c=getRolesText();
            if(!String.isBlank(key3) && key3.contains('&')){
                String recordTypeIdd;
                Integer equalIndex1=key3.indexOf('&');
                String urlStringf = key3.subString(0,equalIndex1);
                if(!String.isBlank(urlStringf) && urlStringf.contains('%20'))
                    urlStringf = urlStringf.replace('%20', ' ');
                
                if(!String.isBlank(urlStringf)){
                    if(Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get(urlStringf).getRecordTypeId()!=null)
                        recordTypeIdd =Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get(urlStringf).getRecordTypeId();
                    else
                        recordTypeIdd =Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
                }
                job.recordTypeId=recordTypeIdd;
            }else{
                String oldrecordType;
                oldrecordType = Schema.SObjectType.Job_Application__c.getRecordTypeInfosByName().get(key3).getRecordTypeId();
                job.recordTypeId=oldrecordType;
            }
            if(outreach1 != null)
                job.Are_you_willing_to_engage_in_outreachSPC__c = outreach1;
            else job.Are_you_willing_to_engage_in_outreachSPC__c = 'Not Answered';
            if(commitmentCPMC != null)
                job.VM_time_commitmentCPMC__c = Boolean.valueOf(commitmentCPMC);
            if(dispactionimp != null)
                job.Disciplinary_Action_Imposed__c = dispactionimp;
            else job.Disciplinary_Action_Imposed__c = 'Not Answered';
            if(thistimecommitmentDRC != null)
                job.Can_you_fulfill_this_time_commitment_DRC__c = thistimecommitmentDRC;
            else job.Can_you_fulfill_this_time_commitment_DRC__c = 'Not Answered';
            if(thistimecommitmentSPC != null)
                job.Can_you_fulfill_this_time_commitmentSPC__c = thistimecommitmentSPC;
            else   job.Can_you_fulfill_this_time_commitmentSPC__c = 'Not Answered';
            if(employeersupport != null)
                job.Do_you_have_employer_support__c = employeersupport;
            else  job.Do_you_have_employer_support__c = 'Not Answered'; 
            if(doyouworkinindustry != null) 
                job.Do_you_work_in_this_industry__c = doyouworkinindustry;
            else  job.Do_you_work_in_this_industry__c = 'Not Answered';
            if(othertestingGroups != null)
                job.Experience_with_other_testing_groups__c = othertestingGroups;
            else  job.Experience_with_other_testing_groups__c = 'Not Answered';
            if(cfainstCode != null)    
                job.Familiar_with_the_CFA_Institute_Code__c = cfainstCode;
            else  job.Familiar_with_the_CFA_Institute_Code__c = 'Not Answered';
            if(rulesandProcedure != null)
                job.Familiar_with_the_Rules_and_Procedure__c = rulesandProcedure;
            else  job.Familiar_with_the_Rules_and_Procedure__c = 'Not Answered'; 
            if(CFAexamcurricula != null)
                job.Have_you_overseen_CFA_exam_curricula__c = CFAexamcurricula;
            else job.Have_you_overseen_CFA_exam_curricula__c = 'Not Answered';
            if(relatedTopics != null)
                job.Have_you_published_on_related_topics__c = relatedTopics;
            else job.Have_you_published_on_related_topics__c = 'Not Answered';
            if(related_topics1 != null)
                job.Have_you_published_on_related_topics1__c = related_topics1;
            else job.Have_you_published_on_related_topics1__c ='Not Answered';
            if(Volunteered_before != null)
                job.Volunteered_before__c = Volunteered_before;
            else job.Volunteered_before__c = 'Not Answered';
            if(teachng_of_CFA != null)
                job.Involved_with_development_teachng_of_CFA__c = Boolean.valueOf(teachng_of_CFA);
            if(gain_or_benefit != null)
                job.Personal_gain_or_benefit__c = gain_or_benefit;
            else job.Personal_gain_or_benefit__c = 'Not Answered';
            
            if(Inappropriate_influence != null)
                job.Inappropriate_influence__c = Inappropriate_influence;
            else job.Inappropriate_influence__c = 'Not Answered';          
            if(Prep_Provider != null)
                job.Relationship_to_Prep_Provider__c = Prep_Provider;
            else job.Relationship_to_Prep_Provider__c = 'Not Answered';
            
            if(Relationship_with_Candidate != null)
                job.Relationship_with_Candidate__c = Relationship_with_Candidate;
            else job.Relationship_with_Candidate__c = 'Not Answered';
            
            if(Relationship_with_Testing != null)
                job.Relationship_with_Testing_Organization__c = Relationship_with_Testing;
            else job.Relationship_with_Testing_Organization__c = 'Not Answered';
            if(Agree_to_the_Conflict_Policy != null)
                job.I_Agree_to_the_Conflict_Policy__c = Boolean.valueOf(Agree_to_the_Conflict_Policy);
            if(disclosed_conflicts != null)
                job.I_have_read_the_disclosed_conflicts__c = Boolean.valueOf(disclosed_conflicts);
            if(cfaDevelopCurricula != null)    
                job.Have_you_developed_the_CFA_curricula__c = cfaDevelopCurricula;
            else job.Have_you_developed_the_CFA_curricula__c = 'Not Answered';
            if(volTimeCommitDRC != null)    
                job.Volunteer_time_commitment_DRC__c = volTimeCommitDRC;
            else job.Volunteer_time_commitment_DRC__c = 'Not Answered';
            
            //Update Preferred Email address
            if( !String.isBlank(con.MDM_Preferred_Email_for_Volunteering__c)){  
                job.VM_Preffered_Email__c = con.MDM_Preferred_Email_for_Volunteering__c;           
                
            }
        if(isSave) {
            job.Status__c = 'Incomplete';
        }else {
            job.Status__c = 'In Progress';
        }
    }
//This methos checks the attachment format    
    Public String checkAttachmentType(){
                 //check for attachment extension

        if(att1.body!=null){
if(att1.Name.toUpperCase().endsWith('.PDF') || att1.Name.toUpperCase().endsWith('.JPG') || att1.Name.toUpperCase().endsWith('.DOCX')||att1.Name.toUpperCase().endsWith('.DOC')||att1.Name.toUpperCase().endsWith('.JPEG')){
                       AttachmentType1 = ValidAttType;
                   }else{
                     AttachmentType1 = InvAttType;
                   }
            }
        if(att2.body!=null){
                if(att2.Name.toUpperCase().endsWith('.PDF') || att2.Name.toUpperCase().endsWith('.JPG') || att2.Name.toUpperCase().endsWith('.DOCX')||att2.Name.toUpperCase().endsWith('.DOC')||att2.Name.toUpperCase().endsWith('.JPEG')){
                    AttachmentType2 = ValidAttType;

                   }else{
                     AttachmentType2 = InvAttType;
                   }
            }

                    if(att3.body!=null){
                if(att3.Name.toUpperCase().endsWith('.PDF') || att3.Name.toUpperCase().endsWith('.JPG') || att3.Name.toUpperCase().endsWith('.DOCX')||att3.Name.toUpperCase().endsWith('.DOC')||att3.Name.toUpperCase().endsWith('.JPEG')){
                       AttachmentType3= ValidAttType;

                   }else{
                       AttachmentType3 = InvAttType;
                   }
            }
                    If (AttachmentType1 == InvAttType||AttachmentType2 == InvAttType||AttachmentType3 == InvAttType){
                       att1 = new attachment();
                       att2 = new attachment();
                       att3 = new attachment();
                       Apexpages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>We only accept the following file types: .pdf/.jpeg/.jpg/.doc/.docx</b></font>'));
                    AttachmentType = InvAttType;
                    }

        Return AttachmentType;
    }
 //This method saves the application, Attachments and updates contact and  to DB   
    Public String insertApplication(){
        String iSuccess;
        try{
        
            Database.SaveResult sr = Database.insert(job, false);
            if(!sr.isSuccess()){
                list<Database.Error> err = sr.getErrors();
                att1 = new attachment();
                att2 = new attachment();
                att3 = new attachment();
                Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, '<font color="red"><b> '+err[0].getMessage()+'</b></font>'));
                return null;
            }
            
            //Update Contact
            //commenting the contact update as Round RObin service is not yet active
            //update con;
            //Save attachments
            List<attachment> attList = new List<attachment>();
            if(att1.body!=null){
                att1.parentID=job.ID;
                attList.add(att1);
            }
            if(att2.body!=null){
                att2.parentID=job.ID;
                attList.add(att2);
            }
            if(att3.body!=null){
                att3.parentID=job.ID;
                attList.add(att3);
            }
            insert attList;
            iSuccess = 'Success';
            }catch(System.TypeException e1){
            att1 = new attachment();
            att2 = new attachment();
            att3 = new attachment();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> fields... '+e1.getDmlFieldNames(0)+'this  '+e1.getDmlFields(0)+'</b></font>'));
            return null;
        }
        
        catch(System.DMLException e){
            att1 = new attachment();
            att2 = new attachment();
            att3 = new attachment();
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> '+e.getDmlMessage(0)+e.getDmlFieldNames(0)+'this  '+e.getDmlFields(0)+'</b></font>'));
            return null;
            
        }
        return iSuccess;    
    }

   
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('True','Yes')); 
        options.add(new SelectOption('False','No')); 
        return options; 
    }
    
    public List<SelectOption> getItems1() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    }
    
    @AuraEnabled
    public static String getPolicy(){
        Document dr=[Select ID from Document where Name='Volunteer Acknowledgement Form'];
        
        String Customlabel = Label.VM_Policydocument;
        String myurl= Customlabel+'/servlet/servlet.FileDownload?file='+dr.id;
        return myurl;
        
    }
}