public class VM_JobApplicationController_Detail {
   
    public string strCharLimit{get;set;}
    public String key1=null;
    public String PolicyDocumentURl {get;set;}
    public Integer numStr{get; set;}
    public Boolean showCongratsData { get; set; }
    public Boolean showApplicationData { get; set; }
    public Boolean submittedApplication {get; set;}
    public SelectOption[] CompetenciesvalueList { get; set; }
    //Attachment
    public Job_Application__c webContact{get;set;}
        public Attachment att1{get; set;}
    public Attachment att2{get; set;}
    public Attachment att3{get; set;}
    public Attachment attach = new Attachment();
    public Attachment attach1 = new Attachment();
    public Attachment attach2 = new Attachment();
    transient public Blob bl {get; set;} 
    transient public Blob bl1 {get; set;} 
    transient public Blob bl2 {get; set;} 
    //public String contentType {get; set;} 
    public String fileName {get; set;} 
    public String fileName1 {get; set;} 
    public String fileName2 {get; set;}
    public String relatedTopics{get; set;}
    public String Agree_to_the_Conflict_Policy{get; set;}
    public String disclosed_conflicts{get; set;}
    //below line added by lakshman
    public list<Attachment> lstAtachment{get;set;}
    //Attachment end
    
    public Job_Application__c job{get; set;}
    
    public Contact con{get; set;}
    
    public String VmAgreementUrl{get;set;}
    public boolean ShowNote{get;set;}
    public VM_JobApplicationController_Detail(ApexPages.StandardController controller){
        ShowNote = false;
        strCharLimit ='Each text box is limited to 255 characters.';
        String url= ApexPages.currentPage().getHeaders().get('referer'); 
        att1 = new Attachment();
        att2 = new Attachment();
        att3 = new Attachment();
        
        //below line added by lakshman
        lstAtachment = new list<Attachment> ();
        
        Document dr=[Select ID from Document where Name='Volunteer Acknowledgement Form'];
        PolicyDocumentURl= Label.VM_Policydocument+'/servlet/servlet.FileDownload?file='+dr.id;
        VmAgreementUrl= System.Label.VM_Agreement_URL;
        
        
        CompetenciesvalueList = new SelectOption[0];
        Schema.DescribeFieldResult fieldResult = Job_Application__c.Select_all_competencies__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            Competenciesvaluelist.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        Integer objIndex;
        if(!String.isBlank(url)){
            objIndex=url.indexOf('job-application');
            objIndex+=16;

            String relativeURL11=url.substring(objIndex);

            key1=relativeURL11.substring(0,15);

        }
        showCongratsData=false;
        showApplicationData=true;
      //Attachment
        this.webContact = (Job_Application__c)controller.getRecord();
      //Attachment end  
        try{
        job=[select Professional_experience_in_investment__c,VM_Conflict_Filter__c, Country__c,Degree1__c, Degree2__c, Degree3__c,I_Agree_to_the_Conflict_Policy__c,
             If_Other__c,
             If_yes_please_explain_Some_examples__c,
             How_long_did_you_worked_in_this_industry__c,I_have_read_the_disclosed_conflicts__c,
             Professional_ethical_situation_encounter__c,
             VMList_your_professional_accomplishments__c,
             CMPC_Engage_in_outreach__c,
             VMIf_yes_discuss_a_recent_article__c,
VMHave_your_presented_on_these_topics__c,
Describe_other_testing_experience__c,
Describe_your_areas_of_expertise__c,             
Describe_your_career_development_action__c,
Describe_your_qualifying_attributes__c,
Address1__c,VM_Job_Title__c,Volunteer_Employer_Type__c,
             status__c,
Designations__c,
             Disciplinary_action_Explain__c,
             Contact__c,
Disciplinary_Action_Imposed__c,
Do_you_have_employer_support__c,
Do_you_work_in_this_industry__c,
Email1__c,
Email2__c,
Email3__c,
Engagement_Name__c,Position__c,Position__r.name,Position__r.Engagement_name__r.name,
Experience_with_other_testing_groups__c,
Familiar_with_the_CFA_Institute_Code__c,
Address2__c,
Familiar_with_the_Rules_and_Procedure__c,
Have_you_developed_the_CFA_curricula__c,
Have_you_overseen_CFA_exam_curricula__c,
Have_you_presented_on_these_topics__c,
Have_you_published_on_related_topics__c,
Have_you_published_on_related_topics1__c,
Have_you_ever_served__c,VM_Preffered_Email__c,
Volunteer_time_commitment_DRC__c,
Have_you_worked_with_diverse_groups__c,
How_can_you_contribute_to_this_role__c,
Address3__c,
Howdidyouhearaboutthisrole__c,
How_long_have_you_worked_in_the_industry__c,
If_YES_list_experiences__c,
Describe_your_role_experience_If_Yes__c,
If_YES_discuss_a_recent_article__c,
If_YES_list_years_types_of_experience__c,
If_Yes_please_explain_CMPC__c,
If_Yes_please_explain__c,
In_what_roles__c,
Inappropriate_influence__c,
Involved_with_development_teachng_of_CFA__c,
Other_potential_conflicts__c,
Personal_gain_or_benefit__c,
Phone__c,
Phone1__c,
Phone2__c,
Phone3__c,
Email__c,
Published_any_articles_or_proceedings__c,
VMWhy_are_you_best_for_this_role__c,
Are_you_willing_to_engage_in_outreachSPC__c,
Professional_accomplishments__c,
Professional_Affiliations__c,
Volunteered_before__c,
Reference_Name1__c,
Reference_Name2__c,
Reference_Name3__c,
Relationship_to_Prep_Provider__c,
Relationship_with_Candidate__c,
Relationship_with_Testing_Organization__c,
VM_time_commitmentCPMC__c,
Role_Type__c,Select_all_competencies__c,Volunteer_Employer__c,Volunteer_Impact__c,Volunteer_Title__c,
             
             What_interests_you_about_this_role__c,
             In_which_of_the_following_industry_areas__c,Current_or_previous_roles_have_you__c,Can_you_fulfill_this_time_commitment_DRC__c,
             Years_in_the_industry__c,Can_you_fulfill_this_time_commitmentSPC__c,RecordTypeID,RecordType.name,Position__r.Start_date__c,Contact__r.FirstName,Contact__r.Lastname,Contact__r.MDM_Job_Title__c,Contact__r.MiddleName,Contact__r.MailingState,Contact__r.Mailingpostalcode,Contact__r.CFAMN__Employer__c,Position__r.VM_Conflicts_Specific_To_The_Role__c,Contact__r.MailingStreet,Contact__r.MailingCity,Contact__r.MailingCountry,Contact__r.Phone,Contact__r.MDM_Preferred_Email_for_Volunteering__c,Contact__r.Email,
             VM_Describe_professional_accomplishments__c,
             VMIfyesdemanding_volunteer_projects__c, 
             Why_are_you_best_for_this_role__c from Job_application__c where id=:key1];

        // below code added by lakshman
        if(job.id != null)
        {
            lstAtachment =[select id,name from attachment where parentID=:job.id];
            
            if(lstAtachment != null && lstAtachment.size()>0)
            {
                showCongratsData = true;
            }
        }

        if(!String.isBlank(job.RecordTypeID))
        {
                if(job.RecordType.name=='CIPMCommittee')
                        numStr = 1;
                else if(job.RecordType.name=='CMPCCommittee')
                    numStr = 2;
                else if(job.RecordType.name=='Advisory Council')
                    numStr = 3;
                else if(job.RecordType.name=='Default')
                    numStr = 4;
                else if(job.RecordType.name=='DRCCommittee')
                    numStr = 5;
                else if(job.RecordType.name=='EACCommittee')
                    numStr = 6;
                else if(job.RecordType.name=='Exam Related Advisor' || job.RecordType.name=='Exam+Related+Advisor')
                    numStr = 7;
                else if(job.RecordType.name=='GIPSCommittee')
                {    numStr = 8;
                    ShowNote=true;
                    strCharLimit ='Each text box is limited to 1000 characters.  It is important that your responses be brief and specific to the volunteer role.  You will have the opportunity to attach additional information, along with your required resume or CV, before submitting the application.';
                }
                else if(job.RecordType.name=='MemberMicro')
                    numStr = 9;
                else if(job.RecordType.name=='SPCCommittee')
                    numStr = 10;
                else if(job.RecordType.name=='Non Exam related Advisory council')
                    numStr = 11;
                else
                    numStr = 12;
            }
        } catch(Exception e){

        }
        try{
            con=[Select FirstName, Lastname,MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet, Email ,Account.name from Contact where id=:job.Contact__c];

            
        }catch(Exception e){

        }
        
        if(job.status__c=='Submitted'){
            submittedApplication=false;
        } else{
            submittedApplication=true;
        }
    }
    
    public List<String> getQuestionsForRecordType(){
        //String recordType=recordtypew;

        string lstjobapp=[select RecordTypeID from job_application__c where id =: key1 ].RecordTypeID;
        String recordTypename=[select Name from RecordType where Id =:lstjobapp].Name;
        String rs;
 
        if(recordTypename=='CIPMCommittee'){
            rs='CIPMCommittee__c';
        } else if(recordTypename=='CMPCCommittee'){
            rs='CMPCCommittee__c';
        } else if(recordTypename=='Advisory Council'){
            rs='Exam_Related_Advisor__c';
        } else if(recordTypename=='Default'){
            rs='Micro__c';

        } else if(recordTypename=='DRCCommittee'){
            rs='DRCCommittee__c';
        
        } else if(recordTypename=='EACCommittee'){
            rs='EAC__c';
        
        } else if(recordTypename=='Exam Related Advisor'){
            rs='Exam_Related_Advisor__c';
        
        } else if(recordTypename=='GIPSCommittee'){
            rs='GIPS__c';
        
        } else if(recordTypename=='MemberMicro'){
            rs='Micro__c';
        
        } else if(recordTypename=='SPCCommittee'){
            rs='SPC__c';
         }else if(recordTypename=='Non+Exam+related+Advisory+council'||recordTypename=='Non Exam related Advisory council'||recordTypename=='Non%20Exam%20related%20Advisory%20council'){
            rs='Non_Exam_Related_Advisor__c';
        }else{
            rs='Micro__c';
        }
         // String recordType='SPC__c';  
       //String recordType='CIPMCommittee__c';
       // String recordTypename=[select Name from RecordType where Id IN ([select RecordTypeID from job_application__c where id =:key1])];
        
        //String recordType='Non_Exam_Related_Advisor__c';
        String query='Select id,Question__c from VM_QuestionsVisibility__c where '+rs+'=true';
        List<VM_QuestionsVisibility__c> listObjVM_QuestionsVisibility=Database.query(query);
        List<String> listQuestions=new List<String>(); 
        if(listObjVM_QuestionsVisibility!=null && listObjVM_QuestionsVisibility.size()>0){
            for(VM_QuestionsVisibility__c objVM_QuestionsVisibility :listObjVM_QuestionsVisibility){
                listQuestions.add(objVM_QuestionsVisibility.Question__c);
            }
        }
        return listQuestions;
    }
    
    public PageReference redirectToEdit(){
        PageReference pg;
        pg = new PageReference(Label.VM_CommunityHomepage+'edit-application?key='+job.id);
       
        return pg;   
    
    }
    
    public PageReference submitApplication(){
              PageReference pg;
    
     if(Agree_to_the_Conflict_Policy != null)
     {           
         job.I_Agree_to_the_Conflict_Policy__c = Boolean.valueOf(Agree_to_the_Conflict_Policy);
     }
     else
     {
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Please accept or decline to the Conflicts of Interest Policy</b></font>'));
                    return null;    
     }        
     if(disclosed_conflicts != null)
     {
          job.I_have_read_the_disclosed_conflicts__c = Boolean.valueOf(disclosed_conflicts);
     }
     else
     {
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Please accept or decline to the Role Description</b></font>'));
                    return null;
     }
     

/*       
          //Attachment
        attach.OwnerId = UserInfo.getUserId();
        attach.ParentId = webContact.Id;
        attach.Name = fileName;
        attach.Body = bl;
        attach.ContentType = contentType;

        
        attach1.OwnerId = UserInfo.getUserId();
        attach1.ParentId = webContact.Id;
        attach1.Name = fileName1;
        attach1.Body = bl1;
        attach1.ContentType = contentType;

        
        attach2.OwnerId = UserInfo.getUserId();
        attach2.ParentId = webContact.Id;
        attach2.Name = fileName2;
        attach2.Body = bl2;
        attach2.ContentType = contentType;

  //Attachment end
 */
        
        try{
            //Attachment 
            
            if(job.I_Agree_to_the_Conflict_Policy__c==false||job.I_have_read_the_disclosed_conflicts__c==false){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Acceptance of the CFA Institute Conflicts Policy and role description are required.</b></font>'));
                    return null;
            }
 /*           if(job.I_have_read_the_disclosed_conflicts__c==false){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b>Please read and understand the disclosed conflicts.</b></font>'));
                    return null;
            }*/
            
   //New attachment code , need to be removed
             
          
            if(att1.body!=null){  
if(att1.Name.toUpperCase().endsWith('.PDF') || att1.Name.toUpperCase().endsWith('.JPG') || att1.Name.toUpperCase().endsWith('.DOCX')||att1.Name.toUpperCase().endsWith('.DOC')||att1.Name.toUpperCase().endsWith('.JPEG')){
                if(!(att1.Body.size() < 5100000))
                {
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> The file size should be less than 5MB!</b></font>'));
                    return null;      
                    
                 }

               
            }
            else
            {
                
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> We only accept the following file types: .pdf/.jpeg/.jpg/.doc/.docx</b></font>'));
                return null;
            }
            //attach1
          }
            if(att2.body!=null){
            if(att2.Name.toUpperCase().endsWith('.PDF') || att2.Name.toUpperCase().endsWith('.JPG') || att2.Name.toUpperCase().endsWith('.DOCX')||att2.Name.toUpperCase().endsWith('.DOC')||att2.Name.toUpperCase().endsWith('.JPEG')){

                if(!(att2.Body.size() < 5100000))
                {
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> The file size should be less than 5MB!</b></font>'));
                    return null;      
                }
            }
else
            {
                
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> We only accept the following file types: .pdf/.jpeg/.jpg/.doc/.docx</b></font>'));
                return null;
            }
            //attach2
          }
            
            //attach 2
            if(att3.body!=null){
            if(att3.Name.toUpperCase().endsWith('.PDF') || att3.Name.toUpperCase().endsWith('.JPG') || att3.Name.toUpperCase().endsWith('.DOCX')||att3.Name.toUpperCase().endsWith('.DOC')||att3.Name.toUpperCase().endsWith('.JPEG')){
                if(!(att3.Body.size() < 5100000))
                {
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> The file size should be less than 5MB!</b></font>'));
                    return null;      
                }
                
            }
else
            {
                
                att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> We only accept the following file types: .pdf/.jpeg/.jpg/.doc/.docx</b></font>'));
                return null;
            }
            //attach3
          }

            //end of new attachment code         
  
/*          
            if(attach.body!=null){  
if(attach.Name.toUpperCase().endsWith('.PDF') || attach.Name.toUpperCase().endsWith('.JPG') || attach.Name.toUpperCase().endsWith('.DOCX')||attach.Name.toUpperCase().endsWith('.DOC')||attach.Name.toUpperCase().endsWith('.JPEG')){
                if(!(attach.Body.size() < 5100000))
                {
                attach = new Attachment();
                attach1 = new Attachment();
                attach2 = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> The file size should be less than 5MB!</b></font>'));
                    return null;      
                    
                 }

               insert attach;
               
            }
            else
            {
                
                            attach = new Attachment();
            attach1 = new Attachment();
            attach2 = new Attachment();ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> We only accept the following file types: .pdf/.jpeg/.jpg/.doc/.docx</b></font>'));
                return null;
            }
            //attach1
          }
            if(attach1.body!=null){
            if(attach1.Name.toUpperCase().endsWith('.PDF') || attach1.Name.toUpperCase().endsWith('.JPG') || attach1.Name.toUpperCase().endsWith('.DOCX')||attach1.Name.toUpperCase().endsWith('.DOC')||attach1.Name.toUpperCase().endsWith('.JPEG')){

                if(!(attach1.Body.size() < 5100000))
                {
                                attach = new Attachment();
            attach1 = new Attachment();
            attach2 = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> The file size should be less than 5MB!</b></font>'));
                    return null;      
                }


                        
                insert attach1;
            }
                            attach = new Attachment();
            attach1 = new Attachment();
            attach2 = new Attachment();
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> We only accept the following file types: .pdf/.jpeg/.jpg/.doc/.docx</b></font>'));
                return null;
            }
            
            //attach 2
            if(attach2.body!=null){
            if(attach2.Name.toUpperCase().endsWith('.PDF') || attach2.Name.toUpperCase().endsWith('.JPG') || attach2.Name.toUpperCase().endsWith('.DOCX')||attach2.Name.toUpperCase().endsWith('.DOC')||attach2.Name.toUpperCase().endsWith('.JPEG')){
                if(!(attach2.Body.size() < 5100000))
                {
                    attach = new Attachment();
                    attach1 = new Attachment();
                    attach2 = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> The file size should be less than 5MB!</b></font>'));
                    return null;      
                }

                insert attach2;
            }
            attach = new Attachment();
            attach1 = new Attachment();
            attach2 = new Attachment();
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'<font color="red"><b> We only accept the following file types: .pdf/.jpeg/.jpg/.doc/.docx</b></font>'));
                return null;
            }
 */           
            //Attachment End
            job.Status__c='Submitted';
            job.Submitted__c=true;
            update job;

          List<attachment> attList = new List<attachment>();
            if(att1.body!=null){
                if(att1.parentID==null){
                  att1.parentID=job.ID;
                }
                  attList.add(att1);
           }
          if(att2.body!=null){
              if(att2.parentID==null){
                  att2.parentID=job.ID;
              }
                  attList.add(att2);
          }
          if(att3.body!=null){
              if(att3.parentID==null){
                  att3.parentID=job.ID;
              }
                  attList.add(att3);
          }
        insert attList;
        lstAtachment.addAll(attList);

            if(showApplicationData){
                
            showApplicationData=false;
            }
            showCongratsData=true;
        }catch(System.DMLException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Following Errors Found'+e.getDmlMessage(0)));    
            //fillInErrorMessage(e.getDMLMessage(0));

            return null;
        }       
        finally
        {
 //           attach = new Attachment();
   //         attach1 = new Attachment();
    //        attach2 = new Attachment();
                 att1 = new Attachment();
                att2 = new Attachment();
                att3 = new Attachment();           
        }
        
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'<b>Your application has been submitted successfully.</b>'));
        return null;
        //attach end;
        
        
        /*try {
        insert attach;
        } catch (DMLException e) {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
        return null;
        } finally {
        attach = new Attachment(); 
        }*/
        //Attachment end
  
        /*
        pg = new PageReference('https://devcfacogn-cfainstitute.cs91.force.com/CFAVolunteer/s/job-application/'+job.id+'/app0303');
        pg.getParameters().put('pageMsg', 'Your Application has been created!');
        pg.setRedirect(true);
        return pg;*/
        
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('True','Yes')); 
        options.add(new SelectOption('False','No')); 
        return options; 
    }
        public List<SelectOption> getItems1() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('True','Yes')); 
        options.add(new SelectOption('False','No')); 
        return options; 
    }
}