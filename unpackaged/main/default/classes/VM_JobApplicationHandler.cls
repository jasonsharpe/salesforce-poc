public class VM_JobApplicationHandler{
    
    public void OnAfterUpdate(List<Job_Application__c> oldjobLst,List<Job_Application__c> newjobLst,Map<Id,Job_Application__c> oldjobMap)
    {
         List<Engagement_Volunteer__c> engVolLst=new List<Engagement_Volunteer__c>();
         //Map<Id,Role__c> roleMap=new Map<Id,Role__c>();
         List<Id> roleId=new List<Id>();
         List<Task> tskLst=new List<Task>();
         for(Job_Application__c j:newjobLst)
         {
             roleId.add(j.Position__c);
             
         }
         Map<Id,Role__c> roleMap=new Map<Id,Role__c>([select id,name,Engagement_name__r.Id,Engagement_name__r.Business_Owner__c,Engagement_name__r.name from Role__c where Id in:roleId]);
        
         for(Job_Application__c j:newjobLst)       
         {
            String oldstatus  = oldjobMap.get(j.Id).Status__c;    
            String newstatus = j.status__c;
            system.debug('old status'+oldstatus+'new status'+newstatus);
            if((oldjobMap.get(j.Id).Status__c!=j.status__c)&&(j.status__c=='Volunteer Accepted'))
            {
                //System.debug('ACCEPETD inseret');
                Engagement_Volunteer__c e = new Engagement_Volunteer__c(); 
                //e.Engagement__c = j.Position__r.Engagement_name__c;
                System.debug('Engagement Name@@'+j.Position__r.Engagement_name__r.Name);
                system.debug('another eng'+roleMap.get(j.Position__c).Engagement_name__r.Id);
                //System.debug('Engagement Id@@'+j.Position__r.Engagement_name__c);
                //System.debug('Role Id@@'+j.Position__c);
                //System.debug(j.Position__c);
                //e.Engagement__c = j.Position__r.Engagement_name__r.id;
                e.ownerid=roleMap.get(j.Position__c).Engagement_name__r.Business_Owner__c;
                e.Engagement__c = roleMap.get(j.Position__c).Engagement_name__r.Id;
                e.Role_del__c= j.Position__c;
                e.contact__c =j.contact__c;
                e.status__c= 'On-boarded';
                engVolLst.add(e);
                System.debug('List Value'+engVolLst);                             
                
                Task t = new Task();
                t.OwnerId = roleMap.get(j.Position__c).Engagement_name__r.Business_Owner__c;
                t.Subject = 'On-board applicant';
                t.Description = 'Volunteer has been on-Boarded for Role '+roleMap.get(j.Position__c).name+' on Engagement '+roleMap.get(j.Position__c).Engagement_name__r.name;
                t.Status = 'Not Started';
                t.Priority = 'Normal';
                
                t.ActivityDate=date.today().adddays(3);
                t.WhatId = roleMap.get(j.Position__c).Engagement_name__r.Id;                
                tskLst.add(t);
           }
            
         }
        Boolean isError = false;
        string errorMsg;
        if(!engVolLst.isEmpty())
        {
         Database.SaveResult[] srList = Database.insert(engVolLst, false);

            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted Engagement Volunteer. ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Engagement Volunteer fields that affected this error: ' + err.getFields());
                        isError = true;
                        errorMsg = err.getMessage();
                        //engVolLst[0].addError(err.getMessage());
                    }
                }
            }
          }
        if(isError == true){
            system.debug('@@@@ isError == True'+errorMsg);
            newjobLst[0].addError(errorMsg);
        }
         
         
         if(!tskLst.isEmpty())
        {
         Database.SaveResult[] srList = Database.insert(tskLst);

            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted Engagement Volunteer. ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Engagement Volunteer fields that affected this error: ' + err.getFields());
                    }
                }
            }
          }      
    }
    
    
    public void OnAfterDelete(List<Job_Application__c> oldjobLst,Map<Id,Job_Application__c> oldjobMap)
    {
        
        for(Job_Application__c job:oldjobLst)
        {
            
        
        }
    
    }
    
    
    
    
    
    
    
    
    
            
       
   }