@isTest
private class VM_JobApplicationHandler_Test {

    @testSetup static void setup(){
       
    
                          
    }
        
    public static testMethod void testMassRecognition() {
         //VM_TestDataFactory_Old o=new VM_TestDataFactory_Old();
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest5478@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest5478@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne5478@gmail.com','Huff','Outreach Management User','Relationship Manager');
        Engagement__c eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        try
        {  
            insert eng;
            
            system.debug('Successful Exe');
        }catch(DMLException ex)
        {
            system.debug('Npp eng'+ex.getDMLMessage(0));
        }
        Role__c role=VM_TestDataFactory.createRole(eng); 
        try
        {  
            insert role;
            
            system.debug('Successful Exe role');
        }catch(DMLException ex)
        {
            system.debug('Npp role '+ex.getDMLMessage(0));
        }
        Account acc=VM_TestDataFactory.createAccountRecord();
 
        Contact con=VM_TestDataFactory.createContactRecord(acc);

        
        Job_Application__c job=VM_TestDataFactory.createJobApplication(role,con); 
        try
        {  
            insert job;
            
            system.debug('Successful Exe job');
        }catch(DMLException ex)
        {
            system.debug('Npp job'+ex.getDMLMessage(0));
        }                                                          
        Test.startTest();
        try
        {  
            job.Status__c='Submitted';
            job.Submitted__c = true;
            update job;
            job.status__c='Volunteer Accepted';
            update job;
            
            system.debug('Successful Update job');
        }catch(DMLException ex)
        {
            system.debug('Npp job'+ex.getDMLMessage(0));
        }       
                           
        Test.stopTest();
    }
    
}