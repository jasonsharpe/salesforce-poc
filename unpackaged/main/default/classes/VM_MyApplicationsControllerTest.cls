/**************************************************************************************************************************************************
Name:  VM_MyApplicationsControllerTest
Copyright © 2018  ITC
=====================================================================
Purpose: 1. TestClass for VM_MyApplicationsController class                                                                                               
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0      Nikhil        06/11/2018     Created         Created the test class

****************************************************************************************************************************************************/

@isTest
public class VM_MyApplicationsControllerTest {

    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    
    @testsetup public static void setup(){
        
        
       /* 
        User RMOwner=VM_TestDataFactory.createUser('RMTest479@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest458@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne658@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='Test Data FY14';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        insert eng;
        Role__c test_role= VM_TestDataFactory.createRole(eng);*/
  
    }
    
    public static testMethod void testclass(){
        try{
        Account portalAccount = VM_TestDataFactory.createAccountRecord();
        portalAccount.IsPartner = true;
        update portalAccount;
        
        Contact portalContact = VM_TestDataFactory.createContactRecord(portalAccount);   
        system.debug('Contact'+portalContact);
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId(); 
        system.debug('partnerRecordTypeId' +partnerRecordTypeId);
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
       	User RMOwner=VM_TestDataFactory.createUser('RMTest4791@gmail.com','RMTest1','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest4581@gmail.com','MDTest1','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne6518@gmail.com','Huff1','Outreach Management User','Relationship Manager');
        String ename ='Test Data FY19';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(ename,RMOwner,MDUSer,ReportsToUSer) ;
        insert eng;
        Role__c test_role= VM_TestDataFactory.createRole(eng);
        //Contact objContact = [SELECT Id from Contact Where LastName='testLastName' LIMIT 1];
        //Contact objContact = [SELECT Id from Contact LIMIT 1];
        //Role__c objRole = [SELECT Id from Role__c Where Position_Title__c='test' LIMIT 1];
        //Role__c objRole = [SELECT Id from Role__c LIMIT 1];
        Job_Application__c test_JA = VM_TestDataFactory.createJobApplication(test_role, portalContact);
        //test_JA.Position__c=test_role.id;
        
        insert test_JA;
        Test.startTest();
        VM_MyApplicationsController.fetchJobApplications(1);
        system.assertEquals(1, [SELECT Id FROM Job_Application__c].size());
        Test.stopTest();
        }
    
        catch(exception e){
        
               
    }
}
}