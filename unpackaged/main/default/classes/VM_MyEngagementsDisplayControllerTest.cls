/**************************************************************************************************************************************************
Name:  VM_MyEngagementsDisplayControllerTest
Copyright © 2018  ITC
=====================================================================
Purpose: 1. TestClass for VM_MyEngagementsDisplayController class                                                                                               
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0                  06/08/2018        Created         Created the test class

****************************************************************************************************************************************************/
@isTest
public class VM_MyEngagementsDisplayControllerTest {

    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    
    @testsetup public static void setup(){
        ContactTriggerHandler.triggerDisabled = true;

        Account portalAccount = VM_TestDataFactory.createAccountRecord();
        // portalAccount.IsPartner = true;
        insert portalAccount;
        
        Contact portalContact = VM_TestDataFactory.createContactRecord(portalAccount);        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
    }
    
    public static testMethod void testclass(){
        try{
       User RMOwner=VM_TestDataFactory.createUser('RMTest471619@gmail.com','RMTest1','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest451981@gmail.com','MDTest1','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne617518@gmail.com','Huff1','Outreach Management User','Relationship Manager');
        String ename ='Test Data FY19';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        eng.Status__c= 'On-boarded';
        insert eng;
        Role__c objRole= VM_TestDataFactory.createRole(eng);
       //Role__c objRole = [SELECT Id from Role__c LIMIT 1];
        
        Contact objContact = [SELECT Id from Contact Where LastName='testLastName' LIMIT 1];
       
        //Engagement__c eng  = [SELECT Id FROM Engagement__c WHERE Name='Test Data FY19' LIMIT 1] ;
        List<Engagement_Volunteer__c> test_volunteers= VM_TestDataFactory.createEngagementVolunteers(eng, objRole, objContact, 1);
        
        insert test_volunteers;
        Test.startTest();
        VM_MyEngagementsDisplayController.fetchMyEngagementDetails(1);
        system.assertEquals(1, [SELECT Id FROM Engagement_Volunteer__c].size());
        Test.stopTest();
        
    }
    
    catch(exception e){
        
    }
    }
}