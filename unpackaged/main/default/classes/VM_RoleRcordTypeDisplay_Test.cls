@isTest
public class VM_RoleRcordTypeDisplay_Test {
	public static Account VM_Account;
    public static Contact VM_Contact;
    public static Engagement__c VM_Engage;
    public static Role__c VM_Role;
    public static Job_Application__c VM_Job;
    
   public static void prepareData(){
                     
       VM_Account = new Account();
        VM_Account.Name ='Test_VMAcc1';
        VM_Account.Phone = '222568974';
        insert VM_Account;
        
        VM_Contact = new Contact();
        //VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        insert VM_Contact;
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest7788@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest565@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne7787@gmail.com','Huff','Outreach Management User','Relationship Manager');
        
        Engagement__c VM_Engage=VM_TestDataFactory.createEngagement('t FY21',RMOwner,MDUSer,ReportsToUSer);
        insert VM_Engage;
        
        VM_Role=VM_TestDataFactory.createRole(VM_Engage);
        VM_Role.VM_JARecordTypeName__c='MemberMicro';
        upsert VM_Role;
        
        VM_Job = VM_TestDataFactory.createJobApplicationMicro(VM_Role,VM_Contact);
         VM_Job.Position__c=VM_Role.Id;
        Insert VM_Job;
        
    }
    public static testMethod void testCreate(){
       prepareData();
      ApexPages.StandardController controller;
      // Engagement__c VM_Engage = [select Id from Engagement__c where Name = 't FY21' LIMIT 1];
       // Role__c VM_Role = [select Id from Role__c where Position_Title__c='test' LIMIT 1];
       // Job_Application__c VM_Job = [select Id from Job_Application__c where RecordType.Name = 'CIPMCommittee' LIMIT 1];
        
        PageReference pgref = new PageReference('apex/VM_CreateRole?retURL=%2Fa2x%2Fo&save_new=1&sfdc.override=1');
        Test.setCurrentPageReference(pgref);
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(VM_Role);
        VM_RoleRecordTypeDisplay RTD = New VM_RoleRecordTypeDisplay(sc);
        RTD.getRecordType();
        RTD.getRecordTypes();
        //VMC.recordTypeSelection();
        RTD.dummy();
        RTD.markupJSON = '[{"Name":"Fields","Columns":"2","Collapsible":"false","field":[{"field":"Contact__c","required":"false"},{"field":"Engagement_Name__c","required":"false"},{"field":"Volunteer_Employer__c","required":"false"},{"field":"Position__c","required":"false"},{"field":"Volunteer_Employer_Type1__c","required":"false"},{"field":"Role_Start_Date__c","required":"false"},{"field":"VM_Job_Title__c","required":"false"},{"field":"Status__c","required":"false"},{"field":"Show_Application__c","required":"false"}]},{"Name":"Volunteer Personal Information","Columns":"2","Collapsible":"true","field":[{"field":"VM_Address__c","required":"false"},{"field":"Professional_Affiliations__c","required":"false"},{"field":"Phone__c","required":"false"},{"field":"VM_Email__c","required":"false"},{"field":"VM_Preffered_Email__c","required":"false"},{"field":"Select_all_competencies__c","required":"false"},{"field":"If_Other__c","required":"false"}]},{"Name":"Conflicts of interest : Select all applicable situations","Columns":"1","Collapsible":"true","field":[{"field":"Personal_gain_or_benefit__c","required":"false"},{"field":"Inappropriate_influence__c","required":"false"},{"field":"Other_potential_conflicts__c","required":"false"}]},{"Name":"Other Conflicts","Columns":"2","Collapsible":"true","field":[{"field":"I_Agree_to_the_Conflict_Policy__c","required":"false"},{"field":"I_have_read_the_disclosed_conflicts__c","required":"false"}]}]'; 
        RTD.getMarkup();
          
        Test.stopTest();
    }
 
}