/**************************************************************************************************************************************************
Name:  VM_RoleRecordTypeDisplay
Copyright © 2018  ITC
=====================================================================
Purpose: 1. Controller Class for VM_RoleRecordTypePreviewer.vfp                                                                                                
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0                  06/18/2018        Created         Created the class

****************************************************************************************************************************************************/

public class VM_RoleRecordTypeDisplay {


    private ApexPages.StandardController controller {get; set;}
    private Role__c objRole;
    public String recordTypeSelection {get;set;}
    public String markupJSON {get;set;}
    public Job_Application__c objJob {get;set;} 
    
    public VM_RoleRecordTypeDisplay(ApexPages.StandardController controller) {

        //initialize the stanrdard controller
        this.controller = controller;
        //this.objRole = (Role__c)controller.getRecord();
        //objJob = new Job_Application__c();
    }
    
    /****************************************************************************************************************************
       Purpose:This method will fetch the all redcord type from Job_Application__c object.                         
       Parameters: 
       Returns: 
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/13/2018             
    ******************************************************************************************************************************/ 
    public List<selectOption> getRecordTypes(){
        
        List<selectOption> lstRecordTypes = new List<selectOption>();  
        
        for(RecordType info : [Select Id, Name from RecordType where sobjectType='Job_Application__c']){
           lstRecordTypes.add(new SelectOption(info.Id, info.Name));
        }
        
        return lstRecordTypes;
    }
    
    /****************************************************************************************************************************
       Purpose:This method will fetch the all redcord type from Job_Application__c object.                         
       Parameters: 
       Returns: 
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/15/2018             
    ******************************************************************************************************************************/
    public String getRecordType() {
        
        List<RecordType> kstrRecord = [Select Id, Name from RecordType where sobjectType='Job_Application__c'];
        return JSON.serialize(kstrRecord);
    }
    
    public PageReference dummy() {
        return null;
    }
    
    /****************************************************************************************************************************
       Purpose:This method will create the markup of all fiels on Role Detail.                         
       Parameters: 
       Returns: 
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/13/2018             
    ******************************************************************************************************************************/
    public Component.Apex.PageBlock getMarkup() {
        objJob = new Job_Application__c();
        Component.Apex.PageBlock pgBlk = new Component.Apex.PageBlock();
        system.debug('=========1==========='+markupJSON);
        if(markupJSON != null && String.isNotBlank(markupJSON) ) {
        system.debug('=========2==========='+markupJSON);    
            List<markupCLass> lstMarkup = (List<markupCLass>)JSON.deserialize(markupJSON, List<markupCLass>.class);
            system.debug('========lstMarkup=============' + lstMarkup);
            
            for( markupCLass detail : lstMarkup ) {
                
                Component.Apex.PageBlockSection pgBlkSection = new Component.Apex.PageBlockSection();
                pgBlkSection.Collapsible = Boolean.valueOf( detail.Collapsible );
                pgBlkSection.columns = Integer.valueOf( detail.Columns );
                pgBlkSection.title = detail.name;
                
                for( fields fld : detail.field ) {
                    system.debug('===============3========='+fld.field);
                    if( fld.field == null ) {
                        Component.Apex.OutputPanel panel = new Component.Apex.OutputPanel();
                        pgBlkSection.childComponents.add(panel);
                    } else {
                        Component.Apex.OutputField fldOp = new Component.Apex.OutputField();
                        fldOp.expressions.value = '{!objJob.' + fld.field + '}';
                        pgBlkSection.childComponents.add(fldOp);
                    }
                }
                pgBlk.childComponents.add(pgBlkSection);    
            }
        }
        return pgBlk;
    } 
    
    public class markupCLass {
        public String name ;
        public String Columns;
        public String Collapsible;
        public List<fields> field ;
    }
    
    public Class fields {
        public String field;
        public String required;
    }
}