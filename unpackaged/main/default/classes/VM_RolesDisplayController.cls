/**************************************************************************************************************************************************
Name:  VM_RolesDisplayController
Copyright © 2018  ITC
=====================================================================
Purpose: 1. Controller Class for VM_RolesDisplay.cmp                                                                                                
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0                  06/07/2018        Created         Created the class

****************************************************************************************************************************************************/
public class VM_RolesDisplayController {
	
    /****************************************************************************************************************************
       Purpose:This method will fetch the details from Role__c based on All Available Roles filter.                         
       Parameters: noOfRecords
       Returns: No of records for the specific Role__c
    
       History                                                            
       --------                                                           
       VERSION  AUTHOR         DATE           DETAIL          Description
       1.0      Nikhil        06/07/2018             
    ******************************************************************************************************************************/ 

    @AuraEnabled
    public static List<Role__c> fetchRoleDetails(Integer noOfRecords) {
        
        List<Role__c> lstRoles =  [SELECT Id, Name, Recruiting_End_Date__c, 
                                   	Recruiting_Start_Date__c,Relationship_Manager_Email_a__c, 
                                   	Available_Positions__c, Show_Opportunity__c  
                                   	FROM Role__c 
                                   	WHERE VMApprovalStatus__c =: true
                                   	AND Available_Positions__c > 0
                                   	AND Recruiting_Start_Date__c <= TODAY
                                   	AND Recruiting_End_Date__c >= TODAY
                                   	LIMIT : Integer.valueOf(noOfRecords)];
        return lstRoles;
    }
}