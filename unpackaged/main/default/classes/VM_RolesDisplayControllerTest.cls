/**************************************************************************************************************************************************
Name:  VM_RolesDisplayControllerTest
Copyright © 2018  ITC
=====================================================================
Purpose: 1. TestClass for VM_RolesDisplayController class                                                                                               
============================================================
History                                                            
-------                                                            
VERSION  AUTHOR         DATE           DETAIL          Description
1.0      Nikhil        06/08/2018     Created         Created the test class

****************************************************************************************************************************************************/
@isTest
public class VM_RolesDisplayControllerTest {
    public static final String PARTNER_RECORDTYPE = 'CFA Contact';

    {
        ContactTriggerHandler.triggerDisabled = true;
    }
    
    @testsetup public static void setup(){
        
        Account portalAccount = CFA_TestDataFactory.createAccount('CFA Institute', 'Society', true);
        portalAccount.IsPartner = true;
        update portalAccount;
        
        Contact portalContact = VM_TestDataFactory.createContactRecord(portalAccount);        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
        
        User RMOwner=VM_TestDataFactory.createUser('RMTest4769@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest45498@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne6758@gmail.com','Huff','Outreach Management User','Relationship Manager');
        String ename ='Test Data FY14';
        Engagement__c eng  = VM_TestDataFactory.createEngagement(Ename,RMOwner,MDUSer,ReportsToUSer) ;
        system.debug('---67676---'+eng);
        insert eng;
    }
    
    public static testMethod void testclass(){
       
        Engagement__c eng  = [SELECT Id FROM Engagement__c WHERE Name='test Test Data FY14' LIMIT 1] ;
        List<Role__c> test_roles= VM_TestDataFactory.createRoles(eng,10);
        insert test_roles;
        Test.startTest();
        VM_RolesDisplayController.fetchRoleDetails(10);
        system.assertEquals(10, [SELECT Id FROM Role__c].size());
        Test.stopTest();
        
    }

}