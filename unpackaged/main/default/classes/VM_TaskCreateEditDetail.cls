public class VM_TaskCreateEditDetail {
    
   @AuraEnabled
  public static Task getNewTask() {
      
      Task tsk=new Task();
  
    return tsk;
  } 
    
    
    @AuraEnabled
    public static String newtask(Task tsk,Id whtId){
  
  
        List<RecordType> rtypes;
        Map<String,String> taskRecordTypes;
        rtypes=new List<RecordType>();
        taskRecordTypes = new Map<String,String>{};
        rtypes=[Select Name, Id From RecordType where sObjectType='Task' and isActive=true];
        Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
        engVol=[select id,Engagement__c,Role_del__c,Contact__c from Engagement_Volunteer__c where Id=:whtId];
        for (RecordType rt : rtypes) {
            taskRecordTypes.put(rt.Name,rt.Id);
        }
        system.debug('inside class');
      try{  
            system.debug('inside class');
            User u=new User();
            u=[select id,contactid from User where id=:UserInfo.getUserID()];
            Contact con=new Contact();
            con=[select id,email,MDM_Preferred_Email_for_Volunteering__c from contact where id=:u.contactid];
            tsk.whoid=u.contactid; 
            tsk.whatId=whtId;
            tsk.recordtypeid=taskRecordTypes.get('TaskRecordExternal');
           // tsk.Subject ='a';
            tsk.Engagement__c=engVol.Engagement__c;
            if(tsk.Task_type__c==null)
            {
                tsk.Task_type__c='Orientation';
            }
            if(tsk.Task_type__c=='Feedback')
            {
                system.debug('Inside feedback type');
                tsk.Feedback_Link__c=Label.VM_FeedbackURL+'?VM_Engagement_Volunteer__c='+engVol.Id+'&VM_Role__c='+engVol.Role_del__c+'&Contact__c='+engVol.Contact__c+'&VM_Engagement__c='+engVol.Engagement__c;  
            
            }
            if(con.email!=null)
            {
                tsk.VM_Volunteer_Preferred_Email__c=con.email;    
            }
            if(con.MDM_Preferred_Email_for_Volunteering__c!=null)
            {
                tsk.VM_Volunteer_Preferred_Email__c=con.MDM_Preferred_Email_for_Volunteering__c;
            }
            
            
            system.debug('value of task '+tsk);      //||tsk.Travel_Hours__c <> ''||tsk.Working_Hours__c <> ''
            
            insert tsk;  
            
            if(tsk.id!=null)
            {
                /*system.debug('inside email');
                Contact con=new Contact();
                con=[select id,email,MDM_Preferred_Email_for_Volunteering__c from contact where id=:u.contactid];
                EmailTemplate templateId = [Select id from EmailTemplate where name = 'VM Task creation Intimation']; 
                system.debug('inside email'+templateId);   
               if(con.MDM_Preferred_Email_for_Volunteering__c!=null) 
               {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {con.MDM_Preferred_Email_for_Volunteering__c};
                
                mail.setToAddresses(toAddresses);
                mail.setTargetObjectId(con.id);
                mail.setWhatId(tsk.id);
                mail.setTemplateId(templateId.id);
                mail.setSaveAsActivity(false);
                mail.setSenderDisplayName('Salesforce Support');
                mail.setSubject('New Task Created : ');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }*/
            }
            
        }
        catch(System.DMLException e){
            System.debug('eeeeeeeeeeeeee'+e.getDMLMessage(0));
            return e.getDMLMessage(0);
        }
      
      return null;
        
  }
  
 @AuraEnabled
  public static List<Task> getTaskLst(Id wId) {
      
      system.debug('Inside Task List');
      User u=new User();
      u=[select id,contactid from User where id=:UserInfo.getUserID()];
  
    return [SELECT id,subject,whoid,whatid,status,Description__c,Feedback_Link__c,Task_type__c,Total_Hours__c,Travel_Hours__c,Working_Hours__c,Start_Date__c,ActivityDate from Task where whatId=:wId AND whoId=:u.contactId];
  }
   @AuraEnabled 
  public static List<String> getTaskType(){
        
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Task.Task_type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        
        return options; 
    }
    
    @AuraEnabled 
  public static List<String> getTaskStatus(){
        
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Task.status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        
        return options; 
    }

@AuraEnabled
  public static Task getTaskDetail(Id tskId) {
      
      system.debug('Inside Task List');
      //User u=new User();
     // u=[select id,contactid from User where id=:UserInfo.getUserID()];
  
    return [SELECT id,subject,whoid,whatid,status,Description__c,Feedback_Link__c,Task_type__c,Total_Hours__c,Travel_Hours__c,Working_Hours__c,Start_Date__c,ActivityDate from Task where Id=:tskId];
  }
    
    @AuraEnabled
    public static void updatetask(Task taskObj) {
        Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
        engVol=[select id,Engagement__c,Role_del__c,Contact__c from Engagement_Volunteer__c where Id=:taskObj.whatid];        
            if(taskObj.Task_type__c=='Feedback')
            {
                taskObj.Feedback_Link__c=Label.VM_FeedbackURL+'?VM_Engagement_Volunteer__c='+engVol.Id+'&VM_Role__c='+engVol.Role_del__c+'&Contact__c='+engVol.Contact__c+'&VM_Engagement__c='+engVol.Engagement__c;  
            
            }
        try
        {    
        update taskObj;
        }catch(System.DMLException e)
        {
            system.debug('Edit Task Exception'+e.getDMLMessage(0));
        
        }
    }  
  
  
}