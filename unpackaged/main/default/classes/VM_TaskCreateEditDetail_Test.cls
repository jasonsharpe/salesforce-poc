@isTest 
//Bhushan
public class VM_TaskCreateEditDetail_Test {
    
    public static Account portalAccount;
    public static Contact portalContact;
    public static user portalUser;
    public static Engagement__c eng;
    public static Role__c role;
    public static Job_Application__c job;
    public static ContactShare conShare;
    public static Engagement__Share engShare;
    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    public static final String ENGAGEMENT_RECORDTYPE = 'Non Confidential';
    public static Engagement_Volunteer__c engVol;

    public static void setupdata(){
        ContactTriggerHandler.triggerDisabled = true;

        portalAccount = VM_TestDataFactory.createAccountRecord();
        // portalAccount.IsPartner = true;
        portalAccount.OwnerId = UserInfo.getUserId();
        insert portalAccount;

        portalContact = VM_TestDataFactory.createContactRecord(portalAccount);
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
        
        //Contact checkcon = [select id, recordtype.name, recordtypeid from contact where id = :portalContact.Id];
        //system.debug('@@@@ portalContact: '+checkcon.RecordTypeId);
        //portalUser = createPartnerUser('volTaskCrEditDet@gmail.com','volTaskCrEditDet','Volunteer','Managing Director', portalContact.Id);
        portalUser = CFA_TestDataFactory.createPartnerUser(portalContact,'CFA Base Partner');

        User RMOwner=VM_TestDataFactory.createUser('RMTes34534t@gmail.com','RMTest','Relationship Manager Staff','');
        User MDUSer=VM_TestDataFactory.createUser('MDTest343@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne56@gmail.com','Huff','Outreach Management User','Relationship Manager');
        
        eng=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        Schema.DescribeSObjectResult engSchemaResult = Schema.SObjectType.Engagement__c;
        Map<String,Schema.RecordTypeInfo> mapengTypeInfo = engSchemaResult.getRecordTypeInfosByName();
        Id engRecordTypeId = mapengTypeInfo.get(ENGAGEMENT_RECORDTYPE).getRecordTypeId();
        eng.RecordTypeId = engRecordTypeId;
        eng.Engagement_Type__c='Committee';
        eng.Approved__c=true;
        insert eng;

        system.debug('@@@@ eng: '+eng.recordtypeid);

        role=VM_TestDataFactory.createRole(eng); 
        role.Confidential__c = false;        
        insert role;
        role__C role1=VM_TestDataFactory.createRole(eng); 
        role1.VMApprovalStatus__c = true; 
        role1.name='sdajfhsdf';
        role1.Engagement_name__c =eng.id;
        insert role1;
        engVol = new Engagement_Volunteer__c();
        engVol.Engagement__c =eng.id;
        engVol.Role_del__c =role1.id;
        engvol.cfa_Parent_Volunteer__c='sdfadsf';
        engVol.Contact__c =portalContact.id;
        insert engVol;
        job=VM_TestDataFactory.createJobApplication(role,portalContact);
        insert job;
        job.Status__c='Submitted';
        job.Submitted__c = true;
        update job;
        job.status__c='Volunteer Accepted';
        update job;
        
        ContactShare conShare = new ContactShare();
        conShare.ContactId =portalContact.Id;
        conShare.UserOrGroupId = portalUser.Id;
        conShare.ContactAccessLevel = 'Read';
        insert conShare;
//        Engagement is public read only at the moment
//        Engagement__Share engShare = new Engagement__Share();
//        engShare.ParentId =eng.Id;
//        engShare.UserOrGroupId = portalUser.Id;
//        engShare.AccessLevel = 'Read';
//        insert engShare;
        
        Engagement_Volunteer__Share engVolShare = new Engagement_Volunteer__Share();
        engVolShare.ParentId =engVol.Id;
        engVolShare.UserOrGroupId = portalUser.Id;
        engVolShare.AccessLevel = 'Read';
        insert engVolShare;
        system.debug('Successful Update job');
    }
    
    
    public static testMethod void testnewTask() {
        setupdata();
        Task tsk = VM_TaskCreateEditDetail.getNewTask();
        
        try
        {         
            System.runAs(portalUser){
                Test.startTest();
                Engagement_Volunteer__c engV=new Engagement_Volunteer__c();
                engV=[select Id,Engagement__c,Engagement__r.name,Role_del__c,Role_del__r.name,Contact__c,Contact__r.name from Engagement_Volunteer__c where Engagement__c=:eng.Id];
                string sId = VM_TaskCreateEditDetail.newtask(tsk, engV.Id);
                system.debug('@@@@ engV.id'+ engV.Id);
                Task tsk2 =VM_TaskCreateEditDetail.getNewTask();
                tsk2.Task_type__c ='Feedback';
                string sId2 = VM_TaskCreateEditDetail.newtask(tsk2, engV.Id);
                
                 
                list<Task> lstTask =  VM_TaskCreateEditDetail.getTaskLst(engV.Id);
                list<string> lstString = VM_TaskCreateEditDetail.getTaskType();
                list<string> lstString1 = VM_TaskCreateEditDetail.getTaskStatus();
                Task tsk1 = VM_TaskCreateEditDetail.getTaskDetail(tsk.Id);
                tsk.Task_type__c ='Feedback';
                update tsk;
                VM_TaskCreateEditDetail.updatetask(tsk);
                Test.stopTest();
            }
        }
        catch(DMLException ex)
        {
            system.debug('VM_TaskCreateEditDetail_Test'+ex.getDMLMessage(0));
        }
        
    }
    public static User createPartnerUser(String usrName,String Lname, String ProfileName,String Designation, Id contactId){
        Id adminProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        List<User> adminUsers = [SELECT Id FROM User WHERE ProfileId = :adminProfileId AND IsActive = TRUE];
//        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        UserRole portalRole;

        System.runAs(adminUsers[0]) {
            portalRole = new UserRole(Name = 'CFA Test');
            RetryForTest.upsertOnUnableToLockRow(portalRole);
        }

        User userWithRole = new User(
                UserRoleId = portalRole.Id,
                ProfileId = adminProfileId,
                Username = System.now().millisecond() + 'userWithRole@test.com',
                Alias = 'batman',
                Email = System.now().millisecond() + 'userWithRole@cfainstitute.com',
                EmailEncodingKey = 'UTF-8',
                FirstName = 'CFA',
                LastName = 'User',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = 'America/Chicago'
        );

        System.runAs(adminUsers[1]) {
            RetryForTest.upsertOnUnableToLockRow(userWithRole);
        }

        System.runAs(adminUsers[2]) {
            Contact con = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId];
            RetryForTest.upsertOnUnableToLockRow(new Account(Id = con.AccountId,OwnerId = userWithRole.Id));
        }

        Profile pf = [SELECT Id FROM Profile WHERE Name=: ProfileName];
        User usr = new User(
                Alias = 'TestUser',
                Email = 'partnertestorg@partnertestorg.com',
                ContactId = contactId,
                EmailEncodingKey = 'UTF-8',
                LastName = Lname,
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = pf.Id,
                Designation__c = Designation,
                TimeZoneSidKey = 'America/Los_Angeles',
                Username = usrName,
                IsActive=true
        );
		RetryForTest.upsertOnUnableToLockRow(usr);

        return usr;
    }

}