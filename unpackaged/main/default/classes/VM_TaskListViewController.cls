public without sharing class VM_TaskListViewController {
    @AuraEnabled 
    public static List<Task> getListTasks(){
        string userId = userinfo.getUserId();
        List<Task> listTasks=new List<Task>();
        if(userId!=null){
            User userObj=[Select contactId from User where Id=:userId];
            if(userObj!=null && userObj.ContactId!=null){
                listTasks=[SELECT Id,Subject,WhatId,Engagement__r.Name,ActivityDate FROM Task where whoid=:userObj.contactId and Status!='Completed' and what.type='Engagement_Volunteer__c'];
            }    
        }
        return listTasks;
    } 
    
    //Added for Extending Volunteers
    @AuraEnabled
    public static List<Engagement_Volunteer__c> getInvitedEngVol(){        
        string userId = userinfo.getUserId();
        List<Engagement_Volunteer__c> invitedEngVolList = new List<Engagement_Volunteer__c>();
        if(userId!=null){
            User userObj=[Select contactId from User where Id=:userId];
            if(userObj!=null && userObj.contactId !=null){
                invitedEngVolList = [Select id, Name,Contact__c,Contact__r.Name,
                                     Engagement__c, Engagement__r.Name,
                                     Role_del__c, Role_del__r.Name,  Role_del__r.Relationship_Manager_Email_a__c  
                                     from Engagement_Volunteer__c where Contact__c=:userObj.contactId and Status__c='Invited'];
            }    
        }
        system.debug('____________Invited Vols_________'+invitedEngVolList );
        return invitedEngVolList;
    }
    @AuraEnabled
    public static void StatusUpdate(String isAcceptOrReject,String RecordId) {
        if(isAcceptOrReject == 'Accept') {
            Engagement_Volunteer__c ob = new Engagement_Volunteer__c();
            ob.Id = RecordId;
            ob.cfa_Accept_Volunteer_Agreement__c = true;
            //ob.cfa_Understand_Role_Details__c = true;
            ob.cfa_Extension_Status__c = 'Extended and Accepted';
            ob.Status__c = 'On-boarded';
            update ob;
        } else {
            Engagement_Volunteer__c ob = new Engagement_Volunteer__c();
            ob.Id = RecordId;
            ob.Status__c = 'Invitation Declined';
            update ob;
        }
    }
    
    @AuraEnabled
    public static String getacknowledgeform(){
        Document dr=[Select ID from Document where Name='Volunteer Acknowledgement Form'];
        String Customlabel = Label.VM_Policydocument;
        String myurl= Customlabel +dr.id;
        system.debug('getacknowledgeform' + myurl);
        return myurl;
    }
}