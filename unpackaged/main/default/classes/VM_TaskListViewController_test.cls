@isTest
public class VM_TaskListViewController_test {
    public static Account portalAccount;
    public static Contact portalContact;
    public static user portalUser;
    
    public static ContactShare conShare;
    public static Engagement__Share engShare;
    public static final String PARTNER_RECORDTYPE = 'CFA Contact';
    public static final String ENGAGEMENT_RECORDTYPE = 'Non Confidential';

    public static void setupdata(){
        ContactTriggerHandler.triggerDisabled = true;

        portalAccount = VM_TestDataFactory.createAccountRecord();
        // portalAccount.IsPartner = true;
        insert portalAccount;
        
        portalContact = VM_TestDataFactory.createContactRecord(portalAccount);        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE).getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        update portalContact;
        
        //Contact checkcon = [select id, recordtype.name, recordtypeid from contact where id = :portalContact.Id];
        //system.debug('@@@@ portalContact: '+checkcon.RecordTypeId);
        //portalUser = VM_TaskCreateEditDetail_Test.createPartnerUser('volTaskCrEditDet@gmail.com','volTaskCrEditDet','Volunteer','Managing Director', portalContact.Id);
        portalUser = CFA_TestDataFactory.createPartnerUser(portalContact,'CFA Base Partner');
        
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
         SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
         }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
         }
        insert obj;
        
        ContactShare conShare = new ContactShare();
        conShare.ContactId =portalContact.Id;
        conShare.UserOrGroupId = portalUser.Id;
        conShare.ContactAccessLevel = 'Edit';
        insert conShare;
        case objcase = new case(Accountid=portalAccount.id,contactid=portalContact.id);
        insert objCase;
        Task tsk= new Task();
        tsk.Subject='abc';
        tsk.ActivityDate=date.today();
        tsk.Status='In Progress';
        tsk.Priority='Normal';
        tsk.WhoId=portalContact.id;
        tsk.whatid=objcase.id;
        insert tsk;
        
    }
    public static testMethod void getListTasks() {
        setupdata();
        List<Task> listTasks=new List<Task>();
        listTasks=VM_TaskListViewController.getListTasks();
        
        System.runAs(portalUser){
        insert listTasks;
        }
        Document document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.Name = 'Volunteer Acknowledgement Form';
        document.FolderId = [select id from folder where name = 'Communities Shared Document Folder'].id;

        insert document;
        VM_TaskListViewController.getacknowledgeform();
    }
    public static testMethod void statusUpdate() {
        
        cfa_VM_extendVolunteerController.getIconName('case');
        cfa_VM_extendVolunteerController.getIconName('Engagement_Volunteer__c');
        
        Account VM_Account = new Account();
        VM_Account.Name ='Test_VMAcc1';
        VM_Account.Phone = '222568974';
        insert VM_Account;
         
        Contact VM_Contact = new Contact();
        VM_Contact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Local Member').getRecordTypeId();
        VM_Contact.LastName = 'Test_LVM1';
        VM_Contact.FirstName = 'Test_FVM1';
        VM_Contact.AccountId = VM_Account.ID;
        VM_Contact.Email = 'TestVM_test01@test.com';
        VM_Contact.Phone = '0123456789';
        insert VM_Contact;
        
        Engagement__c VM_Engage=new Engagement__c();
         User RMOwner=VM_TestDataFactory.createUser('RMTest5236@gmail.com','RMTest','Relationship Manager Staff','Relationship Manager');
        User MDUSer=VM_TestDataFactory.createUser('MDTest5236@gmail.com','MDTest','Outreach Management User','Managing Director');
        User ReportsToUSer=VM_TestDataFactory.createUser('Anne5236@gmail.com','Lange','Outreach Management User','Relationship Manager');
        
        VM_Engage=VM_TestDataFactory.createEngagement('t FY20',RMOwner,MDUSer,ReportsToUSer);
        insert VM_Engage;
        
        
        Role__c VM_Role=new Role__c();
        VM_Role.Engagement_name__c=VM_Engage.id;
        VM_Role.Position_Title__c='Test_VMRole';
        VM_Role.Role_Type__c='CIPM Committee Member';
        VM_Role.Conflict_filter__c='Writers';
        VM_Role.Number_of_Positions__c=123;
        VM_Role.Start_date__c=Date.today();
        VM_Role.Recruiting_Start_Date__c=Date.today();
        VM_Role.Recruiting_End_Date__c=Date.today().addDays(10);
        VM_Role.PC_Check_required__c=true;
        VM_Role.Confidential__c=true;
        
       
        VM_Role.Volunteer_Impact__c='Test';
        VM_Role.Volunteer_Work_location__c='Test';
        VM_Role.Volunteer_Roles_and_Responsibilities__c='Test';
        VM_Role.Volunteer_Experience__c='Test';
        VM_Role.Volunteer_Compet__c='Chief Financial Officer (CFO)';
        VM_Role.Volunteer_Certifications__c='CFA Charterholder';
        VM_Role.Conflicts_Specific_To_The_Role__c='Employment and/or affiliation with prep provider prohibited';
        VM_Role.VMApprovalStatus__c=true;
        insert VM_Role;
        
         List<Engagement__c > lstEng = new List<Engagement__c >();
        lstEng.add(VM_Engage);
        String json1 = JSON.serialize(lstEng);
        String engId = cfa_VM_extendEngagementController.saveEngagement(json1,VM_Engage.Id);
       
       VM_Engage.MD__c = null; 
       VM_Engage.Business_Owner__c = null; 
       VM_Engage.Reports_To__c = null; 
       VM_Engage.Engagement_External_Id__c = null;
       
       update VM_Engage;
       
       Engagement_Volunteer__c engVol=new Engagement_Volunteer__c();
       engVol.Engagement__c=VM_Engage.id;
       engVol.Role_del__c=VM_Role.id;
       engVol.Contact__c=VM_Contact.id;
       engVol.Status__c='On-boarded';
       insert engVol;
       VM_TaskListViewController.getInvitedEngVol();
       VM_TaskListViewController.StatusUpdate('Accept',engVol.id);
       VM_TaskListViewController.StatusUpdate('Reject',engVol.id);
    }
}