@isTest
//Bhushan
public class VM_VolEngAssocFilterTEST {
  public static Account portalAccount;
  public static Contact portalContact;
  public static user portalUser;
  public static Engagement__c eng;
  public static Role__c role;
  public static Job_Application__c job;
  public static ContactShare conShare;
  public static Engagement__Share engShare;
  public static VM_NonConflictRules__c cs_NCR;
  public static VM_LevelThreePairs__c cs_LTP;
  public static JobApplicationRecordTypeSelection__c cs_JobAppRT;
  public static final String PARTNER_RECORDTYPE = 'CFA Contact';
  public static final String ENGAGEMENT_RECORDTYPE = 'Non Confidential';

  public static void setupdata() {
    ContactTriggerHandler.triggerDisabled = true;

    portalAccount = VM_TestDataFactory.createAccountRecord();
    portalAccount.type = 'Default';
    insert portalAccount;

    portalContact = VM_TestDataFactory.createContactRecord(portalAccount);
    Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
    Map<String, Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
    Id partnerRecordTypeId = mapContactTypeInfo.get(PARTNER_RECORDTYPE)
      .getRecordTypeId();
    portalContact.RecordTypeId = partnerRecordTypeId;
    portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
    update portalContact;

    portalUser = CFA_TestDataFactory.createPartnerUser(portalContact,'CFA Base Partner');
    /*
    portalUser = VM_TaskCreateEditDetail_Test.createPartnerUser(
      'volTaskCrEditDet@gmail.com',
      'volTaskCrEditDet',
      'Volunteer',
      'Managing Director',
      portalContact.Id
    );
    */

    User RMOwner = VM_TestDataFactory.createUser(
      'RMTest345@gmail.com',
      'RMTest',
      'Relationship Manager Staff',
      ''
    );
    User MDUSer = VM_TestDataFactory.createUser(
      'MDTest345@gmail.com',
      'MDTest',
      'Outreach Management User',
      'Managing Director'
    );
    User ReportsToUSer = VM_TestDataFactory.createUser(
      'Anne234@gmail.com',
      'Huff',
      'Outreach Management User',
      'Relationship Manager'
    );

    eng = VM_TestDataFactory.createEngagement(
      't FY20',
      RMOwner,
      MDUSer,
      ReportsToUSer
    );
    Schema.DescribeSObjectResult engSchemaResult = Schema.SObjectType.Engagement__c;
    Map<String, Schema.RecordTypeInfo> mapengTypeInfo = engSchemaResult.getRecordTypeInfosByName();
    Id engRecordTypeId = mapengTypeInfo.get(ENGAGEMENT_RECORDTYPE)
      .getRecordTypeId();
    eng.RecordTypeId = engRecordTypeId;
    eng.Engagement_Type__c = 'Committee';
    insert eng;
    Engagement_Volunteer__c engV = new Engagement_Volunteer__c();
    engV.Contact__c = portalContact.id;
    engV.Status__c = 'On-boarded';
    engV.Engagement__c = eng.id;
    role = VM_TestDataFactory.createRole(eng);
    role.Confidential__c = false;
    role.Available_Positions__c = 10;
    insert role;

    job = VM_TestDataFactory.createJobApplication(role, portalContact);
    insert job;
    job.Status__c = 'Submitted';
    job.Submitted__c = true;
    update job;
    job.status__c = 'Volunteer Accepted';
    update job;

    ContactShare conShare = new ContactShare();
    conShare.ContactId = portalContact.Id;
    conShare.UserOrGroupId = portalUser.Id;
    conShare.ContactAccessLevel = 'Read';
    insert conShare;

    cs_NCR = new VM_NonConflictRules__c(
      Name = '1',
      NonConflictRolePair__c = 'Writers:Writers'
    );
    cs_LTP = new VM_LevelThreePairs__c(Name = '1', Data__c = 'Writers:Writers');
    cs_JobAppRT = new JobApplicationRecordTypeSelection__c(
      Engagement_Type__c = 'Paid Contributors',
      Name = '10-8997',
      JobApplication_RecordType__c = 'MemberMicro',
      Role_Type__c = 'Abstractors'
    );
    insert cs_NCR;
    insert cs_LTP;
    insert cs_JobAppRT;
  }

  public static testMethod void testVolEngAssoc1() {
    setupdata();
    //Task tsk = VM_TaskCreateEditDetail.getNewTask();
    Test.startTest();
    try {
      Engagement_Volunteer__c engV = new Engagement_Volunteer__c();
      engV = [
        SELECT
          Id,
          Engagement__c,
          Engagement__r.name,
          Role_del__c,
          Role_del__r.name,
          Contact__c,
          Contact__r.name,
          status__c
        FROM Engagement_Volunteer__c
        WHERE Engagement__c = :eng.Id
      ];

      Engagement_Volunteer__share objShare = new Engagement_Volunteer__share();
      objShare.ParentId = engV.Id;
      objShare.UserOrGroupId = portalUser.Id;
      objShare.AccessLevel = 'Read';
      insert objShare;
      System.runAs(portalUser) {
        VM_VolunteerEngagementAssociationFilter.checkAllConditions(
          engV.Role_del__r.Id
        );
        string s = VM_VolunteerEngagementAssociationFilter.checkConflictFilterNPPApproach(
          role.Id,
          portalContact.Id
        );
      }
      VM_NonConflictRules__c cs_NCR1 = [
        SELECT name, NonConflictRolePair__c
        FROM VM_NonConflictRules__c
        WHERE name = '1'
      ];
      cs_NCR1.NonConflictRolePair__c = 'Graders:Writers';
      update cs_NCR1;

      System.runAs(portalUser) {
        string s = VM_VolunteerEngagementAssociationFilter.checkConflictFilterNPPApproach(
          role.Id,
          portalContact.Id
        );
      }
      delete engV;

      System.runAs(portalUser) {
        VM_VolunteerEngagementAssociationFilter.checkAllConditions(role.id);
      }

      portalContact.CFA_Candidate_Code__c = 'GRAD';
      update portalContact;
      Contact volContact = [
        SELECT
          CFA_Candidate_Code__c,
          CIPM_Candidate_Code__c,
          Investment_Foundations_Candidate_Code__c,
          Name
        FROM Contact
        WHERE ID = :portalContact.Id
      ];


      System.runAs(portalUser) {
        VM_VolunteerEngagementAssociationFilter.checkAllConditions(role.id);
      }
      Test.stopTest();
    } catch (DMLException ex) {
      system.debug('testVolEngAssoc1' + ex.getDMLMessage(0));
    }
  }

  public static testMethod void testVolEngAssoc2() {
    Test.startTest();
    setupdata();
    //Task tsk = VM_TaskCreateEditDetail.getNewTask();

    try {
      Engagement_Volunteer__c engV = new Engagement_Volunteer__c();
      engV = [
        SELECT
          Id,
          Engagement__c,
          Engagement__r.name,
          Role_del__c,
          Role_del__r.name,
          Contact__c,
          Contact__r.name,
          status__c
        FROM Engagement_Volunteer__c
        WHERE Engagement__c = :eng.Id
      ];
      role.Available_Positions__c = 0;
      update role;
      System.runAs(portalUser) {
        VM_VolunteerEngagementAssociationFilter.checkAllConditions(
          engV.Role_del__r.Id
        );
      }
      role.Available_Positions__c = 10;
      update role;
      System.runAs(portalUser) {
        VM_VolunteerEngagementAssociationFilter.checkAllConditions(role.id);
      }

      job.Status__c = 'Submitted';
      job.Submitted__c = true;
      update job;
      System.runAs(portalUser) {
        VM_VolunteerEngagementAssociationFilter.checkAllConditions(role.id);
      }
      Test.stopTest();
    } catch (DMLException ex) {
      system.debug('testVolEngAssoc2' + ex.getDMLMessage(0));
    }
  }
}