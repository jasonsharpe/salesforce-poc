public without sharing class cfa_VM_extendVolunteerController {
    @AuraEnabled
    public static String getIconName(String sObjectName){ 
        String u;
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();
        
        for(Schema.DescribeTabSetResult tsr : tabSetDesc) { tabDesc.addAll(tsr.getTabs()); }
        
        for(Schema.DescribeTabResult tr : tabDesc) {
            if( sObjectName == tr.getSobjectName() ) {
                if( tr.isCustom() == true ) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    u = 'standard:' + sObjectName.toLowerCase();
                }
            }
        }
        for (Schema.DescribeIconResult ir : iconDesc) {
            if (ir.getContentType() == 'image/svg+xml'){
                u = 'custom:' + ir.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                break;
            }
        }
        return u;
    }
    
    public class volunteerWrapper {
        @AuraEnabled
        public Engagement_Volunteer__c Volunteer;
        @AuraEnabled
        public string newRole;
        @AuraEnabled
        public Boolean isChecked;
    }
    
    @AuraEnabled
    public static List<volunteerWrapper> getVolunteers(String engagementId){ 
        List<volunteerWrapper> custWrap = new List<volunteerWrapper>();
        List<Engagement_Volunteer__c> lstExistingVolunteer =cfa_vm_extendVolunteerSelector.getVolunteers(engagementId) ;
        Set<String> setOfExistingVolunteer = new Set<String>();
        for(Engagement_Volunteer__c each: lstExistingVolunteer) {
            setOfExistingVolunteer.add(each.cfa_Parent_Volunteer__c);
        }
        Set<id>parentRolesSet = new Set<Id>();
        Map<id,String> parentToChild = new Map<id,String>();
        List<Engagement__c> lstengage = cfa_vm_extendVolunteerSelector.getEngagement(engagementId);
        //List<Engagement__c> lstengage = [Select id,Name, cfa_Parent_Engagement__c,cfa_Parent_Engagement__r.Name, Approval_Status__c,Approved__c from Engagement__c where id =:engagementId];
        List<Role__c> lstOfRoles = [Select id,Name,cfa_Parent_Role__c from Role__c where Engagement_name__c =:engagementId  AND VMApprovalStatus__c = true];
        for(Role__c each:lstOfRoles) {
            parentRolesSet.add(each.cfa_Parent_Role__c);
            parentToChild.put(each.cfa_Parent_Role__c,each.Name);
        }
        System.debug(parentToChild);
        if(!parentRolesSet.isEmpty()) {
            List<Engagement_Volunteer__c> lstOfVolunteer = [Select id,Name,Role_del__r.Name,Engagement__c,Engagement__r.Name,Role_del__c,Contact__c,Contact__r.Name,Contact__r.FirstName,Contact__r.Email,Contact__r.MDM_Preferred_Email_for_Volunteering__c from Engagement_Volunteer__c where Engagement__c=:lstengage[0].cfa_Parent_Engagement__c AND Role_del__c IN:parentRolesSet AND Id NOT IN:setOfExistingVolunteer];
            for(Engagement_Volunteer__c each: lstOfVolunteer) {
                volunteerWrapper ob = new volunteerWrapper();
                ob.Volunteer = each;
                ob.newRole = parentToChild.get(each.Role_del__c);
                ob.isChecked = false;
                custWrap.add(ob);
            }
            
        }
        return custWrap;
    }
    @AuraEnabled
    public static String checkEligibility(String engagementId) {
        List<Engagement__c> lstengage = [Select id,cfa_Parent_Engagement__c,Approval_Status__c,Approved__c from Engagement__c where id =:engagementId];
        if(!lstengage.isEmpty() && lstengage[0].cfa_Parent_Engagement__c == null) {
            return 'Parent Engagement is missing';
        } else if(!lstengage.isEmpty() && (lstengage[0].Approval_Status__c != 'Approved' || !lstengage[0].Approved__c )) {
            return 'Engagement not Approved';
        }
        List<Role__c> lstOfRole;
        boolean checkStatus = false;
        if(!lstengage.isEmpty() && lstengage[0].cfa_Parent_Engagement__c !=null) {
            
            lstOfRole = [Select id,VMApprovalStatus__c from Role__c where Engagement_name__c =:lstengage[0].id];
            for(Role__c each: lstOfRole) {
                if(each.VMApprovalStatus__c) {
                    checkStatus = true;
                    break;
                }
            }
        }
        
        if(!checkStatus) {
            return 'None of your roles were approved to extend volunteers';
        }
        
        return null;
    }
    
    @AuraEnabled
    public static string saveVolunteers(String strlstOfVolunteer,String engId) {
        try{
            List<Role__c> lstOfRole = [Select id,Name,cfa_Parent_Role__c from Role__c where Engagement_name__c=:engId];
            Map<id,id> parentToChildRole = new Map<id,id>();
            Map<id,String> parentToChildRoleName = new Map<id,String>();
            for(Role__c each: lstOfRole) {
                parentToChildRole.put(each.cfa_Parent_Role__c,each.id);
                parentToChildRoleName.put(each.cfa_Parent_Role__c,each.Name);
            }
            List<volunteerWrapper> lstOfVolunteer = (List<volunteerWrapper>)JSON.deserialize(strlstOfVolunteer, List<volunteerWrapper>.class);
            System.debug(lstOfVolunteer);
            System.debug(engId);
            String URL = 'https://devint-cfainstitute.cs24.force.com/CFAVolunteer';
            
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'cfavolunteers@cfainstitute.org'];
            
            
            
            List<Engagement_Volunteer__c> finalVolList = new List<Engagement_Volunteer__c>();
            List<String> toAddress = new List<String>();
            for(volunteerWrapper each: lstOfVolunteer) { 
                if(each.isChecked) {
                    String roleName = parentToChildRoleName.get(each.Volunteer.Role_del__c);
                    
                    Engagement_Volunteer__c obr = each.Volunteer;
                    obr.cfa_Parent_Volunteer__c = each.Volunteer.Id;
                    obr.id = null;
                    System.debug(obr.Status__c);
                    obr.cfa_Extension_Status__c = 'Invitation Sent';
                    obr.Engagement__c = engId;
                    obr.Role_del__c = parentToChildRole.get(each.Volunteer.Role_del__c);
                    obr.Status__c = 'Invited';   
                    if(each.Volunteer.Contact__r.MDM_Preferred_Email_for_Volunteering__c != NULL){
                        toAddress.add(each.Volunteer.Contact__r.MDM_Preferred_Email_for_Volunteering__c);
                    }else{
                        toAddress.add(each.Volunteer.Contact__r.Email);
                    }
                    
                    finalVolList.add(obr);
                }
                
            }
            insert finalVolList;   
            List<Engagement_Volunteer__c> lstInsertedLst = [Select Role_del__c,Role_del__r.Name,
                                                            id,cfa_Parent_Volunteer__c,Contact__r.FirstName,
                                                            Status__c,Contact__r.MDM_Preferred_Email_for_Volunteering__c,Contact__r.Email from Engagement_Volunteer__c where id IN:finalVolList];
            String acceptLabel = Label.VM_AcceptInvitationURL;
            String taskLabel = Label.VM_CommunityHomepage;
            EmailTemplate invTemplate = [ SELECT Id, Name, Subject, Body,HtmlValue FROM EmailTemplate  WHERE Name ='VM_VolunteerExtensionInvitation']; 
            for(Engagement_Volunteer__c each: lstInsertedLst) { 
                string coomunityurl = taskLabel + '/s/volunteerinvitation?VolunteerId='+each.Id;                     
                string subject = invTemplate.subject;
                string body = invTemplate.HtmlValue;
                body = body.replace('contactName',each.Contact__r.FirstName==null?'':each.Contact__r.FirstName);
                body = body.replace('roleName',each.Role_del__r.Name);
                body = body.replace('this link','<a href="'+coomunityurl +'">this link</a>');
                body = body.replace('Forgot your password?','<a href="'+label.VMforgotpassword+'">Forgot your password?</a>');
                body = body.replace('Contact us.','<a href="'+label.VMContactUs+'">Contact us.</a>');
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
                mail.setSubject(subject);
                System.debug(body);
                mail.setHTMLBody(body);
                mail.setSaveAsActivity(false);
                if(each.Contact__r.MDM_Preferred_Email_for_Volunteering__c != NULL){
                    mail.setToAddresses(new List<String>{each.Contact__r.MDM_Preferred_Email_for_Volunteering__c});
                }else{
                    mail.setToAddresses(new List<String>{each.Contact__r.Email});
                } 
                //mail.setToAddresses(new List<String>{each.Contact__r.MDM_Preferred_Email_for_Volunteering__c});
                
                mailList.add(mail);
                toAddress.clear();
            }
            
            System.debug(finalVolList);
            Messaging.sendEmail(mailList);    
            return engId;
        } catch(Exception e) {
            String errorStr = ''; 
            System.debug(errorStr);
            System.debug(e.getStackTraceString());
            return e.getMessage();
        }        
    }
}