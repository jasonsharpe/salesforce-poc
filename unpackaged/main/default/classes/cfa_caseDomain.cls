public class cfa_caseDomain {

    @AuraEnabled
    public static String getIconName(String sObjectName){ 
        String u;
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();
        
        for(Schema.DescribeTabSetResult tsr : tabSetDesc) { tabDesc.addAll(tsr.getTabs()); }
        
        for(Schema.DescribeTabResult tr : tabDesc) {
            if( sObjectName == tr.getSobjectName() ) {
                if( tr.isCustom() == true ) {
                    iconDesc.addAll(tr.getIcons());
                } else {
                    u = 'standard:' + sObjectName.toLowerCase();
                }
            }
        }
        for (Schema.DescribeIconResult ir : iconDesc) {
            if (ir.getContentType() == 'image/svg+xml'){
                u = 'custom:' + ir.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                break;
            }
        }
        return u;
    }
    
    @AuraEnabled
    public static case getInitDetails(String caseId) {
        List<Case> lstCase= [Select id,ParentId
                          from Case where id=:caseId];
        
        if(!lstCase.isEmpty()) {
            return lstCase[0];
        } else {
            return null;
        }
        
        
    }
        
    @AuraEnabled
    public static string saveCase(String json,String caseId) {
        json = json.replace('{"fields":{', '[{"attributes":{"type":"Case"},');
        json = json.replace('"}}', '"}] ');
        json = json.replace('""','null');
        List<Case> lstCase = (List<Case>)System.JSON.deserialize(json,List<Case>.class);
        try{
            lstCase[0].parentId = caseId;
            insert lstCase;
        } catch(DMLException  e) {
            System.debug(e.getMessage());
            String errorStr = e.getMessage().substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,',':');
            System.debug(errorStr);
            return errorStr;
        }
        
        return lstCase[0].id;
    }
}