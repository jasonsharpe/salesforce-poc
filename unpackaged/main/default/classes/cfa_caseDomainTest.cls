@isTest
public class cfa_caseDomainTest {
    static testMethod void caseDominTestChecks() {
        Id societyTechCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Society Tech Case').getRecordTypeId();
        cfa_caseDomain.getIconName('Case');
        cfa_caseDomain.getIconName('Engagement__c');
        Case obCase;
        Test.startTest();
        obCase = new Case();
        obCase.society_Issue_Sub_Type__c = 'Board Roster';
        obCase.society_Impacted_Functional_Area__c = 'Domain Changes';
        obCase.Status = 'New';
        obCase.Origin = 'Email';
        obCase.Subject = 'Verify Case Create';
        obCase.RecordTypeId = societyTechCaseRecordTypeId;
        obCase.SuppliedEmail = 'test@test.com';
        insert obCase;

        cfa_caseDomain.getInitDetails(obCase.id);
        cfa_caseDomain.getInitDetails('');

        String json = '{"fields":{"ContactId":null,"AccountId":null,"society_Issue_Sub_Type__c":"Board Roster","Status":"New","society_Other_Issue__c":null,"Priority":"","society_Impacted_Functional_Area__c":"Domain Changes","Origin":"Email","SOC_Employee_Request__c":false,"Subject":"Test","Description":null,"SOC_Department__c":"Society Operations","SOC_Department_Status__c":"","society_IT_Priority__c":"","society_IT_Status__c":"","SOC_ServiceNow_ID__c":null,"society_Vendor__c":"","society_Vendor_Reference_Code__c":null,"society_Root_Cause_Analysis__c":"","SOC_Other_Root_Cause__c":null,"Known_Issue_Record__c":null,"society_Case_Close_Comments__c":null,"SOC_Age_New_Status_Hours__c":null,"SOC_Age_Working_Status_Hours__c":null,"SOC_LastModified_Working_Status_Time__c":null,"SuppliedEmail":null,"Id":""}}';
        cfa_caseDomain.saveCase(json,obCase.id);
        Test.stopTest();

    }
}