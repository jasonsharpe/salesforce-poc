public class cfa_vm_extendVolunteerSelector {
    
    public static  List<Engagement_Volunteer__c> getVolunteers(String engagementId){
        List<Engagement_Volunteer__c> lstExistingVolunteer = [Select cfa_Parent_Volunteer__c from Engagement_Volunteer__c where Engagement__c =:engagementId];
        return lstExistingVolunteer;
    } 
    
    public static  List<Engagement__c> getEngagement(String engagementId){
        List<Engagement__c> lstengage = [Select id,Name, cfa_Parent_Engagement__c,cfa_Parent_Engagement__r.Name, Approval_Status__c,Approved__c from Engagement__c where id =:engagementId];
        return lstengage;
    }
}