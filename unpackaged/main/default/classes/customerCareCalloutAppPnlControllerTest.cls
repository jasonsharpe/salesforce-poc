@isTest
public class customerCareCalloutAppPnlControllerTest
{
    public static Account portalAccount;
    public static Contact portalContact;
    public static user portalUser;
     
    public static  testMethod void TestMethod1()
    {
        User adminUser = VM_TestDataFactory.createUser('portal@usr.com', 'user', 'System Administrator', 'Managing Director');
        System.runAs(adminUser){
            adminUser.UserRoleId = [SELECT Id FROM UserROle WHERE Name='Societies' LIMIT 1].Id;
        update adminUser;
            portalAccount = VM_TestDataFactory.createAccountRecord();
            // portalAccount.IsPartner = true;
            insert portalAccount;
            portalContact = VM_TestDataFactory.createContactRecord(portalAccount);            
            User portalUser = CFA_TestDataFactory.createPartnerUsers(new List<Contact>{portalContact},'CFA_Customer_Community',1)[0];
        }
        
        //CFA_Customer_Community
        
        Schema.DescribeSObjectResult contactSchemaResult = Schema.SObjectType.Contact;
        Map<String,Schema.RecordTypeInfo> mapContactTypeInfo = contactSchemaResult.getRecordTypeInfosByName();
        Id partnerRecordTypeId = mapContactTypeInfo.get('Internal CFA Contact').getRecordTypeId();        
        portalContact.RecordTypeId = partnerRecordTypeId;
        portalContact.MDM_Preferred_Email_for_Volunteering__c = 'abc@abc.com';
        portalcontact.CFAMN__PersonID__c='sdfhd';
        portalcontact.Partner_Id__c='843758';
        portalcontact.Email = 'test@test.com';
        update portalContact;
        
        boolean isSB =[SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        SocietyOps_Backoffice__c obj = new SocietyOps_Backoffice__c(name='trigger-details',CaseReassignment_TemplateName__c='SocietyOps-Case Reassignment',CaseComment_TemplateName__c='SocietyOps-New Comment Notification');
        if(isSB==true)
        {
            obj.No_reply_email__c='researchchallenge@cfainstitute.org';
        }
        else
        {
            obj.No_reply_email__c='noreply.societyoperations@cfainstitute.org';
        }
        insert obj;
        case objcase = new case(Accountid=portalAccount.id,contactid=portalContact.id);
        insert objCase;
        Test.startTest();
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Search',string.valueof(objCase.id),'test');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Membership',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Preferences',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Profile',string.valueof(objCase.id),'');                                                   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_CFAprogram',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_MemAppReview',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_InternalSupLink',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_OfflineOrder',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_RADadjustments',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_ChangeTestArea',string.valueof(objCase.id),''); 
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Intenal_NetSuiteHistory',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_Pricing_1',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_BasnoApp',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_InternalAdmTool',string.valueof(objCase.id),'');                         
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_CAT',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_VitalSource',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Cisco_QM',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Cisco_WFM',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_MAImpersonation',string.valueof(objCase.id),'');  
        
        objcase.contactid = null;
        update objcase;
        
        customerCare_Callout_AppPanel_Controller.getURLs('',string.valueof(objCase.id),''); 
        
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Membership',string.valueof(objCase.id),''); 
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Preferences',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Profile',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_CFAprogram',string.valueof(objCase.id),'');  
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_InternalSupLink',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_OfflineOrder',string.valueof(objCase.id),'');   
        
        portalcontact.CFAMN__PersonID__c=null;
        update portalcontact;
        
        objcase.contactid=portalContact.id;
        update objcase;
        
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_RADadjustments',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_ChangeTestArea',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Intenal_NetSuiteHistory',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_Pricing',string.valueof(objCase.id),'');   
        
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Membership',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Preferences',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_Profile',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_CFAprogram',string.valueof(objCase.id),'');   
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_InternalSupLink',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Customer_MAImpersonation',string.valueof(objCase.id),'');
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_OfflineOrder',string.valueof(objCase.id),''); 
        
        portalcontact.Email =null;
        update portalcontact;  
        
        customerCare_Callout_AppPanel_Controller.getURLs('CCAP_Internal_OfflineOrder',string.valueof(objCase.id),''); 
        
        portalcontact.Partner_Id__c =null;
        update portalcontact;  
        customerCare_Callout_AppPanel_Controller.getURLs('',string.valueof(objCase.id),''); 
        Test.stopTest();
    }
    
    @isTest
    public static void getMapReviewURLTest(){
        customerCare_Callout_AppPanel_Controller.getMapReviewURL('CCAP_Internal_MemAppReview');
    }
}