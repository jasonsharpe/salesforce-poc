//This Class is for LWC
public with sharing class customerCare_Callout_AppPanel_Controller{
    @AuraEnabled
    public static String getURLs(String buttonTitle,String caseId,String SearchText) {
        try{
            if(SearchText != '') {
                String  orgURLMetadata = DynamicEnvVariableService.genericdatafetcher(buttonTitle);
                System.debug(orgURLMetadata);
                if(buttonTitle=='CCAP_Search') {
                    if(orgURLMetadata.contains('CCAP_Search')) {
                        orgURLMetadata = orgURLMetadata.replace('CCAP_Search', SearchText);
                    }
                } 
                return orgURLMetadata;
            } 
            if(caseId !=null || caseId != '') {
                //Buttons using Direct URls
                if (buttonTitle == 'CCAP_Internal_BasnoApp' || buttonTitle == 'CCAP_Internal_InternalAdmTool' || buttonTitle == 'CCAP_Internal_CAT' || buttonTitle == 'CCAP_Internal_VitalSource'
                    || buttonTitle == 'CCAP_Cisco_QM' || buttonTitle == 'CCAP_Cisco_WFM') {
                        String  orgURLMetadata = DynamicEnvVariableService.genericdatafetcher(buttonTitle);
                        System.debug('Direct Links'+ orgURLMetadata);
                        return  orgURLMetadata;
                    }
                List<Case> caseList = [Select Contact.CFAMN__PersonID__c,ID,Contact.Partner_Id__c,ContactId,Contact.Email from Case where id =: caseId];
                if(!caseList.isEmpty()){                
                    
                    String  orgURLMetadata = DynamicEnvVariableService.genericdatafetcher(buttonTitle);
                    
                    if(buttonTitle == 'CCAP_Customer_Membership' || 
                       buttonTitle == 'CCAP_Customer_Preferences' || 
                       buttonTitle == 'CCAP_Customer_Profile' ||
                       //buttonTitle == 'CCAP_Internal_CFAprogram' ||
                       buttonTitle == 'CCAP_Internal_MemAppReview' ||
                       buttonTitle == 'CCAP_Internal_InternalSupLink' ||
                       buttonTitle == 'CCAP_Customer_MAImpersonation'){
                           
                           if(caseList[0].ContactId != null && caseList[0].Contact.CFAMN__PersonID__c != null ){
                               //Buttons using Contact Person ID
                               if(buttonTitle == 'CCAP_Customer_Membership') {
                                   orgURLMetadata += caseList[0].Contact.CFAMN__PersonID__c;
                               } else if(buttonTitle == 'CCAP_Customer_Preferences') {
                                   orgURLMetadata += caseList[0].Contact.CFAMN__PersonID__c;
                               } else if (buttonTitle == 'CCAP_Customer_Profile') {
                                   orgURLMetadata += caseList[0].Contact.CFAMN__PersonID__c;
                               }  else if (buttonTitle == 'CCAP_Internal_InternalSupLink') {
                                   orgURLMetadata += caseList[0].Contact.CFAMN__PersonID__c;
                               } else if(buttonTitle == 'CCAP_Customer_MAImpersonation'){
                                   orgURLMetadata += caseList[0].Contact.CFAMN__PersonID__c;
                               }
                               System.debug('Links with PersonID '+ orgURLMetadata);
                               return  orgURLMetadata;
                           } else if(caseList[0].ContactId == null){
                               return 'Error - No Contact found';
                           } else if(caseList[0].Contact.CFAMN__PersonID__c == null) {
                               return 'Error - No Person Id found';
                           }
                           
                       }
                    
                    //Buttons using Contact Email Address  
                    if (caseList[0].ContactId !=null && caseList[0].Contact.Email != null && buttonTitle == 'CCAP_Internal_OfflineOrder') {
                        orgURLMetadata += caseList[0].Contact.Email;
                        System.debug('Links with Email'+ orgURLMetadata);
                        return  orgURLMetadata;
                    } else if(caseList[0].ContactId == null && buttonTitle == 'CCAP_Internal_OfflineOrder'){
                        return 'Error - No Contact found';
                    } else if(caseList[0].Contact.Email == null && buttonTitle == 'CCAP_Internal_OfflineOrder') {
                        return 'Error - No Email found';
                    }
                    
                    if(caseList[0].ContactId != null && caseList[0].Contact.Partner_Id__c != null) {
                        //Buttons using Contact Partner ID    
                        if (buttonTitle == 'CCAP_Internal_RADadjustments' ) {
                            orgURLMetadata += caseList[0].Contact.Partner_Id__c;
                        }  else if (buttonTitle == 'CCAP_Internal_ChangeTestArea') {
                            orgURLMetadata += caseList[0].Contact.Partner_Id__c;
                        }  else if (buttonTitle == 'CCAP_Intenal_NetSuiteHistory') {
                            orgURLMetadata += caseList[0].Contact.Partner_Id__c;
                        } else if (buttonTitle == 'CCAP_Internal_CFAprogram') {
                            orgURLMetadata += caseList[0].Contact.Partner_Id__c;
                        } else if (buttonTitle == 'CCAP_Internal_CIPMprogram') {
                            orgURLMetadata += caseList[0].Contact.Partner_Id__c;
                        }  else if (buttonTitle == 'CCAP_Internal_Pricing') {
                            if(orgURLMetadata.contains('CCAP_Internal_Pricing') && caseList[0].Contact.Partner_Id__c!= null) {
                                orgURLMetadata = orgURLMetadata.replace('CCAP_Internal_Pricing',caseList[0].Contact.Partner_Id__c);
                            }
                            
                        }
                        System.debug('Links with Partner_ID ' + orgURLMetadata); 
                        return orgURLMetadata;
                    } else if(caseList[0].ContactId == null){
                        return 'Error - No Contact found';
                    } else if(caseList[0].Contact.Partner_Id__c == null) {
                        return 'Error - No Partner Id found';
                    }
                    return orgURLMetadata;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static String getMapReviewURL(String buttonTitle){
		if(buttonTitle == 'Marketing_Cloud') {
			return 'https://mck68p5n252jkphjfb4r0xxczt81.login.exacttarget.com/sso/d4e8153c2d20403d361e6f6a354e3d37402277370563271623293b53242b0a232a391b2c3f54322136062e740a2f2b395d2e374b332a324063264b2d6a16060e1a15011a17413e314b2d20266b0e03651f363d532330541f363d53232c4a1f076677012a43292b0b472c294133233b462e2058777767057f7316';
		} else {
			return  DynamicEnvVariableService.genericdatafetcher(buttonTitle);
		}
    }
}