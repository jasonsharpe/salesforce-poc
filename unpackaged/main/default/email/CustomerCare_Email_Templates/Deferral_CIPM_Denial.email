Dear {!Contact.FirstName}, 

We are sorry to learn that you will be unable to attend the CIPM exam because of your health problems. While we are sympathetic with your situation, it would not qualify for a deferral. Registration deferrals to the next available exam are considered on a case-by-case basis for life-threatening illnesses (candidate or immediate family), expectant mothers, natural disasters, and mandatory military service. This does not guarantee a deferral to any candidate for any condition but instead provides CFA Institute with the discretion to determine which deferral requests it approves and denies. Many conditions may make it difficult or impossible to attend the exam but are not eligible for a deferral; candidates are under no obligation to attend the exam.

You will pay only the registration fee when registering for a future exam. For information on fees and deadlines, please visit: https://www.cfainstitute.org/programs/cipm/exam

We truly appreciate your interest in CFA Institute and hope you will register for a future exam.

Regards, 

{!Case.OwnerFirstName} | Global Customer Care | CFA Institute