{!Contact.FirstName}, 

Thank you for contacting us with your request. We no longer collect the last four characters of the passport number or the passport expiration date during CFA exam registration. Since we no longer collect these details at the time of registration, you are not required to update this information with CFA Institute. 

However, if the country/region that issued your passport has changed or the name in your CFA Institute account does not exactly match the names on your passport, then we require that information is updated prior to exam day. Any name mismatch, including but not limited to typos, misspellings, nicknames, and suffixes not shown on the passport, will result in you being denied admission to the exam. We strongly encourage you to review your passport and update your CFA Institute account if needed. Please visit https://profile.cfainstitute.org/passport to review your information. 

Additionally, if you do not present a valid international travel passport during check-in for both exam sessions or the information in our records does not match exactly with the information on your passport, you will not be permitted to take the exam and you will forfeit your registration fee. Please visit https://www.cfainstitute.org/about/governance/policies/cfa-program-identification-policy for complete information regarding the CFA Program Identification Policy.

I hope this information is helpful. If I can be of further assistance, please reply to this email.

Regards,

{!Case.OwnerFirstName} | Global Customer Care | CFA Institute