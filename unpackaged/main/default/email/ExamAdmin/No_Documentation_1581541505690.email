<html>
 <head> 
  <title></title> 
 </head> 
 <body style="height: auto; min-height: auto;">
  {{{Recipient.CFAMN__PersonID__c}}} 
  <br /> Dear&nbsp;{{{Recipient.FirstName}}}, 
  <br /> 
  <br /> We have received your request for testing accommodations. Unfortunately, the 
  <a href="https://www.cfainstitute.org/-/media/documents/form/medical_professional_questionnaire.ashx">Medical Professional Questionnaire</a> , Comprehensive Diagnostic Evaluation Report, and Supporting Documentation were not included in your request. Please submit all of the required documentation as soon as possible, but no later than 
  <b>19 August 2020</b>. We are unable to review your request until we have received all of the required documentation.&nbsp; 
  <br /> 
  <br /> The Comprehensive Diagnostic Evaluation Report is an assessment conducted by your healthcare provider with the results presented in a report which is supplied to you. There are general guidelines listed on the CFA&reg; Exam Disability Accommodations website and on the first page of the 
  <a href="https://www.cfainstitute.org/-/media/documents/form/medical_professional_questionnaire.ashx">Medical Professional Questionnaire</a>&nbsp;(also attached) that will assist your evaluator in composing an appropriate evaluation report. At a minimum the documentation should: 
  <br /> &nbsp; 
  <ul> 
   <li>Clearly state the diagnosed disability;</li> 
   <li>Describe the functional limitations resulting from the disability, especially as it pertains to taking an exam;</li> 
   <li>Be current (as a guideline, CFA Institute recommends that evaluations be completed within the last 3 years);</li> 
   <li>With the exception of visual impairments and physical disabilities, the qualified professional should make a diagnosis with particular consideration to the Diagnostic and Statistical Manual of Mental Disorders (DSM-V) or equivalent diagnostic standards for non-U.S. candidates;</li> 
   <li>Include the complete educational, developmental, and medical history relevant to the disability for which testing accommodations are being requested;</li> 
   <li>With the exception of visual impairments and some physical disabilities, <b>include a list of the objective, norm-referenced test batteries (e.g., WJ-IV, WAIS-IV, WIAT-III, GORT-V, etc.) and scores used to document the disability</b>, <u>beyond self-reported or subjective scales, measures, interviews, or medical letters of attestation</u>;</li> 
   <li>For requests based upon visual impairments, the report should consist&nbsp;of eye scans, retinal imaging, and results from visual acuity tests, retinoscopy, refraction tests, keratometry tests, peripheral visual field tests, and/or intraocular pressure measurement;</li> 
   <li>Describe the specific accommodations requested;</li> 
   <li>Be typed or printed on official letterhead, and be signed by an evaluator qualified to make the diagnosis (including information about license/certification and area of specialization)</li> 
  </ul> 
  <br /> In order to help support your request, we also&nbsp;encourage candidates requesting accommodations to submit evidence of a chronic history of impairment/limitation across settings. This can be demonstrated through previous evaluation reports, proof of prior accommodations, proof of special services/educational plans, scores on standardized exams (e.g., college-entrance and/or professional exams), academic records/transcripts, etc.&nbsp; 
  <br /> 
  <br /> For more information, please visit the 
  <a href="https://www.cfainstitute.org/en/programs/cfa/exam/disability-accommodations">CFA&reg; Exam Disability Accommodations</a> website. 
  <br /> 
  <br /> Please let me know if you have any further questions or concerns. Thank you for your participation in the CFA Program. 
  <br /> 
  <br /> Kind Regards, 
  <br /> 
  <br /> {{{Sender.Signature}}}
 </body>
</html>