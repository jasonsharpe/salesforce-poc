Dear {!Contact.FirstName}, 

We regret to learn that you may be unable to attend your exam. While we are sympathetic with your situation, we cannot offer you another deferral or any type of refund as you have received a prior deferral. A candidate typically receives only one deferral during their candidacy. Registration deferrals to the next available exam are considered on a case-by-case basis for life-threatening illnesses (candidate or immediate family), expectant mothers, natural disasters, and mandatory military service. This does not guarantee a deferral to any candidate for any condition but instead provides CFA Institute with the discretion to determine which deferral requests it approves and denies.  

You are certainly under no obligation to take the CFA exam, and we hope that you will be able to continue with the program at a later date. You will pay only the registration fee when registering for a future exam. For information on fees and deadlines, please visit: https://www.cfainstitute.org/programs/cfa/exam#FeesSection

We appreciate your understanding of our policy.

Regards, 

{!Case.OwnerFirstName} | Global Customer Care | CFA Institute