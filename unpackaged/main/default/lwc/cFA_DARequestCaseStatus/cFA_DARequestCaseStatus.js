import { LightningElement,track ,api,wire } from 'lwc';
import checkStatus from "@salesforce/apex/CFA_UpdateDARequestCaseStatus_Handler.checkStatus";
import updateCaseStatus from "@salesforce/apex/CFA_UpdateDARequestCaseStatus_Handler.updateCaseStatus";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';

export default class CFA_DARequestCaseStatus extends LightningElement {

    
    @api recordId;
    @track visible = false;
    @track fieldValue;
    // @wire(getRecord, { recordId: '$recordId'})
    // wiredRecord({ error, data }) {
    //     if (data) {
    //         if(fieldValue == "Initiated" || fieldValue == "Additional Information Required"){
    //             this.visible =true;
    //         }else{
    //             this.visible = false;
    //         } 
    //     }if(error){
    //     }
    // }
    connectedCallback(){

        this.getStatus();
    }
    getStatus () {

        if (this.recordId == undefined) {
            return;
        }
        checkStatus({
            recordId: this.recordId
        })
        .then(response => {         
            if (response) {
                this.fieldValue = response;
                if(this.fieldValue === 'Initiated' || this.fieldValue === 'Additional Information Required'){
                    this.visible =true;
                }else{
                    this.visible = false;
                }
            }          
        })
        .catch(error => {
            console.log("Error: " + (error.message || error.body.message));          
        });        
    };

    setRecordId(){
        updateCaseStatus({
            recordId: this.recordId
        })
        .then(response => {       
            if (response === 'success') {
                const evt = new ShowToastEvent({
                    title: "Success!",
                    message: "The record has been updated successfully.",
                    type : "success"
                });
                this.dispatchEvent(evt);
                this.getStatus();
                eval("$A.get('e.force:refreshView').fire();");
         
            }else{
                const evt = new ShowToastEvent({
                    title: "Error!",
                    message: "Update Faild, something went wrong",
                    type : "error"
                });
                this.dispatchEvent(evt);                            
            }       
        })
        .catch(error => {
            console.log("Error: " + (error.message || error.body.message));          
        }); 
    }
}