import { api, LightningElement, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import CONTACT_FIELD from '@salesforce/schema/Case.ContactId';

export default class CaseRestrictionsBanner extends LightningElement {

    @api recordId;

    @wire(getRecord, { recordId: '$recordId', fields: [CONTACT_FIELD] })
    case;

    get contactId() {
        return getFieldValue(this.case.data, CONTACT_FIELD);
    }

}