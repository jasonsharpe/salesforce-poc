import { LightningElement, wire, track, api } from 'lwc';
import { getRecord, getFieldValue} from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getData from '@salesforce/apex/CustomerCare_ContactTabs.getData';
import {refreshApex} from '@salesforce/apex';
import TIME_ZONE from '@salesforce/i18n/timeZone';

const timeZone = TIME_ZONE;

const FIELDS = [
'Contact.Name',
'Contact.Partner_Id__c',
'Contact.CFAMN__PersonID__c'

];

export default class CustomerCareData extends LightningElement {

@track error;
@track isDataAvailable = false;
@track showSpinner = false;
@track data = [];
@track contact = {};
@track error_msg;
@track isModalOpen = false;
@track isMemberShipModal;
@track isMemberShipAppModal;

@track personId;
@track submitDate;

@track customerId;
@track orgPartnerId;
@track orgtype;

@track modalTitle = '';

@api recordId;

timeZone = TIME_ZONE;

@track enrollmentColumns = [
    { label: 'Programs', fieldName: 'Program', type: 'text' },
    { label: 'Exam Cycle(Year, Level, Cycle)', fieldName: 'Cycle', type: 'text' },
    { label: 'Registration Date', fieldName: 'RegistrationDate', type: 'date', 
    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit", timeZone : this.timeZone
    }
    },
    { label: 'Registration Status', fieldName: 'RegistrationStatus', type: 'text' },
    { label: 'Appointment Status', fieldName: 'AppointmentStatus', type: 'text' },
    { label: 'Exam Date', fieldName: 'ExamDate', type: 'date',
    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit", timeZone : this.timeZone
    }
},
    { label: 'Completed Date', fieldName: 'CompletedDate', type: 'date',

    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",timeZone : this.timeZone
    }
},
    { label: 'Test Area Name', fieldName: 'TestAreaName', type: 'text' },
    { label: 'Test Center Code', fieldName: 'TestCenterCode', type: 'text' },
    { label: 'RAD', fieldName: 'IsRAD', type: 'text' },
    { label: 'Entrance Qualification Selected', fieldName: 'QualificationReason', type: 'text' },
    { label: 'Enrollment Date', fieldName: 'EnrollmentDate', type: 'date' ,

    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",timeZone : this.timeZone
    }

},
    { label: 'Cancellation Date', fieldName: 'CancellationDate', type: 'date',

    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",timeZone : this.timeZone
    }
}

];

@track  examResultColumns = [
    { label: 'Programs', fieldName: 'Program', type: 'text' },
    { label: 'Exam Cycle(Year, Level, Cycle)', fieldName: 'Cycle', type: 'text' },
    { label: 'Completed Date', fieldName: 'CompletedDate', type: 'date',
    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",timeZone : this.timeZone
    }

},
    { label: 'Exam Attendance Status', fieldName: 'ExamAttendanceStatus', type: 'text' },
    { label: 'Exam Results Status', fieldName: 'ExamResultsStatus', type: 'text' },
    { label: 'Exam Visibility Status', fieldName: 'ExamResultsVisibilityStatus', type: 'text' }
];


@track scholarshipColumns = [
    { label: 'Program', fieldName: 'Program', type: 'text' },
    { label: 'Type', fieldName: 'Type', type: 'text' },
    { label: 'Exam Cycle(Year, Level, Cycle)', fieldName: 'Cycle', type: 'text' },
    { label: 'Scholarship Start Date', fieldName: 'ScholarshipApplicationDate', type: 'date',
    typeAttributes: {

        year: "numeric", month: "2-digit", day: "2-digit",
        hour: "2-digit", minute: "2-digit", timeZone : this.timeZone
    } 

},
    { label: 'Communication Date', fieldName: 'CommunicationDate', type: 'date',

    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",
        hour: "2-digit", minute: "2-digit", timeZone : this.timeZone
    } 
},
    { label: 'Scholarship End Date', fieldName: 'ScholarshipApplicationDeadline', type: 'date',

    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",
        hour: "2-digit", minute: "2-digit", timeZone : this.timeZone
    } 

},
    { label: 'Status', fieldName: 'Status', type: 'text' },
    { label: 'Reason', fieldName: 'Reason', type: 'text' },
    { label: 'Society', fieldName: 'Society', type: 'text' }
];


@track loginHistoryColumns = [
    { label: 'Person Id', fieldName: 'PersonId', type: 'text' },
    { label: 'Login Status', fieldName: 'LoginStatus', type: 'text' },
    { label: 'Login Attempt Date', fieldName: 'LoginAttemptDate', type: 'date',

    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",timeZone : this.timeZone
    }
}

];



@track membershipColumns = [

    {label: 'Membership Organization', type: 'button', initialWidth:250,
             typeAttributes: { label: { fieldName: 'Organization'},
             name: {fieldName: 'Organization'},variant:'base', title: 'Click to View Details'}},

    { label: 'Current Status', fieldName: 'CurrentStatus', type: 'text' },
    { label: 'Join Date/Time', fieldName: 'JoinDate', type: 'date' ,
        typeAttributes: {
            year: "numeric", month: "2-digit", day: "2-digit",
            hour: "2-digit", minute: "2-digit", timeZone : this.timeZone
        } 
    },
    { label: 'Member Year', fieldName: 'MemberYear', type: 'text' },
    { label: 'Is Affiliate', fieldName: 'IsAffiliate', type: 'text' },
    { label: 'Is On Professional Leave', fieldName: 'OnProfessionalLeave', type: 'text' },
    { label: 'Membership Type', fieldName: 'MembershipType', type: 'text' },
];


@track customerHistoryColumns = [
    { label: 'Person Id', fieldName: 'PersonId', type: 'text' },
    { label: 'Given Name', fieldName: 'GivenName', type: 'text' },
    { label: 'Surname', fieldName: 'Surname', type: 'text' },                                            
    { label: 'Date Of Birth', fieldName: 'DateOfBirth', type: 'date', 
    
    typeAttributes: 
        { month: '2-digit', day: '2-digit', year: 'numeric', timeZone : 'UTC'} },
    { label: 'Passport Number', fieldName: 'PassportNumber', type: 'text' },
    { label: 'Passport Expiration Date', fieldName: 'PassportExpirationDate', type: 'date',
        typeAttributes: 
        { month: '2-digit', day: '2-digit', year: 'numeric', timeZone : 'UTC' }
    },
    { label: 'Valid From', fieldName: 'ValidFrom', type: 'date',
        typeAttributes: 
        { month: '2-digit', day: '2-digit', year: 'numeric', timeZone : this.timeZone
      }
    },
    { label: 'Modified By', fieldName: 'ModifiedBy', type: 'text' }
];


@track membershipApplicationColumns = [
    {label: 'Partner ID', type: 'button',
             typeAttributes: { label: { fieldName: 'CustomerPartnerId'},name: {fieldName: 'CustomerPartnerId'},
             variant:'base', title: 'Click to View Details'}},

    { label: 'CFA Member Application Stage', fieldName: 'CFAMemberApplicationStage', type: 'text' },
    { label: 'CFA Selected Member Type', fieldName: 'CFASelectedMemberType', type: 'text' },
    { label: 'Membership Application Submitted Date', fieldName: 'MembershipApplicationSubmittedDate', type: 'date',
    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit", timeZone : this.timeZone
    }

},
    { label: 'Last Status Change Date', fieldName: 'LastStatusChangeDate', type: 'date',
    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",timeZone : this.timeZone
    }

},
    { label: 'Application Status Expiration Date', fieldName: 'ApplicationStatusExpirationDate', type: 'date',

    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit",timeZone : this.timeZone
    }

}
    
];



@wire(getRecord, { recordId: '$recordId', fields: FIELDS })
fetchedContact({error,data}){
    if(data){
        this.contact = data;
        this.handleLoad(data.fields.Partner_Id__c.value);
    }
}

handleLoad(personId){
    console.log(timeZone);
    console.log(TIME_ZONE);
    this.tabName = 'Enrollment';
    this.tabLabel = 'Enrollment';
    console.log(personId);
    try{
    this.getData(this.tabName,this.tabLabel,personId);
    }
    catch(e){
        console.log(e);
    }
}

handleTabClick(event) {
    try{
    this.tabName = event.target.value;
    this.tabLabel = event.target.label;
    this.personId = this.contact.fields.Partner_Id__c.value;
    this.getData(this.tabName, this.tabLabel, this.contact.fields.Partner_Id__c.value);
    }
    catch(e){
        console.log(this.contact.fields);
        console.log(1,e);
    }
    
    
}

getData(tabName, tabLabel, personId){
    this.data = [];
    this.isDataAvailable = false;
    this.showSpinner = true;
    try {
        getData({
            tabName: tabName,
            tabLabel: tabLabel,
            personId: personId
        })
            .then(result => {
                this.data = JSON.parse(result);
                
                    if (this.data.length > 0 || (this.data.Memberships && this.data.Memberships.length > 0)) {
                        this.isDataAvailable = true;

                        if (this.tabName === 'Memberships') {

                            let tmpData = [];
                            this.customerId = this.data.CustomerId;
                            for (let i = 0; i < this.data.Memberships.length; i++) {
                                this.data.Memberships[i].OnProfessionalLeave = this.data.OnProfessionalLeave.toString();
                                this.data.Memberships[i].MemberYear = this.data.MemberYear.toString();
                                this.data.Memberships[i].IsAffiliate = this.data.IsAffiliate.toString();
                                this.data.Memberships[i].JoinDate = this.data.Memberships && this.data.Memberships[i].JoinDate.toLowerCase().includes('z') ? this.data.Memberships[i].JoinDate : this.data.Memberships[i].JoinDate+'Z';
                                tmpData.push(this.data.Memberships[i]);
                            }

                            this.data = tmpData;
                            this.showSpinner = false;
                        }


                        else if (this.tabName === 'History') {
                            let differenceInTime;
                            let differenceInDays;
                            let tmpData = [];

                                for (let i = 0; i < this.data.length; i++) {
                                    this.data[i].DateOfBirth = this.data[i].DateOfBirth && this.data[i].DateOfBirth.toLowerCase().includes('z') ? this.data[i].DateOfBirth : this.data[i].DateOfBirth+'Z';
                                    this.data[i].PassportExpirationDate = this.data[i].PassportExpirationDate && this.data[i].PassportExpirationDate.toLowerCase().includes('z') ? this.data[i].PassportExpirationDate : this.data[i].PassportExpirationDate+'Z';
                                    this.data[i].ValidFrom =this.data[i].ValidFrom && this.data[i].ValidFrom.toLowerCase().includes('z') ? this.data[i].ValidFrom : this.data[i].ValidFrom+'Z';
                                    
                                    differenceInTime = new Date().getTime() - new Date(this.data[i].ValidFrom).getTime();
                                    differenceInDays = differenceInTime / (1000 * 3600 * 24);
                                    
                                    if (differenceInDays < 1826) {
                                        tmpData.push(this.data[i]);
                                    }

                                }
                                this.data = tmpData;
                        }
                        else if(this.tabName.includes('Scholarship')){
                             for (let i = 0; i < this.data.length; i++) {
                                this.data[i].ScholarshipApplicationDate = this.data[i].ScholarshipApplicationDate && this.data[i].ScholarshipApplicationDate.toLowerCase().includes('z') ? this.data[i].ScholarshipApplicationDate : this.data[i].ScholarshipApplicationDate+'Z';
                                this.data[i].CommunicationDate =  this.data[i].CommunicationDate && this.data[i].CommunicationDate.toLowerCase().includes('z') ? this.data[i].CommunicationDate : this.data[i].CommunicationDate+'Z';
                                this.data[i].ScholarshipApplicationDeadline =  this.data[i].ScholarshipApplicationDeadline && this.data[i].ScholarshipApplicationDeadline.toLowerCase().includes('z') ? this.data[i].ScholarshipApplicationDeadline : this.data[i].ScholarshipApplicationDeadline+'Z';
                             }    
                        }

                        else if(this.tabName.includes('Enrollment')){
                            for (let i = 0; i < this.data.length; i++) {
                                this.data[i].Cycle = this.data[i].Year+' '+this.data[i].Level+' '+this.data[i].Cycle;
                                this.data[i].IsRAD = this.data[i].IsRAD ? 'true' : '';
                                
                                this.data[i].RegistrationDate =  this.data[i].RegistrationDate && this.data[i].RegistrationDate && this.data[i].RegistrationDate.toLowerCase().includes('z') ? this.data[i].RegistrationDate : this.data[i].RegistrationDate+'Z';
                                this.data[i].ExamDate =  this.data[i].ExamDate && this.data[i].ExamDate.toLowerCase().includes('z') ? this.data[i].ExamDate : this.data[i].ExamDate+'Z';
                                this.data[i].CompletedDate =  this.data[i].CompletedDate && this.data[i].CompletedDate.toLowerCase().includes('z') ? this.data[i].CompletedDate : this.data[i].CompletedDate+'Z';
                                this.data[i].EnrollmentDate =  this.data[i].EnrollmentDate && this.data[i].EnrollmentDate.toLowerCase().includes('z') ? this.data[i].EnrollmentDate : this.data[i].EnrollmentDate+'Z';
                                this.data[i].CancellationDate = this.data[i].CancellationDate && this.data[i].CancellationDate.toLowerCase().includes('z') ? this.data[i].CancellationDate : this.data[i].CancellationDate+'Z';
                            
                            }
                        }
                        else if(this.tabName.includes('MembershipApplication')){
                            
                            for (let i = 0; i < this.data.length; i++) {
                                
                               // this.data[i].nameUrl = '/lightning/n/Customer_Care_Membership_Application?c__personId='+personId+'&c__submitDate='+this.data[i].MembershipApplicationSubmittedDate;
                               this.data[i].MembershipApplicationSubmittedDate = this.data[i].MembershipApplicationSubmittedDate && this.data[i].MembershipApplicationSubmittedDate.toLowerCase().includes('z') ? this.data[i].MembershipApplicationSubmittedDate : this.data[i].MembershipApplicationSubmittedDate+'Z';
                                this.data[i].LastStatusChangeDate = this.data[i].LastStatusChangeDate && this.data[i].LastStatusChangeDate.toLowerCase().includes('z') ? this.data[i].LastStatusChangeDate : this.data[i].LastStatusChangeDate+'Z';
                                this.data[i].ApplicationStatusExpirationDate =  this.data[i].ApplicationStatusExpirationDate && this.data[i].ApplicationStatusExpirationDate.toLowerCase().includes('z') ? this.data[i].ApplicationStatusExpirationDate : this.data[i].ApplicationStatusExpirationDate+'Z';
                                
                            }
                        }
                    }
                    else {
                        this.isDataAvailable = false;
                        this.error_msg = 'No Data Available';
                    }
                    this.showSpinner = false;
            })
            .catch(error => {
                console.log(error);
                this.showSpinner = false;
                if (error.body != undefined){
                    this.isDataAvailable = false;
                    this.error_msg = error.body.message;
                }
                else{
                    this.isDataAvailable = false;
                    this.error_msg = 'No Data Available';
                }
            });
    }
    catch (e) {
        console.err(e);
        this.showSpinner = false;
    }
}

handleMembershipsRowCLick(event){
    
    let row = event.detail.row;
    
    this.isModalOpen = true;
    this.isMemberShipModal = true;
    this.isMemberShipAppModal = false;

    this.orgPartnerId = row.OrganizationPartnerId;
    this.orgtype = row.MembershipType;
    this.modalTitle = 'Memberships'
    
   
}

handleMembershipApplicationRowCLick(event){


    let row = event.detail.row;
    
    this.isModalOpen = true;
    this.isMemberShipModal = false;
    this.isMemberShipAppModal = true;

    this.personId = this.contact.fields.CFAMN__PersonID__c.value;
    this.submitDate = row.MembershipApplicationSubmittedDate;
    this.modalTitle = 'Membership Application'
}

closeModal(){
    this.isModalOpen = false;
}

showNoDataMessage(msg) {
    const evt = new ShowToastEvent({
        title: '',
        message: msg,
        variant: 'error',
    });
    this.dispatchEvent(evt);
    this.isDataAvailable = false;
    this.showSpinner = false;
}

}