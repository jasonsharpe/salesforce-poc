import { LightningElement, track,api,wire } from 'lwc';
import {FlowAttributeChangeEvent, FlowNavigationFinishEvent} from 'lightning/flowSupport';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getCaseFiles from '@salesforce/apex/Case_MarkFilesDeletion.getCaseFiles';
import deleteFiles from '@salesforce/apex/Case_MarkFilesDeletion.deleteFiles';


const columns = [
    {label: 'File Name', fieldName: 'title', type:'text'},
    {label: 'Owner', fieldName: 'owner', type:'text'},
    {label: 'Size', fieldName: 'size', type:'text'},
    {label: 'Source', fieldName: 'source', type:'text'},
];

export default class CaseMarkFilesDeletion extends LightningElement {

    @api recId;

    @track data;
    @track originalCdls
    selectedRows;
    
    columns = columns;
    isDataLoaded = false;

    renderedCallback() {
        if(!this.isDataLoaded){
            console.log(112);
        getCaseFiles({caseId : this.recId}).then(result => {
            console.log(result);
            this.data = result;
            this.isDataLoaded = true;
        })
    }
    }
   
    selectRows(event){
        this.selectedRows = event.detail.selectedRows;
    }

    deleteRows(){
        let fileIds = [];
        for(let i=0;i<this.selectedRows.length;i++){
        
            fileIds.push(this.selectedRows[i].contentDocumentId);
        }
        
        deleteFiles({fileIds :fileIds }).then(() => {
            this.showNotification('Success', 'Files deleted', 'success');
            this.dispatchEvent(new FlowNavigationFinishEvent());
        });
    }

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

}