import { LightningElement , track , api} from 'lwc';
import getMapReviewURL from '@salesforce/apex/customerCare_Callout_AppPanel_Controller.getMapReviewURL';

export default class CustomerCare_MembershipAppReview extends LightningElement { 

    handleClick(event) {
        var buttonTitle = event.target.value;     
        getMapReviewURL({buttonTitle : buttonTitle})
        .then(result => {
            window.open(result,'_blank')
        })  
    }
}