import { LightningElement, wire, track, api } from 'lwc';
import { NavigationMixin, CurrentPageReference } from 'lightning/navigation';
import getMembershipTransactions from '@salesforce/apex/CustomerCare_ContactTabs.getMembershipTransactions';
import TIME_ZONE from '@salesforce/i18n/timeZone';

const timeZone = TIME_ZONE;


const columns = [
    { label: 'Transaction Date', fieldName: 'TransactionDate', type: 'date',
    typeAttributes: {
        year: "numeric", month: "2-digit", day: "2-digit", timeZone : timeZone
    }
    },
    { label: 'Member Year', fieldName: 'MemberYear', type: 'text' },
    { label: 'Transaction Type Display Name', fieldName: 'TransactionTypeDisplayName', type: 'text' },
    { label: 'Reason Code', fieldName: 'ReasonCode', type: 'text' },
    { label: 'Sales Order Id', fieldName: 'SalesOrderId', type: 'text' },
    { label: 'Is Affiliate', fieldName: 'WasAffiliate', type: 'text' },
    { label: 'Was On Professional Leave', fieldName: 'WasOnProfessionalLeave', type: 'text' }
];



export default class CustomerCare_MembershipTransactionTab extends NavigationMixin(LightningElement) {
    @track data = [];
    @track columns = columns;
    @track params;

    @api customerId;
    @api orgPartnerId;
    @api orgtype;
    timeZone = TIME_ZONE;

    
    
    connectedCallback() {
        
        let customerId = this.customerId; 
        let orgPartnerId = this.orgPartnerId; 
        let orgtype = this.orgtype;
        console.log(customerId,orgPartnerId,orgtype );
        getMembershipTransactions({
            customerId: customerId,
            orgPartnerId: orgPartnerId,
            orgtype: orgtype
        })
            .then(result => {
                let tmpData = [];
                this.data = JSON.parse(result);
                for (let i = 0; i < this.data.length; i++) {
                    if(this.data[i].WasOnProfessionalLeave != null)
                    this.data[i].WasOnProfessionalLeave = this.data[i].WasOnProfessionalLeave.toString();
                    if(this.data[i].MemberYear != null)
                    this.data[i].MemberYear = this.data[i].MemberYear.toString();
                    if(this.data[i].WasAffiliate != null)
                    this.data[i].WasAffiliate = this.data[i].WasAffiliate.toString();
                    if(this.data[i].ReasonCode != null)
                    this.data[i].ReasonCode = this.data[i].ReasonCode.toString();
                    this.data[i].TransactionDate =this.data[i].TransactionDate && this.data[i].TransactionDate.toLowerCase().includes('z') ? this.data[i].TransactionDate : this.data[i].TransactionDate+'Z';
                    tmpData.push(this.data[i]);
                }
                this.data = tmpData;
                
            })
    }

    getQueryParameters() {

        var params = {};
        var search = location.search.substring(1);

        if (search) {
            params = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
                return key === "" ? value : decodeURIComponent(value)
            });
        }

        return params;
    }
}