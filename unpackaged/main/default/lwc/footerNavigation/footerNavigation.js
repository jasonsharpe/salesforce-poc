/**
 * Created by ryanskelton on 1/8/20.
 */

import {LightningElement, api} from 'lwc';
import { classSet } from 'c/x7sUtils';

export default class FooterNavigation extends LightningElement {

    @api userLink1;
    @api userLink1Text;
    @api userLink1OpenInNewPage = false;

    @api userLink2;
    @api userLink2Text;
    @api userLink2OpenInNewPage = false;

    @api userLink3;
    @api userLink3Text;
    @api userLink3OpenInNewPage = false;

    @api userLink4;
    @api userLink4Text;
    @api userLink4OpenInNewPage = false;

    @api userLink5;
    @api userLink5Text;
    @api userLink5OpenInNewPage = false;

    @api userLink6;
    @api userLink6Text;
    @api userLink6OpenInNewPage = false;

    @api userLink7;
    @api userLink7Text;
    @api userLink7OpenInNewPage = false;

    @api userLink8;
    @api userLink8Text;
    @api userLink8OpenInNewPage = false;

    @api customClass = '';

    @api container = 'x-large';
    @api align = 'right';


    /**
     * return SLDS alignment class and any additional SLDS or CSS class(es) on <section> element
     * @returns {string}
     */

    get componentClass() {
        return `slds-is-relative ${this.customClass}`;
    }

    get pageContainerClass() {
        return classSet('slds-grid slds-wrap')
            .add({
                'slds-container_center': this.container !== 'none',
                'slds-container_small': this.container === 'small',
                'slds-container_medium': this.container === 'medium',
                'slds-container_large': this.container === 'large',
                'slds-container_x-large': this.container === 'x-large',
                'cCenterPanel': this.container === 'default',
                'slds-grid_align-end': this.align === 'right',
                'slds-grid_align-center': this.align === 'center'
            })
            .toString();
    }
}