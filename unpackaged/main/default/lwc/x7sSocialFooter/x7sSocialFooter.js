/**
 * Created by ryanskelton on 3/14/20.
 */

import {LightningElement, api} from 'lwc';
import SelfService from '@salesforce/resourceUrl/SelfService';

export default class X7SSocialFooter extends LightningElement {
    facebookURL = SelfService + '/images/fb.png';
    linkedInURL = SelfService + '/images/lk.png';
    twitterURL = SelfService + '/images/tw.png';
    youtubeURL = SelfService + '/images/youtube.png';
    instaURL = SelfService + '/images/insta.png';
    wechatURL = SelfService + '/images/what.png';
    weiboURL = SelfService + '/images/weibo.png';

}