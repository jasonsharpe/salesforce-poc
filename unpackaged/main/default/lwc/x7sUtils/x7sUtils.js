/**
 * Created by 7Summits on 11/8/19.
 */

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

/**
 * simple date formatter
 * @param date
 * @param params
 * @returns {string}
 */
const formatDate = (date, params) => {
    let options = {
        weekday: 'short',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        timeZone: 'America/Chicago',
        timeZoneName: 'short',
    };

    if( params ) {
        // options = Object.assign( options, params ); // update options with params
        options = params; // replace all options with params
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleString
    return new Date( date ).toLocaleString( 'en-US', options );
};

/**
 * cookie getter
 * @param cookieKey
 * @returns {string}
 */
const getCookie = (cookieKey) => {
    let cookieName = `${cookieKey}=`;
    let cookieArray = document.cookie.split(';');

    for (let cookie of cookieArray) {
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1, cookie.length);
        }
        if (cookie.indexOf(cookieName) === 0) {
            return cookie.substring(cookieName.length, cookie.length);
        }
    }
};

/**
 * cookie setter
 * @param cookieKey
 * @param cookieValue
 * @param expirationDays
 */
const setCookie = (cookieKey, cookieValue, expirationDays) => {
    let expiryDate = '';

    if (expirationDays) {
        const date = new Date();
        date.setDate(date.getDate() + expirationDays);
        expiryDate = `; expires=" ${date.toUTCString()}`;
    }

    document.cookie = `${cookieKey}=${cookieValue || ''}${expiryDate}; path=/`;
};

/**
 * Return the querystring part of a URL
 */
const getQueryParameters = () => {
    var params = {};
    var search = location.search.substring(1);

    if (search) {
        params = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
            return key === "" ? value : decodeURIComponent(value)
        });
    }

    return params;
};

/**
 * dispatches a toast event
 * https://developer.salesforce.com/docs/component-library/bundle/lightning-platform-show-toast-event/documentation
 * @param {string} title
 * @param {string} message
 * @param {string} variant - Values: 'error', 'warning', 'success' or 'info'
 * @param {string} mode - Values: 'dismissable', 'pester', or 'sticky'
 * @returns {boolean}
 */
const showToast = (title, message, variant, mode) => {
    const event = new ShowToastEvent({
        title: title,
        message: message,
        variant: variant ? variant : 'success',
        mode: mode ? mode : 'dismissable'
    });
    return dispatchEvent(event);
};

/**
 * formats a text string with placeholders
 * @param label
 * @param args
 * @returns {*}
 */
const formatText = (label, ...args) =>{
    for(let x = 0; x < args.length; x++){
        label = label.replace(`{${x}}`, args[x]);
    }
    return label;
};

/**
 * Generate a pseudo-GUID using method from https://stackoverflow.com/a/2117523
 * @returns {void | string}
 */
const uuidv4 = () => {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
);
};

/**
 * Return a textual message of a thrown error by a Lightning Data Service.
 * https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.data_error
 *
 * ...but test to see if is a non-LDS thrown error first.
 *
 * @param error
 * @returns {string}
 */
const stringifyThrownLdsError = (error) => {
    let errorMessage = 'Unknown Error';
    if (error.hasOwnProperty('message')) {
        errorMessage = error.toString();
    } else if (Array.isArray(error.body)) {
        errorMessage = error.body.map(e => e.message).join(', ');
    } else if (typeof error.body.message === 'string') {
        errorMessage = error.body.message;
    }
    return errorMessage;
};


/**
 * Return a DOM attribute ready string of styles to apply, given an object property/value
 * set of CSS styles. Assists in the legibility of coding inline style lists.
 *
 * @param styleObj
 * @returns {string}
 */
const stringifyStyles = (styleObj) => {
    return Object.keys(styleObj).reduce( (accum, styleProperty) => {
        if ((styleObj[styleProperty] !== 'undefined') && styleObj[styleProperty] && (styleObj[styleProperty].indexOf('undefined') === -1)) {
        return accum.concat(`${styleProperty}:${styleObj[styleProperty]};`);
    } else {
        return accum;
    }
}, '');
};


export {
    formatDate,
    getCookie,
    setCookie,
    getQueryParameters,
    showToast,
    formatText,
    uuidv4,
    stringifyThrownLdsError,
    stringifyStyles
};
export { classSet } from './classSet';