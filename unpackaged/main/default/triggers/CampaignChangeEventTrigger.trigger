/*****************************************************************
Name: CampaignChangeEventTrigger
Copyright © 2021 ITC
Purpose: Trigger for CampaignChangeEvent
History
VERSION AUTHOR DATE DETAIL Description
1.0 Vadym Merkotan 12.03.2021 CampaignChangeEventTrigger created
*****************************************************************/
trigger CampaignChangeEventTrigger on CampaignChangeEvent (after insert) {
	// Passing handler to run method of CDCTriggerDispatcher class
    CDCTriggerDispatcher.run(new CampaignChangeEventTriggerHandler());
}