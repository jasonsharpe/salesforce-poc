/*****************************************************************
Name: CampaignMemberPlatformEventTrigger
Copyright © 2020 ITC
============================================================
Purpose: Handle events on insert the CampaignMemberPlatformEvent__e records
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   24.11.2020   Created   Handle Campaign Member Platform Events
*****************************************************************/
trigger CampaignMemberPlatformEventTrigger on CampaignMemberPlatformEvent__e (after insert) {
 Boolean isDisabled = DynamicTriggerStatus.getTriggerStatus('CampaignMemberPlatformEventTrigger');
    if (!isDisabled) {
        CampaignMemberPlatEventTriggerHandler.handleCampaignMemberPlatformEvents(Trigger.new);
    }
}