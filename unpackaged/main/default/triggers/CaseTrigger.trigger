/************************************************************************************************** 
* Trigger Name      : CaseTrigger
* Purpose           : This trigger is used for validating Escalated cases,handling Email and Web cases,
clearing contactId for SocialMedia cases and finding contact for web cases.   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 29-Sept-2017  
***************************************************************************************************/
trigger CaseTrigger on Case (before Insert, after Insert, before Update, after Update, before Delete, after Delete) {
    
    TriggerDispatcher.Run(new CaseTriggerHandler());

    List<RecordType> listRecordTypes=[SELECT Id,Name FROM RecordType
                                      WHERE Name = 'CustomerCare Front Office Record Type' 
                                      OR Name = 'CustomerCare Back Office Record Type' 
                                      OR Name = 'CustomerCare Case Reopen Record Type'];
    Id recordTypeIdForDARequest = CFA_RecordTypeUtility.getRecordTypeId(Case.getSObjectType(), Label.CFA_ADALabel, CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
    Id recordTypeIdForCBTDARequest = CFA_RecordTypeUtility.getRecordTypeId(Case.getSObjectType(), 'CBT DA Request' , CFA_DescribeSchemaUtility.getSObjectAPIName('Case'));
    List<Case> listDARequestCases = new List<Case>();
    
    List<Id> conId=new List<Id>();
    Set<Id> setRecordTypesIds=new Set<Id>();
    List<Case> listCases=new List<Case>();   
    Map<Id,Case> MapOldCases=new Map<Id,Case>();
    List<Case> selfServiceCases = new List<Case>();
    List<Case> contentDocumentCreationCasesList  = new List<Case>();
        

    for(RecordType recordType :listRecordTypes){
        setRecordTypesIds.add(recordType.Id);
    }
    if(!trigger.isDelete) // Added by Dhana Prasad to bypass for delete
    for(Case caseObj :Trigger.new){
        System.debug(caseObj);
        if(setRecordTypesIds.contains(caseObj.recordTypeId))
            listCases.add(caseObj);
            if(!String.isBlank(caseObj.ContactId))
                conId.add(caseObj.ContactId);
        if(caseObj.recordTypeId == recordTypeIdForDARequest)
        {
            if(!String.isBlank(caseObj.ContactId))
            {
                listDARequestCases.add(caseObj);
            }
        }

        if(caseObj.Origin == ''){
            selfServiceCases.add(caseObj);
        }
    }
    
    if(Trigger.isUpdate){
        for(Case caseObjold :Trigger.Old){
             if(setRecordTypesIds.contains(caseObjold.recordTypeId))
                 MapOldCases.put(caseObjold.id,caseObjold);
        }
    }
    
    if(trigger.isBefore){
        if (trigger.isInsert){
            CustomerCare_Case_TriggerHandler.ccValidatedEscalatedCases(listCases);
            CustomerCare_Case_TriggerHandler.ccHandleJunkEmailCases(listCases);
            CustomerCare_Case_TriggerHandler.ccHandleChildCases(listCases,conId);
           
           
        }
        if (trigger.isUpdate){           
            CustomerCare_Case_TriggerHandler.ccValidatedEscalatedCases(listCases);
            CustomerCare_Case_TriggerHandler.updateCustomerTypeWhenCaseClosed(listCases,MapOldCases);
            CustomerCare_Case_TriggerHandler.ccSetRecordTypeIdOnReopenCase(listCases,MapOldCases);
        }  
    }
    
    if(trigger.isAfter){
        if (trigger.isInsert){ 
            CustomerCare_Case_TriggerHandler.ccHandleEmailCases(listCases);
            CustomerCare_Case_TriggerHandler.ccHandleWebCases(listCases);
            if(selfServiceCases.size() > 0){
                CustomerCare_Case_TriggerHandler.ccHandleWebCases(selfServiceCases);
            }
            CustomerCare_Case_TriggerHandler.ccFindContactforEmailWebCases(listCases);
            CustomerCare_Case_TriggerHandler.ccemailMessageCreation(listCases);
            CustomerCare_Case_TriggerHandler.ccHandleParentCases(listCases);
            
           
        }
        if (trigger.isUpdate){ 
            
            SocietyOpsCaseTriggerHandler.sendEmail(Trigger.oldMap,Trigger.newMap);
        }
    }
    
}