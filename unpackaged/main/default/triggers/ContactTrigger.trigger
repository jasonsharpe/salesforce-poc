/*****************************************************************
Name: ContactTrigger
Copyright © 2021 ITC
============================================================
Purpose:
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   29.01.2021   Created   CSR:
*****************************************************************/

trigger ContactTrigger on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Passing handler to run method of TriggerDispatcher class
    TriggerDispatcher.Run(new ContactTriggerHandler());
}