/************************************************************************************************** 
* Trigger Name      : ContentVersionTrigger
* Purpose           : This trigger is used for create content document link for case contact related case of DA requet and CBT DA request cases,
* Version           : 1.0 Initial Version
* Organization      : MTX
* Created Date      : 08-June-2020  
***************************************************************************************************/
trigger ContentVersionTrigger on ContentVersion  (before Insert, after Insert, before Update, after Update, before Delete, after Delete) {
    System.debug('in content version trigger');
    TriggerDispatcher.Run(new CFA_ContentVersionTriggerController());
}