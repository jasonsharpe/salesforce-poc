trigger EmailMessageTrigger on EmailMessage (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Passing handler to run method of TriggerDispatcher class
    TriggerDispatcher.Run(new EmailMessageTriggerHandler());
}