/*****************************************************************
Name: MembershipApplicationTrigger
Copyright © 2021 ITC
============================================================
Purpose: Trigger for Membership_Application__c
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Alona Zubenko   09.02.2021   Created   Create the Trigger
*****************************************************************/

trigger MembershipApplicationTrigger on Membership_Application__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Passing handler to run method of TriggerDispatcher class
    TriggerDispatcher.Run(new MembershipApplicationTriggerHandler());
}