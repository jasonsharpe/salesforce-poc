trigger MembershipChangeEventTrigger on Membership__ChangeEvent (after insert) {
    // Passing handler to run method of CDCTriggerDispatcher class
    CDCTriggerDispatcher.run(new MembershipChangeEventTriggerHandler());
}