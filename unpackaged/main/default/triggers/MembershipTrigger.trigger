/*****************************************************************
Name: MembershipTrigger
Copyright © 2021 ITC
Purpose: Trigger for Membership__c
History
VERSION AUTHOR DATE DETAIL Description
1.0 Vadym Merkotan 5.03.2021 MembershipTrigger created
*****************************************************************/
trigger MembershipTrigger on Membership__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new MembershipTriggerHandler());
}