/**************************************************************************************************
* Trigger Name      : SocialPostTrigger
* Purpose           : This trigger is used for clearing SocialPersonaId in SocialPost record.   
* Version           : 1.0 Initial Version
* Organization      : Cognizant
* Created Date      : 08-Oct-2017  
***************************************************************************************************/

trigger SocialPostTrigger on SocialPost (before insert,after insert) {
    if(trigger.isBefore){
        if (trigger.isInsert){
            CustomerCare_SocialPost_TriggerHandler.ccClearSocialPersonaId(Trigger.new);
        }
    }
    if(trigger.isAfter){
        if (trigger.isInsert){
            //CustomerCare_SocialPost_TriggerHandler.ccSendNotificationToCaseOwner(Trigger.new);
        }
    }    
}