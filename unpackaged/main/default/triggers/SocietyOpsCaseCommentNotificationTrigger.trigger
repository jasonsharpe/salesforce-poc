/**
* @author 7Summits Inc
* @date 06-21-2019
* @description This trigger is responsible for sending emails when the case comments are either added or updated
* by other than the owner of the case. This is only applicable for Society Tech Case record type
*/

trigger SocietyOpsCaseCommentNotificationTrigger on CaseComment (after insert,after update) {

    if(trigger.isAfter){

        if (trigger.isInsert || trigger.isUpdate ){
            SocietyOpsCaseCommentTriggerHandler.sendEmail();

        }

    }


}