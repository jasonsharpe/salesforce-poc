/*****************************************************************
Name: UserTerritory2AssociationTrigger
Copyright © 2021 ITC
============================================================
Purpose:
============================================================
History
-------
VERSION  AUTHOR          DATE         DETAIL    Description
1.0      Vadym Merkotan  31.03.2021   Created   
*****************************************************************/
trigger UserTerritory2AssociationTrigger on UserTerritory2Association (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new UserTerritory2AssociationTriggerHandler());
}