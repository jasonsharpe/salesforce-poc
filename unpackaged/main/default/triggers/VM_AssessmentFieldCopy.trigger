trigger VM_AssessmentFieldCopy on Assessment__c (before insert) {
 
    List<RecordType> rtypes;
    Map<String,String> assessmentRecordTypes;
    rtypes=new List<RecordType>();
        assessmentRecordTypes = new Map<String,String>{};
        rtypes=[Select Name, Id From RecordType where sObjectType='Assessment__c' and isActive=true];
        for (RecordType rt : rtypes) {
            assessmentRecordTypes.put(rt.Name,rt.Id);
        }
    
    for(Assessment__c Assess : Trigger.new){
    
        if (Assess.RecordTypeId== assessmentRecordTypes.get('Engagement Assessment')) 
     {
        Assess.Engagement_Assessment_del__c=Assess.Engagement__c;
         System.debug('w$$$$$$$$$$$$$$$$$$'+Assess.Engagement_Assessment_del__c);
     }  
        else {
            Assess.Volunteer_Assessment__c=Assess.Engagement_Volunteer__c;
        }
    }
    
}